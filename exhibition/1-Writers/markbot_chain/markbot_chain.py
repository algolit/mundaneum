from pattern.fr import parsetree
# from pattern.en import parsetree
from collections import namedtuple
from colored import fg, bg, attr
import os
import random
import re
import time
import sys


# prevent sigint?
import signal
def handler(_, __):
    pass

signal.signal(signal.SIGINT, handler)

#Largeur phrase + suffix (tronçonnage)
max_char = 75
suffix = "..."
def tronconnage(abcd):
    abcd = abcd.replace('\n','')
    abcd = abcd.replace('\r','')
    if len(abcd) <= max_char:
        return abcd
    return abcd[:max_char] + suffix

def delay_print(a):
    for c in a:
        sys.stdout.write(c)
        sys.stdout.flush()
        time.sleep(0.04)

MarkovModel = namedtuple('MarkovModel', ['keys', 'data', 'history'])

def clear():
    os.system('clr' if os.name == 'nt' else 'clear')

def as_key (str):
  return str.lower()

def make_key (inp):
  return tuple([as_key(w) for w in inp])

def make_markov_model (corpus, history=3):
  model = MarkovModel(keys = [], history=history, data = {})

  for i in range(history, len(corpus)):
    key = make_key(corpus[(i-history):i])
    if not key in model.keys:
      model.keys.append(key)
      model.data[key] = []

    model.data[key].append(corpus[i])

  return model

def generate (model, text_length = 50, seed = None):
  if seed is None:
    key = random.choice(model.keys)
  else:
    key = make_key(seed)

  text = list(key)

  while (len(text) < text_length):
    if (key in model.keys):
      text.append(random.choice(model.data[key]))
      new_key = list(key[1:])
      new_key.append(text[-1])
      key = make_key(new_key)
    else:
      return text

  return text

def get_model_statistics (model):
  statistics = {
    'size': len(model.data),
    'alternatives': { '1': 0, '2': 0, '3': 0, '4': 0, '5': 0, '5+': 0 }
  }

  for key, entries in model.data.items():
    l = len(entries)

    if l == 1:
      statistics['alternatives']['1'] += 1
    elif l == 2:
      statistics['alternatives']['2'] += 1
    elif l == 3:
      statistics['alternatives']['3'] += 1
    elif l == 4:
      statistics['alternatives']['4'] += 1
    elif l == 5:
      statistics['alternatives']['5'] += 1
    else:
      statistics['alternatives']['5+'] += 1

  return statistics

def make_text (words):
  text = ''
  first_word = True
  captilize = True

  for word in words:
    if re.match(r'\W', word) is not None:
      text += word

      if re.match(r'[!?\.]+$', word) is not None:
        captilize = True

      if re.match(r'\n+$', word) is not None:
        first_word = True

    else:
      if not first_word:
        text += ' '

      text += word.capitalize() if captilize else word
      captilize = False
      first_word = False

  return text
while True:
    f = open ('data.txt', 'a')
    delay_print('\n%s%sStart\n%s' % (fg(118), bg(0), attr(1)))
    time.sleep(0.5)
    delay_print('\nChargement des fichiers...\n')
    time.sleep(0.5)
    delay_print('%s%sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx%s' % (fg(113), bg(113), attr(0)))
    delay_print('100%\n')
    time.sleep(0.5)
    delay_print('\n')
    delay_print('%s%s|_ Sentence.txt\n%s' % (fg(226), bg(0), attr(0)))
    time.sleep(0.5)

    choicefile = open("sentence.txt","r")
    tabulation = ('%s%s\t|_ %s' % (fg(226), bg(0), attr(0)))
    numberOfLine = 0
    linelist=[]
    for phrase in choicefile:
        linelist.append(phrase)
        numberOfLine += 1
    for i in range(0,70):
        print(tronconnage(tabulation + linelist[i]))
        time.sleep(0.1)
    choice=random.choice(linelist)
    print('\n',numberOfLine ,'%s%squestions chargées%s' % (fg(11), bg(0), attr(0)))
    time.sleep(0.5)
    delay_print('%s%s\n↓ Question sélectionnée ↓\n%s' % (fg(118), bg(0), attr(0)))
    i = input(choice + '\n' + '%s%sVotre réponse: %s' % (fg(129), bg(0), attr(0)))
    i = i.strip()
    if len(i) > 0:
        f.write(i+'\r\n')
        f.flush()

    delay_print('%s%s\nRéponse enregistrée dans le fichier data !\n%s' % (fg(118), bg(0), attr(1)))
    time.sleep(1)

    delay_print('%s%s\n|_ data.txt\n%s' % (fg(226), bg(0), attr(0)))
    time.sleep(0.5)
    datas = open("data.txt","r")
    listdata=[]
    dataNumber = 0
    for data in datas:
        listdata.append(data)
        dataNumber += 1
    for v in range(0,25):
        print(reversed(tronconnage(tabulation + listdata[v])))
        time.sleep(0.1)
    time.sleep(3)
    clear()
    with open('data.txt','r') as h:
        raw_text = h.read()
        parsed_text = parsetree(raw_text)
    # tokens = [[word.string for word in sentence.words] for sentence in parsed_text.sentences]
        tokens = []

    for sentence in parsed_text.sentences:
        tokens.extend([word.string for word in sentence.words])

    model = make_markov_model(tokens, history = 1)

    delay_print("%s%s\nMarkov process%s" % (fg(14), bg(0), attr(0)))
    time.sleep(1)
    print("\n")

    delay_print("%s%s*****\n%s" % (fg(226), bg(0), attr(0)))
    delay_print("%s%sMaking model (monogram)%s" % (fg(226), bg(0), attr(0)))
    print('\n')
    model = make_markov_model(tokens, history = 1)
    print(get_model_statistics(model))
    print('\n')
    print(model)
    delay_print("%s%s\nFichier data.txt parsed mot par mot%s" % (fg(226), bg(0), attr(0)))
    time.sleep(2)

    clear()
    delay_print("%s%s\nGenerating text of monogram model%s" % (fg(129), bg(0), attr(0)))
    print('\n')
    for i in range(5):
        delay_print(str.lower(make_text(generate(model, 50))))
        print('\n')

    delay_print('%s%s\n______________________________________________________\n%s'% (fg(124), bg(0), attr(0)))
    time.sleep(2)
    delay_print("\n")
    time.sleep(1)
    clear()

    delay_print("%s%s\n*****\n%s" % (fg(226), bg(0), attr(0)))
    delay_print("%s%sMaking model (bigram)%s" % (fg(226), bg(0), attr(0)))
    print('\n')
    model = make_markov_model(tokens, history = 2)
    print(get_model_statistics(model))
    print('\n')
    print(model)
    delay_print("%s%s\nFichier data.txt parsed par groupe de deux mots%s" % (fg(226), bg(0), attr(0)))
    time.sleep(2)
    clear()

    delay_print("%s%s\nGenerating text of bigram model%s" % (fg(129), bg(0), attr(0)))
    print('\n')
    for i in range(5):
        delay_print(str.lower(make_text(generate(model, 50))))
        print('\n')

    delay_print('%s%s\n______________________________________________________\n%s'% (fg(124), bg(0), attr(0)))
    time.sleep(2)
    delay_print("\n")
    time.sleep(1)
    clear()
    time.sleep(0.5)
    delay_print('%s%s\nFin du processus, merci de votre participation\n%s' % (fg(118), bg(0), attr(0)))
    time.sleep(0.5)
    delay_print('%s%s\n Reboot dans ...\n%s' % (fg(124), bg(0), attr(0)))
    time.sleep(0.5)
    delay_print('%s%s3\n%s' % (fg(118), bg(0), attr(0)))
    time.sleep(1)
    delay_print('%s%s2\n%s' % (fg(226), bg(0), attr(0)))
    time.sleep(1)
    delay_print('%s%s1\n%s' % (fg(124), bg(0), attr(0)))
    time.sleep(1)
    clear()
