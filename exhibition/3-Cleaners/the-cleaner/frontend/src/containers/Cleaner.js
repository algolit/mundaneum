import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { lineEditCancel, lineEditDone, lineEditSentence, lineEditStart, lineMarkClean, lineMarkDirty } from '../actions/lines';
import { documentLoad, documentStore } from '../actions/document';
import { messageClose, printPoem } from '../actions/application';
import Cleaner from '../components/Cleaner'


function mapStateToProps ({ document, lines, application }) {
  // Make a selection of the state
  console.log(lines);
  return  { document: document, ...lines, ...application  }
}

function mapDispatchToProps (dispatch) {
  // Select action creators to be bound with dispatch
  return bindActionCreators({
    documentLoad,
    documentStore,
    lineEditCancel, 
    lineEditDone, 
    lineEditSentence, 
    lineEditStart, 
    lineMarkClean, 
    lineMarkDirty,
    messageClose,
    printPoem
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cleaner);