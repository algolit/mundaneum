import { poemGet as apiPoemGet, print as apiPrint } from '../services/api';

export const MESSAGE_CLOSE = 'MESSAGE_CLOSE';
export const SESSION_START = "SESSION_START";
export const PRINT = "PRINT";
export const PRINT_FAIL = "PRINT_FAIL";
export const PRINT_SUCCESS = "PRINT_SUCCESS";

export const messageClose = () => ({ type: MESSAGE_CLOSE });

export const sessionStart = () => ({ type: SESSION_START });

export const printPoem = () => (dispatch, _) => {
  dispatch({ type: PRINT })
  apiPoemGet()
    .then((data) => {
      apiPrint(data['text'])
        .then(() => dispatch(printSuccess()))
        .catch(() => dispatch(printFail()))
    })
    .catch(() => dispatch(printFail()))
};

export const printSuccess = () => ({ type: PRINT_SUCCESS });

export const printFail = () => ({ type: PRINT_FAIL });