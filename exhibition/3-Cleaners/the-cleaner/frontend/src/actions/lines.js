import api, {
  linePost as apiLinePost
} from '../services/api';

// lineeditstart
export const LINE_EDIT_START = "LINE_EDIT_START";
export const LINE_EDIT_DONE = "LINE_EDIT_DONE";
export const LINE_STORE = "LINE_STORE";
export const LINE_EDIT_CANCEL = "LINE_EDIT_CANCEL";
export const LINE_STORE_FAIL = "LINE_STORE_FAIL";
export const LINE_STORE_SUCCESS = "LINE_STORE_SUCCESS";
export const LINE_MARK_CLEAN = "LINE_MARK_CLEAN";
export const LINE_MARK_DIRTY = "LINE_MARK_DIRTY";
export const LINE_EDIT_SENTENCE = "LINE_EDIT_SENTENCE";


export const lineEditStart = (lineIdx) => ({ type: LINE_EDIT_START, lineIdx });

export const lineEditCancel = () => ({ type: LINE_EDIT_CANCEL });

export const lineEditDone = () => ({ type: LINE_EDIT_DONE });

export const lineEditSentence = (activeLineText) => ({ type: LINE_EDIT_SENTENCE, activeLineText });

export const lineStore = () => (dispatch, getState) => {
  const { lines, application } = getState();
  const { document, line, sentence, clean } = lines[application.activeLine];
  
  dispatch({ type: LINE_STORE });
  apiLinePost(document, line, sentence, clean)
    .then(() => dispatch(lineStoreSuccess()))
    .catch(() => dispatch(lineStoreFail()))  
} 

export const lineStoreFail = () => ({ type: LINE_STORE_FAIL });

export const lineStoreSuccess = () => ({ type: LINE_STORE_SUCCESS });

export const lineMarkClean = (lineIdx) => ({ type: LINE_MARK_CLEAN, lineIdx });

export const lineMarkDirty = (lineIdx) => ({ type: LINE_MARK_DIRTY, lineIdx });