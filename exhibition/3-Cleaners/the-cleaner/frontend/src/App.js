import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Cleaner from './containers/Cleaner';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Route exact path="/" component={ Cleaner } />
        </Router>
      </div>
    );
  }
}

export default App;
