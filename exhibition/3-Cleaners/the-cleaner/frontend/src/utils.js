import { en } from './messages';

export const simpleUpdate = (state, props) => ({...state, ...props})

export const extractUpdate = (state, source, props) => simpleUpdate(state, extractProps(source, props));
 
export const extractProps = (source, props) => {
  const data = {};
  props.forEach(key => {
    if (key in source) {
      data[key] = source[key];
    }
  });
  return data;
}

export const translate = (key) => {
  return en[key];
}

export const t = translate;