import { 
  MESSAGE_CLOSE,
  PRINT,
  PRINT_SUCCESS,
  PRINT_FAIL
} from '../actions/application';

import {
  LINE_EDIT_START,
  LINE_EDIT_DONE,
  LINE_EDIT_CANCEL,
  LINE_MARK_CLEAN,
  LINE_MARK_DIRTY,
  // LINE_STORE,
  // LINE_STORE_FAIL,
  // LINE_STORE_SUCCESS
} from '../actions/lines';

import {
  DOCUMENT_LOAD,
  DOCUMENT_LOAD_FAIL,
  DOCUMENT_LOAD_SUCCESS,
  DOCUMENT_STORE,
  DOCUMENT_STORE_FAIL,
  DOCUMENT_STORE_SUCCESS
} from '../actions/document';

import { simpleUpdate, t } from '../utils';

const initialState = {
  cleanCount: 0,
  documentLoading: false,
  documentStoring: false,
  sentenceStoring: false,
  message: null,
  canPrint: false,
  hasUnsavedChanges: false
}

export default function example (state = initialState, action) {
  switch (action.type) {
    case MESSAGE_CLOSE:
      return simpleUpdate(state, { message: null })
    

    case DOCUMENT_LOAD:
      return simpleUpdate(state, { documentLoading: true, message: t('document_load'), hasUnsavedChanges: false })
    case DOCUMENT_LOAD_FAIL:
      return simpleUpdate(state, { documentLoading: false, message: t('document_load_fail') })
    case DOCUMENT_LOAD_SUCCESS:
      return simpleUpdate(state, { documentLoading: false, message: null })
    

    case DOCUMENT_STORE:
      return simpleUpdate(state, { documentStoring: true, 'message': t('document_store') })
    case DOCUMENT_STORE_FAIL:
      return simpleUpdate(state, { documentStoring: false, 'message': t('document_store_fail') })
    case DOCUMENT_STORE_SUCCESS:
      return simpleUpdate(state, { documentStoring: false, 'message': null, hasUnsavedChanges: false })



    case LINE_EDIT_DONE:
      return simpleUpdate(state, { cleanCount: state.cleanCount + 1, hasUnsavedChanges: true, canPrint: true })
    case LINE_MARK_CLEAN:
      return simpleUpdate(state, { cleanCount: state.cleanCount + 1, hasUnsavedChanges: true, canPrint: true })
    case LINE_MARK_DIRTY:
      return simpleUpdate(state, { cleanCount: state.cleanCount + 1, hasUnsavedChanges: true, canPrint: true })


    case PRINT:
      return simpleUpdate(state, { message: t('print_active') })
    case PRINT_SUCCESS:
      return simpleUpdate(state, { message: null, canPrint: false })
    case PRINT_FAIL:
      return simpleUpdate(state, { message: t('print_fail') })

    default:
      return state;
  }
}
