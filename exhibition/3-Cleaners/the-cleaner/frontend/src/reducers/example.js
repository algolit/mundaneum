import { GREETING_CHANGE } from '../actions/example';

const initialState = {
  greeting: 'world'
}

export default function example (state = initialState, action) {
  switch (action.type) {
    case GREETING_CHANGE:
      return {
        ...state,
        greeting: action.greeting
      };
    default:
      return state;
  }
}
