"""
  Importer for a collection of texts 
"""

import re
import argparse
import glob
import os.path
import random
from collections import namedtuple
from spellchecker import SpellChecker
from pattern.fr import parsetree
from langdetect import detect

# quality reasons
NO_WORDS = 'NO_WORDS'
TOO_MUCH_UNRECOGNIZED_WORDS = 'TOO_MUCH_UNRECOGNIZED_WORDS'
TOO_MUCH_NON_ALPHABETICAL_CHARS = 'TOO_MUCH_NON_ALPHABETICAL_CHARS'
from collections import namedtuple
AVERAGE_WORD_LENGTH_TOO_SHORT = 'AVERAGE_WORD_LENGTH_TOO_SHORT'
MARKED_BY_USER = 'MARKED_BY_USER'
MARKED_CLEAN = 'MARKED_CLEAN'
CLEAN = 'CLEAN'

IMPORTED = 'IMPORTED'
CLEANED = 'CLEANED'
ANNOTATED = 'ANNOTATED'

spell = SpellChecker(language="fr")

## text dataset is a list of 

def simpleTree(text):
  return parsetree(text, tokenize = True, tags = True, chunks = False, lemmata = True)

def removeHyphens (text):
  return re.sub(r'-\n(\w)', '\\1', text, flags=re.M)

def removeWordWrap (text):
  return re.sub(r'\n(\w)', ' \\1', text, flags=re.M)

def removeMultiNewlines(text):
  return re.sub(r'(?:\s*\n){4,}', '\n\n', text, flags=re.M)

def clean (text):
  return removeMultiNewlines(removeWordWrap(removeHyphens(text)))


def filterOutNoise (text):
  output = ''
  for line in text.splitlines():
    if line:
      if re.fullmatch(r'[^a-z]+', line.strip('\n'), flags=re.I):
        # print('no letters')
        # print('**NOISE**: {}'.format(line))
        pass
      else:
        tree = simpleTree(line)
        lemmas = set()
        words = set()
        
        for sentence in tree.sentences:
          for word in sentence.words:
            if  re.fullmatch(r'\w+', word.string):
              lemmas.add(word.lemma)
              words.add(word.string)
        
        misspelled = spell.unknown(lemmas)
        
        if words:
          if (len(lemmas) < 8 and len(misspelled) * 2 > len(lemmas)) or \
            (len(lemmas) > 7 and len(misspelled) * 3 > len(words)):
            # print('too much misspelled')
            # print('**NOISE**: {}'.format(line))
            pass
          else:
            letters = re.sub('[^a-z]', '', line, flags=re.I)
            notLetters = re.sub('[a-z]', '', line, flags=re.I)

            if len(line) < 15 and len(notLetters) * 2 > len(letters):
              pass
              # print('too much non words')
              # print('**NOISE**: {}'.format(line))
            else:
              avgwordlength = sum([len(word) for word in words]) / float(len(words))
              if avgwordlength < 3:
                # print('words too short')
                # print('**NOISE**: {}'.format(line))
                pass
              else:
                output += line + '\n'
  return output        

def importFile(path):
  print(path)
  with open(path, 'r') as h:
    print("Loading: {}".format(path))
    text = clean(h.read())
    language = detect(text)
    print("Detected document lanuage {}".format(language))
    if (language == 'fr'):
      speller = SpellChecker(language=language)
      return filterOutNoise(text) + '\n'
    else:
      return ''


def importFolder(path):
  files = glob.glob(os.path.join(path, '*.txt'))
  corpus = ''
  # importFile(random.choice(files))

  for fp in glob.glob(os.path.join(path, '*.txt')):
    corpus += importFile(fp)

  with open('output.txt', 'w') as w:
    w.write(corpus)
    print('done!')

importFolder("/home/gijs/Documents/Bedrijf/Projecten/Algolit/mons/git/data/re-ocred")