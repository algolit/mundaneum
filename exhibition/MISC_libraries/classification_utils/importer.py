"""
  Importer for a collection of texts 
"""

import re
import argparse
import glob
import os.path
import random
from collections import namedtuple
from spellchecker import SpellChecker
from pattern.fr import parsetree
from langdetect import detect

from . import db
from . import clean_states
from . import settings
from .utils import log, debug

spellers = {}

def simpleTree(text):
  return parsetree(text, tokenize = True, tags = True, chunks = False, lemmata = True)

def removeHyphens (text):
  return re.sub(r'-\n(\w)', '\\1', text, flags=re.M)

def removeWordWrap (text):
  return re.sub(r'\n(\w)', ' \\1', text, flags=re.M)

def removeMultiNewlines(text):
  return re.sub(r'(?:\s*\n){4,}', '\n\n', text, flags=re.M)

## Chain actions to clean a wordwrapped text
def reflow (text):
  return removeMultiNewlines(removeWordWrap(removeHyphens(text)))

def isClean (line, speller):
  if line:
    if re.fullmatch(r'[^a-z]+', line.strip('\n'), flags=re.I):
      debug('**NOISE** ({}): {}'.format(clean_states.NO_LETTERS, line))
      return (False, clean_states.NO_LETTERS, [])
    else:
      tree = simpleTree(line)
      lemmas = set()
      words = set()
      
      for sentence in tree.sentences:
        for word in sentence.words:
          if  re.fullmatch(r'\w+', word.string):
            lemmas.add(word.lemma)
            words.add(word.string)
      
      if speller:
        misspelled = speller.unknown(lemmas)
      else:
        misspelled = set()
      
      if words:
        if (len(lemmas) < 8 and len(misspelled) * 2 > len(lemmas)) or \
          (len(lemmas) > 7 and len(misspelled) * 3 > len(words)):
          debug('**NOISE** ({}): {}'.format(clean_states.TOO_MUCH_UNRECOGNIZED_WORDS, line))
          return (False, clean_states.TOO_MUCH_UNRECOGNIZED_WORDS, misspelled)
        else:
          letters = re.sub('[^a-z]', '', line, flags=re.I)
          notLetters = re.sub('[a-z]', '', line, flags=re.I)

          if len(line) < 15 and len(notLetters) * 2 > len(letters):
            # pass
            debug('**NOISE** ({}): {}'.format(clean_states.TOO_MUCH_UNRECOGNIZED_WORDS, line))
            return (False, clean_states.TOO_MUCH_UNRECOGNIZED_WORDS, misspelled)
          else:
            avgwordlength = sum([len(word) for word in words]) / float(len(words))
            if avgwordlength < 3:
              debug('**NOISE** ({}): {}'.format(clean_states.AVERAGE_WORD_LENGTH_TOO_SHORT, line))
              return (False, clean_states.AVERAGE_WORD_LENGTH_TOO_SHORT, misspelled)
            else:
              return (True, clean_states.MARKED_CLEAN, misspelled)
      
      return (False, clean_states.EMPTY, [])
  else:
    debug('**NOISE** ({}): {}'.format(clean_states.EMPTY, line))
    return (False, clean_states.EMPTY, [])
      

def importFile(path, conn):
  with open(path, 'r') as h:
    document = os.path.basename(path)
    text = reflow(h.read())
    language = detect(text)
    speller = getSpeller(language)
    sentences = []

    log("Loading: {}".format(path))
    debug("Detected document lanuage {}".format(language))
    
    for line, sentence in enumerate(text.splitlines()):
      clean, reason, misspelled = isClean(sentence, speller)
      sentences.append({
        'document': document,
        'line': line,
        'sentence': sentence,
        'clean': clean,
        'clean_annotation': reason,
        'checked': False,
        'classification': -1,
        'detected_language': language,
        'misspelled': ','.join(misspelled)
      })
      debug(misspelled)
      # db.add_sentence(document=document, line=line, sentence=sentence, clean=clean, clean_annotation=reason, checked=False, classification=-1, detected_language=language, misspelled=misspelled)
    db.add_sentences(conn, sentences)
    log("Inserted {} sentences".format(len(sentences)))

def getSpeller(language):
  if language not in spellers:
    try:
      spellers[language] = SpellChecker(language)
    except:
      spellers[language] = None

  return spellers[language]

def importFolder(path):
  log('Importing folder {}'.format(path))

  conn = db.connect()

  for fp in glob.glob(os.path.join(path, '*.txt')):
    importFile(fp, conn)

if __name__ == '__main__':
  log('Starting import')
  db.init_table()
  importFolder(settings.RAW_DATA_PATH)
  log('Finished, exporting')