from flask import Flask, request
import glob
from escpos import printer


app = Flask(__name__)
header = 'Algoliterator'
# header = 'The Cleaner'

def find_printer ():
  return glob.glob('/dev/ttyUSB*')[0]

@app.route('/print', methods=["post"])
def print ():
  text = request.form['text']
  thermal_printer.set(bold=True)
  thermal_printer.text(header)
  thermal_printer.set(bold=False)
  thermal_printer.text('\n\n')
  thermal_printer.text(text)
  thermal_printer.text('\n\n\nDataworkers, Mons 2019')
  thermal_printer.cut()


thermal_printer = printer.Serial(find_printer(), 19200)

if __name__ == "__main__":
    app.run(host='0.0.0.0')