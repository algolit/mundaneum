#!/usr/bin/env/ python

# This script creates a sorted frequency dictionary with stopwords
# The output is printed in a txt-file and in a Logbook in Context

 #    Copyright (C) 2016 Constant, Algolit, An Mertens

 #    This program is free software: you can redistribute it and/or modify
 #    it under the terms of the GNU General Public License as published by
 #    the Free Software Foundation, either version 3 of the License, or
 #    (at your option) any later version.

 #    This program is distributed in the hope that it will be useful,
 #    but WITHOUT ANY WARRANTY; without even the implied warranty of
 #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #    GNU General Public License for more details: <http://www.gnu.org/licenses/>.


from collections import Counter
import string
import nltk



'''
This script creates a frequency dictionary of words used in a text filtering out stopwords
'''

# VARIABLES


# textfiles
source = 'Le livre ne subsistera plus que sous deux formes : le livre d’enseignement élémentaire et \
l ouvrage d imagination. On ne peut songer à utiliser les fiches pour apprendre à lire aux enfants ni \
pour leur donner les premières notions. On ne peut non plus décomposer un poème ou un roman en ses \
éléments ultimes.'
#source = open('Livre_de_demain_clean.txt', 'rt')
destination = open('frequency_livre_de_demain.txt', 'wt')

stopwords = []
with open('stopwords-fr.txt', 'rt') as f:
	txt = f.readlines()
	for line in txt:
		stopwording = line.split(',')
		for wording in stopwording:
			stopwords.append(wording)

#print(stopwords)

## FUNCTIONS

# PREPROCESSING TEXT FILE
## remove caps + breaks + punctuation
def remove_punct(f):
	tokens = (' '.join(line.replace('\n', '') for line in f)).lower()
	for c in string.punctuation:
		tokens= tokens.replace(c,"")
		tokens = tokens.strip()
		#print("tokens", type(tokens))
	return tokens

# remove stopwords
def remove_stopwords(tokens, stopwords):
	tokens = tokens.split(" ")
	words =[]
	for token in tokens:
		if token not in stopwords:
			words.append(token)
	return words

## create frequency dictionary 
def freq_dict(words):
	frequency_d = {}
	# tokens = tokens.split(" ")
	for word in words:
		try:
			frequency_d[word] += 1
		except KeyError:
			frequency_d[word] = 1
	return frequency_d

## sort words by frequency (import module)
def sort_dict(frequency_d):
	c=Counter(frequency_d)
	frequency = c.most_common()
	return frequency

# write words in text file 
def write_to_file(frequency, destination):
	for key, value in frequency:
		destination.write(("{} : {} \n".format(key, value)))
	destination.close()



## SCRIPT

# execute functions
# tokens = remove_punct(source)
words = source.split()

#words = remove_stopwords(tokens, stopwords)

frequency_d = freq_dict(words)

frequency = sort_dict(frequency_d)

write_to_file(frequency, destination)

#source.close()

destination.close()

# -------------------------------------------
