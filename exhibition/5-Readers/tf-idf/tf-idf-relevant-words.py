#https://medium.freecodecamp.org/how-to-process-textual-data-using-tf-idf-in-python-cd2bbc0a94a3

import math, re, sys, string
from nltk.corpus import stopwords

sentences = {}
#  regex to find punctuation
punc = r'[.?\-",]+'

# sentences datasets

# get data from php form (selected data)
textToAnalyse = sys.argv[1]

#  open the file to train
fp = open("text/"+textToAnalyse)
text = fp.read()
# split the text by sentences
sentencesFromText = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', text)

i = 0
for sentence in sentencesFromText:
	sentences[i] = sentence
	i = i + 1

stopwords = stopwords.words('french')
newStopWords = ["un", "une", "me", "se", "cette", "alors", "au", "aussi", "autre", "avant", "avec", "avoir", "bon", "car", "ce", "cela", "ces", "ceux", "chaque", "ci", "comme", "comment", "dans", "des", "du", "de", "dedans", "dehors", "depuis", "devrait", "doit", "donc", "dos", "début", "elle", "elles", "en", "encore", "est", "et", "eu", "fait", "faites", "fois", "font", "hors", "ici", "il", "ils", "je",  "juste", "la", "le", "les", "leur", "là", "ma", "maintenant", "mais", "mes", "mine", "moins", "mon", "mot", "même", "ni", "ne", "nommés", "notre", "nous", "ou", "où", "par", "parce", "pas", "peut", "peu", "plupart", "pour", "pourquoi", "quand", "que", "quel", "quelle", "quelles", "quels", "qui", "sa", "sans", "ses", "seulement", "si", "sien", "son", "sont", "sous", "soyez", "sujet", "sur", "ta", "tandis", "tellement", "tels", "tes", "ton", "tous", "tout", "trop", "très", "tu", "voient", "vont", "votre", "vous", "vu", "ça", "étaient", "état", "étions", "été", "être"]
stopwords.extend(newStopWords)

# useful function
# Split the sentences in words but don't display it
def splitInWordsNoDisplay(s):
	#split by whitespaces
	words = s.split()
	# remove punctuation from each word
	table = str.maketrans('', '', string.punctuation)
	stripped = [w.translate(table) for w in words]
	# convert to lower case
	stripped = [word.lower() for word in stripped]
	# removing stop_words
	filtered_words = [word for word in stripped if word not in stopwords]		
	# return the list of words
	return filtered_words


# split the texts by words
def splitInWords(s):
	#split by whitespaces
	words = s.split()
	# remove punctuation from each word
	table = str.maketrans('', '', string.punctuation)
	stripped = [w.translate(table) for w in words]
	# convert to lower case
	stripped = [word.lower() for word in stripped]
# removing stop_words
	filtered_words = [word for word in stripped if word not in stopwords]

	for word in words:
		if word in stopwords:
			print('<div class="lowercase-wrapper"><span class="lower-word stopword word">'+word.lower() + '</span><span class="regular-word stopword word">'+word+'</span><span class="space-word"></span></div>')
		else:
			print('<div class="lowercase-wrapper"><span class="lower-word word">'+word.lower() + '</span><span class="regular-word word">'+word+'</span><span class="space-word"></span></div>')
	
	# return the list of words
	return filtered_words


# use the TF formula for each word
def computeTF(dic):
	# create a tfDic
	tfDic = {}
	# count the size of the array
	wordsCount = len(dic)
	for word in dic:
		#count word frequency by text
		wordFreq = dic.count(word)
		# divide word frequency by number of words
		tfDic[word] = wordFreq/float(wordsCount)

		print('<div class="tf-transform-wrapper"><span class="tf-transform">'+ str(round(tfDic[word],3))+'</span>'+'<span class="tf-transform-word" data-formula="'+str(wordFreq)+'/'+str(int(wordsCount))+'">'+word+'</span>' + '<span class="space-word"></span></div>')

	return tfDic

# use the IDF formula on each documents
def computeIDF(docs):
	idfDic = {}
	# count the number of documents in that case it's sentences
	docCount = len(docs)
	#print(docCount)
	freq = {} 
	# count how many documents is containing the word
	for count, text in docs.items(): 
		words = splitInWordsNoDisplay(text)
		for word in words:
			#print(word)
			if (word in freq): 
				freq[word] += 1
			else: 
				freq[word] = 1
	# math log total number of document by word frequency in all documents
	for count, frequency in freq.items():
		idfDic[count] = math.log10(docCount/frequency)
		#idfDic[count] = docCount/frequency
		print('<div class="idf-transform-wrapper"><span class="idf-transform">'+ str(round(idfDic[count],3))+'</span>'+'<span class="idf-transform-word" data-formula="<i>math.log10</i>('+str(docCount)+'/'+str(frequency)+')">'+count+'</span>' + '<span class="space-word"></span></div>')
	
	return idfDic
	#print(freq)

def computeTFIDF(TF, IDF):
	tfidfDic = {}
	for count, val in TF.items():
		tfidfDic[count] = val*IDF[count]
		print('<div class="tfidf-transform-wrapper"><span class="tfidf-transform">'+ str(round(tfidfDic[count],3))+'</span>'+'<span class="tfidf-transform-word" data-formula="'+str(round(val,3))+' * '+str(round(IDF[count],3))+'">'+count+'</span>' + '<span class="space-word"></span></div>')
	return tfidfDic

print('<div class="result-text">')
print('<h2>Results</h2>')
# get the IDF number
IDF = computeIDF(sentences)

# run the different formula on the sentences
for count, sentence in sentences.items():
	print('<div class="sentence inline">') 
	# split the sentences by words 
	words = splitInWords(sentence)
	print('<span class="space-sentence"></span>')
	print('</div>') 
	#get the TF number
	TF = computeTF(words)

	# multiply TF by IDF to get the TF-IDF value
	# print the rsult text

	TFIDF = computeTFIDF(TF, IDF)
print('</div>')

# print the basic text
print('<div class="original-text">')
print('<h2>Original text</h2>')
print(text) 
print('</div>')









