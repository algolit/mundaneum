L’Afrique aux noirs! Telle donc l’œuvre à laquelle il nous faut
travailler.

A Léopold II de faire entendre de nouveau sa parole, à lui de prendre
l’initiative de ce rapatriement des nègres américains. Qu’il se mette
en relation avec le Moïse noir, qu’il fasse offrir des terres et des
positions à ceux qu’enthousiasme la parole de ce nouveau prophète, et
qu’ainsi notre Roi achève glorieusement la noble tâche qu’il s’est
proposée: appeler à la civilisation le continent africain.

Rendre l’Afrique aux noirs!

Cette œuvre est digne d’un cœur d’homme et de chrétien, digne aussi du
souverain éclairé d’un peuple libre et travailleur.

28 juillet 1888