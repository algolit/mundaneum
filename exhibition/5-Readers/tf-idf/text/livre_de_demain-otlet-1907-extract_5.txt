Dans l'intéressante étude qu’il a publiée sur l'Esprit d’invention (13), M. F. Mallieux, auquel j'avais expliqué cette possibilité
d'établir une méthode d'invention, a noté cette idée dans les termes
suivants : « On a suggéré, dit-il, de cataloguer les inventions relatives
à un objet, de les classer suivant les moyens employés, d'établir la
série des efforts faits graduellement dans chaque sens et des résultats
graduellement obtenus. Les lacunes deviendraient évidentes et
instructives, les limites atteintes ne le seraient pas moins et, certes,
de tels tableaux auraient une grande valeur. Ces dernières considérations s'appliquent plutôt à la découverte scientifique ou industrielle
qu'aux découvertes à faire dans les sciences morales, la littérature et
les beaux-arts. Mais la transposition en est facile. Il s’agit de créer
un état d'esprit ».

Des méthodes analogues peuvent et devraient, en effet, être
essayées dans tous les domaines de l’activité humaine, et leur rôle
pourrait ne pas se borner à la solution de problèmes posés, mais
conduirait fatalement à l'énoncé de nouveaux problèmes à résoudre.

Telles sont, dans leurs grandes lignes, les conséquences possibles
de l’extension rationnelle des méthodes de documentation, tel est
le rôle que pourrait jouer dans l’histoire du progrès le vaste Institut
International et Universel, où auraient été concentrées, synthétisées,
dépouillées et enregistrées toutes les sciences, vers lequel continueraient à affluer, en multiples et intarissables courants, les conquêtes
sans cesse renouvelées et toujours grandissantes de l'esprit humain.