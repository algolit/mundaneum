**Writing with Otlet**

Writing with Otlet is a character generator that uses the spoken portrait code as its database. Random numbers are generated and translated into a set of features. By creating unique instances, the algorithm reveals the richness of the description that is possible with the portrait code while at the same time embodying its nuances.

The script runs on python3.6 and 3.7. Encountered some problems with encoding when not using an environment where numpy was present. Tested on raspberry pi with raspbian 4.14 and mac with Mojave 10.14.2

Plug the buttons as explained here:
https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/robot/buttons_and_switches/

The PIN to use are the GDN number 6 and GPIO18 number 12. 


