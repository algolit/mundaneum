#!/usr/bin/env/ python
# -*- coding: <utf-8> -*-

'''
It automatises the Oulipo constraint Litterature Definitionelle:
http://oulipo.net/fr/contraintes/litterature-definitionnelle

invented by Marcel Benabou in 1966
In a given phrase, one replaces every significant element (noun, adjective, verb, adverb) 
by one of its definitions in a given dictionary ; one reiterates the operation on the newly received phrase, 
and again.


Copyright (C) 2018 Constant, Algolit, An Mertens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details: <http://www.gnu.org/licenses/>.

'''

from __future__ import division
import nltk
from nltk.corpus import wordnet as wn
from nltk import word_tokenize
import nltk.data
import random
import re
import time
import wikipedia
# import Image
import glob
import os
import sys

from colored import fg, attr

## Attributes

bold = attr(1)
underlined = (4)
reset = attr(0)

## colors
white = fg(15)
spring_green = fg(48)
light_gray = fg(7)
sky_blue = fg(109)
yellow = fg(100)

## FUNCTIONS

def choose_a_tree(dict):
	collection = random.choice(list(trees.items()))
	# Give name gardener
	gardening = collection[0]
	cut = " in "
	if cut in gardening:
		index = re.search(cut, gardening).start()
		gardener = gardening[:index]
		print ("gardener", gardener)
		source = gardening[index+3:]
		print ("source", source)
	else:
		gardener = gardening
		source = 'Unknown'
	# Trees (s)he planted
	tree = collection[1]
	# if (s)he planted more than 1 tree, pick 1
	if type(tree) == list:
		tree = random.choice(tree)
	return gardener, source, tree

# find bio gardener on Wikipedia
def bio(gardener):
	try:
		bio = wikipedia.page(gardener)
		short_bio = bio.summary
	except:
		short_bio = "{}There is no English Wikipedia page about this person.{}".format(sky_blue, reset)
	return short_bio

# tokenize source and get Part-of-Speech tags for each word
def growing(seed):
	plant_with_flowers = []
	# create tuple of tuples with pairs of word + POS-tag
	leaves = word_tokenize(seed)
	analyzing_soil = nltk.pos_tag(leaves)
	#print "analyzing_soil", analyzing_soil
	# for each pair:
	for growing in analyzing_soil:
		#print "growing", growing
		# look for nouns & replace them with their definition
		if growing[1] == "NN":
			if wn.synsets(growing[0]):
				plant = wn.synsets(growing[0])
				#print "plant growing 0: ", plant
				plant_with_flowers.append('{}{}{}'.format(spring_green, plant[0].definition(), white))
				#print "plant with flowers append: ", plant_with_flowers
			else:
				plant_with_flowers.append(growing[0])
		else:
			# non-nouns are left as words
			plant_with_flowers.append(growing[0])
	return plant_with_flowers

#remove '<' and '>'
def windy_weather(tree_with_flowers):
	trimmed_tree = " ".join(tree_with_flowers).replace(' .', '.')
	return trimmed_tree


def await_enter():
	while True:
		if 10 == ord(sys.stdin.read(1)):
			return

# DATA

# Create collection of species & gardeners // key in dictionary needs to be without ,
trees = {
"Hermann Hesse in Baume. Betrachtungen und Gedichte": ["For me, trees have always been the most penetrating preachers.", "Nothing is holier, nothing is more exemplary than a beautiful, strong tree.", "When a tree is cut down and reveals its naked death-wound to the sun, one can read its whole history in the luminous, inscribed disk of its trunk: in the rings of its years, its scars, all the struggle, all the suffering, all the sickness, all the happiness and prosperity stand truly written, the narrow years and the luxurious years, the attacks withstood, the storms endured.", "And every young farmboy knows that the hardest and noblest wood has the narrowest rings, that high on the mountains and in continuing danger the most indestructible, the strongest, the ideal trees grow.", "Trees are sanctuaries. Whoever knows how to speak to them, whoever knows how to listen to them, can learn the truth. They do not preach learning and precepts, they preach, undeterred by particulars, the ancient law of life.", "A tree says: A kernel is hidden in me, a spark, a thought, I am life from eternal life. The attempt and the risk that the eternal mother took with me is unique, unique the form and veins of my skin, unique the smallest play of leaves in my branches and the smallest scar on my bark. I was made to form and reveal the eternal in my smallest special detail.", "A tree says: My strength is trust. I know nothing about my fathers, I know nothing about the thousand children that every year spring out of me. I live out the secret of my seed to the very end, and I care for nothing else. I trust that God is in me. I trust that my labor is holy. Out of this trust I live.", "So the tree rustles in the evening, when we stand uneasy before our own childish thoughts: Trees have long thoughts, long-breathing and restful, just as they have longer lives than ours. They are wiser than we are, as long as we do not listen to them. But when we have learned how to listen to trees, then the brevity and the quickness and the childlike hastiness of our thoughts achieve an incomparable joy. Whoever has learned how to listen to trees no longer wants to be a tree. He wants to be nothing except what he is. That is home. That is happiness."],\
"Maya Angelou": ["When great trees fall, rocks on distant hills shudder, lions hunker down in tall grasses, and even elephants lumber after safety.", "When great trees fall in forests, small things recoil into silence, their senses eroded beyond fear."],\
"Chris Maser in Forest Primeval: The Natural History of an Ancient Forest": "What we are doing to the forests of the world is but a mirror reflection of what we are doing to ourselves and to one another.", \
"Ralph Waldo Emerson": "The creation of a thousand forests is in one acorn.",\
"William Blake": "The tree which moves some to tears of joy is in the eyes of others only a green thing that stands in the way. Some see nature all ridicule and deformity... and some scarce see nature at all. But to the eyes of the man of imagination, nature is imagination itself.",\
"John Lubbock in The Use Of Life": "Rest is not idleness, and to lie sometimes on the grass under trees on a summer's day, listening to the murmur of the water, or watching the clouds float across the sky, is by no means a waste of time.",\
"Franklin D. Roosevelt": "A nation that destroys its soils destroys itself. Forests are the lungs of our land, purifying the air and giving fresh strength to our people.",\
"Kahlil Gibran in Sand and Foam": "Trees are poems that the earth writes upon the sky.",\
"Clarissa Pinkola Estes in The Faithful Gardener: A Wise Tale About That Which Can Never Die": "To be poor and be without trees, is to be the most starved human being in the world. To be poor and have trees, is to be completely rich in ways that money can never buy.",\
"Dorothy Parker in Not So Deep As A Well: Collected Poems": "I never see that prettiest thing, a cherry bough gone white with Spring. But what I think, how gay it would be to hang me from a flowering tree.",\
"Jodi Thomas in Welcome to Harmony": "When trees burn, they leave the smell of heartbreak in the air.",\
"Santosh Kalwar": "All our wisdom is stored in the trees.",\
"Thomas Hardy in Under the Greenwood Tree": "To dwellers in a wood, almost every species of tree has its voice as well as its feature.",\
"Vera Nazarian in The Perpetual Calendar of Inspiration": "Listen to the trees as they sway in the wind. Their leaves are telling secrets. Their bark sings songs of olden days as it grows around the trunks. And their roots give names to all things. Their language has been lost. But not the gestures.",\
"Andrea Koehle Jones in The Wish Trees": "I'm planting a tree to teach me to gather strength from my deepest roots.",\
"Marcel Proust": "We have nothing to fear and a great deal to learn from trees, that vigorours and pacific tribe which without stint produces strengthening essences for us, soothing balms, and in whose gracious company we spend so many cool, silent, and intimate hours.",\
"Bernd Heinrich in The Trees in My Forest": "The very idea of managing a forest in the first place is oxymoronic, because a forest is an ecosystem that is by definition self-managing.",\
"Mokokoma Mokhonoana": "Plants are more courageous than almost all human beings: an orange tree would rather die than produce lemons, whereas instead of dying the average person would rather be someone they are not.",\
"George Orwell": "The planting of a tree, especially one of the long-living hardwood trees, is a gift which you can make to posterity at almost no cost and with almost no trouble, and if the tree takes root it will far outlive the visible effect of any of your other actions, good or evil.",\
"Richard Mabey in Beechcombings: The narratives of trees": "To be without trees would, in the most literal way, to be without our roots.",\
"Seneca": "When you enter a grove peopled with ancient trees, higher than the ordinary, and shutting out the sky with their thickly inter-twined branches, do not the stately shadows of the wood, the stillness of the place, and the awful gloom of this doomed cavern then strike you with the presence of a deity?",\
"Tim Flannery in Peter Wohllebens The Hidden Life of Trees": "A tree's most important means of staying connected to other trees is a wood wide web of soil fungi that connects vegetation in an intimate network that allows the sharing of an enormous amount of information and goods.",\
"Terry Pratchett in Good Omens: The Nice and Accurate Prophecies of Agnes Nutter, Witch": "Jaime had never realised that trees made a sound when they grew, and no-one else had realised it either, because the sound is made over hundreds of years in waves of twenty-four hours from peak to peak. Speed it up, and the sound a tree makes is vrooom.",\
"Kamand Kojouri": "Think the tree that bears nutrition: though the fruits are picked, the plant maintains fruition. So give all the love you have. Do not hold any in reserve. What is given is not lost; it shall return.",\
"Jim Robbins in The Man Who Planted Trees: Lost Groves, Champion Trees, and an Urgent Plan to Save the Planet": ["Planting trees may be the single most important ecotechnology that we have to put the broken pieces of our planet back together.", "What an irony it is that these living beings whose shade we sit in, whose fruit we eat, whose limbs we climb, whose roots we water, to whom most of us rarely give a second thought, are so poorly understood. We need to come, as soon as possible, to a profound understanding and appreciation for trees and forests and the vital role they play, for they are among our best allies in the uncertain future that is unfolding."],\
"Laura Lafargue in Correspondence Volume 2 1887-1890": "I wish the trees would go into leaf that I might find out what they are. In their present undress I cannot recognise them. It's true that I doubt if I should know my best friends--men or women--with their clothes off.",\
"Scott Blum in Waiting for Autumn": "I'm such a fan of nature, and being with the trees every day fills me with joy.",\
"Mehmet Murat ildan": ["Why pay money for the horror movies? Just go to a street without trees!", "When you save the life of a tree, you only pay your debt as we all owe our lives to the trees!"],\
}

# add image random
#### check script death of the authors, 1942!!!
# imagefile = random.choice(glob.glob('images/*.jpg'))
# image = imagefile[7:]
# print (image)



# GROWTH

gardener, source, tree = choose_a_tree(trees)
short_bio = bio(gardener)

# generate metadata
os.system('clear')
print('Using a quote by {}{}{}\n'.format(bold, gardener, reset))
print (short_bio)

await_enter()

# Publish book
# print("\chapter{Growing a Tree}\n")
# print('Anais Berck\n')
# print('Using a quote by '+gardener+'\n')

# generate introduction
# write some text on Algolithmic Forest, Oulipo, Mercel Benabou, Litterature Definitionnelle
# print('write some text on Algolithmic Forest, Oulipo, Mercel Benabou, Litterature Definitionnelle')
# print("\nTree quote by " + gardener + '\n' + tree + '\n')
# print("Short bio about the author:\n" + short_bio)

# # Print chapter: define how much chapters you want to generate and how you want to create paragraphs!!!
# print growth
while len(tree) < 15000:
	os.system('clear')

	# offer flowering plant
	print("Tree quote: " + '\n' + tree + '\n')
	# print("Tree quote: " + '\n' + tree + "\n")

	# cultivating the tree
	tree_with_flowers = growing(tree)
	trimmed_tree = windy_weather(tree_with_flowers)
	#trimmed_tree = " ".join(windy_weather)

	await_enter()
	
	print("Grown tree quote:" + '\n'+ trimmed_tree +"\n")
	# print("Grown tree quote:" + '\n'+ trimmed_tree +"\n")

	await_enter()

	# flowering plant becomes new seed
	# time.sleep(2)
	tree = trimmed_tree