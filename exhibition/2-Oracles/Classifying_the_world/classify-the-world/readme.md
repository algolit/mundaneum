# Install
Install python dependencies (in a virtual env):

```
pip install -r requirements.txt
```

Make sure the dataset (uci-news-aggregator.csv) is in the same folder

# Run
```
bash run.sh
```

The interface should be available on localhost:5000/classifying-the-world