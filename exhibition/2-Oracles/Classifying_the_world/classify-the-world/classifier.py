##Loading all the required libraries
import pandas as pd
import numpy as np

from sklearn.naive_bayes import MultinomialNB

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split

from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix

#library for regular expretion
import re

import os.path

from joblib import dump, load

MODELFILE = 'classifying-the-world.joblib' # change to gz for compression
DATAFILE = 'wikipedia.csv'
 
stop_words = ["un", "une", "me", "se", "cette", "alors", "au", "aussi", "autre", "avant", "avec", "avoir", "bon", "car", "ce", "cela", "ces", "ceux", "chaque", "ci", "comme", "comment", "dans", "des", "du", "de", "dedans", "dehors", "depuis", "devrait", "doit", "donc", "dos", "début", "elle", "elles", "en", "encore", "est", "et", "eu", "fait", "faites", "fois", "font", "hors", "ici", "il", "ils", "je",  "juste", "la", "le", "les", "leur", "là", "ma", "maintenant", "mais", "mes", "mine", "moins", "mon", "mot", "même", "ni", "ne", "nommés", "notre", "nous", "ou", "où", "par", "parce", "pas", "peut", "peu", "plupart", "pour", "pourquoi", "quand", "que", "quel", "quelle", "quelles", "quels", "qui", "sa", "sans", "ses", "seulement", "si", "sien", "son", "sont", "sous", "soyez", "sujet", "sur", "ta", "tandis", "tellement", "tels", "tes", "ton", "tous", "tout", "trop", "très", "tu", "voient", "vont", "votre", "vous", "vu", "ça", "étaient", "état", "étions", "été", "être"]

def trainClassifier ():
  #Loading the data which is from Kaggle
  #This dataset contains four news class -b for business, e- entertainment, -t for technology and science, -m for medical
  print("Reading data file")
  data = pd.read_csv(DATAFILE, encoding='latin-1')
  data = data[['CATEGORY','TITLE']]
  news_data = data

  #Printing all different types of categories
  news_data.CATEGORY.unique()
  news_data.groupby('CATEGORY').describe()


  #converting category column into numeric target NUM_CATEGORY column
  # news_data['NUM_CATEGORY']=news_data.CATEGORY.map({'b':0,'e':1,'m':2,'t':3,'Economy-Business_Finance':0,'Health':2,'Lifestyle_leisure':2,'Nature_Environment':4,'Politics':0,'Science_Technology':3})
  news_data['NUM_CATEGORY']=news_data.CATEGORY
  print("data category")
  print(news_data.CATEGORY)
  news_data.head()

  #Splitting dataset into 60% training set and 40% test set
  x_train, x_test, y_train, y_test = train_test_split(news_data.TITLE, news_data.NUM_CATEGORY, random_state=50)
  print("Vectorizing")

  vect = CountVectorizer(analyzer='word', stop_words = stop_words)

  #converting training features into numeric vector
  X_train = vect.fit_transform(x_train)
  #converting training labels into numeric vector
  X_test = vect.transform(x_test)

  #Training and Predicting the data
  #start = time.clock()
  
  print("Training")
  mnb = MultinomialNB(alpha =0.2)
  mnb.fit(X_train,y_train)

  print("Training finished")

  return (mnb, vect)
  
def initClassifier (forceTraining = False):
  if os.path.exists(MODELFILE) and not forceTraining:
    print("Loading stored model")
    (model, vectorizer) = load(MODELFILE)
  else:
    print("Training a new model")
    (model, vectorizer) = trainClassifier()
    print("Storing model")
    dump((model, vectorizer), MODELFILE)

  return (model, vectorizer)

def tokenData(token, model, vector_labels):
  try: 
    idx = vector_labels.index(token)
    
    return {
      'token': token,
      'probabilities': [float(model.feature_log_prob_[y][idx]) for y in range(len(model.classes_))],
      'counts': [int(model.feature_count_[y][idx]) for y in range(len(model.classes_))]
    }

  except ValueError:
    return {
      'token': token
    }

def annotatedPrediction (text, model, vectorizer, tokenizer, vector_labels):
  # Make vector
  x = vectorizer.transform([text])

  return {
    'prediction': int(model.predict(x)[0]),
    'probabilities': [float(num) for num in model.predict_proba(x)[0]],
    'tokens': [ tokenData(token, model, vector_labels) for token in tokenizer(text) ]
  }


#This function return the class of the input news
def predict (text, model, vectorizer):
  test = vectorizer.transform(text)
  pred= model.predict(test)
  if pred  == 0:
       return 'Généralités'
  elif pred == 1:
      return 'Philosophie et psychologie'
  elif pred == 2:
      return 'Religion, théologie'
  elif pred == 3:
      return 'Sciences sociales'
  elif pred == 4:
    return 'inoccupée'
  elif pred == 5:
    return 'Sciences pures'
  elif pred == 6:
    return 'Sciences appliquées'
  elif pred == 7:
    return 'Arts. Divertissements. Sports'
  elif pred == 8:
    return 'Langue. Linguistique. Littérature'
  else:
      return 'Géographie. Biographie. Histoire '

if __name__ == '__main__': 
  (model, vectorizer) = initClassifier()

  ##Copy and paste the news headline in 'x'
  x=["mon café est très bon"]
  r = predict(x, model, vectorizer)
  print("\n")
  print("— Predictive category of the article: ")
  print(r)


  tokenizer = vectorizer.build_tokenizer()
  vector_labels = vectorizer.get_feature_names()

  print(annotatedPrediction(x[0], model, vectorizer, tokenizer, vector_labels))