Une cartouche informatique est un module électronique d'extension intégré dans un boîtier en plastique comportant un circuit imprimé, que l'on peut insérer facilement dans un microordinateur ou une console de jeu.
La cartouche informatique est généralement dotée d'une mémoire morte, contenant un programme, et parfois, elle peut également contenir des coprocesseurs, comme c'est le cas dans certaines cartouches de jeux ou applications musicales sur le standard MSX. Il arrive cependant qu'elle ne contienne que des puces permettant d'augmenter les fonctionnalités ou la puissance de l'ordinateur.
Parmi les ordinateurs ayant supporté des cartouches informatiques on peut citer :

Le ZX81 comportait à l'arrière un port d'extension, sur lequel on pouvait ajouter des extensions mémoire sous forme de cartouche.
Le standard japonais MSX et ses évolutions MSX2, MSX2+ et MSX turbo R
Le micro-ordinateur soviétique Elektronika МС 0511
Le micro-ordinateur américain Texas Instrument-99/4A
L'Amiga 500 avait une trappe, sous l'ordinateur permettant d'ajouter une cartouche contenant 512 Kio de mémoire vive additionnelle.Parmi les consoles de jeux ayant supporté des cartouches informatiques, alors appelées cartouches de jeu vidéo, on peut citer :

Atari 2600
Colecovision
Console Vectrex
Nintendo NES et Super Famicom
Séga Master System