L'acronyme TPU peut désigner :

Tensor Processing Unit, un type de processeur dédié au calcul d'apprentissage des réseaux de neurones ;
Time Processing Unit, une technique d'optimisation sur les processeurs Motorola.
Thermoplastic polyurethane, en français polyuréthane thermoplastique.