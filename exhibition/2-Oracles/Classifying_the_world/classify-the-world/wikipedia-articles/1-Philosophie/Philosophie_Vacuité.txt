Le terme vacuité a plusieurs significations :

Tout d'abord, de façon générique, la vacuité est l'état de ce qui est vide.
Dans le domaine de la philosophie et de la littérature, la vacuité est aussi le vide intellectuel ou l'absence de valeur ; on parlera ainsi de la vacuité d'une existence.
D'autre part, ce concept a une acception plus spécifique dans le bouddhisme : la śūnyatā, « vacuité ultime des réalités intrinsèques ».