# Classifying the World

Librarian Paul Otlet's life work was the construction of the Mundaneum. This mechanical collective brain would house and distribute everything ever committed to paper. Each document was classified following the Universal Decimal Classification. Using telegraphs and especially, sorters, the Mundaneum would have been able to answer any question from anyone.

With the collection of digitized publications we received from the Mundaneum, we built a prediction machine that tries to classify the sentence you type in one of the main categories of Universal Decimal Classification. You also witness how the machine 'thinks'. During the exhibition, this model is regularly retrained using the cleaned and annotated data visitors added in Cleaning for Poems and The Annotator.

The main classes of the Universal Decimal Classification system are:

0 - Science and Knowledge. Organization. Computer Science. Information Science. Documentation. Librarianship. Institutions. Publications

1 - Philosophy. Psychology

2 - Religion. Theology

3 - Social Sciences

4 - vacant

5 - Mathematics. Natural Sciences

6 - Applied Sciences. Medicine, Technology

7 - The Arts. Entertainment. Sport

8 - Linguistics. Literature

9 - Geography. History 


## Install
Install python dependencies (in a virtual env):

```
pip install -r requirements.txt
```

Make sure the dataset (uci-news-aggregator.csv) is in the same folder

## Run
```
bash run.sh
```

The interface should be available on localhost:5000/classifying-the-world

## Authors
Sarah Garcin, Gijs de Heij, An Mertens 

## License
Copyright (C) Algolit, Brussels, 2019
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details: <http://www.gnu.org/licenses/>.