## The Algoliterator

by Algolit 

The Algoliterator is a neural network trained using the selection of digitized works of the Mundaneum archive.  

With the Algoliterator you can write a text in the style of the International Institutions Bureau. The Algoliterator starts by selecting  a sentence from the archive or corpus used to train it. You can then  continue writing yourself or, at any time, ask the Algoliterator to  suggest a next sentence: the network will generate three new fragments  based on the texts it has read. You can control the level of training of  the network and have it generate sentences based on primitive training,  intermediate training or final training. 

When you're satisfied with your new text, you can print it on the thermal printer and take it home as a souvenir. 

You can find the code base of this installation [here](https://gitlab.constantvzw.org/algolit/algoliterator.clone)

And a video example [here](https://peertube.video/videos/watch/599467c7-4e4e-421b-92dc-e84ff4de99ec).

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.video/videos/embed/599467c7-4e4e-421b-92dc-e84ff4de99ec" frameborder="0" allowfullscreen></iframe>

![data_workers-71](data_workers-71.jpg)