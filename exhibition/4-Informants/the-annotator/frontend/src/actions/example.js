export const ACTION_NAME = 'ACTION_NAME';
export const GREETING_CHANGE = 'GREETING_CHANGE';

export const actionName = () => ({
  type: ACTION_NAME
});

export const greetingChange = (greeting) => ({
  type: GREETING_CHANGE,
  greeting // short for: greeting: greeting
})