import { 
  sentenceGet as apiSentenceGet,
  sentencePost as apiSentencePost,
  sentenceMarkDirty as apiSentenceMarkDirty
} from '../services/api';

export const SENTENCE_LOAD = 'SENTENCE_LOAD';
export const SENTENCE_LOAD_SUCCESS = 'SENTENCE_LOAD_SUCCESS';
export const SENTENCE_LOAD_FAIL = 'SENTENCE_LOAD_FAIL';
export const SENTENCE_POST = 'SENTENCE_POST';
export const SENTENCE_POST_SUCCESS = 'SENTENCE_POST_SUCCESS';
export const SENTENCE_POST_FAIL = 'SENTENCE_POST_FAIL';
export const SENTENCE_SUBMIT = 'SENTENCE_SUBMIT';
export const SENTENCE_CLASS_SELECT = 'SENTENCE_CLASSIFICATION_PICK';
export const SENTENCE_MARK_DIRTY = "SENTENCE_MARK_DIRTY";
export const SENTENCE_MARK_DIRTY_SUCCESS = "SENTENCE_MARK_DIRTY_SUCCESS";
export const SENTENCE_MARK_DIRTY_FAIL = "SENTENCE_MARK_DIRTY_FAIL";


export const sentenceLoad = () => (dispatch, _) => {
  dispatch({ type: SENTENCE_LOAD })
  apiSentenceGet()
    .then((data) => dispatch(sentenceLoadSuccess(data)))
    .catch(() => dispatch(sentenceLoadFail()));
} 

export const sentencePost = () => (dispatch, getState) => {
  const state = getState().sentence;
  dispatch({ type: SENTENCE_POST })
  apiSentencePost(state.sentence_id, state.selectedClass)
    .then(() => {
      dispatch({ type: SENTENCE_POST_SUCCESS });
      dispatch(sentenceLoad())
    })
    .catch(() => dispatch({ type: SENTENCE_POST_FAIL }));
}

export const sentenceMarkDirty = () => (dispatch, getState) => {
  const state = getState().sentence;
  dispatch({
    type: SENTENCE_MARK_DIRTY
  });
  apiSentenceMarkDirty(state.sentence_id)
    .then(() => {
      dispatch({ type: SENTENCE_MARK_DIRTY_SUCCESS });
      dispatch(sentenceLoad())
    })
    .catch(() => dispatch({ type: SENTENCE_MARK_DIRTY_FAIL }))
}

export const sentenceClassSelect = (selectedClass) => ({
  type: SENTENCE_CLASS_SELECT,
  selectedClass: selectedClass  
})

export const sentenceLoadSuccess = ({ sentence_id, sentence, before, after }) => ({
  type: SENTENCE_LOAD_SUCCESS,
  sentence_id, sentence, before, after })

export const sentenceLoadFail = () => ({ type: SENTENCE_LOAD_FAIL })