import React from 'react';

export const SentenceClassList = ({
  sentenceClasses,
  sentenceClassSelected,
  sentenceClassSelect
}) => (
  <section className="sentence--classlist">
    { sentenceClasses.map((sentenceClass, k) => (
        <button className="sentence--classlist--class" onClick={ () => sentenceClassSelect(k) } data-selected={ (k === sentenceClassSelected) }>
          { sentenceClass}
        </button>
      ))}
  </section>
);

export default SentenceClassList;