import React, { Component } from 'react';
import Sentence from './Sentence';
import SentenceActions from './SentenceActions';
import SentenceClassList from './SentenceClassList';

import { t } from '../utils'

export const Annotator = class extends Component {
  componentDidMount = () => {
    this.props.classesLoad();
    this.props.sentenceLoad();
  }
  
  render = () => {
    const {
      classesLoading,
      message,
      sentence,
      before,
      after,
      sentenceClasses,
      sentenceLoading,
      selectedClass,
      sentenceClassSelect,
      sentenceLoad,
      sentencePost,
      sentenceMarkDirty
    } = this.props;

    return <section id="annotator">
      <section id="application-message">{ message }</section>
      <Sentence sentence={ sentence } before={ before } after={ after }/>
      <SentenceActions
        sentenceLoading={ sentenceLoading }
        sentenceClassSelected={ selectedClass }
        sentenceLoad={ sentenceLoad } 
        sentencePost={ sentencePost } 
        sentenceMarkDirty={ sentenceMarkDirty } />
      <section className="interface--hint">{ t('select_class_explanation') }</section>
      <SentenceClassList
        classesLoading={ classesLoading } 
        sentenceClasses={ sentenceClasses } 
        sentenceClassSelected={ selectedClass } 
        sentenceClassSelect={ sentenceClassSelect } />
    </section>
  }
}

export default Annotator;