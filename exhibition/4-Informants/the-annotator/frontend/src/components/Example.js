import React from 'react';

export const Example = ({ greeting, greetingChange }) => (
  <section className="example-component">
    <h1>Hello, { greeting }!</h1>
    <input value={ greeting } onChange={ (e) => greetingChange(e.target.value) } />
  </section>
);

export default Example;