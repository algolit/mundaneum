import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { sentenceClassSelect, sentenceLoad, sentencePost, sentenceMarkDirty } from '../actions/sentence';
import { classesLoad } from '../actions/classes';
import Annotator from '../components/Annotator'


function mapStateToProps ({ sentence, classes, application }) {
  // Make a selection of the state
  return  { 
    ...sentence,
    sentenceClasses: classes,
    ...application  }
}

function mapDispatchToProps (dispatch) {
  // Select action creators to be bound with dispatch
  return bindActionCreators({
    classesLoad,
    sentenceClassSelect,
    sentenceLoad,
    sentenceMarkDirty,
    sentencePost
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Annotator);