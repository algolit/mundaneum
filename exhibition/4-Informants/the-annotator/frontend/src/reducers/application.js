import { MESSAGE_CLOSE } from '../actions/application';

import {
  CLASSES_LOAD,
  CLASSES_LOAD_FAIL,
  CLASSES_LOAD_SUCCESS
} from '../actions/classes';

import { 
  SENTENCE_LOAD,
  SENTENCE_LOAD_FAIL,
  SENTENCE_LOAD_SUCCESS,
  SENTENCE_POST,
  SENTENCE_POST_FAIL,
  SENTENCE_POST_SUCCESS,
  SENTENCE_MARK_DIRTY,
  SENTENCE_MARK_DIRTY_FAIL,
  SENTENCE_MARK_DIRTY_SUCCESS
} from '../actions/sentence';

import { simpleUpdate, t } from '../utils';

const initialState = {
  sentenceLoading: false,
  classesLoading: false,
  message: null
}

export default function example (state = initialState, action) {
  switch (action.type) {
    case MESSAGE_CLOSE:
      return simpleUpdate(state, { message: null })
    
    case CLASSES_LOAD:
      return simpleUpdate(state, { classesLoading: true, message: t('classes_load') })
    case CLASSES_LOAD_FAIL:
      return simpleUpdate(state, { sentenceLoading: false, message: t('classes_load_fail') })
    case CLASSES_LOAD_SUCCESS:
      return simpleUpdate(state, { classesLoading: false, message: '' })
    
    case SENTENCE_LOAD:
      return simpleUpdate(state, { sentenceLoading: true, message: t('sentence_load') })
    case SENTENCE_LOAD_FAIL:
      return simpleUpdate(state, { sentenceLoading: false, message: t('sentence_load_fail')})
    case SENTENCE_LOAD_SUCCESS:
      return simpleUpdate(state, { sentenceLoading: false, message: '' })
    
    case SENTENCE_MARK_DIRTY:
      return simpleUpdate(state, { message: t('sentence_mark_dirty') })
    case SENTENCE_MARK_DIRTY_FAIL:
      return simpleUpdate(state, { message: t('sentence_mark_dirty_fail') })
    case SENTENCE_MARK_DIRTY_SUCCESS:
      return simpleUpdate(state, { message: null })

    case SENTENCE_POST:
      return simpleUpdate(state, { sentenceLoading: true, message: t('sentence_post')})
    case SENTENCE_POST_FAIL:
    return simpleUpdate(state, { sentenceLoading: false, message: t('sentence_post_fail')})
    case SENTENCE_POST_SUCCESS:
    return simpleUpdate(state, { sentenceLoading: false,  message: t('sentence_post_success')})

    default:
      return state;
  }
}
