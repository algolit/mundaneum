#!/bin/bash

sudo cp lxde-pi-rc.xml /etc/xdg/openbox
mkdir /home/pi/.config/autostart
cp config.desktop "/home/pi/.config/autostart/${1}.desktop";
sudo chown -R pi:pi "/home/pi/.config/autostart"