






















                                 V
                                  G
                                   I
                                I
                             C O I V G I t È S S  M O N D I A L L
                             C
                               O
                                    t
                                               D
                                                I
                                                  A
                                             N
                                     È
                                          M
                                            O
                                       DEI
               A S S O C I A T I O N S  I N T E R N A T I O N A L E S  N°46
                                                                       N°4
                                                                              6
                                     G
                              s
                         Deuxième ession : ANG-BRUXELLES,  15-18 uin  1913
                                                    j
                   Organisé  par l'Union  des  Associations  Internationales
                         Office  central  : Bruxelles,  3bis,  rue  de  la  Régence
                                                                             0
                                                                        1 1 9 1 3 . 0 6
                                                                         9
                                                                           3
                                                                            .
                                                                          1
                                                                              6
                       u
                                       Documents
               Actes s  du  Congrès .  —  Document s  préliminaires. .
               Acte
                                                    préliminaires
                         Congrès.
                      d
                                    —
                   Les   Grandes      lignes   d'un    Projet     d'Institut
                      International       à   créer    en   Europe      pour
                      favoriser    le  développement        des   relations
                      économiques        entre   les  Etats   d'Outre-Mer
                      et  ceux   d'Europe
                                          R A P P O R T
                                            PRES IN  TÉ  PAR
                                    R A Y M O N D  D E  W A H A
                                           [338  ( «  :  4) ]
                              CONSIDÉRATIONS         GÉNÉRALES
                     1.  U n  grand  nombre  d'Etats  d'Outre-Mer,  par  exemple  le
                  Canada,  l'Australie,  les  Etats  de  l'Amérique  latine  intéressés
                  à  l'ouverture  du  canal  de  Panama,  etc.,  font  à  l'heure  qu'il  est
                  de  vigoureux  efforts  pour  augmenter  leurs  productions  et  leurs
                  échanges  avec  les  Etats  d'Europe.
                     2 .  Les  informations,  dont  on  dispose  actuellement  en  Europe
                  sur  les  Etats  d'Outre-Mer, sont  généralement  jugées  insuffisantes
                  pour  la plupart  d'entre  eux.  Les  rapports qu'envoient  les  consuls
                  d'Allemagne,  d'Autriche,  de  France  et  d'autres  pays  sur  la
                  situation  économique  des  Etats  d'Outre-Mer  sont  considérés
























                                                                                                                                      —  3  —
                     par  les  industriels  et  les  commerçants  d'Europe  comme  étant                    consulter.  Ils  auraient  aussi  à  faire  le  choix  des  renseignements
                                     t
                     pour  la  plupart rop  académiques,  trop  discontinus  et  comme                      à  mettre,, par  voie  de publication,  à la portée  du public  européen.
                                              t
                                         t
                     arrivant  bien souvent rop ard.  Une  étude  attentive de  la partie
                     économique  et  commerciale  des  grands  journaux d'Europe,  ainsi                      2.  L'Institut  se sçrvirait  de  plusieurs  espèces  de  publications
                     que  des  revues  économiques  et  commerciales,  fait  voir  que  les                 pour  s'adresser  aux  consommateurs  et  aux  producteurs  euro-
                                                                                                                                          l
                     Etats  d'Outre-Mer  sont  négligés  beaucoup  plus  qu'il  ne convien-                 péens. Toutes  les publications cle 'Institut  paraîtraient  en autant
                     drait,  eu  égard  aux  rapports  économiques  déjà  existant  et  aux                 d'éditions  qu'il  y  a  de  langues  principales  en  Europe.
                                                                                                                                          l
                     besoins  d'exportation  des  Etats  d'Europe  qui  croissent  de  jour                   Les  publications régulières  de 'Institut  seraient  :
                     en  jour.  Enfin,  le  dernier  rapport  annuel  de  la  Chambre  de                     A.  Des  Correspondances  hebdomadaires  ou  moins  fréquentes,
                     commerce  de  Hambourg,   la  création  à  Aix-la-Chapelle  d'un                       à  servir  gratuitement  à  tous  les  journaux  et  à  toutes  les  revues
                     Institut  pour  favoriser  les  rapports  de  tous  genres  entre  l'Alle-             d'Europe  qui  voudraient  en  réimprimer  le  contenu.  I l  y  aurait
                     magne  et  les  pays  de  l'Amérique latine, ainsi que  plusieurs  autres              une  Correspondance  spéciale  pour  chaque  grand  pays  d'Outre-
                     faits  analogues  des  derniers  temps,  démontrent  le  besoin  qu'on                 Mer,  et  des  Correspondances  collectives  pour  des  groupes  de
                     éprouve  en  Europe  d'être  mieux  renseigné  sur  la  situation                      pays  d'importance  moyenne.
                     économique  des  pays lointains.
                                                                                                              Le  contenu  des  Correspondances  se  composerait  :
                       Pour  venir  en  aide  aux  efforts  des  pays  d'Outre-Mer,  et  pour
                     satisfaire  le  besoin  d'information  des  consommateurs  et  des                       a)  D'un  ou  de  deux  articles  de  fond  traitant  de  questions
                     producteurs  européens,  i l  conviendrait  dé  créer  en  Europe  un                  économiques  d'intérêt  général  ;
                     Institut  International  qui  concentrerait  les  renseignements                         b)  De  notes  de  feuilleton sur  des  particularités  géographiques,
                     concernant la situation  économique  des  pays  d'Outre-Mer,  et                       des  institutions  sociales,  etc.
                     les porterait à la  connaissance  des  consommateurs  et  des  produc-                   Ces  articles  de  fond  et  ces  notes  de  feuilleton auraient pour  but
                     teurs  d'Europe.                                                                       d'attirer  sans  cesse  l'attention  des  lecteurs  des  journaux  et
                                                                                                            revues  d'Europe  sur  les  choses  d'Outre-Mer ;
                                                                                                              c)  De  renseignements  commerciaux.  Notes  brèves  destinées
                                   ACTIVITÉS    D E  L ' I N S T I T U T
                                                                                                            à  être  reproduites  dans  la  partie  commerciale  des  feuilles  d'Eu-
                                                                                                            rope.  C'est  par  ces  notes  fréquentes  et  régulières  que  les  consom-
                                         l
                       i .  En  premier  lieu 'Institut  à  créer  aurait  à  organiser  un
                     service  de  documentation.  I l  faudrait  que  les  Gouvernements                    mateurs  et  producteurs  d'Europe  seraient  tenus  au  courant  des
                     d'Outre-Mer  lui  envoient,  régulièrement  et  aussi  rapidement                      occasions d'achat  et  de vente  dans  les pays lointains.
                     que  possible,  les  statistiques  et  autres  publications  officielles                 B.  U n  Périodique  Mensuel..  Celui-ci  offrirait  une  analyse
                     concernant  les  productions  et  le  commerce.  Les  Chambres  de                     plus  complète,  plus  détaillée  et  plus  approfondie  de  la situation
                                                               l
                     Commerce  et  les  organisations  analogues  de 'Agriculture et  de                    économique  des  pays  d'Outre-Mer.  I l serait  divisé  en  départe-
                     l'Industrie  en  feraient  autant  de  leurs  publications.  L'Institut                ments  pour  des  groupes  de  pays,  par  exemple  un  département
                     à  son  tour  s'abonnerait  aux  publications  économiques  de  tous                   pour  les  colonies  anglaises,  un autre pour les Etats-de  l'Amérique
                     pays  et  tâcherait  de  se  procurer,  autant  que  possible  par  un                 latine,  etc.  Dans  chaque  département l y  aurait autant  de subdi-
                                                                                                                                               i
                     service  de  correspondants  spéciaux  qu'il  lui  faudrait  organiser                 visions  qu'il  y  aurait  de  groupes  d'industries  à  prendre  en  consi-
                     dans  les  pays d'Outre-Mer, tous  les  renseignements  utiles  concer-                dération.
                     nant  les  productions et  les  besoins  de  ces  pays.                                  Le  Périodique  mensuel pourrait  publier, à l'instar des  rapports
                                      l
                       Les  bureaux  de 'Institut  auraient  ensuite  la  tâche  de  classer                consulaires  de  différents  pays,  des  monographies  d'industrie.  Ce
                     les  matériaux  et  d'en  faire  un  répertoire  synoptique,  facile  à                genre  présente  le  double  avantage  de  comporter  une  méthode
























                                                —  4  —                                                                               —  5  —

                      très  scientifique,  et  de  permettre par  conséquent  au  Périodique                et  internationale, de  nature  à  contribuer au  rapprochement  des
                      de  prétendre  à  la  haute  estime  des  élites  intellectuelles  ;  et              peuples,  i l appert  que  cet  Institut  peut  être  considéré comme  un
                      d'autre  part,  de  fournir  des  renseignements  précieux et directe-                facteur  de  l'Organisation internationale et  de  la  Paix  universelle.
                      ment  utilisables, pourvu qu'ils soient  prompts à  venir et  d'actua-                  Cela  est  d'autant  plus  vrai  que  les  renseignements  portés  par
                      lité,  à  tous  les  industriels  qui  voudraient  élargir  leurs  rapports           les  publications  de  l'Institut  dans  les  populations  européennes
                      commerciaux  avec  les  pays  d'Outre-Mer.  On  pourrait  même                        rendront  plus  vivant  parmi  elles  le  sentiment  de  l'interdépen-
                      donner  un  relief  tout  particulier  au  Périodique  mensuel  en                    dance  économique  des  peuples.
                      ajoutant,  aux  travaux  élaborés  dans  les  bureaux  de  l'Institut,
                      des  articles  faits  par  des  savants,  des  hommes  politiques  et  des
                      hommes  d'affaires  des  pays  d'Outre-Mer  sur  les  ressources  et  les                        O R G A N I S A T I O N  D E  L ' I N S T I T U T
                     besoins de  leurs pays.  Ces  articles seraient  naturellement traduits                  Pourront  être  membres  de 'Institut  ; les Gouvernements  (pour
                                                                                                                                      l
                      en  langues  européennes  par  les  bureaux  de  l'Institut.
                                                                                                            les  Etats  fédératifs,  le  Gouvernement  central  et  les  Gouverne-
                        Tout  consommateur  et  producteur d'Europe  pourrait  s'abon-                      ments  des  Etats  particuliers), les  Provinces,  les  Municipalités,
                     ner  à  bas  prix  au  Périodique  mensuel  ou  à  celle  de  ses  parties             les  Chambres  de  Commerce,  les  Organisations  de  l'industrie
                     qui  l'intéresserait  le  plus.  On  pourrait  agrémenter  les  différentes            et  de  l'agriculture, les  sociétés,  les  entreprises  de  tout  genre  et
                      éditions  du  Périodique  d'annonces  à  insérer  par  des  maisons                   les particuliers.
                     d'Outre-Mer  désirant  exporter  leurs  produits en  Europe.
                                                                                                              L'Institut  sera  dirigé  par  une  Direction  ou  un  Secrétariat
                        3.  L'Institut  pourrait  peut-être  aussi  procéder  à  des                        général  qui  se  composera  d'aussi  peu  de  têtes  que  possible,  afin
                      enquêtes  internationales  sur  des  questions  d'intérêt  international,             de ui assurer  une  grande  liberté  et  une  grande  rapidité d'action.
                                                                                                               l
                     par  exemple  sur  l'état  des  quais  et  dépôts  dans  les  ports  de                  La  direction sera  responsable vis-à-vis d'une Assemblée  générale
                      mer,  etc.  I l  réunirait  de  la  sorte  des  matériaux  qui pourraient             composée  de  délégués  des Gouvernements,  Municipalités, Organi-
                      servir  de  point  de  départ  à  des  conventions  internationales, par              sations,  Sociétés  et  particuliers adhérents.  Ces  délégués  se  réuni-
                      exemple  en  vue  de  régler  d'une  façon  uniforme  la  police  des                 ront  une  fois  par  an  en  assemblée générale  au  siège  de 'Institut.
                                                                                                                                                               l
                      quais  et  les  conditions de  dépôt.  I l s'ensuivrait  une  amélioration            Les  Gouvernements,  Municipalités  et  Organisations  pourront  se
                      qui  faciliterait  le trafic  international  et  qui permettrait d'abaisser           faire  représenter par autant  de  délégués, qu'ils verseront  d'unités
                      les  primes  d'assurances  maritimes, etc.  L'Institut  à  créer  pour-               de  cotisation.  Les  sociétés  privées  et  les  particuliers  adhérents
                      rait  donc  jouer,  dans  le  domaine  de  la  législation  économique                à 'Institut  formeront  des  groupes  qui,  à  leur  tour  seront  repré-
                                                                                                              l
                      internationale,  un  rôle- analogue  à  celui  que  joue  déjà  l'Office              sentés  par  autant  de  délégués  qu'ils  paieront  d'unités  de  coti-
                      International  du Travail à Bâle,  dans le domaine  de la  législation                sation.
                      internationale  de  protection  ouvrière.                                               I l  y  aura  à 'Institut  un  Bureau  national  pour  chaque grand
                                                                                                                          l
                        4.  Les  Statistiques  du  Commerce  international,  réunies  et                    pays  d'Outre-Mer,  et  des  Bureaux  collectifs  pour  chaque  groupe
                      mises  autant  que  possible  en  concordance  par 'Institut,  ensuite                de  pays  d'importance  moyenne.  Chaque  bureau  national  élabo-
                                                                 l
                      portées  par  ses  publications  à  la  connaissance  de  tout  le  monde             rera  les  parties  des  publications  de  l'Institut  qui  concerneront
                      en  Europe,  pourraient  parfois  exercer  sur  la  législation  de  tel              son  pays.  I l faudrait  employer  dans  chacun  des  bureaux  natio-
                                                                                                                          u
                      ou  tel  pays  une  influence  qui  la  rendrait  plus  favorable  aux                naux  au moins n ressortissant  du pays en question. Ces  fonction-
                      échanges internationaux.                                                              naires  devront  être  bien  au  courant  de  la  situation  économique
                                                                                                            de  leur  patrie  et  posséder  au  moins  une  langue  européenne.
                        5.  Puisque  les  travaux  exécutés  par 'Institut  proposé  pour-                  Us  seront  tout  désignés  comme  organisateurs  et  directeurs  de
                                                           l
                      raient  donner  le  branle  à  des  actes  de  législation  nationale                 sections  nationales  d'un  Musée  Commercial  international  à























                                                —  6  —

                      rattacher  éventuellement  à  l'Institut.  L'Institut  pourra,  du
                      moment  qu'il  dispose  de  collaborateurs  parfaitement  au  courant
                      des  choses  d'Outre-Mer,  qu'il  s'agit  de  faire  connaître  aux
                      Européens,  organiser  un  service  précieux  de  renseignements
                                                          q
                                                                           l
                      individuels,  à  donner  à  des  particuliers ui s'adresseront  à ui.
                                     B U D G E T  D E  L ' I N S T I T U T

                        Dépenses.—  i . Loyer. .  Traitements et  pensions  des  fonction-
                                            2
                      naires.  3.  Frais  d'impression, papier,  ports,  frais  de  bureau.
                        Recettes.  —  1 . Cotisations  des  Gouvernements,  Municipalités,
                      Organisations,  Sociétés  et  particuliers  adhérant  à  l'Institut.
                      2 .  Droits d'abonnement  au  Périodique  mensuel.  3.  Droits  perçus
                     pour  les  annonces  insérées  au  Périodique.
                                                         l
                        A  part  les  ressources  ordinaires  de 'Institut,  il  est  indispen-
                     sable  qu'il  dispose  d'un  capital  de  premier  établissement.
                      L'assemblée  générale  qui  décidera  la  création  de 'Institut  pro-
                                                                    l
                      posé  devra  le  doter  de  ce  capital.



                                      SIÈGE   D E  L ' I N S T I T U T
                        De  préférence  à  Bruxelles,  en  connexion  étroite  avec  les
                      Institutions  qu'y  possède  déjà 'Union  des  Associations  Interna-
                                                  l
                      tionales.  Motifs  :  la  situation géographique  de  la  Belgique,  son
                      caractère  de  pays  neutre,  le  fait  que  Bruxelles  possède  déjà  le
                      noyau  central  des  organisations  mondiales,  auquel  i l  paraît
                      opportun  de  se  rattacher.

