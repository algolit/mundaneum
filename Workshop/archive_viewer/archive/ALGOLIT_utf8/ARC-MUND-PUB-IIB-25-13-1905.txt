










                         Institut Iriter-rratio nal de  Biblroq  ra  phte
                                                   1<,'DICE BIBLlOGRAPHIQUE [oa5"."J·



                    CLASSIFICATIUN               BIBLIOGRAPHlijUE



                                       DEOIMALE

                                                ..
                                                ?.-
                                             -
                          TABLES GENERALES REFONDUES

                                    etablies en vue de Ja p,ublication du
                                       BIBLIOGRAPH IQU     E  UN IVERSEL












                                lIftlNUEL DU REPERTOIRE BIBLIOGRAPHIQUE
                                                   UNIVEHSEL
                                    [Pt;HUCATION  :.." 63  DE L'IXSTll'u't I:,\TEnNATlO:-;AI?  DE HIBLIOGRAFIIIE]

                              Cette publication resume et condense  en  un seul volume mis a jour
                            les diverscs publications fragmentaires que l'Institut  International de
                            Bibliographie  a fait paraitre jusqu'a  ce jour  sur  son organisation,  ses
                            methodes et ses travaux. Elle contient les matieres detaillees ci-apres  .
                             .  L'ouvrage comporte  un volume de plus de 2,000 pages,  relie  en pleine
                            percaline, avec disposition speciale cl'onglets facilitant sa consultation. Le
                            . prix de souscription est fixe a 30 francs par exemplaire, port  en plus. Ises
                                                          ill extenso 1es tables publiees separc­
                            tables de classification reproduisent.
                            ment dans Ia publication  IiO 25.
                            ORGANISATION, TRAVAUX,     METHODES
                              Organisation.
                                   I. Le Repertoire bibliographique universel de l'Institut Interna­
                                      tional de Bibliographie.
                                  II. Statistique g enerale des services et des travaux.
                                  III. Actes  offici?ls,  statuts, etc.
                              Catalogue des travaux.
                                  IV. Catalogue du Repertoire bibliographique universel. (Prototype
                                      clu manuscrit.)
                                  V. Catalogue de 1a «  Bibliographia Universalis ». (Contributions
                                      imprtrnees.)    .
                                  VI. Catalogue des publications diverses .
                              .Reg les  et methodes.
                                 VII. Etablissement des repertoires  sur fiches  :" Repertoires  des
                                      matieres, repertoires des auteurs, autres repertoires.
                                VIII. Redaction des notices bibliographiques.
                                  IX. Publication des  notices biblfographiques  en recueils et  sur
                                      "fiches.
                                  X. Modes divers de cooperation au Repertoire bibliographique
                                 •
                                                                  .
                                       universel.
                                  XI. Organisation des diverses collections de documents.
                                 XII. Materiel et  accessbires.  a l'usage des repertoires.
                            TABLES DE CLASSIFICATION BIBLIOGRAPHIQUE
                                     Expose et reg les de la Classification decimale  ,
                                     Table principale.
                                     'I'ables auxili alres ,
                                     Index alphabetique general.

                                                lvlANUEL ABREGE
                               ntJ REPERTOlHE BIBLIOGRAPHIQUE UNIVERSEL
                                    lPUBl.ICATJON N° 65  DE r.Txs rrrtrr I:-;TRRNATIONAL  DE BrnLIOGRAPHIE]
                              Ce Manuel abrege comprend  1es memes matieres que .le Manuel com­
                            plet (publication nO 63) decrit ci-dessus, sauf pour les Tables de Classifioaiion
                            bibliograpltiqlte, qui sont Iirnitees  aux divisions principales (environ mille
                            rubriques). En souscription, 3 francs.       .
                              Les souscripteuts au Manuel abrege pourront eventuellement completer
                            cet ouvrage par I'acquisition des fascicu1es des Tables de classification
                            relatifs  aux branches qui les interessent, Pour la vente de cesfascicules,
                            voir ci-apres Classification bibliographiqlte decim.ale (publication no 25).













                                 GYNECOLOGIE.    -  PEDIATRIE         618. I  (0)



                   618    Gynecologie, Obstetriq ue,                    Pe-

                             diatrie.

                                            DIVISIONS GENERALES
                                618.1  GYN ECOLOGIE. Maladies des organes genitaux de la femme.
                                618.2  Grossesse normale.
                                618.3  Pathologie de la groseeeee.
                                618.4  Travail.
                                618.5  Pathologie du travail.
                                618.6  Suites de couches.
                                618.7  Accidents des suites de couches. Hygiene du nouveau-ne  ,
                                6IS.8  Operations obetetr-ical es  .
                                618.9  PEDJATR1E.  Maladies des enfants.



                     618.1    Gynecologie    et  Obstetrique.

                                 En general, la plupart des affections, ne presentan t aucuue particularite qui
                                les distingue chez  l'homme et chez  la femme,  sent classees  a 6I6  et 617'
                                Lorsque ces particularites meritent d'etre signalees, on dispose des subdivisions
                                communes:
                                          05  Formes specialee ,
                                          053  Affections suivant les sexes. Femmes.
                                 Les affections medicales et chirurgicales propres aux crgnnes genitaux de la
                                femme et a leurs fonctions et plus specialement I'obstetr iquc son.t classeesa 6r8. r  .
                                 . Le classement par subdivisions  communes est fait ici de la meme maniere
                                que pour la pathologie interne[6r61 a savoir :
                                 IO Ciassament par O1-galles at par systemcs,  -  Les nombres qui correspon-
                                dent aces subdivisions sont precedes de deux zeros. Ex.  :
                                          005  Hemorragies  ,
                                          6r8.r5  Maladies de l'uterus  .
                                          618. r5.005  Hemcrragies uterines.
                                 2" Ciassement par maladies.  -  Les Hombres qui correspondent a ces subdi-
                                visions sont precedes d'un zero. Ex.  :
                                          03 Semiologie  .
                                          618.15  Maladies de l'uterus  .
                                          6r8.r5.03  Semiologie des maladies de l'uterus  .
                                          6J8.:(5.005  Hemorrag'ies uterines  .
                                          618.15.005.03  Semiologfe des hemorragies uterines  .
                                 La maniere de combiner les subdivisions  communes  avec  les subdivisions
                                principales  a ete exposee en detail sous 6r6. 0
                     6I8.I  (0)  Generalites. Ouvrages generaux.
                                 La gynecologie,  et to utes  se s divisions, peuvent etre combinees  avec les
                                subdivisions de gcneralites, de forme, de lieu  et de temps.  Voir les tables
                                generales des subdivisions communes, dout les principales ant etc reproduites
                                sous 6,6 (0). Ex.  :
                                          618.1 (02)  Traites de g'ynecolog'ie.












                             618. I I     GYNECOLOGIE.     -  PEDIATRIE


                               6I8.II    Ovaires.
                                  . 12   Trompes  de Fallope  .
                                  .13    Organes peri-uterins.
                                  ,.137.1    Ligaments peri-uterins,
                                  . 137,II     Ligaments larges  .
                                  . 137.12              ronds  .
                                  . 137.13              sacres  .
                                  . 137.14              tubo-ovariens  .
                                  . 137.2    Tissu cellulaire et peritoiue  .
                                  . 137.3    Muscles  .
                                  .137,4-    Vaisseaux.
                                  . 137·5    Nerfs .
                                  . 137.8    Cul-de-sac de Douglas  .
                                  . 14   Uterus et col uterin .
                                  . 147·7    Col de l'uterus
                                                         .
                                  . 147.8    Corps de l'uterus  .
                                  ,147·9     Uterus  gravicle.
                                  .15    Vagin.
                                  . 16   Vulve .
                                  .17    Troubles fon,ctionnels.  Maladies de la 'menstrua­
                                           tion.
                                  .171     Troubles de la periode d'etablissement de la mens-
                                             truation.                   -:
                                  172                                                 .
                                  .        Troubles de la peri ode .d'activite de la menstruation
                                  . 173    Troubles de la menopause
                                                                 .
                                  .  174   Menorr hagie  .
                                  . 175    Dysmenorrhee  .
                                  .176     Amenorrhee,
                                  . 177    Sterilite  .
                                  .178     Leucorrhee.
                                   179                  .
                                  .        Autres troubles
                                  . 18   Affections du perinee chez la femme .
                                  . 19   Glande mammaire .
                              618.2    Obstetrique.    Grossesse.
                                         Classer a 618.2 (02} les Traites d'accoucbement et d'obstetrique et tout ce qui  a
                                          trait a I 'accouchement en general.
                                  .21
                                         Physiologie de la grossesse  .
                                  . 22   Signes de la  grossesse  .
                                  . 23   Duree de la  grossesse  .
                                  .  24  Hygiene de la grossesse.












                                 GYNECOLOGIE.     -  PEDIATRIE           618.52


                      618.25   J Grossesses multiples. Jumeaux.
                         .28    Grossesse chez des femmes ayant subi des ope­
                                  rations  gynecologiques.

                     6r8.3     Pathologie   de la  grossesse.
                         . 31   Grossesse extra-uterine  .
                                   On traitera la.  grossesse extra-uterine  et chacune de  ses subdivisions
                                    comme une maladie. On aura, par consequent, pour le traitement chirur­
                                    gical de la grossesse abdominale, 618.318.0895 et on mettra a 6r8.3r.oI
                                    l'anatomie pathologique de la  extra-uterine.
                                                      gross?sse
                         . 3II    Grossesse ovarienne  .
                         . 312             tubaire .
                         . 313             peri-uterine  .
                         .  314            anormale  .
                         . 315             vaginale  .
                         . 316             intestinale  .
                         . 318             abdominale  .
                         .32     Pathologie de l'ceuf.  -
                         . 33              du fcetus. Mort et retention  .
                         . 34              des annexes du fcetus .
                         _.35              de la  caduque.
                         . 36              du placenta  .
                         . 37,  -          de l'amnios .
                         .38               du cordon ombilical.
                         .39     Avortements.  Fausse-couches.  Accouchements
                                  prernatures.
                               Accouchement. Travail       -(Physiologie).

                                 Voir aussi I73. 4 Infanticide [Morale), 3+0.6r5 A.vortement (.Medecine legale)
                                  34:>.62I Avortement (Droit penal).
                                 Mecanisme du travail.
                                 Presentations. Positions.
                                 Evolution clinique du travail.
                                 Direction du travail normal.
                      6r8.s    Pathologie   du travail.

                          .51    Anomalies du travail  dependant de l'impuissance
                                   des forces  expulaives  .
                          . 52   Obstacles mecaniques,












                             6r8.53        GYNECOLOGIE.     -  PEDIATRIE


                                6r8.53    Anomalies du fcetus  .
                                  . 54    Hernorragies de l'accouchement.
                                   . 55   Rupture et dilaceration des voies  genitales  .
                                   .56    Retention du placenta  .
                                  . 57    Inversion de l'uterus.
                                  . 58    Prolapsus du cordon  .
                                  . 59    Autres  complications pathologiques  .

                               618.6    Etat  puerperal (Physiologre, Hygiene).
                                          Soins a donner, y compris ceux interessant l'enfant.

                               618·7    Pathologie   de l'etat  puerperal.
                                  ·7r     Maladies de la lactation. F'ievre de lait.
                                   ·72    F'ievre  puerperale.
                                  . 73    Metrite. Peritonite  .
                                   ·74    Septicemic.
                                   .75    Eclampsie puerperale,
                                   ·76    Manie  puerperale.
                                  ·77     Phlebite. T'hrombite. Phlegmatia.
                                  ·78     Autres affections puerperales.
                                   ·79    Mort subite  apres la delivrance.
                               618.8    Operations obstetricales,

                                  . 8r    Levier.  Forceps  .
                                   . 82   Version  .
                                  . 83    Embryotomie  .
                                  .84     Dilatation du col.
                                  .85    Symphyseotomie.
                                  . 86    Operation cesarienne  .
                                   . 87   Ablation du  placenta  .
                                   . 88   Provocation de l'accouchement .
                                  . 89    Autres operations obstetricales
                                                                      .
                                  .  89r   Antisepsie  .
                                   .  892  Anesthesie obstetricale .
                                   .  893   Reduction de l'uterus  gravide  .
                                   .895     Incision du col.
                                   .  897   Operation de Porro  .












                                GYNECOLOGIE.        PEDIATRIE            6r8·9


                    6r8·9     Pediatric. Maladies des enfants.

                                 On classe a 618.9 les ouvrages generaux relatifs  :lUX  maladies des  en­
                                fants. Ex.;
                                       618,9 (02)  Trait" de Pediatr-ie.
                                 La plupart des affections medicates ne presentent aucune particula.rite qui les
                               disti ngue chez l'enfant et  on les classe  a la Pathologie interne et externe, 6I6
                                et 6r7. Lorsque  ces particularites meritent d'etre signalees,  on dispose de
                                a subdivision commune:
                                       053  ?frection  selon les ages. Enfants.
                                 Toutefois, on pourra reunir sous 618.9  tcutes les questions de physiologie,
                                      de therapeutique, de pathologie concernant l'enfance, en combinant
                               d'bygiene, ...
                               par: Ia division 618,9 Pediatrie  avec  to utes les autres divisions de la raede­
                               cine 6Il a 617. Ex.  ;
                                       618.9: 616.312.0024 Stomatite gnngreneuse chez I'enfant.
































































































                                    l\r[EDECINE COMPAREE                619.19




                   619   Medecine           comparee,         Art vete-
                              .     .
                            rlnalre .
                                  .
                                On ne classe ici que la medecine comparee en general et tout ce qui concerne
                               l'anatomie, la physiologie, la pathologie et la therapeutique des animaux.utiles
                               ou domestiques  ,  Ces memes questions etudiees  au point  de  vu?  des autres
                               anirnaux sonr'classees avecla Zoologie [59].
                                Voir aussi les divisions suivantes  :
                                      ,636  Zootechnie, elevage.
                                       682,r  "Marechalerie.
                                       6r4·9  Police sanitaire des animaux.
                                       6r4·3r7  Inspection des viandes.  ??
                    619 (0)  General ites.  Ouvrages generaux.

                                L'art veterinaire- et toutes ses divisions peuvent  etre corubinees  avec  les
                               subdivisions de generalites, de forme, de lieu et de temps.
                                Voir les subdivisions communes, dont Ies principa.les  ont ete reproduites
                               sous 6r6 (0). Ex. :
                                       619 (05)  Revues de medecine veterinaire  ,
                    619      Medecine    comparee generale.

                                Ces questions sontclassees ici it 6r9: suivi du nombre propre a chacuue de
                               ces questions, conformement  aux divisions de la medecine generate  6II  a.
                                                                       .
                               6r8. Ex.  :
                                       619; 616.5  Les maladies du sang en rrredecine comparee.
                   619.1 a.9  Art veterinaire.

                                Les questions d'art veterinaire propres a cbaque espece d'animaux domes­
                               tiques sont classees de 619,1 a. 6 [9.9. Ex. :
                                      619.4  Medecine veterinaire du pore.
                                Ces divisions concordent avec celles de, 636 Zootechnie. Elles peuvent etre
                               subdivisees a leur tour camme lamedecine comparee genera.le. Ex.  :
                                      6I9.4 : 6r6.5  Les maladies du sang chez le pore.
                                Les maladies propres aux animaux et qui ne sont pas inscrites a. la patho­
                               logie interne ni externe, a 6r6.et it 617, sont classees a 6r6. 999 Autres maladies
                               generales. Ainsi on classera a
                                      6I9.5 : 6r6.999  Cholera des poules (qui u'a aucun rapport  avec Ie
                                                 cholera humain).
                                      6I9.4: 6[6·999  Rouget du pore.
                                              I
                   619.1'    Les  equides domestiques.

                       .II    Cheval.
                       • I2   Ane .
                       . I3   Mulet .
                       . I9   AiItres equides domestiques  .












                              6I9.2            MEDECINE COMPAREE


                               6Ig.2     Les  grands   ruminants. Betail.
                                   .21    Bceuf. Vache.

                               6Ig.3     Mouton. Chevre .
                                   . 31   Mouton
                                   .32    Chevre.

                               6Ig.4     Pores.
                               6Ig.5     Oiseaux de basse-cour    .
                                   . 51   Coq. Poule  .
                                   . 52   Dindon  .
                                   . 53   Pintade .
                                   . 54   Faisan  .
                                   . 55   Paon
                                   .56    Pigeon domestique.
                                             Voir 6 I 9.62 Pigeons voyageurs
                                                               .
                                   . 57   Canard  .
                                   . 58   Oie  .
                                   . 59
                                  .       Autres oiseaux de basse-cour.
                               6I9·6    Oiseaux   exploites pour   leurs  plumes, leur
                                          travail. Oiseaux    d'agrernent  .
                                   . 61   Oiseaux exploites pour leurs plumes. Autruches
                                                                                       .
                                   . 62   Pigeons voyageurs  .
                                   . 68   Oiseaux  d'agrernent et d'ornementation  .
                                   . 68r    Cygnes  .
                                   . 682    Oiseaux de  cage et de voliere  .
                                   . 683    Perroquets.

                               6?9.7    Chien.

                               6Ig.8    Chat.
                               6Ig.g    Autres   animaux     domestiques     ou  utiles
                                                                         '.
                                           (cobayes, lapins, etc.).













                                                                                                    I









                         CLASSIFICA TION BIBLIOGRAP RIQUE DEClivlALE
                              [PunLlCATIO:-l ?? 25  DE L'I:-:sXITUT INTERNATJO:-JAL  nn HmUOGRAPHIB]

                        NOTICE.  -  La Classification bibliographique decimale est universelle,
                      internationale, encyclopedique, it -la fois particuliere et generale ; eUe est
                      documentaire, elle s'exprime en une notation concise, eIle est fort etendue
                      (environ 25,000 divisions) et indefiniment extensible. Les bases en ont ete
                      adoptees par le Congres international de Bibliographie en r895 et en r897
                      a Bruxelles et  en 1900 a Paris, et elle  a deja recu une large application,
                      tant en Europe que dans le Nouveau Monde.
                        Cette classification consiste en une vaste table systematique des matieres,
                      dans Iaquelle tous les sujets de connaissances sont repartis par classes,
                      sons-classes ct divisions, en passant du general au particulier, du tout a la
                      partie,  du genre a I'espece.
                        Chacune des rubriques de cette table est representee par  un nombre
                      classificateur compose d'un ou de plusieurs chiffres, suivant Ie degre  de
                      g  eneralite. Ces noinbres sont decimaux, en ce sens que chaque  chiffre vers
                      la droite du nornbre ne modifie pas la valeur ordinale des chiffres prece­
                      dents. mais correspond a une subdivision de Ill. matiere representee par les
                      chiffres precedents. L'ordre dans Iequel les nornbres se suivent est aussi
                                                                         .
                      I'ordre decimal.
                        La table systematique est completes par  un index al phabetique des
                      matieres,  dans lequel toutes los rubriques de lao premiere table  sont
                      rangees  en  un seul ordre alphabetique et sont sui vies du nombre classi-
                                                                          .
                      ficateur correspondant. Exemples  :
                           Philosophie.            Economie financiere      332
                      ?  2  Religion.            g Economie politique       33
                      .s'3  Sciences sociales.   ,g Monnaie  ,              332.4
                      -g 3r  Statistique.'       :g Philosophie             I
                      ? 32   Politique.          i Politique, question du travail  33r
                      e
                        33   Economie financiere.  .. Religion              2
                      :E 331   Questions du travail.  :l Sciences sociales  3
                      ? 3.t?   Econornie politique. ] Statistiquc           31
                     .
                        332·4    Monnaie.          Travail                  331
                        L'indexation  suivant  ces tables,  ou inscription  sur  les documents a
                      classer du numero classificateur correspondant  a la matiere principale  dont
                      ils traitent, permet doric de former des collections de documents rangees
                      dans un ordre methodique parfait et susceptibles d'accroissements et d'in­
                      tercalations continuels.
                        D'une maniere generale, comme classification des matieres uniforme et
                      internationale, la classification hibliographique universelle est susceptible
                      d'etre appliquee  au classement des diverses espcces de documents et des
                      materiaux dont les travailleurs intellectuels ont a se servir, et elle fournit,
                      a, cet effet, des cadres tout prets,  traces d'avance : classement des reper­
                      toires bibliographiques et des catalog-ues; classement des ouvrages  eux­
                      memes dans les bibllotheques ; classemen t des notes, observations, extraits
                      et documents divers destines a. des etudes et a des travaux personnels;
                      classement des tables de matiere des recueils periodiques ; classement de
                      documents graphiques, illustrations  et photographies,  de cliches;  de
                      brevets, de specimens, de catalogues industriels, de circulaires  commer­
                      ciales et toutes autres  applications  a. la documentation, prise dans Ie sens
                      le plus large.
                        Le jour au la classification documentaire universelle se sera repandue,
                      on son application aura ete generalisee, au .lieu d'avoir it  se familiariser
                      avecvingt cles differentes, variant d'apres  les institutions qui conservent
                      et qui classent les documents, le public des chercheurs pourra, it I'aide
                      d'une seule cle, c'est-a-dire d'unememe table de classification des matieres,
                      se faire ouvrir les tresors de tous les depots de documents. Une economic
                      considerable de temps pourra etre realisee ainsi, et le chercheur benefi­
                      ciera des avantages de la connexion etroite etablie entre toutes les sources
                      documentaires de  nos connaissances. La classification bibliographique
                      universelle permettra enfin de creer I'entente et la cooperation dans les
                      travaux. Au point de vue des collaborations internationales, elle pourra
                      jouer  un role similaire a celui qu'on  attend de la langue internationale,
                     ,
                      qui ne cherche pas a contrarier les langues particulieres ni a s'y substituer,
                      mais bien a servir d'auxiliaire et de complement pour les relations exte­
                      rieures.
      I








                           FASClCULES.  -  Cette  edition de la classificatiori bibliographique
                         deci nale reunit les diverses  editions fragmentaires parues jusqu'a  ce
                         jour. Elle a ete publiee par fascicules. La souscription a cette publication
                          est close, mais les personnes desireuses de recevoir les fascicules relatifs
                         a  une branche determinee peuvent  souscrire aces fascicules  aux prix
                                                                          '
                         indiques au tableau ci-apres,
                           Dorenavant, pour recevoir en entier .les  Tables diveloppees, il y aura lieu
                         de souscrire au .[IIIanuel du Ripertoi1'e biblio{;1'aphiq1te uuiuersel (voir'publication
                                                                         "
                         nv 63), dans lequel elles ont ete iricorporees.
                    "  ?
                   ?  ?                          TI:ERE
                    ? ?B  1=============================================================
                   ""  "".?  INDICE                                                    EN
                   Z  ?   mBLIOGRAPHIQUE  I          UENOMI:-< ATIO:-<                FRANC?
                                       Expose                               -.
                               »              et regIes de la Classification decimale
                        [(.),=,« », :,A-Z]  Table des subdivisions communes.  ....    2  »
                              [53]  .  Sciences physiques (Mecanique rationnelle et Phy-
                                         sique)  .                                    I  »
                     4        [77]     'Sciences photographiques.                     I  »
                     6       [629.,1]  Industries  de  la Locomotion (Locomotion  par
                                         terre et par eau, Aerostationl,          8   I  )}
                                                                                 10
                  I  §        [791     Sports (Tourlsme, Cyclisme, Automobilisme)  132  I 2  )} )}
                              [34]
                                       Droit.
                             (615]     Therapeutique  .                          26   I  )}
                    I?
                  I  II      [616]     Pathologie interne                        28   I I  )} )}
                             [6I7J
                                       Pathologie externe
                                                                                 14
                   '12       [6I2]     Physiologic.             .  ..    .       38   I  )}
                    13     [618+6'19]  Gynecologie. Pediatric. Medecine comparee  12  I  »
                    14        [35].    Administration.          ...      .       44  .  I  »
                                  .
                    15        [9]      . Histoire , Geographic. Biographie. Genealogie  .  100  2  »
                    16         »       Resume des tables. Tables generales methodiques
                                         abregees, Index alphabetique general abreg e  ..  44  I  »
                    17        [0]      Generalites. Bibliographie. Bibliotheconornie. So-
                                         cietes savantes, etc.  .             .  52   2  »
                    18        [3]      Sciences sociales. Statistique.
                                         ?nseign?ment. ?ssistance. E_conomie pOlitique'l  68  2  )}
                                                               Folklore
                                                                         .
                                                                       .
                                                                            .
                                                                              -.
                    I9        [I]     '/ Philosophie. QuestIOns morales  .  .  .  .  .  .  .  28  I  )}
                    20        [63]     Agriculture. Agronomie. Sciences agricoles  46  I  )}
                                                                  ..
                    21        [2]      Sciences religieuses  _  -                68   2  )}
                           .
                    22       [4+8]     Philologie et Litterature                 45  "2  »
                    23     [355+623J   Sciences militaires                       33   2  )}
                    24      [5I+52J    Mathematiques et Astronomic               18   I  »
                    25   [548+549+ 55]  Mineralogie, Cristallographie. Geologie  ,22  I  )}
                    26  [56+57+58+59]  Sciences biologiques. Paleontolog ie.  Anthropo-
                                         logie. Botanique. Zoologie  .           74
                    27'     J6rr]      Anatomie             '  •.                31
                    28     [613+614]   Hygiene privee et Hygiene publique        22
                    29        [7J      Beaux-Arts: Architecture. Sculpture.  Peinture.
                                         Photographie. Gravure. Musique  ...     96
                    30      [64+65]    Sciences appliquces diverses  : Economie domes­
                                         tique. Stenographic. Imprimerie et edition. Tran­
                                         sports. Comptabilite  .  .  .  .  .  .  .  .  .  .  I  )}
                    31       [62]      Sciences de l'ingenieur  :  Mecanique, Electricite
                                         industrielle. Mines. Ponts et chaussees,  Che­
                                         mins de fer et  tramways. Travaux maritimes et
                                         hydrauliques. Technologie sanitaire. Locorno­
                                         tion en general.  .  .  .  .  .  .  .  .  .  .  .  II3  3  )}
                            [54+66]    Sciences chimiques  :  Chimie  pure.  Industries
                                         chimiques. Metallurgie.
                                                                       .
                                                                         .
                                                                           .
                                                              .
                                                               _.
                                                                  .
                                                                     .
                         [67+68+69]   l  Industries diverses. Professions et metiers divers'. .  223  4  »
                              »          Construction.  .  .  .  .  .  .  .     rr8   3  II
                                                         .        .    .   .  .
                              »        Index alphabetique general  .  .  .  .  .  .  .  .  350  3  )}
                                       Organisation, travaux, methodes de l'Institut Inter-
                                        national de Bibliographie      -              2  »
                                                   NOMBRE  TOTAL  DES  PAGES    2259

