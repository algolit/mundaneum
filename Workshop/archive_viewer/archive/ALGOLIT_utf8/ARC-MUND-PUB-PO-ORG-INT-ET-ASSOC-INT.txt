AVERTISSEMENT

Cette Å“uvre littÃ©_rajre appartient au domaine public. _
Elle a Ã©tÃ© numensee par le Mundaneum, centre dâ€™archives

RÃ¨gles dâ€™utilisation des copies numÃ©riques dâ€˜Å“uvres littÃ©raires, rÃ©alisÃ©es par le Mundaneum _ _ _ _ _ _ _
Lâ€™usage des copies numÃ©riques rÃ©alisÃ©es par le Mundaneum, dâ€™Å“uvres littÃ©raires qu_â€™elles dÃ©tiennent,_m-apres dÃ©nommÃ©es_Â« d_ocuments numenses >>, implique un
certain nombre de rÃ¨gles de bonne conduite, prÃ©cisÃ©es dans le prÃ©sent texte. Il sâ€™articule selon les trms axes protection, utilisation et reproduction.

Protection

1. Droits dâ€™auteur

La premiÃ¨re page de chaque document numÃ©risÃ© indique les droits dâ€™auteur dâ€™application sur lâ€™Å“uvre littÃ©raire. Les Å“uvres littÃ©raires numÃ©risÃ©es par le
Mundaneum appartiennent majoritairement au domaine public.

Pour les Å“uvres soumises aux droits dâ€™auteur, le Mundaneum aura pris le soin de conclure un accord avec leurs ayant droits afin de permettre leurs
numÃ©risation et mise Ã  disposition. Les conditions particuliÃ¨res dâ€™utilisation, de reproduction et de communication de la copie numÃ©rique sont prÃ©cisÃ©es
ci-dessous.

Dans tous les cas, la reproduction de documents frappÃ©s dâ€™interdiction par la lÃ©gislation est exclue.

2. ResponsabilitÃ©

MalgrÃ© les efforts consentis pour garantir la meilleure qualitÃ© et accessibilitÃ© des documents numÃ©risÃ©s, certaines dÃ©fectuositÃ©s peuvent y subsister â€”

telles, mais non limitÃ©es Ã , des incomplÃ©tudes, des erreurs dans les fichiers, un dÃ©faut empÃªchant lâ€™accÃ¨s au document, etc. -.

Le Mundaneum dÃ©cline toute responsabilitÃ© concernant les dommages, coÃ»ts et dÃ©penses, y compris des honoraires lÃ©gaux, entraÃ®nÃ©s par lâ€™accÃ¨s et/ou
lâ€™utilisation des documents numÃ©risÃ©s. De plus, le Mundaneum ne pourra Ãªtre mises en cause dans lâ€™exploitation subsÃ©quente des documents numÃ©risÃ©s ; et la
dÃ©nomination â€˜Mundaneumâ€™, ne pourra Ãªtre ni utilisÃ©e, ni ternie, au prÃ©texte dâ€™utiliser des documents numÃ©risÃ©s mis Ã  disposition par lui.

3. Localisation
Chaque document numÃ©risÃ© dispose dâ€˜un URL (uniform resource lÃ§cator) qui permet dâ€˜accÃ©der au document. Le Mundaneum encourage les utilisateurs Ã  utiliser
cet URL lorsquâ€™ils souhaitent faire rÃ©fÃ©rence Ã  un document numÃ©risÃ©.

Utilisation

4. GratuitÃ©

Le Mundaneum met gratuitement Ã  la disposition du public les copies numÃ©riques dâ€™Å“uvres littÃ©raires appartenant au domaine public : aucune rÃ©munÃ©ration ne
peut Ãªtre rÃ©clamÃ©e par des tiers ni pour leur consultation, ni au prÃ©texte du droit dâ€™auteur.

Pour les Å“uvres protÃ©gÃ©es par le droit dâ€™auteur, lâ€™usager se rÃ©fÃ©rera aux conditions particuliÃ¨res dâ€™utilisation prÃ©cisÃ©es ci-dessous.

5. Buts poursuivis

Les documents numÃ©risÃ©s peuvent Ãªtre utilisÃ©s Ã  des fins de recherche, dâ€™enseignement ou Ã  usage privÃ©.

Quiconque souhaitant utiliser les documents numÃ©risÃ©s Ã  dâ€™autres fins et/ou les distribuer contre rÃ©munÃ©ration est tenu dâ€™en demander lâ€™autorisation au
Mundaneum, en joignant Ã  sa requÃªte, lâ€™auteur, le titre, et lâ€™Ã©diteur du (ou des) document(s) concernÃ©(s).

Demande Ã  adresser au Service Archives, Mundaneum,

Rue des Passages, 15 Ã  7000 Mons. Courriel : archives@mundaneum.be

6. Citation
Pour toutes les utilisations autorisÃ©es, lâ€™usager sâ€˜engage Ã _ citer dans son travail, les do_cuments utilisÃ©s, par !a mention Â« Mundaneum - Mons Â»
accompagnÃ©e des prÃ©mswns indispensables Ã  lâ€™identification des documents (auteur, titre, date et heu dâ€™edmon, cote).

7. Exemplaire de publication

Par ailleurs, quiconque publie un travail â€” dans les limites des utilisations autorisÃ©es - basÃ© sur une partie substantielle dâ€™un ou plusieurs document(s)
numÃ©risÃ©(s), sâ€™engage Ã  remettre ou Ã  envoyer gratuitement au Mundaneum un exemplaire (ou, Ã  dÃ©faut, un extrait) justificatif de cette publication.
Exemplaire Ã  adresser au Service Archives, Mundaneum,

Rue des Passages, 15 Ã  7000 Mons. Courriel : archives@mundaneum.be

8. Liens profonds

Les liens profonds, donnant directement accÃ¨s Ã  un document numÃ©risÃ© particulier, sont autorisÃ©s si les conditions suivantes sont respectÃ©es :

a) les sites pointant vers ces documents doivent clairement informer leurs utilisateurs quâ€™ils y ont accÃ¨s via le site web du Mundaneum ;

b) lâ€™utilisateur, cliquant un de ces liens profonds, devra voir le document sâ€™ouvrir dans une nouvelle fenÃªtre ; cette action pourra Ãªtre accompagnÃ©e de
lâ€™avertissement â€˜Vous accÃ©dez Ã  un document du site web du Mundaneumâ€™.

Reproduction

9. Sous format Ã©lectronique

Pour toutes les utilisations autorisÃ©es mentionnÃ©es dans le prÃ©sent texte le tÃ©lÃ©chargement, la copie et le stockage des documents numÃ©risÃ©s sont permis ;
Ã  lâ€™exception du dÃ©pÃ´t dans une autre base de donnÃ©es, qui est interdit.

10. Sur support papier

Pour toutes les utilisations autorisÃ©es mentionnÃ©es dans le prÃ©sent texte les fac-similÃ©s exacts, les impressions et les photocopies, ainsi que le

copiÃ©/collÃ© (lorsque le document est au format texte) sont permis.

11. RÃ©fÃ©rences

Quel que soit le support de reproduction, la suppression des rÃ©fÃ©rences au Mundaneum dans les documents numÃ©risÃ©s est interdite.

- -â€” â€” â€” â€”â€”-â€”â€”â€”-â€”- â€”-â€”-â€”-==â€”-.:Ã¦-â€”

CÃœNÃœRES

l'E:â€”

ASSÃœEUH10NS Ã