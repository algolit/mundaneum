











                            Institut International de
                                                           Bibliographie
                        J>UBLICA TION No 25         INDICE BIBLIOGRAPHIQUE [025-4]·




                         CLASSIFICATION             BIBLIOGRAPffiijUE



                                          DROIMALE

                                                _.+._

                             TABLES GENERALES REFONDUES

                                       etablies  en  vue de la publication du
                            Repertoire     Bibliog?aphique       Universel



                                      EDITION      FRANQAISE
                                           publiee avec Ie concours du
                                 BUREAU
                                          BIBLIOGRAPHIQUE DE PARIS


                                  B'ASCICULE                 N° 13
                                    Tables des  divisions [618] et  [6Ig]
                       Gynecologic.      Pediatrie. Medecine        comparee















                                 INSTITUT INTERNATIONAL DE BIBLIOGRAPHIE
                          BRUXELLES, I,  rue du Musee.     PARIS, 44.  rue de Rennes.
                                         ZURICH, 3g, Eidmattstrasse.

                                                 l.902












                                        TABLES GENERALES DE LA
                             CLASSIFICATLON BIBLIOGRAPfIIQUE           DECIMALE
                             -  PUBLICATION NO 25 DE L'INSTITUT INTERNATIONAL DE BIBLIOGRAPHIE
                            L'Edition francaise, developpee et refondue, des TABLES GENERALES de
                                                                       l'Institut Interna­
                           la Classification bibliograPhique decimale, a ete preparee par
                           tional de Bibliographie,  avec le concours d'un grand  nombre de collabora­
                           teurs et plus specialement  du Bureau bibliographique  de Paris. Cette edition
                           reunira les diverses editions fragmentaires publiees jusqu'a 'ce jour; elle
                           paraltra par fascicules successifs. Afin de ne pas entraver la bonne marche
                           de l'impression,  I'ordre suivi clans la publication  sera celui dans Iequel Ies
                           manuscrits prepares par  les collaborateurs parviendront  a l'Institut.
                             Quanclla collection sera complete, il sera facile de faire relier les fasci­
                           cules en un volume, dans l'ordre methodique des matieres et conformement
                           au tableau cl'arrangement qui  sera public ulterieurernent .Pour faciliter
                                                                         .
                           cet arrangement, les tables n'ont pas recu  de pagination continue, mais les
                           introductions et les index alphabetiques ont ete imprimes  sur papier  teinte,
                             On peut  des a  present  souscrire a I'edition complete,  au prix de 20 fr.,
                           port compris.
                             L'ouvrage complet porte le nO 25 des publications  de l'Institut Interna­
                           .tional de Bibliographie;  en outre, les divers fascicules sont numerotes
                           separement,  Les souscripteurs recevront separement les divers 'fascicules
                           au moment de. leur publication.
                                           ONT PARU A CE      JOUR:
                           Fascicule 1tO  I  -  Int1'oduction  gellerale  au» Tables de la  Classification  deci­
                                            male et Resume des  Regles  de la  Classification  decimale,
                           Fascicule nO  2  -  Table des S21bdivisiolls communes.
                           Fascicule nO 3  -  Tables de la division  [53]  Sciences physiques (Micanique
                                            raiiounelle et  Physique).  .    .
                           Fascicule  nO  4  -  Table de la division  [77]  Sciences photographiques.
                           Fascicule ftO 5  -  Table de la division  [62I. 3]  Electricit« industrielle.
                           Fascicule nO 6  -  Table de la division  [629. I]  11ld-ustries de la Locomotion
                                           (Locomoieon par  terre ei par ease, Airostation}.
                           Fascicule n°  7  -  Table de la division  [79] Sports (Tourisme, Cyciiem«,
                                            A utomobilisme).
                           Fascicule 11° 8  -  Table de la divisi09Z [34] Droit.
                           .Fascicule nO  9  -  Table de la dlvisiolt [615] Thb'apeutique.
                           Fascicule 1.° IO  -  Tablede la division  [616] Pathologie  interne.
                           Fascicule nO II  -  Table de la division [617] Pathologic  externe.
                           Fascicule 11° I2  -  Table de la division [612] Physiologie.
                           Fascicule no I3  -  Table des divisiolls [618 et 619] Gynecologie. Pediatrie.
                                            Medec£m  comparee.

                                          BULLETIN ET ANNUAIRE
                           DE L'INSTITUT INTERNATIONAL DE BIB_LIOGRAPHIE

                             BULLETIN DE L'INSTITUT INTERNATIONAL DE BIBLIOGRAPHIE. [Comptes rendus
                           des travaux de l'Institut; etudes .documentees et informations relatives a
                           .la Bibliographie; analyses d'ouvrages bibliographiques; renseignements
                           speciaux  sur  tout  ce qui  concerne l'organisation internationale de la
                            Bibliographie scientifique et la formation du Repertoire bibliographique
                           universel.] Parait  en six fascicules iIi·So (0.25 X 0.16), br., par  an. Les
                            abonnernents sont faits pour  un an, a partir  du Ier janvier. Prix: Union
                            postale  : 15 fro (en cours  depuis 1S95). Dlstribue gratuiternent aux membres
                            de l'Institut. I1 a .ete publie egalement divers tires a part d'articles impor­
                            tants parus dans ce Bulletin et qui figurent dans la collection des publica­
                            tions de l'Institut.
                             ANNUAIRE DE L'INSTITUT INTERNATIONAL  DE BIBLIOGRAPHIE pour I'annee
                            1902.  -  Bruxelles,  au siege de I'Institut, grand in-So (0.25 X 0.16), br.
                            106 p.                                       Prix :  I franc





                                                                                    J





                              Institut International de     Bibliographie
                         PUBLICATION N' 35            lNDICE BIBLJOGRAPHIQUE [025.4]




                          CLASSIFICATION             BIBLIOGRAPHIijUE

                         _
                                            D:ECIMALE

                                                 -.?.-


                               TABLES GENERALES REFONDUES
                                        etablies en  vue de la publication du
                             Repertoire Bibliographique           Universel




                                       EDITION      FRANQAISE
                                             publlee avec Ie concours du
                                   BUREAU BIBLIOGRAPHIQUE DE      PARIS


                                    FASCICU LE                N°   13

                                      Tables des divisions [618]  et  [619]
                        Gynecologie.       Pediatrie, Medecine        comparee














                                   INSTITUT INTERNATIONAL DE BIBLIOGRAPHIE
                            BRUXELLES,  r  rue du Muscle.    PARIS, 44,  rue de Rennes.
                                           ZU RICH, 39, Btdrriattetrasse.
                                                   1.902'                               •




















                                                                                                  ,  .





















































                   •











                                                                                                   ,.












                                   GYNECOLOGIE.       PEDIATRIE         618. I  (0)



                      618   Gynecologie,           0  bstetriq ue,        Pe-

                               diatrie.

                                               DIVISIONS GENERALES:
                                  618.1  GYN ECOLOGIE.  Maladies des organes genttaux de la .femme.
                                  6I8.2  Grossesse norrnale.
                                  618.3  Pathologie de la grossesse.
                                  618-4  Travail.
                                  618.5  .  Pathologie du travail.
                                  618.6  Suites de couches.
                                  618.,  Accidents des suites de couches. Hygiene du nouveau-ne.:
                                  618.8  Operations obs teteicales  .
                                  618.9  PEDIATRIE .. Maladies des enfant s  ,



                       618.1    Gynecologic     et  Obstetrique.

                                   En general, la plupart des affections, ne presentan t aucune parficular-ite qui
                                  les distingue chez  l'homme  et chez  la femme,  sont classees  a 6r6  et 617.
                                  Lorsque ces pat-ticularites meriteub d'etre signalees, on dispose des subdivisions
                                  communes:
                                            05  Formes speciales.
                                            053  Affections suivant les sexes. Femmes.
                                   Les affections medicates et chirurgicales propres aux organes genitaux de la
                                  femme eta le?rsfonetions  et p?us specia.lement I'obstetrique sont classees a 618.1.
                                   Le classement par subdivisions  communes est fait ici. de la meme maniere
                                  que pour la pathologie intemejere], a savoir:
                                   1° Ciassament par organes et pqr systenies,  -  Les nombres qui correspon-
                                  dent a ces subdivisions sont precedes de deux zeros. Ex.  :
                                            005
                                               Hemorragies -.
                                            6I?Lx5  Maladies de I'uterus  .
                                            618.15.005  Hemorrag ies uterlnes.
                                   2° Ciassemant par maladt·es.  -  Les nombres qui correspondent a ces subdi-
                                  visions sont precedes d'un zero. Ex.  :
                                            03 Semiologie  .
                                            6r8.r5 Majadies de I'uterus  .
                                            618.15.03  Semiologie des  mala?ies  de l'uterus  .
                                            6J8.15.005  Hemorragies uterines  .
                                            6rS.r5.ob5.o3  Semiologie des hemorrugies uterines  .
                                      .
                                   La maniere de combiner les subdivisions  communes  avec  les subdivisions
                                  principales  a He exposee en detail sous 616.0
                        618.1  (0)  Generalites. Ouvtages gerieraux.
                                   La gynecologic,  et toutes  ses divisions, peuvent etre combinees  avec les
                                  subdivisions de generalises, de forme, de l ieu  et de temps.  Voir lea tables
                                  generales des subdivisions communes, dont les principales ont ete reproduites
                                  sous 6,6 (0). Ex. :
                                            6,8 . .' (Q2)  Tra.ites de gynccologie.












                            618. I I      GYNECOLOGIE.    -  PEDIATRIE

                               6I8.II    Ovaires.
                                 • 12    Trompes  de Fallope  .
                                 . 13    Organes peri-uterins  .
                                 . 137.1    Ligaments peri-uterine  .
                                 . 137·II     Ligaments larges  .
                                 . 137.12              ronds  .
                                 . 137.13              sacres  .
                                 . 137.14              tubo-ovariens  .
                                 . 137.2     Tissu cellulaire et peritoine  .
                                 . 137.3     Muscles  .
                                 . 137.4     Vaisseaux .
                                  . 137.5    Nerfs  .
                                                                      •
                                 . 137.8     Cul-de-sac de Douglas  .
                                 ·14     Uterus et col uterin.
                                 . 147·7    Col de l'uterus
                                                        .
                                 . 147.8'    Corps de l'uterus  .
                                 . 147·9    Uterus gravide  .
                                 .15     Vagin.
                                 . 16    Vulve  .
                                 .17     Troubles fonctionnels.  Maladies de la menstrua-
                                           tion.
                                 .171     Troubles de la pericde d'etablissement de la mens-
                                            truation.
                                 ·172     Troubles de la pericde d'activite de la menstruation.
                                 . 173    Troubles de la  menopause ..
                                 .  174   Menorrhagic ..
                                 . 175    Dysmenorrhee  .
                                 . 176    Amenorrhee .
                                 . 177    Sterilite  .
                                 . 178    Leucorrhee  .
                                 ·179     Autres troubles.
                                 .i8    Affections du  perinee chez la femme.
                                 . 19   Glande mammaire   .
                                       Obstetrique.   Grossesse.
                                         Classera 618.2 lO2'j les Traites d'accoacbement et d'obstetrique et tout ce qui a
                                          trait a l'accouchement en general.
                                 .21    Physiologie de la  grossesse  .
                                 :22    Signes de la  grossesse  .
                                 .
                                 . 23   Duree de la
                                                    grossesse  .
                                  24
                                 .       Hygiene de la grossesse.











                                    GYNECOLOGIE.    -  PEDIATRIE           6I8.52


                        618.25     Grossesses  multiples. Jumeaux.
                           .28     Grossesse chez des femmes ayant subi des  ope­
                                    rations  gynecologiques.
        ..
                        618.3    Pathologie   de la  grosse  sse.
                           .31     Grossesse extra-uterine.
                                     On traitera  la  grossesse extra-uterine  et cbacune de  ses subdivisions
                                       comme une maladie. On aura, par consequent.. pour Ie traitement chirur­
                                       gical de la grossesse abdominale, 6r8.3r8.0895 et on mettra ? 618.31.01
                                       I'anatomie pathologique de la grossesse extra-uterine.
                           _.3II     Grossesse ovarienne  .
                            . 3I2            tubaire .
                            . 313            peri-uterine  .
                           .  3I4            anormale .
                           . 3 IS            vaginale  .
                            . 316            intestinale  .
                            . 3I8            abdominale  .
                            . 32   Pathologie de I'ceuf .
                            . 33             du fcetus. Mort et retention .
                            . 34              des annexes du fcetus .
                            . 35              de la  caduque  .
                            . 36             du  placenta  .
                            . 37              de l'amnios .
                            . 38              du cordon ombilical.
                            .39    Avortements.  Fausse-couches.  Accouchements
                                     prernatures.
                                                        I
                                 Accouchement. Travail        [Physiologic).
                                   Voir aussi I73.4 Infanticide (Morale), 3?o.6I5 Avortement [Medecine legale)
                                                                        .
                                    343.62I Avortement (Droit penal).
                                   Mecanisme du travail.
                                   Presentations. Positions.
                                   Evolution clinique du travail.
                                   Direction du travail normal.
                        618.5    Pathologie    du travail.

                            .51    Anomalies du travail dependant  de l'impuissance
                                     des forces expulsives  .
                            . 52   Obstacles mecaniques.











                             618.53]      GYNECOLOGIE.     -  PEDIATRIE


                               618.53    Anomalies du fcetus,
                                  .54    Hemorragies de .l'accouchement.,
                                  . 55   Rupture et dilaceration des voies genitales  .            ,
                                  . 56   Retention du  placenta  .
                                  . 57   Inversion de l'uterus  .                                  i-
                                  . 58   Prolapsus du cordon  .
                                  . 59   Autres  complications pathologiques  .

                              618.6    Etat   puerperal (Physiologie, Hygiene).
                                         Soins a donner, y compris  ceux interessant l'enfant.

                              618·7    Pathologie    de l'etat  puerperal.
                                  . 71   Maladies de la lactation. Eievre de lait .
                                  . 72   F'ievre  puerperale  .
                                  ,73    Metrite. Peritonite.
                                  ·74    Septicemie,
                                  . 75   Eclarnpsie puerperale  .
                                  . 76   Manie puerperale  .
                                  ,77    Phlebite. Thrombite. Phlegmatia.
           'I                     ·78    Autres affections puerperales.
                                  ·79    Mort subite  apres la delivrance,
                              618.8    Operations obstetricales.
                                                                                                   11
                                  . 81   Levier.  Forceps  .
                                                                                                   f?
                                  . 82   Version  .
                                  . 83   Em bryotomie  .
                                  .84    Dilatation du col.
                                  . 85   Syrnphyseotomie  .
                                  . 86   Operation cesarienne  .

                                  .  87  Ablation du  placenta  .
                                                                                 .,
                                  . 88   Provocation de l'accouchement  .
                                  . 89   Autres operations obstetricales  .
                                                     '
                                  .891     Antisepsie.
                                  .  892   Anesthesie obstetricale .
                                  .  893   Reduction de l'uterus gravide  .
                                  .895     Incision du col.
                                  .  897   Operation,  de Porro  .











                                    GYNECOLOGIE.        PEDIATRIE            618·9


                        618·9     Pediatrie. Maladies des enfants.
                                     On classe a 6r8.9 les ouvrages generaux relatifs  aux  maladies des  en­
                                   fants. Ex:
                                           618.9 (02)  Traite de Pediatric.
                                     La pI upart des affections medicales ne presentent aucune particularite qui les
                                   distingue chez I'enfant et  on les c1asse  a la Pathologie interne et externe, 616
                                   et 617. Lorsque  ces  par ticularites meritent d'etre signulees,  on dispose de
                                    a subdivision commune:
                                           053 Affection selon les ages. Enfants.
                                     Toutefois,  on pourra reunir sous 618.9  toutes  les questions de physiologie,
                                   d'hygiene, de therapeutique, de pathologie concernant l'enfance, en combinant
                                   par: la division 618,9 Pediatric  avec  to utes les autres divisions de la merle­
                                   cine 6u a 617. Ex.  :
                                           618.9: 616.3I2.0024 Stomatite gangreneuae chez I'enfant.



















        tT

        11



           I'T








           !I
           'I



















           il










           Ii

















           I




           I,





          I?





          .. '---











                                       ]\tI:EDECINE COMPAREE               619.19




                      019    Medecine          comparee.          Art vete-
         ,
                                  .    .
                                rrnarre,

                                    On ne classe ici que la medecine comparee en general et tout c? qui concerne
                                   l'anatomie, la physiologie, la pathoiogie et la therapeutique des animaux utiles
                                   Oll domestiques.  Ces memes questions etudiees  au point  de vue des autres
                                   animaux sent classees avec la Zoologie [59].
                                    Voir aussi les divisions suivantes  :
                                          636   Zootechnie, elevage  ,
                                          682.1  Marechalerie.
                                          614.9  Police sanitaire des animaux.
                                          614.317  Inspection des viandes.

                       619 (0)   Generalites. Ouvrages generaux.
                                    L'art veterinaire  et toutes ses divisions peuvent  etre  combinees  avec les
                                   subdivisions de generalites, de forme, de lieu et de temps.
                                    Voir les subdivisions communes, dont Ies  principales  ont ete reproduites
                                   sous 616 (0). Ex. :
                                          619 (05)  Revues de medecine veterina.ire  .

                       619       Medecine    COmparee generale.
                                    Ces     sont classees ici a 6[9: suivi Ciu nombre propre a chacune de
                                      Q.uestiollS
                                   ces questions, conformement  aux divisions de la medeciue generate  6II  a
                                   618. Ex. :
                                          619 ; 616.5  Les maladies du sang ep medecine comparee.
                       619.1 a.9  Art veterinaire.
                                                           a. chaque espece d'animaux domes­
                                    Les questions d'art veterinaire propres
                                  tiques sout classeea de 619. I a 6 [9.9. Ex.  ;
                                          619.4  Medecine veterinaire du pore.
                                    Ces divisions concordent avec celles de 6_36 Zoatechnie. Elles peuvent etre
                                  subdivisees a leur tour camme la medecine comparee generate. Ex.  :
                                          619.4: 616.5  Les maladies du sang chez Ie pare.
                                    Les maladies propres aUK an imaux et qui ne sont pas inscrites a. la patho­
                                  logie interne ni externe, a 616 et a 617, sont claseees a 616. 999 Autres maladies
                                   generales Ainsi 'on classera a
                                          619.5: 616.999  Cholera des poules (qui n'a aucun rapport  avec Ie
                                                     cholera humain}.
                                          6I9.4: 6[6.999  Rouget du pore.
                       619.1     Les  equides domestiques.

                           .11    Cheval.
                           . 12   Ane .
                           . 13   Mulet .
                           . 19   Autres equides domestiques  .









        J












                           619.2            MEDECINE COMPAREE


                             6I9·2    Les   grands   ruminants. Betail    ,
                                 . 21   Bceuf. Vache  .
                             6I9·3    Mouton. Chevre.
                                 .31    Mouton
                                 .32    Chevre,
           I
                             6I9.4    Pores.
           I
                             6I9.5    Oiseaux de basse-cour.
          ''I
           ,                     .51    Coq. Paule.
                                 .52    Dindon  .
                                 . 53   Pintade .
                                 . 54   Faisan.
           t                     .55    Paon  .

                                 . 56   Pigeon domestique.
                                          Voir 6 I 9.62 Pigeons voyageurs
                                                             .
                                 . 57   Canard  .
                                 . 58   Oie  .
                                 . 59   Autres oiseaux de basse-cour.

                             6I9·6    Oiseaux   exploites pour  leurs  plumes,   leur
                                        travail. Oiseaux   d'agrernent  .
                                 . 61   Oiseaux exploites pour leursplumes. Autruches.
                                                                                                   ,
                                 .62    Pigeons voyageurs.
                                 . 68   Oiseaux d'agrement et d'ornementation  .
                                 . 681    Cygnes  .
                                 . 682    Oiseaux de cage ei: de voliere  .
          I,
                                 . 683    Per:roquets  .
                             6I9-:7   Chien.
                             6I9·8    Chat.

                             619.9    Autres   animaux     domestiques     ou  utiles
                                         (cobayes, lapins, etc.),
           I·
           t










           I
          •.              -















































































            I






                                                                                           I



































































































                                    MANUELS POUR L'USAGE
                      DES REPERTOIRES BIBLIOGRAPHIQUES SPECIAUX



                       L'Institut  a prepare  une collection de MANUELS pour l'usage et la
                     formation des repertoires bibliographiques  de chaque science particuliere.
                     Ces manuels comprennent,  outre la partie  des t-ables de classification
                     'bfbliographique propres aJa  science envisagce,  un expose general des
                     principes  de la classification, des regles pour la redaction des notices
                     bibliographiques,  des  reg les  pour  la publication  des  recueils biblio­
                     graphiques  et la formation des repertoires  sur fiches, des regles pour
                     la cooperation  au Repertoire bibliographique  universel  et enfin des
                     conseils pratiques pour I'organisation  des bibliotheques,  la formation
                     de  leurs catalogues  et le  classement  _ des  ouvrages  sur  les  rayons.
                     Ces manuels sont destines  aux specialistes de chaque branche des con-
                     naissances. 11s reproduisent,  sous une forme pratique, les divers documents
                     deja parus ailleurs, pour Ia pr$p?rilti?p. et l'emploi  des recueils et des
                      repertoires bibliographiques speciaux.
                       Les divers manuels de la collection  se vendent separement. Chaque
                      manuel forme  un numero special des publications  de l'Institut- Interna­
                      tional de Bibliographie.

                                       ONT PARU A CE JOUR:

                      Publitation 1?O 26.  -  Manuel pour l'usage  du Repertoire bibliographique des
                        Sciences Physiques [025.4: 53].          Prix:  2 francs.
                      Publicaiion 1?O 40.  -  Manuel pour l'usage  des  Reper.toires bibliographiques
                        [025.4].                                 Prix: 2 francs.
                      Publication no  4I.  -  Manuel pour l'ztsage  du Repertoire bibliographique  des
                        Sciences Agricoles [025'4: 63].          Prix: 5 francs,
                      Publication 1?O 45.  -  Manuel pour l'usage  du Repertoire bibliogl'aphique  des
                        Sciences Photographiques [025·4: 77].    Prix: 2 francs.
                      Publication no  48.  -  Manuel pour l'usage  du Repertoire bibliograjhique  de la
                        Locomotion et des Sports [025.4: ?29.I] + [025·4: 79]·
                                                                 Prix: 2 francs.






     \





































                                             L'AUXILIAIRE BIBLIOGRAPHIQUE
                                  JMPRlMERIE DE L'!NSTITUT INTEJl.NATIONAL DE BIBLIOGRAPHIE
                                              RUE VEYDT, 70, BRUXELLES
                                                DIRECT., EM. GODTS••



                                                                                        ,.


                                                                                                    \'
                                                                                         ;i--.
                                                                                        l ....
                                                                                        '-'"











                                                                                     l

