






















                             C C  O  N  G  R  È È  S S  M  O  N  D  I I  A  L
                                   R
                             C O N G R È S
                                              D
                                  G
                                                 A
                                                   L
                                         M
                                N
                              O
                                           O
                                         M O N D I A L
                                             N
                                       I>E
                                         8
                                         8
                                       I>E
                                       I>E8
                         A
                              O
                              O
                         A
                                         N
                                                     T
                                           T
                                             E
                           T
                             I
                             I
                                                 N
                                                   A
                                               R
                           T
                                N
                                  S
                                                            A
                                                              L
                                                        O
                                                          N
                                  S
              A A
              A S S O C I A T I O N S  E  N  T  E  R  N  A  T  I I  O  N  A  L  E  S  N°28
                                                                 S
                                                               E
                S
                S
                  S
                      C
                                       E
                                       E N T E R N A T I O N A L E S
                      C
                                N
                        I
                        I
                    O
                    O
                  S
                                     G
                                        H
                                   :
                                        H
                                     G
                                                          9
                                                          1
                                                          1
                                                       n
                                                       n
                                                           3
                                                           3
                                                           1
                                                           1
                                                          9
                                                       i
                                                     8
                                                    1
                                                   5
                                                   1
                                                   15-18
                                                       i
                                                       u
                                                       u
                                                      j
                                                      j
                                   :
                                i
                                i
                                s
                                 n
                                o
                                o
                                s
                              s
                               e
                               e
                               s
                               s
                              s
                           m
                         e
                          x
                          u
                                 n
                        D D e u x i i è è m e e  session :  G A H  D-B  RU  XELLES, , ,  1 5 - - 1 8  juin  1913
                        Deuxième
                                             X
                                             X
                                            U
                                           R
                                            U
                                              E
                                                E
                                                E
                                                L
                                                L
                                                 S
                                              E
                                               L
                                               L
                                                 S
                                          B
                                          -
                                          -
                                         D
                                         D
                                           R
                                       A
                                       A
                                          B
                                                  Internationale
                                                  Internationale
                              l'Unio
                  Organis
                  Organis
                  Organisé é é  pa r r  l'Unio n n  de s s  Association s s  Internationales s s
                                    de
                          pa
                          par l'Union
                                    des Associations
                                       Association
                                         3bis
                         Office
                                                    Régenc
                                                    Régence
                                         3bis, rue de la
                                             ru
                                   Bruxelles
                                   Bruxelles
                         Offic e e  centra l l  : Bruxelles, , ,  3bis , ,  ru e e  d e e  l  l a a  Régenc e e  3  e  Section
                         Offic
                             centra
                                                d
                                  :
                             central
                                  :
              Actes  du  Congrès.  —   Documents    préliminaires.
                                          R A P P O R T
                                               DE
                                     M.  P A U L  T O U L O N ,
                               Délégué  de la  Société  d'Encouragement
                                 pour 'Industrie  Nationale  (France)
                                      l
                                            [ 6 2 ( 0 1 7 ) ]
                    Le  progrès  et le  développement  continus  du commerce  et de
                  l'industrie,  qui sont  la caractéristique  la  plus  frappante  de la
                  civilisation  moderne,  étendent  sans  cesse  les  échanges  interna-
                  tionaux  et  rapprochent  dans  tous  les pays  tous  les  modes  de
                  production  et  tous  les  produits-  I l  paraît  donc  nécessaire  de
                 rechercher  s'il n'y a  pas lieu  d'établir  des règles  internationales
                  de  standardisation,  c'est-à-dire  d'unification en ce qui  concerne
                  la  fabrication  industrielle.
                    I l  convient  d'examiner  tout  d'abord  si,  au  point  de vue
                  général,  l'unification  des  éléments  essentiels  de  la  fabrication
                  industrielle  et  des  matières  présente  une utilité  incontestable.
                  Si  ce  besoin  existe,  i l  a  dû  se  manifester  dans  les  diverses
                  branches  de la  construction  mécanique  et  des  exemples  pour-
                  ront  être  dès à présent  signalés.  Enfin,  i l y  a  lieu  de  déterminer
                  la  méthode  à  suivre  pour  arriver  à un accord  industriel  SUT  le
                  choix  des éléments  à  unifier  et la fixation  des types  unifiés.

























                                                                                                           les  principes  et  les  règles  qui  paraissaient  les  mieux^ établis.
                       Tout  d'abord,  l'utilité  d'unifier  certains  éléments  de  la  fabri-
                     cation  industrielle  est-elle  manifeste  ?  Est-ce  une  nécessité  qui             I l  importe  que  l'industrie,  sans  cesse  à  l'affût  des  perfection-
                     résulte  du  progrès  économique  et  du  développement  des                          nements,  soit  toujours  prête  à  améliorer  ses  méthodes  et  à  modi-
                    machines  ?                                                                            fier  son  outillage  et  ne  reste  pas  enchaînée  par  une  routine
                       Pour  répondre  à  la  question,  i l  suffit  de  se  placer  au  véri-            aveugle,  rebelle  aux  changements  nécessaires.
                     table  point  de  vue  qui  domine  et  dirige  tout  l'effort  industriel.             L'unification  au  point  de  vue  industriel  a  donc  ses  limites
                     Les  recherches  et  les  travaux  incessants  du  manufacturier  et                  bien  définies,  qu'il  est  sage  de  ne  pas  franchir,  sous  peine  de
                     du  Commerçant  tendent  à  produire  dans  des  conditions  de                       paralyser  l'essor  du  génie  industriel  et  le  progrès  économique;
                    plus  en  plus  économiques,  à  développer  et  à  faciliter  la  consom-             la  prudence  commande,  dans  tous  les  cas  douteux,  de  rester
                    mation  qui,  par  l'extension  croissante  des  moyens  de  transport,                plutôt  en  dessous  de  ces  limites  que  de  les  dépasser.
                    ucoorde  les  frontières  d'un  pays,  quelle  que  soit  son  étendue,                  Si  le  besoin  d'unifier dans  le  domaine  industriel  apparaît  avec
                    et  rayonne  aujourd'hui sur  le  monde  entier.  U n  des  facteurs  les              une  telle  évidence,  des  tentatives  ont  été  faites  pour  satisfaire  à
                     plus  essentiels  de  cette  économie  consiste  à  unifier  par  des                 une  nécessité  qui  s'impose.  C'est,  en  effet,  ce  que  l'on  constate
                    règles  internationales  les  éléments  fondamentaux  qui  intervien-                  dans  les  branches  les  plus  diverses  de  la fabrication.
                    nent  dans  la  fabrication industrielle.  La  détermination  des  qua-                  En  Angleterre,  YEngineering  Standards  Committee  a  été  con-
                    lités  essentielles  des  matériaux  mis  en  œuvre,  la  fixation  des                stitué  en  1901  dans  ce  but,  par  Y Institution  of  Civil  Engineers,
                    formes  et  des  dimensions  de  certains  objets  simples  qui  entrent               l'Institution  of  Mechanical  Engineers,  Y Institution  of  Naval
                    dans  la  construction,  principalement  de  ceux  destinés  à  consti-                Architects,  Ylron  and  Steel  Institut  e,  Y Institution  of  Electrical
                    tuer  des  éléments  pour  la  formation  d'ensembles  et  de  ceux  qui               Engineers.  Ses  travaux  se  sont  étendus  à  quarante-six  questions
                    doivent  être  interchangeables,  parfois  même,  l'unification  de                    différentes.
                    quelques  règles  pour  le  dessin  ou  la  forme  de  certaines  machines               Une  Commission  électrotechnique  internationale  a  été  fondée
                    de  grand  emploi,  le  choix  des  essais  et  des  spécifications  pour              à  la  suite  du  Congrès  de  Saint-Louis  et  forme  une  des  sections
                    la  réception  uniforme  de  quelques  produits, sont  visiblement  des                de  YEngineering  Standard  Committee;  des  Comités  locaux,  con-
                    avantages  économiques  qui  peuvent  être  aussi  précieux  pour  le                  stitués  dans  divers  pays,  d'accord  avec  cette  Commission,  pré-
                    consommateur  que  pour  le  producteur.  Les  études  et  La  compa-                  parent  toutes  les  questions  d'unification  qui  intéressent  l'in-
                    raison  des  machines  exigeront  moins  de  travail;  des  approvi-                   dustrie  électrique.
                    sionnements  pourront être  constitués  avec  une  moindre  dépense;                     Aux  Etats-Unis,  le  Bureau  of  Standards  (Washington)  a
                    l'entretien  de  l'outillage,  devenu  parfois  si  important,  sera                   produit  un  grand  nombre  de  travaux.
                    rendu  plus  facile;  le  machinisme,  de  plus  en  plus  perfectionné,                 En   France,  la  Société  d'Encouragement  pour  l'Industrie
                    pourra  s'accroître  et  s'étendre  plus  largement  pour  l'amélioration              Nationale  a  poursuivi  depuis  1891  l'unification  des  divers  sys-
                    de  plus  en  plus  grande  du  bien-être  général  et  surtout  en  faveur            tèmes  de  vis  et  d'écrous  employés  dans  les  constructions  méca-
                                                                                                                                  1
                    des  classes  les  moins  fortunées,  qui  tirent  le  plus  grand  profit             niques.  Ces  travaux  ont  abouti  à  l'adoption  d'un  système  de
                    de  l'abaissement  du  prix  des  produits.                                            filetage  unifié  qui  a  été  adopté  par  de  nombreuses  industries.
                                                                                                           La  Société  d'Encouragement  pour  l'Industrie  Nationale  a  récem-
                      I l  ne  paraît  donc  pas  douteux  que  l'unification  ou  la  standar-            ment  étendu  le  système  d'unification aux  vis  horlogères.
                    disation  dans  le  domaine  industriel  doit  être  un  progrès  consi-
                    dérable,  qu'elle  aura  un  large  champ  d'application  et  que  le                    Les  travaux  de  la  Conférence  internationale  des  Chemins
                    besoin  d'entente  internationale  sur  cette  question  se  fera  de  plus            de  fer  et  en  partie  ceux  du  Congrès  international  des  Chemins
                    en  plus  sentir.                                                                      de  fer  se  rattachent  à  l'unification  au  point  de  vue  industriel.
                      Toutefois,  l'unification  en  ce  qui  concerne  l'industrie,  appli-                 Il  en  est  de  même  des  travaux  du  Congrès  international  de
                                                                                                                                                      du
                                                                                                                                                          Froid,
                    quée  sans  mesure  ou  étendue  trop  hâtivement,  aurait  de  très                   Navigation,  de  l'Association  internationale France.  de  la
                                                                                                                                         du
                                                                                                                                             Gaz
                                                                                                                                                 en
                                                                                                           Société
                                                                                                                  technique
                                                                                                                            de
                                                                                                                               l'Industrie
                    grands  inconvénients.  Le  progrès  des  recherches  scientifiques                      L'Association  internationale  pour  l'Essai  des  matériaux,
                    et  les  inventions  amènent  des  transformations  incessantes  dans























                                              —  4  —

                    Y American  Society  for  Testing  Materials,  ont  publié  des  tables                d'impuissance  et  de  stérilité,  que  de  tenter  des  unifications  d'élé-
                    pour  l'unification  des  essais  et  des  types  de  spécifications.                 ments  industriels  avec  des  unités  de  mesure  variables  suivant  les
                      I l  n'est  presque  pas  d'industrie  qui  n'ait  fait  des  consta-                pays-
                    tations  ou  des  recherches  en  vue  de  simplifier  et  unifier;  i l  suffit         L'unification  au  point  de  vue  industriel  est  un  problème  d'une
                    de  citer,  à  titre  d'indication  :                                                  telle  étendue  et  d''une  telle  complexité,  elle  met  en  jeu  de  si
                      L'unification  des  verres  de  montre;                                             graves  intérêts  et  a  des  répercussions  si  lointaines, que  de  longues
                      L'unification  des  verres  d'optique;                                              et  patientes  études  sont  nécessaires.  11  faut  faire  appel  au  con-
                      L'étalonnage  des  couleurs;                                                         cours  de  toutes  les  initiatives et  de  tous  les  pays.  I l  semble  que
                      La  fixation  des  unités  photographiques;                                          la  meilleure  méthode  consiste  à  utiliser  tous  les  efforts  déjà  faits
                      L'unification  de  certains  produits  pharmaceutiques;                             et  à  en  provoquer  de  nouveaux.  Dans  chaque  pays  et  dans  chaque
                      L'unification  des  analyses  chimiques.                                             branche  de  l'industrie,  des  associations  devraient  étudier  les
                      Cette  énumération  est  nécessairement  incomplète  et  ne  donne                  questions  qui  les  intéressent,  préparer  des  propositions  en  vue
                    qu'une  idée  générale  de  l'étendue,  de  la  complexité  et  de  la                de  l'unification  ou standardisation.
                    variété  des  questions  qui  peuvent  être  utilement  examinées  en                   Une   Commission  internationale  pourra  être  constituée,  com-
                    vue  de  la  standardisation.  Dans  les  divers  pays  où  l'industrie               prenant  des  représentants  autorisés  des  divers  pays  et  des  prin-
                    présente  le  plus grand  développement,  la  détermination  de  règles               cipales  industries. Cette  Commission  aura  pour  mission  de  réunir
                    uniformes,  le  choix  de  types  élémentaires  bien  déterminés,  en                 tous  les  documents  relatifs  à  la  question,  de  provoquer  les
                    un  mot,  la  standardisation  technique  et  industrielle  a  suscité                études  et  travaux  préliminaires  nécessaires  pour  la  standardi-
                    des  initiatives,  provoqué  des  groupements,  déterminé  un  mou-                    sation,  de  coordonner  les  propositions  présentées  par  les  asso-
                    vement  général  dans  les  directions  les  plus  variées  pour  arriver             ciations  et  comités  nationaux;  son  rôle  ne  consistera  pas  à  faire
                    à  résoudre  le  problème  et  fixer  des  bases  bien  étudiées.  C'est  une          directement  des  propositions  pour  la  standardisation,- mais  à
                    éclatante  confirmation  de  l'utilité  manifeste  de  la  standardi-                 réaliser  l'accord  entre  les  diverses  propositions  qui  lui  seront
                    sation  et  des  progrès  économiques,  avantageux  pour  tous,  qu'il                transmises  ;  ce  sera  un  organisme  international destiné  à  les  con-
                    est  légitime  d'en  espérer.                                                         cilier,  un  arbitre  chargé  d'établir  l'accord  et  d'aboutir  à  une
                                                                                                          entente  universelle.
                      Ces  initiatives,  quelles  qu'en  soient  la  valeur  et  l'étendue,
                    suffiront-elles  à  résoudre  un  problème  aussi  compliqué  ?  Quelle                  Il  devra  faire  appel  à  toutes  les  bonnes  volontés  et  saura,  en
                    que  soit  l'importance  industrielle  d'une  nation r  est-il  possible,             présence  de  divergences  de  vue  inévitables,  montrer  quel  intérêt
                    sans  entente  internationale, de  fixer  des  règles  et  une  standardi-            supérieur  et  quels  avantages  pour  tous  présentent  certaines  unifi-
                    sation  qui  puissent  réunir  l'assentiment  de  tous  ?  Les  progrès               cations  judicieusement  choisies.
                    des  nations  les  plus  jeunes  dans  le  domaine  industriel,  l'ex-                  En  résumé,  l'ensemble  des  considérations  qui  viennent  d'être
                    tension  toujours  croissante  du  machinisme,  montrent  avec  évi-                  sommairement   exposées  conduit  aux  conclusions  suivantes,  à
                    dence  que  l'unification,  pour  être  efficace,  doit  être  concertée              soumettre  aux  votes  du  Congrès  mondial  :
                    entre  les  divers  pays  et  entre  les  diverses  industries;  c'est  une
                    œuvre  qui  doit,  par  essence,  être  internationale  pour  avoir  une
                    autorité  suffisante  et  produire tout  son  effet  utile.
                      La  base  de  cette  œuvre  d'unification,  au  point  de  vue  tech-
                    nique  et  industriel,  est  le  choix  des  unités  de  mesure,  système                                   C O N C L U S I O N S
                    métrique  généralisé,  unités  électriques,  etc.  Pour  unifier,  i l  est
                    indispensable  qu'au  préalable  des  unités  de  mesure  internatio-                   1°  La  standardisation  internationale  au  point  de  vue  technique
                    nales  soient  adoptées;  les  éléments  de  détail  qui,  dans  chaque               et  industriel,  est  une  nécessité  qui  s'impose  et  un  progrès  à  réa-
                    industrie,  pourront être  unifiés,  ne  le  seront  utilement  qu'à  cette           liser.  Cette  standardisation  doit  être  strictement  limitée  aux  élé-
                    condition  nécessaire.  Ce  serait  une  œuvre  vaine,  frappée  d'avance             ments  pour  lesquels  elle  présente  un  évident  intérêt  économique,























                                              —   6  —

                    sans  risquer  d'apporter  aucune  entrave  au  développement  de
                    l'industrie;
                      2°  La  standardisation  technique  et  industrielle  doit  avoir  pour
                    base  des  unités  de  mesure  internationales  :  système  métrique,
                    unités  électriques^  etc.;
                      3°  Une  Commission  internationale  permanente  sera  nommée
                    peur  Vunification  au  point  de  vue  industriel.  Cette  Commission
                    réunira  les  documents  et  provoquera  les  études  et  propositions  à
                    préparer  par  les  associations  des  divers  pays;  elle  coordonnera
                    ces  propositions;  elle  s'efforcera  de  les  concilier  et  d'arriver  à  un
                    accord  définitif.  Elle  préparera  ainsi  les  conclusions  à  soumettre
                    à  la  sanction  d'un  Congrès  mondial.

