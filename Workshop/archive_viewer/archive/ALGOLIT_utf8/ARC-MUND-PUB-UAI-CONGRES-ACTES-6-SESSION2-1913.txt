





















                                             . . .  . . .
                                      S
                                    È
                                           O
                                         M
                                         M
                              O
                            C O N G R È S
                            C C O N G R È S  M O N O I A I . . .
                                   R
                                 G
                               N
                                               I
                                                A
                                                A
                                                  I
                                                  I
                                               I
                                           O
                                            N
                                            N
                                              O
                                              O
                                       K
                                       K
                                       H H H K S  S  S
                                  S
                S
                                               N
                                               N
                                N
                              O
                          T
              A A  S  S  O  C  I I  A  T  I I  O  N  S  I N T E R N A T I O N A L E S  A C T E S
              A S S O C I A T I O N S
                                                  A
                                                  A
                                           E
                                          T
                                        N
                                        N
                                          T
                                      I
                  S
                                             R
                                             R
                                           E
                                      I
                                                     I
                                                     I
                     C
                                                           A
                   O
                                                       O
                                                       O
                                                         N
                                                         N
                                                   T
                                                               E
                                                                 S
                                                               E
                         A
                                                   T
                                                             L
                                                           A
                                                                 S
                                                             L
                                              ,
                                              ,
                                        BRUXELLES
                                        BRUXELLES
                                      O
                                      O
                              session
                              sessio
                                                     191
                                                     1913
                        Deuxièm e e  sessio n n  : GÂNO BRUXELLES, 15-19 9 9  jui n n  191 3 3  N ° 6
                                                  jui
                                                  juin
                        Deuxièm
                        Deuxième
                                   :
                                    GÂN
                                    GÂN
                                   :
                                               15-1
                                               15-1
                  Organis
                  Organis
                  Organisé é é  pa r r  l'Unio n  de s s  Associations s s  Internationale s s
                                                 Internationale
                                                 Internationales
                             l'Unio
                                  n
                                       Association
                          pa
                                       Association
                                    des
                                    de
                             l'Union
                          par
                                                 I
                                  Bruxelles ,
                                  Bruxelles
                       Offic
                       Offic
                       Offic e e e  centra  l l  : : :  Bruxelles  , ,  Itbis, , ,  ru  e e  d d de e e  I In n n  Régenc e e e
                                                   Régenc
                            centra l
                            centra
                                                   Régenc
                                            ru
                                            ru e
                                         Itbis
                                         Itbis
                                                                       1913.04
              Actes  du  Congrès.  —  Documents    préliminaires.
                  La   deuxième      session    du   Congrès     mondial.
                    C'est  du  15  au  18 uin  prochain, qu'aura  lieu  à  Gand-Bruxelles,  la
                                    j
                  deuxième  session  du  Congrès  Mondial  des  Associations  Interna-
                  tionales.  La  lettre  d'invitation,  le  programme  du  Congrès  ainsi
                  que  le  questionnaire  de  l'enquête-referendum  préalable  aux  rapports
                  généraux  qui seront  présentés  sur  les  divers  points  de  l'ordre  du  jour
                  précisent  l'objet  de  la  session.
                    Les  adhésions  des  Associations  Internationales  arrivent  nom-
                  breuses.  Les  associations  qui  n'ont  pas  encore  désigné  leurs  délégués
                  sont  instamment  priées  de  le  faire.
                           A.  —  Intérêt  du  Congrès  pour  les  Associations.
                    Toutes  les  Associations  Internationales  ont  un  intérêt  à  prendre
                  part  aux  travaux  du  Congrès,  pour  les  motifs  suivants  :
                    1.  —  D'abord  les  questions  mêmes  inscrites  à  l'ordre  du  jour  ont
                  été choisies  parmi  celles ui directement intéressent tous les  organismes
                                      q
                  internationaux.  Le  Congrès  est  une  vaste  enquête,  un  échange  de
                  vues  sur  des  points  que  rencontrent  dans  leur  gestion  les  dirigeants
                  de  toutes  les  Associations.
                    2.  —  Le  Congrès  est  un  moyen  offert  à  chaque  Association  défaire
                  connaître  à  une  élite  internationale, les  résultats généraux  de  l'œuvre
                  réalisée  par  elle  et  d'établir  d'utiles  relations.
                                     l
                    3.  —  Le  principe de a coopération,  si heureusement  mis  en  lumière
                  par  la  première  session  du  Congrès,  trouvera  l'occasion  de  se  déve-
                  lopper  lors  de  la  deuxième  session.  C'est  ainsi  que  le  concours  de
                  l'Union  Interparlementaire,  qui  dispose  de  groupes  puissants  dans
                  tous  les  parlements,  a  été  promis  pour  appuyer  les  mesures interna-
                  tionales  qui  auraient  fait  l'objet  de  conclusions  étudiées  avec  soin  et
                  largement  appuyées.  D'autres  organismes  internationaux  ont  aussi
                  fait  l'offre  de leur concours  pour  des  questions qui leur sont  communes
                  avec  d'autres  Associations.























                                                                                                                                    —  3  —

                      4.  —  Enfin,  i l  n'est  pas  indifférent,  pour  le  succès  des  buts  parti-    g)  Documentation  :  bibliothèque  internationale  et  principaux
                    culiers  poursuivis  par  chaque  Association,  que  le  mouvement  inter-           fonds  internationaux dans  les  bibliothèques  nationales, bibliographie,
                    national  général  apparaisse  dans  toute  son  ampleur,  aux  yeux  des            archives,  musées  spéciaux  et  principales  collections  existantes.
                    gouvernements  et  de  l'opinion  publique.                                            Si  chaque  Association  consent  à  établir  un  tel  rapport,  leur  en-
                                                                                                         semble  constituera  le  bilan  général  du  mouvement  international et
                               B.  —  Participation  aux  travaux  du  Congrès.                          permettra  de  dresser  un  tableau  qui  promet  d'être  tout  à  fait  saisis-
                                                                                                         sant.  Les  deux  rapports  qui  ont  été  publiés  déjà  dans  La  Vie  Inter-
                      L a  participation aux  travaux  du  Congrès  Mondial  est  rendue  facile
                                                                                                         nationale,  l'un  sur  le  Droit  d'auteur  et  le  Bureau  International  de
                    par  toutes  les  mesures  qui ont  été  prises.
                                                                                                                                 I
                                                                                                         Berne  (M. Rothlisberger, . I ,  p.  201), l'autre sur les  Poids  et  Mesures,
                                                                                                                               t
                      1.  —  L a  représentation  peut  se  faire  par  tel  nombre  de  délégués        le  Système  métrique  et  le  Bureau  International  du  Mètre  (M.  Guil-
                    que  les  Associations  jugeront  utile.  Le  Congrès  pouvant  intéresser           laume,  t. I I ,  p.  5), permettent  de  se  rendre compte  de  l'intérêt  puis-
                                                                                                                  I
                    tous  les  membres  des  Comités  des  Associations,  ils  sont  tous  invités       sant  qui  s'attache  à  de  tels  travaux  et  les  utiles  enseignements  q u i
                    à  suivre  les  séances.  L a  délégation  peut  être  éventuellement  exercée
                                                                                                         en  ressortent.
                    par  les  délégués  belges  des  Associations.  Ceux-ci  étant  sur  place,
                                                                                                           5.  —  Une  attention  toute  spéciale  est  appelée  sur  l'établissement
                    peuvent  facilement  et  sans  frais  assister  au  Congrès  et  faire  ensuite      du  budget  dont  chaque  Association  Internationale devrait  pouvoir
                     rapport  à  leur  Association.                                                      disposer  pour accomplir la tâche  qu'elle  s'est  imposée.  L'ensemble  des
                      2.  —  U n  questionnaire  d'enquête  très  complet  a  été  dressé  pour          budgets  ainsi  réunis permettra de  dresser  le  budget  général  de  l'inter-
                    recueillir  l'opinion  des  Associations  sur  les  questions  à l'ordre  du  jour.
                                                                                                         nationalisme  et  de  montrer  les  sommes  relativement  faibles  néces-
                    Une  première  forme  de  participation consiste  à  envoyer  des  réponses
                                                                                                         saires  pour  promouvoir des  réformes  d'un  intérêt  universel.
                    à  toutes  ou  à  quelques-unes  de  ces  questions,  ou  à  formuler des  con-
                    clusions  et  motions les  concernant.
                                                                                                                  C.  —  Coopération  des  Associations  Internationales.
                      3.  —  Le  questionnaire  servira  de  base  aux  discussions.  Des  ré-
                     ponses  orales  pourront  alors  être  recueillies  de  la  part  des  délégués,      L a  première  session  du  Congrès  a  provoqué  d'utiles  mesures  de
                    mais  i l  est  désirable  qu'elles  soient  envoyées  immédiatement  par            coopération  entre  les  Associations.  I l  est  désirable  que  la  deuxième
                     écrit,  pour  faciliter l'élaboration  d u  rapport  général.                       session  soit  l'occasion  de  nouveaux  rapprochements  et  que  des  rap-
                       4.  —  Enfin  i l est  demandé  aux  Associations  un  rapport  sur  l'inter-     ports  sur  dés  coopérations  limitées  à  certains  domaines  soient  pré-
                     nationalisation  dans  leur  domaine  propre  et  sur  les  travaux  qu'elles       sentés.  L'étude  parue  dans  La  Vie  Internationale  (1912,  t.  I , p.  61),
                                                                                                                                                          l
                     ont  accomplis.  Ce  rapport  peut  laisser  de  côté  les  détails  d'organisa-    sur  la  Coopération  entre  l'Union  Interparlementaire, 'Institut  de
                     tion  (fondation, statuts,  règlements  personnels,  etc.),  qui  ont  déjà          Droit  International  et  le  Bureau  de  la  Paix  et  entre  ces  organismes
                                                                                                            l
                                                                                                                                 f
                     figurés  dans  Y Annuaire  de  la  Vie  Internationale.  I l  est  désirable        et a Conférence  de  la  Paix, ournit  un remarquable  exemple  de  ce  que
                     qu'elles  exposent  les  résultats  acquis  et  les  problèmes  qui  restent  à      peut  et  pourrait  la  coopération.  L a  Semaine  sociale  organisée  en  1912,
                                                                                                                         l
                     résoudre.                                                                            à  Zurich  et  dont a  première  idée  fut  lancée  au  Congrès  Mondial  de
                                                                                                                 f
                       L'exposé  de  l'état  actuel  de  l'internationalisation,  dans  les  divers       1910,  en ournit  un  a u t r e : L a  Vie  Internationale  lui  a  également  con-
                                                                                                                             t
                                                                                                                               I
                     domaines  des  études  et  de  l'activité  pratique,  devrait  comprendre            sacré  une  étude  (1912, . I ,  p.  61).
                                                                                                                       j
                     notamment  les  points suivants  :                                                     A  l'ordre  du our  de  la  Conférence  de  l'Union Interparlementaire,
                                                                                                                                                                    l
                       a)  Organismes  internationaux  existants,  projetés  ou  suggérés;                qui  aura  lieu  à  La  Haye,  du  3  au  5  septembre  1913,  a  été  inscrite a
                                                                                                                                     l
                       b)  Coopération  réalisée  ou  projetée  entre  organismes  de  la  même           question  de  la  c Coopération  de 'Union  et  de  ses  groupes  aux  œuvres
                     spécialité  ou  avec  d'autres  ;                                                    internationales  >•.
                       c)  Publications  internationales  existantes  ou  en  projet  (revues,              Pour  faciliter  les  ententes,  des  séances  particulières  seront  éven-
                     annuaires,  encyclopédies,  etc.)  ;                                                 tuellement  organisées  au  Congrès  Mondial,  à  leur  demande,  entre  les
                      d)  Réglementation  internationale officielle ou  libre  ;                          délégués  d'Associations  poursuivant  des  buts  connexes  et  désirant
                       e)  Unification  et  systèmes  d'unités  ;                                         rechercher  en  commun  des  terrains  d'entente  et  de  coopération.
                       i)  Langage  :  terminologie,  classification,  notations,  usage  des
                    langues naturelles, langues artificielles ;
























                                               —  4  —

                                 D.  —  Concours  des  Associations  officielles.                        trouvent  dans  les  travaux  du  Congrès  Mondial  une  documentation
                      Les  Associations  Internationales officielles ont été invitées  à prendre         abondante  sur  l'action  des  diverses  Associations,  sur  leur  organisa-
                    part  aux  travaux  du  Congrès.  Ces  Associations  se  trouvent  dans  une         tion,  sur la valeur  de leurs travaux.  Ils peuvent  aussi  se rendre  compte
                    situation  spéciale  à  raison  du  caractère  de  leur  organisation.  Cer-         du  mouvement  général  auquel  ils  sont  invités  à  chaque  instant  à
                    taines  se  considèrent  liées  par  leurs  règlements  organiques  et  empê-        participer,  de  la  corrélation  qui  existe  entre  les  résolutions  particu-
                    chées  de  prendre  une  part  active  au  Congrès.                                  lières  des  Associations,  des  grandes  œuvres  d'ensemble  que  celles-ci
                                                                                                         ont  entreprises  sur  la  base  de a  coopération  et  de  l'impérieuse  néces-
                                                                                                                                   l
                      I l  est  désirable  qu'elles  examinent  à  nouveau  Ja question  en  tenant
                    compte  des  considérations  suivantes  :  l'assistance  au  Congrès  n'im-          sité  de  voir  les  États  s'y  associer et  harmoniser avec  leurs  desiderata
                    plique  pas  nécessairement  l'affiliation  à 'Union  ; cette  assistance  peut      l'organisation  de  leurs  propres  services  intérieurs.
                                                      l
                                               i
                    se  faire  ad  audiendum  et  pour nformation  ; alors  même  que  certaines           Les  délégués  des  Gouvernements  assisteront  aux  séances  du  Con-
                    Associations  jugeraient  qu'elles  n'ont  que  peu  à  retirer  de  semblables      grès  ad  audiendum.  E n  outre,  une  séance  spéciale  leur  sera  réservée
                    réunions  —  ce  qui  est  on  ne  peut  plus  contestable,  —  i l leur  est  de-   exclusivement,  séance  dans  laquelle  aura  lieu  un  échange  de  vue  offi-
                    mandé  d'agir  dans  un  esprit  de  solidarité  et,  par  la  présence  de  leurs   cieux  sur  la  coopération  qu'il  est  désirable  de  voir  apporter  par  les
                    délégués  à  qui  pourront  être  posées  des  questions  d'ordre  pratique,         Gouvernements  aux  Associations  Internationales et  à leur  Union.
                    de  ne  pas  priver  les  autres  Associations  des  indications et  renseigne-
                    ments  utiles qu'ils pourraient apporter  aux  débats.  L'action  libre  des                           F.  —  Publications  du  Congrès.
                    Associations  est  souvent  stérile,  si elles ne  sont  pas  en contact  avec  les
                    Associations  officielles  similaires. D'autre  part, celles-ci  à  défaut  d'un       L a  publication  des  documents  du  Congrès  se  poursuit.  A  ce  jour
                    tel contact  risquent souvent  d'être  retardées  dans leur  développement,          elle  se  compose  de  :
                    privées  qu'elles  sont  du bénéfice  d'utiles  initiatives.                           a)  ACTES  D U ONGRÈS  :  publiés  en  documents  séparés  qui  seront
                                                                                                                        C
                      Une  séance  du  prochain  Congrès  est  spécialement  réservée  aux               réunis  ultérieurement  en  volume.
                    délégués  des  Associations  officielles,  afin  de  leur  permettre de  déli-
                                                                                                           N °  I.  Lettre  d'invitation.
                    bérer  entr'eux  sur  l'attitude  qu'il  importe  aux  organismes  officiels
                                                                                                           N°  2.  Programme  du  Congrès.
                                                                          l
                    d'avoir  à l'égard  des  organismes  libres et  sur leur coopération  à 'Union         N°  3.  Organisation  et  marche  des  travaux.
                    des  Associations  Internationales.
                                                                                                           N °  4.  Résumé,  sous  forme  de  thèses,  des  principes  et  des  faits  qui
                                                                                                         sont  à  la  base  de 'Union  des  Associations  Internationales.
                                                                                                                        l
                                 E.  —  Représentation  des  Gouvernements.
                                                                                                           N °  5.  Enquête-référendum  sur  les  questions  à  l'ordre  du our  du
                                                                                                                                                              j
                      Les  Gouvernements  ont  été  invités  à  se  faire  représenter  au  pro-         Congrès,  avec  références  aux  publications  de  l'Union.
                                      i
                    chain  Congrès.  Cette nvitation  a  été  adressée  sous une  double  forme  :
                                                                                                           b)  D O C U M E N T S  D U  CONGRÈS  :  Rapports  et  Communications,  pu-
                                  l
                    directement  par 'Union  aux  représentants  des  puissances  à  Bruxelles,          bliés  dans  la  revue  La  Vie  Internationale.
                    et  par  la  voie  diplomatique ordinaire,  à  l'intervention  du  Ministère
                    des  Affaires  étrangères  de  Belgique,  à  raison  du  patronage  spécial            c)  P U B L I C A T I O N S  CONNEXE S  :
                    accordé  à 'Union  par  le  Gouvernement  belge.
                             l
                                                                                                           1.  Compte  rendu  de  la  réunion  préparatoire  du  Congrès  d'avril  1912
                      Plusieurs  Gouvernements  ont  accepté  cette  invitation.  Ils  seront            (Publication  n°  33).  Publié  aussi  dans la  Vie  Internationale,  I ,  p.  148.
                    représentés  notamment  par  leur  ministre  à  Bruxelles.                             2.  Annuaire  de  la  Vie  Internationale  : monographie  descriptive  des
                      L'intérêt  des  Gouvernements  à  se  faire  représenter  au  Congrès  est
                                                                                                         510  Associations  Internationales existantes  (Publications  n  o s  3  et  47).
                    multiple.  Ils sont  généralement  représentés  auprès  de  tous  les  grands
                                                                                                           3.  L'Union  des  Associations  Internationales  :  notice  générale  sur
                    congrès.  I l est  logique  qu'ils  le  soient  auprès  d'un  « Congrès  des  Con-
                    grès  ».  Des  sections  nationales  ayant  été  créées  au  sein  du  Musée         l'Union  et  ses  services  (Publication n°  25a).
                    International,  à  Bruxelles,  il  est  naturel  que  ces  sections  soient            Ces  diverses  publications  contiennent  les  matériaux  préparatoires
                    placées  sous  le  haut  protectorat  des  divers  Gouvernements.  Ceux-ci           du  congrès  et  les  sources  à  consulter.  E n  voici l'indication  à  ce  jour,
                                                                                                         par  questions.























                                             —  6  —                                                                                —  7  —

                      Les  abréviations  employées  sont  les  suivantes  :                                D.  Participation  des  Etats  et  des  diverses  nationalités.  Exemples
                         Congrès  =  I .  Actes  du  Congrès  de  i g i o .                                     empruntés  à  des  associations.  —  Annuaire,  I ,  79,  289,
                                     I I .  Documents  préliminaires  du Congrès de  1913.                      35L  614.
                         Code      =  Conclusions  générales  codifiées  présentées  au                    E.  Budget  idéal.  Exemples.  —  Annuaire,  passim  ;  Congrès  I ,  259  ;
                                        Congrès  de  1910  et  incluses  dans  les  Actes                       Revue,  I I , 355.
                                        de  ce  Congrès  (p. 39).
                         Annuaire  =  Annuaire  de  la  Vie  Internationale  :  I  (190S-                 V  e  S E C T I O N  :
                                        1909)  ;  I I  (1910-1911).                                        A.  Organisation  des  publications.  —  Congrès  I ,  100-137  '•  Annuaire,
                         Revue     =  L a  Vie Internationale ; désignation  par  tomaison.                     I I ,  623  ;  Revue,  I I ,  397.
                         Union     =  Publication  n°  25  :  L ' U n i o n  des  Associations             B.  Organisation  de  la  documentation.  —  Union,  139  ;  Congrès  I ,
                                        Internationales.                                                        87-164,  157,  481,  1143,  1207;  Code,  5  e  question.
                    I  r  o  S E C T I O N  :                                                              C.  Université  et  Enseignement  international.  —  Union,  144;  An-
                                                                                                                nuaire,  I , 949,  I I , 1731  ; Revue,  I I , 93  ; Congrès  I , 269.
                      A.  Coopération  entre  associations  internationales.  —  Code,  i  r  e  ques-
                           tion  ;  Congrès  I , 39,  257,  823,  1155  ;  Revue,  I , 1.                  D.  Utilisation  des  Expositions  universelles.  —  Revue,  I , 312  (n°  17),
                                                                   6
                      B.  Codification  des  vœux  des  Congrès.  —  Union,  104  ;  Congrès  I ,               314,  543, 627,  648.
                           37-I9 -                                                                        V I  e  S E C T I O N  :
                                6
                      C.  Union  des  Associations  internationales.                                       A.  Terminologie,  Nomenclature  internationale.  —  Congrès  I ,  165,
                           a)  Union  :  Union,  96-98,  112-162  ;  b)  Centre  international,                 204,  589-627,  1115.
                           Union,  20,  140;  Revue,  I I ,  123;  c)  Participation  des  Etats,
                                                                                                           B.  Emploi  des  langues  dans  les  relations  internationales.  —  Congrès  I ,
                           Revue,  I I , 357,  I I I , 290.
                                                                                                                1123-1142  ;  Code,  6  e  question.
                    I I  e  S E C T I O N  :
                      A.  Régime  juridique  des  Associations  internationales.  —  Revue,                       G.  —  Exposition  des  Associations  Internationales.
                           I ,  488-498;  Congrès  I , 53,  317-333,  825,  1051.                          Cette  Exposition  aura  lieu  au  Musée  International.  Les  Associa-
                      B .  Droit  international  conventionnel.  —  Congrès  I .  329;  Annuaire         tions,  qui  n'y  auraient  pas  encore  organisé  leur  stand,  sont  invitées
                           (texte  des  principales  conventions internationales).                       à  se  mettre immédiatement  en relation à  ce  sujet  avec l'Office Central.
                      C.  Conception  et râle  des  Associations  internationales.  —  Revue,  I ,  1  ;  L'Office  leur  facilitera  de  toutes  manières  cette  participation.  Des
                           Congrès  I , 273  ; Congrès  I I , n°  4  ; Annuaire,  I , 65,  I I ,  195.   types  de  Stands  complets  ont  été  étudiés  et  exécutés  déjà  pour  plu-
                                                                                                         sieurs  Associations.  Ils leur ont  permis  de  réaliser  rapidement  et  aisé-
                    I I I  e  S E C T I O N  :
                                                                                                         ment  le  programme  d'exposition  dont  le  détail  a  été  publié  dans  la
                      Unification  des  systèmes  d'unités.  —  Système  universel  des  poids  et       notice  générale  sur 'Union  (Publication n°  25«, p.  112  à  119).
                                                                                                                          l
                           mesures.  Standardisation  technique  et  industrielle. Congrès  I ,
                           59-73,1091-1115  ; Revue,  I I I , 5.
                                                                                                                             H .  —  Fêtes  du  Congrès.
                    I V  e  S E C T I O N  :
                                                                                                           Le  Congrès  se  tiendra au  cours  de  la  grande  Exposition Universelle
                      A.  Moyens  divers  d'accroître  l'utilité  et  le  rendement  des  Associations   organisée  à  Gand,  exposition  dont  l'importance est  aussi  considérable
                           internationales  et  des  Congrès  internationaux.  —  Annuaire,              que  celle  qui  a  eu  lieu  à  Bruxelles  en  1910.  L'exposition  des  fleurs
                           I ,  29-166  ; Congrès  I , 75  ; Revue,  I I , 397.                          (Floralies)  lui donne  un  intérêt  exceptionnel.
                      B.  Réalisation  des  résolutions  des  Congrès.  Revue,  I I , 201,  I I I ,  5 ;   Toute  une  série  de  Congrès  internationaux  y  auront  lieu,  suivant
                           (Cas  particuliers.)  Annuaire,  statuts  et  règlements  des diver-          un  plan aussi  systématique  que  possible,  tendant  à grouper  les  Congrès
                           ses  grandes  associations.                                                   en  semaines,  en  quinzaines,  de  manière  à  réunir  les  personnes  des
                                                                                                         mêmes  spécialités  tout  en  laissant  à  chaque  Congrès  son  existence
                      C.  Règlement-type  des  Congrès.  —  Annuaire,  passim.  (Cas  parti-
                           culiers.)  Congrès  1,  439-464.                                               autonome  et  en  lui  permettant  de  constituer  la  suite  des  Congrès























                                              —  8  —

                    antérieurs  sur  le  même  sujet  ( i ) .  U n  palais,  construit  en  matériaux
                    définitifs,  a  été  spécialement  aménagé  pour  héberger  les  Congrès.
                      Une  journée  du  Congrès  est  réservée  à  Bruxelles  et  à a  visite  des
                                                                     l
                    institutions  qui  y  sont  établies.
                      De  grandes  fêtes  auront  lieu  au  moment  du  Congrès,  dont  la  date
                    coïncide  avec la  sortie d'un grand  cortège historique.

                                   J
                                 • .  —  Facilites  de  voyage  et de  logement.
                      Voyage.  —  I l est  recommandé  aux  congressistes  de  se  munir  d'un
                    abonnement  de  cinq  jours  ou  de  quinze  jours,  qui  leur  permettront
                    de  circuler  sur  tous  les  chemins  de  fer  belges  (2).  Ce  système  est  plus
                    avantageux  qu'aucune  autre  combinaison  et  permettra  aux  congres-
                    sistes  de  se  rendre  à  Bruxelles,  Bruges,  Ostende  ou  Blankenberghe,
                    ou  à  une  ville  voisine  à  leur convenance,  si les  hôtels  de  Gand  étaient
                    encombrés.
                      Les  cartes  d'abonnement  s'obtiennent  dans  toutes  les  stations  et
                    haltes  des  chemins  de  fer  belges,  sur  demande  verbale  faite au moins
                    une  heure  d'avance  et  sans  autre  formalité  que  la  remise  du  portrait
                    de  l'intéressé,  photographié  sur  papier  6  X  4,  la  hauteur  de  la  tête
                    étant  d'au  moins  1  centimètre.  Les  cartes  peuvent  être  obtenues  dès
                    la  veille  du  premier  jour  de  leur  validité,  à  partir  de  18  heures.  Elles
                    ne peuvent  être  utilisées  ce  jour-là.
                      I l  suffit  aux  étrangers  d'envoyer  leur  photographie  à  la  première
                    station  d'entrée,  en ndiquant la  nature et  la  classe  de  l'abonnement,
                                     i
                      j
                    le our  et  l'heure  de  leur  passage, pour  que a  carte  désirée  soit  tenue
                                                         l
                    à  leur disposition.
                      Les  cartes  doivent  être  restituées  à  une  station  quelconque  des
                                                                  j
                                                                        l
                                               t
                    chemins  de  fer  belges,  au  plus ard  le  lendemain  du our  de 'expira-
                    tion,  avant  midi.
                      Logement.  —  U n  Comité  de  logement  a  été  institué  à  Gand,  36,  rue /
                    Digue  de  Brabant.  Ce  Comité  publiera la  liste  des  hôtels  de  Gand  ; l
                                                                              i
                                      l
                    aura  pendant  toute a  durée  de 'Exposition,  un  bureau  à a  gare  de
                                                l
                                                                       l
                    Gand-Sud,  un  bureau  à  la  gare  de  Gand-Saint-Pierre,  un  bureau  à
                    l'Exposition.  I l conservera  également  son  bureau  actuel  où  toutes  les
                    correspondances  devront  être  adressées.  Les  bureaux  seront  ouverts
                    de  8 heures  du  matin  à  10  heures  du  soir.
                      Moyennant  une  légère  rétribution  (25  centimes),  des  chasseurs
                    seront  mis  à  la  disposition  des  étrangers  pour  les  conduire  à  leurs
                    appartements.
                      (1)  Sur  l'Exposition de  Gand,  voir  La  Vie  Internationale,  t .  I , p.  545,
                    591,  627  et  le  Calendrier  des  Congrès.
                      (2)  Abonnement  de  5  et  15  jours  sur  tous  les  chemins  de  fer  belges  :
                    5  jours,  i  r  e  classe,  fr.  30.75;  2  e  classe,  fr.  20.50;  3  e  classe,  fr.  11.75;
                    15  jours  :  i  r  e  classe,  fr.  61.50;  2  e  classe,  41  francs;  3  e  classe,  fr.  33.50.
                    Garantie,  5  francs.

