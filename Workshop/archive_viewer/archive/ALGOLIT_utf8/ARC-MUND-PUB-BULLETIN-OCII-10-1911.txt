

















                      Bulletin du 10 Janvier 1911





                     1. CONGRÈS MONDIAL. — Les Actes du Congrès Mondial sont
                   en voie d'achèvement; plus de  800  pages de texte sont déjà im-
                   primées. La distribution peut être prévue pour le commencement
                   de mars.
                     2. LES CONGRÈS DE 1910. — Sur la proposition du Président
                   de la Chambre Belge, une Commission parlementaire a été nommée
                   pour l'examen des vœux des Congrès réunis à Bruxelles en 1910,
                   qui concernent les pouvoirs publics. Des démarches ont été entre-
                   prises auprès de divers Parlements étrangers, à l'effet de les voir
                   accueillir également les vœux des Congrès. Les Commissaires
                   généraux près de l'Exposition de Bruxelles ont promis de faire
                   une place spéciale aux vœux des Congrès dans le rapport qu'ils
                   adresseront à leur gouvernement.
                     3. RECUEIL DES VŒUX ET RÉSOLUTIONS DES CONGRÈS.
                   — En vue de faciliter la tâche des Commissions parlementaires et
                   des Commissaires généraux,l'Office central des Institutions inter-
                   nationales a décidé la publication d'un recueil contenant les réso-
                   lutions et vœux émis par les Congrès en 1910.
                     Les   secrétaires   généraux   qui   n'en   auraient   pas   encore   fait
                   parvenir le texte à l'Office central, sont priés de le faire dans le
                   plus bref délai.
                     4.  MUSÉE  INTERNATIONAL.  —  L'Exposition des Associa-
                    tions Internationales a été transformée en Musée International.
                    La notice générale sur le Musée et ses deux premiers catalogues
                    ont fait l'objet d'une distribution gratuite aux Associations Inter-
                    nationales ; celles d'entr'elles qui n'auraient pas reçu ces publica-
                    tions   sont   priées   d'en   aviser   l'Office   central.   De   nombreuses
                    donations ont été faites au Musée, parmi lesquelles il y a lieu de
                    signaler les collections complètes élaborées par le Congrès Inter-
                    national de la Route et par l'Association Internationale de la








                                       — 2 —
                Tuberculose. Presque toutes les Associations qui ont envoyé des
                objets   et   documents   à   l'Exposition   des   Associations   Interna-
                tionales,   se   sont   empressées   d'en   faire   don.   Le   gouvernement
                espagnol a fait don d'une collection importante à la section dos
                sciences administratives du Musée.
                 5. REVUE INTERNATIONALE DES CONGRÈS ET CONFÉ-
                RENCES. — REVUE DE LA VIE INTERNATIONALE. —
                La  Revue  Internationale  des Congrès et Conférences,  dont le but
                a été de suivre le mouvement des Congrès réunis cette année à
                l'occasion de l'Exposition de Bruxelles, a fait paraître  11  numé-
                ros. Pour continuer cette revue sur une base élargie, un projet
                a été étudié, dont les éléments sont ci-annexés. Les Associations
                et   Bureaux   permanents   des   Congrès   sont   instamment   priés   de
                bien vouloir faire connaître leur avis au sujet de la publication
                de  cette  revue,   en  répondant  à  l'enquête  dont  le  questionnaire
                est ci-annexé.
                 6.  LISTE DES CONGRÈS DE  1910. —  Le Congrès Mondial
                a   émis   le   vœu   de   voir   publier   une   liste   des   Congrès   futurs.
                Les Associations sont priées de faire connaître les dates de leurs
                prochaines réunions.
                 La liste des Congrès de  1910  a paru dans la  Revue, Interna-
                tionale des Congrès et des Conférences. La liste des Congrès anté-
                rieurs à  1910  figure dans  l'Annuaire, de la Vie  Internationale,
                édition 1908-1909.

                 7.  ANNUAIRE DE LA VIE INTERNATIONALE 1910-1911.
                —  Cet annuaire est en préparation. Il paraîtra dans les premiers
                mois de 1911.

                 S. BIBLIOTHÈQUE INTERNATIONALE ET SERVICES DE
               DOCUMENTATION.  —  Ces services sont établis en connexion
               avec ceux de l'Institut International de Bibliographie. La Biblio-
               thèque Collective Internationale est en voie de développement.
               Les   collections   déposées   par   divers   organismes,   comprennent
               actuellement environ  60,000  unités bibliographiques. Le Réper-
               toire Bibliographique Universel comprend dix millions de notices
               classées  ;  le Répertoire de Documentation (texte et images pho-
               tographiques) comprend environ  10,000  dossiers et  200,000  pièces.
               Une   partie   spécialement   consacrée   aux   institutions   interna-
               tionales a été constituée au sein de ces diverses collections et
               c'est par les soins de l'Office central qu'elle est élaborée. Le cata-
               logue  en  sera  publié   prochainement.   Les   Associations   sont
               invitées à compléter leur documentation propre au sein de cette
               organisation. Il est désirable qu'un fonds soit constitué par








                                        - 3 -
                chaque   Association,   fonds   comprenant   d'une   part   toutes   les
                publications   émanant   de   l'Association   (les   publications   elles-
                mêmes   ou   leur   liste   catalographique)  ;  d'autre   part,   toutes   les
                publications   concernant   l'objet   de   l'Association   (dépôt   des
                ouvrages de leur Bibliothèque ou catalogue de celle-ci).
                  9.  SERVICE DE  LIBRAIRIE  DES ASSOCIATIONS INTER-
                NATIONALES.  —  L'Office prépare l'édition d'un catalogue des
                publications des Associations, offertes en vente. Ce catalogue sera
                envoyé   notamment   aux   grandes   bibliothèques   publiques.   Les
                Associations sont invitées à faire connaître celles do leurs publi-
                cations  qu'elles  désirent  voir  figurer  au  catalogue  et  à  indiquer
                les   prix   de   vente   (avec   les   réductions   éventuelles   consenties   à
                l'Office). Quelques Associations ont remis en gestion à l'Office,
                les stocks de leur publication.

                 10.  LISTE   D'ADRESSES   DES   INSTITUTIONS   INTERNA-
               TIONALES.  —  L'Office a dressé une nouvelle liste des noms et
               des   adresses   des   Associations   Internationales,   avec   l'indication
               du   délégué   chargé   des   relations   extérieures   de   l'Association,
               conformément au vœu du Congrès Mondial, qui a demandé que
               chaque   Association   internationale   délègue   un   membre   qui   sera
               chargé,   d'une   manière   permanente,   de   la   gestion   des   relations
               de cette  Association  avec  chacune  des  autres,  avec  les  groupe-
               ments   d'Associations   et   avec   l'Office   central.   Les   Associations
               sont priées de faire parvenir d'urgence les renseignements néces-
               saires.   L'Office   tiendra   la   liste   d'adresses   à   la   disposition   des
               Associations   qui   lui   en   feront   la   demande,   dans   le   but   de
               faciliter les relations entre Associations.








                Bulletin à détacher et à renvoyer dûment rempli s. v. p.






                                   (Nom de l'Association)
                       RÉPONSE AUX QUESTIONS

                   posées parla circulaire du  10 Janvier 1911
                                de l'Office central
                                    [Publication n°  10]

                 1.  VŒUX  ET  RÉSOLUTIONS.  —  L'Association enverra-t-elle
               les résolutions do sa dernière session?





                 2.  LISTE   DES   CONGRÈS   DE  1911.  —  Où   et   quand   se
                réunira   la   prochaine   session   de   l'Association   ou   des   congrès
                qu'elle organise?





                 3.  SERVICE DE LIBRAIRIE DES ASSOCIATIONS INTER-
                NATIONALES.  —  Quelles publications l'Association désire-t-elle
                voir   figurer   sur   le   catalogue   du   Service   de   librairie?   (Indiquer
                aussi les prix.)







                  4.  LISTE   D'ADRESSES   DES   INSTITUTIONS   INTERNA-
                TIONALES.  —  A quelle adresse doivent être envoyées les cor-
                respondances et publications destinées à l'Association?








                                      — 6 —
                 5.  REVUE DE LA VIE INTERNATIONALE.—  a)  Le pro-
                jet d'une  Revue de la Vie internationale  doit-il être encouragé?
                b)  Quelle   critique   ou   suggestion   fait   naître   l'examen   du   pro-
                gramme?  c)  L'Association   est-elle   disposée   éventuellement   à
                coopérer   à  la  Bévue  en  lui   procurant   des   articles   de   fond  de
                membres et des communications pour les chroniques?  d)  L'As-
                sociation   est-elle   disposée   éventuellement   à   rendre   possible
                l'édition   de   la  Revue,  en   procurant   la   souscription   d'un
                certain nombre d'abonnements à  15  francs parmi ses membres,
                ou en participant à la constitution d'un fonds de garantie, non
                solidaire,   ne   devant   jouer   qu'en   cas   d'insuffisance   de   recettes
                et limité à un essai de deux années?











                 6.  COLLECTION   CENTRALE   DE   DOCUMENTS.   Depuis
               le dernier envoi fait à l'Office, n'y a-t-il pas de publications nou-
               velles ou non encore envoyées qu'il pourra recevoir?





                 7.  DÉLÉGUÉ DES ASSOCIATIONS POUR LES RELATIONS
               EXTÉRIEURES.  —  Quel est le nom du délégué de l'Association
               pour les relations extérieures, et à quelle adresse doit être envoyée
               la correspondance?

