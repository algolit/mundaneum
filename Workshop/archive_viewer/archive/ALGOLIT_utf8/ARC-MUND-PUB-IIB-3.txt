







                         J
                        !
                     Q£fice International        de
                       )
                         HOTEL RAVENSTEIN, BRUXELLES









                 DECIMAL CLASSIFICATION











                       Tables          geographiques









                                    --$o_?---'     --










                                        BRUXELLES

                           IMPRIMERIE  VEUVE   FERDINAND  LARCIER
                                      26-28,  RUE  DES  lIIINlMES
                                             1895






























































































                     Office International        de   Bibliographie


                         HOTELRAVENSTEIN, BRUXELLES









                 DECIMAL CLASSIFICATION











                       Tables          geographiques









                                   --.-?-'--











                                       BRUXELLES
                           IMPRIMERIE  VEUVE  FERDINAND   LARCIER
                                     26-28,  RUE  DES  MINlMES
                                            1895





























































                                                                                               1























                                                                       -
















                             TABLES      GEOGRAPHIQUES


    f

                         INDEX GEOGRAPHIQUE METHODIQUE

                    3   Geographic ancienne.

                      31   China.
                      32   Egyptia.
                      33   Judea.
                      34   India.
                      35   Medo-Persia,
                      351    Chaldrea.
                      352    Assyria, Nineveh, Niniva.
                      353    Media.
                      354    Babylonia.
                      355    Persia.
                      356    Partha.
                      357    Sassania,
                      358    Mesopotamia.
                      359   Sussana.
                      36   Kelts. Celtes .
                      37
                     .     Roma.
                      38   Grece.
                      391    Grecan  Archipelago, Archipel grec,
                      392    Asia Minor, Western.
                      393     »»      Eastern.
                      394    Syria, Arabia.
                      395    Asia, North-Western.
                      397    Africa.
                      398    Europa, Sud-Est.
                    4   Europa.
                      4  I  Scotland, Ecosse.
                      415    Ireland.
                      42   England, Angleterre.
                      43   Germany, Allemagne,  Deutschland.
                      431    Prussia.
                      432.1   Saxe .













 ..,..,  I
 ??--??=====================================(
                                         ¥  iii       .,.,.._......  -==  t"}  W  !¥riiWQ  @_.-









                               433    Bavaria.
                               434.4    Alsace, Elsass.
                               434.5    Lorraine, Lothringhen.
                               434.6    Baden.
                               434.7    Wurtemberg.
                               435.1    Hamburg.
                               435.2    Bremen.
                               435,4    Brunswick.
                               435.6    Westphalia, Autriche.
                               436    Oesterreich, Autriche,
                               437    Bohemia.
                               438    Poland, Pologne.
                               439    Hungary.
                               439.5   Bosnia.
                               439.6    Herzegovina.
                               44   France, Frankreich.
                               45   Italie.
                               458    Sicilia.
                               46   Spain, Espagne.
                               469    Portugal.
                               47   Russia.
                               471    Finland.
                               479    Caucasia,
                               48   Scandinavie.
                               481    Norway, Norge.
                               485    Sweden, Suede.
                               489    Denwark,
                               491    Seeland,
                               492    Hollande, Nederland.
                               493    Belgique.
                               494    Schweiz, Suisse.
                               495    Grece, Griechland.
                               496    Turquie.
                               497    Servia, Bulgaria, Montenegro.
                               498    Roumania.
                               499    Archipel grec.
                             5   Asia.
                               51   Chine.
                               52   Japon.
                               53   Arabia.
                               54   India.
                               55   Persia.                                                    1
                               56   Turquie  d'Asie.
                               57   Siberia.
                               58.    Afghanistan.
                               584   Turquestan.









                         588    Bal utchistan.
                         593    Siam.
                         596    Cambodge.
                         597    Cochinchine francaise.
                         598    Annam,
                         599    Tonkin.

                       6   Africa.
                         611    Tunisie.
                         612    Tripoli.
                         62   Egypt.
                         63   Abyssinia.
                         64   Maroc.
                         65   Algerie.
                         661    Sahara.
                         662    Soudan.
                         663    Senegambie,
                         664    Sierra- Leone.
                         665    Guinea.
                         666    Liberia.
                         667    Ashantee.
                         668    Dahomey.
                         669    Cote d'Or, Gold Coast.
                         672    Congo.
                         678    Zanzibar.
                         679    Mozambique.
                         682    Transvaal.
                         684    Natal.
                         685    Orange.
                         686    Kaffraria, Caffrerie,
                         687    Cape Colony.
                         69   Madagascar. Mauritius.

                       7   Amerique du Nord. North America.

                         71   Canada.
                         72   Mexico.
                         728    Amerique centrale, Central America.
                         728.1   Guatemala.
                         728.3
                                 Honduras.
    I                    728.4   San Salvador.
                                 Nicaragua.
                         728.5
                                 Costa- Rica.
                         728.6
                         729    Inde orientale, West India.
                         729.1   Cuba.
                         729.2   Jamaica.
                         729.3   Santo  Domingo.





                                                                                                 r.

                                                                                                 )
                           729.4    Hayti.
                           729.5    Porto Rico.
                           729.8    Barbados.
                                    Bermudas.
                           729.9
                           73  United States. Etats-Unis.

                         8   South America.  Amerique du Sud.
                           8I   Brazil.
                           82   Argentine.
                           83   Chili.
                           84   Bolivia.
                           85   Peru.
                           86   Colombia, Ecuador.
                           87   Venezuela.
                           88   Guiana.
                           89   Paraguay, Uruguay.

                         9   Oceania.

                           9I   Malaysia.
                           911    Borneo.
                           912    Celebes.
                           913    Moluccas.
                           914    Philippines.
                           92   Sonde (Iles de la).
                           921    Sumatra.
                           922    Java.
                           93   Australasia.
                           94   Australia.
                           946    Tasmania.
                           g5   New Guinea, Nouvelle-Gurnee,
                           96   Polynesia.
                           969    Hawaii.
                           98   Regions arctiques.
                           99   Regions antarctiques.

                                                                                                J


                                                                                              I
                                                                                            ?
    I.

    )




                         INDEX GEOGRAPHIQUE ALPHABETIQUE (*)

                    Abyssinia        63  Brunswick     4354 Germany          43
                    Afghanistan     581  Bulgaria       497  Gold Coast     669
                    Africa,  A.     397                     Grece, A         38
                    Africa            6  Caffrerie      686  Grece          495
                    Algerie          65  Cambodge       596  Grecian  Archipelago,A 391
                    Allemagne        43  Canada          71  Griechland     495
                    Alsace         4344  Cape Colony    687  Guatemala     7281
                    Amerique du Nord  7  Caucasia           Guiana           88
                                                        479
                    Amerique centrale  728  Celebes     912  Guinea         665
                    Amerique du Sud   8  Celtes, A       36
                    Angleterre       42  Central America  728  Hamburg     4351
                    Annam           598 Chaldeea, A     351  Hawaii         969
                    Arabia,  A.     394  Chili           83 Hayti          7294
                    Arabia           53                     Herzegovina
                                        China, A         31                4396
                    Archipel grec, A.  391  Chine        51  Hollande       492
                    Archipel grec   499  Cochinchine francaise  597  Honduras  7283
                    Argentine        82  Colombia        86 Hungary         439
                    Artic Regions    98  Congo beige    675
                    Ashantee        667  Congo francais  672  Iceland       491
                    Asia, A,  Ns-Western  395  Costa Rica  7286  lndes orientales  729
                    Asia Minor. A.      Cote d'Or       669  India, A        34
                        Western      392  Cuba         7291  India           54
                        Eastern      393                    Ireland         415
                    Asie              5                     Italia           45
                                        Dahomey         668
                    Assyria, A.     352  Denmark
                    Australasia      93  Deutschland    489  Jamaica       7292
                    Australia        94                  43  Japon           52
                    Autriche        436                     Java            922
                                        Ecosse
                                                         41  Judea, A        33
                    Baden           4346  Ecuador        86
                    Babylonia, A.    354  Egypte, A      32.  Kaffraria     686
                    Baluchistan      588 Egypt           62  Kelts, A        36
                                        Elsass
                    Barbados        7298                4344
                    Bavaria          433  England        42  Liberia        666
                    Belgique        493  Espagne         46  Lorraine      4345
                    Bermudas            Etats-Unis       73
                                    7299                    Lothringhen    4345
                    Bohemia          437  Europa, A, Sud-Est  398
    J               Bolivia          84  Europa           4  Madagascar      69
                    Borneo
                                     911                    Malaysia         91
                    Bosnia          4395  Finland       471  Maroc           64
                    Bremen          4352  France         44 Maurice, He      69
                    Bresil           81  Frankreich      44 Media, A        353
                      (oJ La lettre A qui est joint. it certains noms de pays indique qu'il s'agit de geographie ancienne,




     l.










                      Medo-Persia, A   35  Poland         438 Sonde, Iles de la  92
                      Mesopotamia,  A  358 Pologne        438  South America    8
                      Mexico           72 Polynesia        96 Spain            46
                      Moluccas        913  Porto Rico    7295  Suede          485
                      Montenegro      497 Portugal        469  Suisse         494
                      Mozambique      676  Prussia         43  Sumatra        921
                                                              Susiana, A      359
                      Natal           684 Regions arctiques  98  Sweden       485
                      Nederland       492 Regions antarctiques  99 Syria, A   394
                      New Guinea       95  Roma,A          37
                      Nicaragua       7285  Roumania      498  Tasmania       946
                      Nineveh. A      352  Russia          47  Tonkin         599
                      Niniva, A       352                     Transvaal       682
                      Norge           481  Soudan         662 Tripoli         612
                      North America     7  San Salvador   7284  Tunisie       611
                      Nouvelle Guinee  95  Santo Domingo  7293  Turkestan     584
                      Norway          481  Sahara         661  Turquie        496
                                          Sassania, A     357 Turquie  d'Asie  56
                      Oceania           9  Saxe          4321
                      Oesterreich     436  Scandinavie     48  United States   73
                      Orange          685  Schweiz        494 Uruguay          89
                                          Scotland         41
                      Paraguay         89 Senegambie      669  Venezuela       87
                      Parthia, A      356  Servia         497
                      Persia, A       355  Siam           593  West Indies    729
                      Persia           55  Siberia         57 Westphalia     4356
                      Peru             85  Sicilia        458 Wurtemburg     4347  .
                      Philippines     914  Sierra Leone   664  Zanzibar       678





































































































































































