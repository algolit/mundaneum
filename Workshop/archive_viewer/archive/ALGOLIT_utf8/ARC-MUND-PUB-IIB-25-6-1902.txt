









                            Inst:'tut Iriter-rratiorral de Bibliographie
                        PUBLICATION N' 25           IND1CE BIBLIOGRAPHIQUE [025·4]



                         GLASSIFICATION             BIDLIOGRAPffiijUE


                                          DEOI?1ALE

                                                   '9'-
                                                _.
                             TABLES GENERALES REFONDUES
                                       etablies en  vue de la publication  du
                            Repertoire Bihliographique           Universel




                                      EDITION      FRANQAISE
                                           publiee avec Ie concours du
                                  BUREAU BIBLIOGRAPHIQUE      DE PARIS
                                                   et du
                                       TOURING CLUB DE FRANCE


                                   F'?SCICULE                 N°   6
                                       Tables de la division  [629.1]
                          Industries de la Locomotion
                                   [LOCOMOTION PAR TERRE ET PAR EAU, AEROSTATION 1














                                 . INSTITUT INTERNATIONAL DE BIBLIOGRAPHIE
                            BRUXELLES, I,  rue du Musee.   PARIS,44,  rue de Rennes
                                          ZURICH, 39,  Eidmattstrasse.
                                                  1902












                                          TABLES GENERALES DE LA
                               CLASSIFICATION     BIBLIOGRAPHIQUE DECIMALE
                                PUBLICATION N" 25 DE L'INSTITUT INTERNATIONAL DE BmLIOGRAP}lIE
                              L'Edition  francaise, developpie et  refondue, des T ABLESGENER/J-LES de
                            la Classification
                                        bibliograPhique decimale, a ete  preparee par l'Institut ] nterna­
                            tional de  Bibliographie, avec le concours d'un  grand nombre de collabora­
                            teurs et  plus specialemen  du Bureau  bibliographique de Paris. Cette
                            edition reunira les diverses editions  fragrnentaires publiees jusqu'a ce jour;
                            elle
                                paraitra par fascicules successifs. Afin de ne pas entraver la bonne
                            marche de  l'impression, I'ordre  suivi  dans la  publication  sera celui dans
                            lequel les  manuscrits  prepares par les collaborateurs  parviendront a
                            l'Institut.
                             ,Quand la collection sera  complete, il sera facile de faire relier les fasci­
                            cules' en un  volume, dans l'ordre methodique des matieres et conformement
                            au tableau  d'arrangement qui  sera  public ulterieurement. Pour faciliter
                            cet  arrangement, les tables n'ont pas reyu de  pagination continue, mais les
                            introductions et les index  alphabetiques  ont ere  imprirnes  sur  papier teinte
                              On  peut des a  present souscrire a I'edition  complete,  au  ,
                            port compris.                                  prix de 20 fr.,
                                                                           .
                              L'ouvrage complet porte le nO 25 des  publications de I'Institut Interna­
                            tional de  Bibliographie;  en. outre, les divers fascicules sont numerotes
                            separernent. Les  souscripteurs recevront  separement les divers fascicules:
                            au moment de leur  publication.
                                             ONT PARU A CE JOUR:
                                                                     Classification decimale
                            Fascicei» nO I -ltdroduction /!ellerale aux Tables de la
                              et Resume des  Regles de la  Classification decimale,
                            Fasciculo ttO 2  -  Table des Subdivisions cotnmuues,
                            Fascicule nO 3  -  Tables de la division  [5-3] Sciellces  physiqttes (Mecanique
                              raiionneile et  Physique},
                            Fascicule nO  4  -  Table de la division  [77]  Sciences  photogl'aphiqttes.
                            Fasoicule nO 5  -  Table de la divisio}t  [6?r.3] Electrici# industrielle.
                            Fascicule nO 6  -  Table de la division
                                                         [629. I] I1tdztst1'i?s  de  la Locomotion
                              (LoCjJ1notiMz par terre et  par eau, aerostatiow),
                            Fascicule nO  .7  -  Table de la division  [79] Sports (Tourisme, Cyclisme,
                             A uto1:Jwbilisme).
                           Fascioute nO 8 __:_  Table de la division  [34] Droit.
                           Fascicule nO  -  Table de la division
                                     9                   [6I5] Therahutique.­
                           Fascicule nO IO  -  Table de la d'ivision  [6I6] Pathologic interne.
                                           BULLETIN ET ANNUAIRE
                           DIJ L'INSTI.TUT INTERNATIONAL DE BIBLIOGRAPHIE
                                             -
                                                                                  .
                                                             BIBLIOGRAPHIE.lComptes rendus
                             BULLETIN DE L'INSTITUT INTERNATION?L DE
                           des travaux de l'Institut; etudes documentees et informations relatives a
                           la
                             Bibllographte ; analyses d'ouvrages .bibliographiques; renseignements
                           speciaux  sur  tout  ce qui  concetne  l'organisation -internationale de la
                           Bibliographie scientifique et la formation du  Repertoire bibliographique
                           universel.] Paratt  en six fascicules in- 8°  (Q.25 X 0_16), br., par  an. Les
                           abonnements sont faits pour  un an, a
                                                          partir du ler janvier. Prix: Union
                           postale: fr. 15 (en cours  depuis 1895). Distribue  gratuitement auxmembres
                                                                     part d'articles  impor­
                           de l'Institut. Il a 'ete publie egalement divers tires a
                           tants parus dans ce Bulletin et qui figurent dans la collection des
                           tions de l' Institut.  '                            publica-
                             ANNu..uRE DE L'INSTITUT INTE?NATIONAL
                                                             DE  BIBLIOGRAPHIE, pour I'annee
                           1899.  -  Bruxelles, au siege de l'Institut, 1899, grand in.,_8° (0.25 X 0,16). br.,
                           106 p. (Extrait du Bulletin de l'Institut     Prix: 1 franc.
                                                          1899,-P. 73.)












                       Institut International de      Bibliographie
                   PUBLICATION N' 25           INDICE BIBLIOGRAPHIQUE [025.4]



                    CLASSIFICATION BIBLIOGRAPHlijUE


                                     DECIMALE

                                           -.?.-

                        TABLES GENERALES REFONDUES

                                  etablies en  vue de la  publication  du
                       Repertoire Bihliographique           Universel



                                 EDITION FRANyAISE
                                      publies  avec Ie concours du
                            BUREAU BIBLIOGRAPHIQUE DE PARIS
                                             at du
                                 TOURING CLUB DE FRANCE


                             F'A.SCICULE                N°   6
                                  Tables de la division [629.1]
                     Industries de la Locomotion

                             ILOCOMOTION PAR TERRE ET PAR EAU, AEROSTATION J













                            INSTITUT INTERNATIONAL DE BIBLIOGRAPHIE
                       BRUXELLES, I,  rue du Musee.·  PARIS, 44,  rue de Rennes
                                    ZURICH, 39, Eidmattstrasse.
                                             1902




























































































                                         LOCOMOTION




                             Locomotion.             Industrie         des
                               Transports         .:
                                  La locomotion se subdivise en trois grandes subdivisions:
                                       62g.II  Locomotion  sur routes.
                                       629.12  Locomotion par  eau,
                                       629.r3  Navigation aerienne.
                               Les tables des subdivisions commune'S de forme, lieu, langue, temps etc.(Tables
                                 I, II, III, IV et V) sont applicables dans toute I'eteudue de la division 629.1,
                                 ainsi que  la  table des divisions de formes speciales  caracteriseee par le
                                 signe (00) qui est applicable  a. toute la division 6; mais on ne doit utiliser
                                 ces subdivisions communes que lorsque des divisions specialee equivalentes
                                 n'ont pas ete prevuee.
                               On pourra  faire  usage aussi, dans toute l'eteudue de la division 629.1, des
                                 subdivisions analytiques speciales dont la table est donnee ci-apres, 629.10
                                 et qui so.it caructerisees par  le  zero simple, sans parentheses.  On les em­
                                 ploiera pour specifier certains points de vue sous lesquels on peut envisager
                                 les sujets traites,  ou certains details des organes de construction.
                               La designation des materiaux entrant dans la composition des organes, ainsi
                                 que celle de ... matieres premieres utilisees dans l'emplci du materiel pourront
                                 etre specifiees a l'aide des subdivisions communes 0023 et 0024.
                    629·I.O    Questions generales. [Subdivisions analytiques spe­
                                 ciales.]
                         .01     Organes el emcntaires  entrant dans 1a construction des
                                  appareils de transport.
                                  Ces organes sont differents sirivant le but que les appareils  out pour objet
                                   de realiser.  On  trouvera  aux trois  subdivisions principales 62g.II,
                                   629_12  et 629.13 les  subdivisions analytiques speciales relatives  aux
                                   organes des appareils propres a. chaque genre de locomotion .
                         . 02    Moteurs mecaniques speciaux  aux vehicu1e?  de  toute
                                  nature. Differ ents types.
                                     Les moteurs mecaniques, autres que  ceux destines aux vehicules, sont
                                      classes sous l'indice 621 et ses subdivisions.
                         .021      Moteurs a vapeur.
                            . 1      Moteurs proprement  dits  .
                            . 2
                                     Generateurs .
                            .3       Foyers et bouilleurs.  '
                            ·4       Accessoires speciaux.
                         . 022     Moteurs a gaz ou a air  comprime  .
                            . I      Moteurs proprement dits  .
                            . 2      Reservoirs  .
                            . 3      Appareils de compression  .
                            ·4       Accessoires speciaux.
                         . 023     Moteurs a petrole ou a combustible Iiquide  .
                            . I      Moteurs a essence ou a a1cool.
                            . 2      Moteurs a huile lourde .•
                            . 3      Carburateurs et brule urs  .
                            ·4       Accessoires speciaux.











                          629.1.024             LOCOMOTro?


                            629.1.024     Moteurs electrtques.
                                   . 1      Dynamos  .
                                   . 2      Accumulateurs  .
                                   . 3      Cornmutateurs  .
                                   .4       Accessoires speciaux.
                           62g.II    Locomotion  sur  routes.  Construction  de  vehicules
                                       terrestres.
                                         Moyens de transport parvterre  uutres que les chemins de fer et les tram­
                                          ways; pour ces de-niers, voir 625.
                                              629. lID  Organes elementaires entrant dans la contruction des
                                                     appareils.
                                              629.III  Moyens de transport primitifs.
                                              629.112  Voitures a traction animale.
                                              6zg.1I3  Voitures a traction mecanique. Automobiles.
                                              629.117  Vehicules legere a. plus de deux roues. Voiturettes.
                                              629.II?  Vehicules .legere a deux roues  au plus. Cycles. Veloci-
                                                     pedes.
                              · II.OI    Organes elementaires entrant dans la construction des
                                          appareils de transports par terre.
                                            Ex.  : 629.113.6.016 Freins pour voitures a moteurs electriques.
                                 .0Il     Organes  formant la  charpente constituti ve des a ppareils
                                              Chassis. Cadres, Entretoises, COtes, Fonds. etc.
                                 . 012    Organes de support et de roulements  .
                                            Essieux, Roues, Baltes de roues, Jantes, Raies, Moyeux, Bandages,
                                             Glissieres, Patins, Rouleaux.
                                 . 013
                                           Organes d'attelage et de traction .
                                            Brancards, Limons, Traits, Palonniers,
                                 • 014    Organcs moteurs  .
                                            Leviers, Pedales, Manivelles, Chaines, Eng-renag'es"
                                            Pour les moteurs mecaniques, voir 629.1.02.
                                 . 015    Organes d'appui, de conduite et de direction  .
                                            Renes, Guides, Selles, Sieges, Guidons, Volants.
                                 . 016    Org anes de retenue et  d'enrayage  .
                                            Sabots d'eurayage, Freins, Cliquets, Bequille.
                                 . 017    Organes d'entretien et de  proprete  .
                                            C16'S, Burettes, Carters, Garde-boue, etc.
                              ,I!.OI8     Organes accessoires, d'avertissements ou d'appel.
                                            Signaux, .Lanternes.
                                 019      Autres organes  .
                                 .
                           62g.II1     Moyens de  transp?rt primitifs.
                              . Il!.I    Appareils de transport portes a. bras ou a. dos d'hommes  .
                              . II!.II     Pour fardeaux .
                                            Hottes, Crochets, Brancards.
                              · II!. 12    Pour les personnes ..
                                            Civieres, Palanquins, Chaises a. porteurs.
                              · Il!.3    Appareils de transport a. roues, tires a. bras.
                              . r r r.Br   Pour les fardeaux  .
                                            Brouettes, Diables, etc.
                              . IlL32      Pour les personnes  .
                                            Vinaigrettes, Pcusse-pousse, Fauteuils roulants .
                               • IlLS
                                         Appareils de transport portes directement par les essieux,











                                        LOCOMOTIO.::-:T


                   62g.IIL7      Appareils de transport par glissement.
                                   Traineaux.
                      · IlLS     Appareils de transport a rouleaux.
                      . I I2   Vehicules ou voitures de  transport  a traction animale .
                                 On pourra dans les divisions ci-dessous specifier la destination speciale des
                                  voitures par l'emploi du signe de relation suivi du numero indiquant
                                  l'objet considere.
                                      Ex.  : 629.Il2.5: 343.3 Yoitures cellulaires,
                      . II2.1,   Voitures a deux roues  .
                      . II2.3   Voitures a plus de deux roues  .
                      · II2.31    Pour Ie transport des fardeaux ou des marchanclises.
                                -
                      . II2.32    Pour Ie transport des personnes  .
                      · II2.S   Voitures        pour le  transport  en  commun des
                                        speciale?
                                  personnes.
                                    Omnibus, Diligences, Breaks.
                      .112.6    Voitures speciales pour le transport des fardeaux et
                                  marchandises.
                                    Haquets, Camions.
                      . II2·7   Voitures pour services speciaux  .
                                  Roulottes.
                      · I I3   Voitures a traction mecanique. Automobiles.
                                Voitures automobiles  sur routes  en general. Voitures  a vapeur et  autres
                                 et leurs moteurs.
                                Pour les vehicules allant sur rails, chemins de fer et tramways, voir 625 et
                                 656.
                                Pour les voiturettes, cycles et motocyc1es, voir plus loin 629.II7 et 629.118.
                                On pourra dans cette division ainsi que dans les suivantes, jusqu'a 629.110 in­
                                 elus, etablir a l'aide du tiret les subdivisions suivantee d'apres la destination
                                 et la nature des veb icules :
                                    -  r Locomotives routieres.
                                    -  2 Tracteurs.
                                      3 Trains sur route.
                                    -
                                      4 Voitures lourdes pour marchandises.
                                    -
                                    -  5 Voitures pour transport en commun
                                    -  6 Voitures de famille.
                                      7 Voitures legeres.
                                    -
                                    -  8 Voitures pour courses.
                                       A?tres
                                    -      voitures.
                                      9
                      .II3.!              voitures a moteurs rnecaniques.  Moteurs a
                                 ?rel11ieres
                                  bras, a ressorts, etc. Premiers  essais de moteurs a
                                  .vapeur.
                      .II3.2    Voitures a vapeur.
                      · II3.21    Voitures a vapeur avec foyer au charbon.
                                     Leblant, de Diou, Bailee, Scott, Rowen, Serpollet.
                      . II3.22    Voitures a vapeur avec foyer  au petrole  .
                                     SerpolJet, e?c.
                     -.II3.23     Voitures a vapeur sans foyer, a eau surchauffee.
                                     Lamm, Francq, etc.
                                Voitures a air et a gaz corriprimes et leurs moteurs.
                                  Voitures a air com prime et a reservoir independant.
                                     Mekorski, etc.











                                                 LOCO]\!IOTION


                                           Voitures a air comprime a reservoir alimente en route
                                             par canalisation speciale.
                                              Popp, Conte, etc.
                               . II3·43    Voitures a gaz d'eclairage comprime  .
                               . II3·44    Voitures a acide carbonique comprime  .
                               . II3.5    Voitures a petrole ou a combustible liquide  .
                               . II3.51    Voitures avec moteurs a essence de petrole  .
                                · II3.52   Voitures avec moteurs a huile lourde.
                               . II3.6    Voitures a moteur electrique  .
                               .lr3.61     Voitures de differents  systemes a prise de courant
                                               exterieur  .
                                . II3.62   . Voitures a conducteurs aeriens et trolleys  .
                                · II3.63    Voitures a conducteurs souterrains,
                                . 113.64   Voitures a contacts superficiels  .
                                · II3.65    Voitures a accumulateurs.
                                . 113·7   Voitures mixtes a moteurs combines  .
                                . 113.71    Voitures a petrole et a electricite .
                                .  117   Vehicules legers  a plus de deux roues  .
                                · II7·1   Premiers essais de vehicules leg ers a plus dedeuxroues.
                                . II7·2   Vehicules legers mus par l'homme .
                                             Tricyles, Quadricycles.
                                · II7·3   Vehicules legers a moteurs a petrole.
                                             Motocycles, Voiturettes, Tricycles, Quadricycles
                                . II7·4   Vehicules leg ers a moteurs electrrques  .
                                . Il8    Vehicules  legers  a deux roues au plus  .
                                          Cycles, Velocipedes.
                               · I IS. I  Premiers essais de velocipedes.
                                             Celeriferes, Draisiennes,  etc.
                                . 11S.2
                                          Velocipedes a une seule place mus par l'homme .
                                              Cycles, Bicycles.
                                . tlS.3   Bicyclettes  .
                                . IIS·4   Velocipedes a plusieurs places mus par l'homme  .
                                             Tandem, Sociables, Triplettes,  etc .
                                . 1·IS.5  Velocipedes a  une seule place et moteur mecanique.
                                . IIS.6   Velocipedes a plusieurs ?aces et moteur mecanique  .
                             629.12    Locomotion par  eau.  Moyens de 'transport par voie
                                         fiuviale et  par  voie de mer.
                                           Pour la theorie de la navigation, voir 527 j pour la pratique commerciale,
                                            voir 656.
                                .1:1.01   Organes elerncntaires entrant dans la  construction des
                                            appareils de transport par eau.
                                .12.011     Org anes formant la charpente constitutive des  ap­
                                              pareils.
                                                Co que, Ossature, Banes, Carlingues, Bordages, Doublages
                                                Pour les hlindages des navires, voir 623.9.
                                            Org anes de stabilite  .
                                . 12.012
                                              Flotteurs, Balanciers, Quilles.












                                        LOCOMOTION


                   62g.12.014     Organes de propulsion".
                      . 12.014.1    Organes de propulsion a main  .
                                      Rames, Pagayes, Gaffes.
                      . 12.014.2    Organes de propulsion a voiles  .
                                      Mats, Vergues, Voiles, Greement.
                                    Organes de propulsion mecaniques.
                                      Roues, Helices, Propulseurs divers.
                                      Pour les machines motrices, voir 62I.
                      . 12.015    Organes de manoeuvre ou de direction .
                                    Gouvernail, Barres.
                      . 12.016    Organes de retenue, de protection et de sauvegarde  .
                                    Ancres, Bouees.
                      . 12.018
                                  Organes de signaux et d'avertissement .
                                    Signaux optiques, Pavilions, Signaux  senores.
                      . I2.01g    Autres organes  .
                      . 121    Moyens de  transport primitifs par eau  .
                                 Radeaux, Cbalands, Bateaux de  riviere reruorques, a r?l.111eS ouu voiles.
                      . 122    Bateaux de riviere  .
                      . 122.1    Bateaux de riviere amateurs mecaniques  .
                      .122.11     Bateaux a moteurs mecaniques mus par l'hornme au
                                    les animaux.
                      . 122.12    Bateaux a moteurs a vapeur a roues  .
                      . 122.13    Bateaux a moteurs a vapeur a helices  .
                      . I 22. 14  Bateaux a moteurs  a utres que la vapeur et a propul­
                                    scurs de divers systernes.
                      . 122.15    Bateaux a traction par chaine noyee. Bateaux toueurs  .
                      . 122.16    Bateaux a traction par l'electricite .
                      .123     Batiments de mer.
                      . 123. I   Batiments de mer a rames et it voiles  .
                                  Pour les batiments legers et 1.1. navigation de plaisance, voir 1"29.125
                      .123.II     Batiments de  mer anciens  ou de mcdeles prirnitifs a
                                    rames.
                                      Galeres, Triremes, Radeaux.
                      . 123.12    Batiments de mer anciens a voiles .
                                    Caraques, N efs, Galions ... Caravelles.
                      .123.13     Batiments de mer a voiles de fort tonnage. Batirrients
                                    a un ou plusieurs ponts.
                                      Vaisseaux, Fregates,  etc.
                      . 123.14    Batiments de mer a voiles pour usages speci aux .
                                    Voiders, Cargo-boats, Petroliers  ,
                      . 123.15    Batiments de servitude  .
                                    Chalands, Bugalets, Citernes flottantes,  etc.
                      . 123.2   Batiments de mer a moteurs mecaniques  .
                                  Pour les batirnents legera et la navigation de plaisance, voir 629.I25.
                      .123.21     Batiments a vapeur a roues.'
                      . 123.22    Batiments a helices  .
                      .123.23     Batiments speciaux pour le transport rapide des mar­
                                    chandises et des passagers.
                                               "
                                      Paq uebots,













                                                  LOCOMOTION


                             629.123.24     Batirrients sp eciaux pour  les marchandises.
                                              Cargo-boats a vapeur, Petroliers.
                                . 123.25    Batiments de servitude  .
                                              Remorqueurs.
                                .125     Batiments legers pour  la navigation de  plaisance  et
                                           embarcations.
                                . 125.1   A rames et a. voiles  .
                                . 125.II    Batiments legers de types anciens ou primitifs  .
                                              Pirogues.
                                . 125.12    Yachts a. voiles  .
                                . 125.13    Canots, Chaloupes  .
                                . 125.2    A moteurs mecaniques  .
                                .125 .21    Types anciens de batiments leg ers a. moteurs meca-
                                   .
                                               niques.
                                .125.22     Yachts et petits batiments a.  vapeur.
                                :125.23     Chaloupes a. vapeur.
                                . 125.24    Chaloupes a. petrole  .
                                . 125.25    Chaloupes electriques  .
                             629.13    'Navigation aerienne. 'Aeronautique. Moyens de trans­
                                          port par l'air.
                                            Pour la theor ie, voir 533.6 Aeronautique.
                                .13.01    Organes element aires entrant dans, la construction des
                                            appareils de transport aerien.
                                  .01I      Organes formant la  charpente constitutive des aeros­
                                             tats.
                                               Enveloppes, Ballons, Nacelles, Filets.
                                  .012      Organes formant la charpente constitutive des appareils
                                              d'aviafion ou aeroplanes.
                                                Cadres, Chassis, Ailes .
                                  . 014     Org anes de propulsion et d'orientation.
                                              Helice, Gouvernail, Stabilisnteurs, Equilibrantes.
                                  . 015     Organes de manoeuvres et d'aiterrissage  .
                                              Freins, Ancres, Soupapes.
                                  . 019     Appareils accessoires divers  .
                                .131     Premiers essais de navigation aerienne.'
                                           Aerostats, Mcntgolfieres.
                                .132     Appareils plus legers que l'air.  Ballons libres  a  gaz
                                           hydrogens.
                                . 132.1    Ballons captifs  .
                                . 132.2    Ballons dirigeables  .
                                .135     Appareils dits plus lourds que l'air.
                                .135.1'
                                           Appareils d'aviation .
                                . 135.2    Aeroplanes  .
                                • 135.3    Cerfs-volants.













                                   MANUELS POUR L'USAGE
                     DES REPERTOIRES BIBLIOGRAJ;iHIQUES           SPECIAUX




                      L'Institut  a prepare  une collection de MANUELS pour l'usage et la
                    formation des repertoires bibliographiques de chaque science particuliere,
                    Ces manuels comprennent,  outre la Fartie des tables de classification
                    bibliographique propres a la science envisagee, un expose general  des
                    principes de la classification, des regles pour la redaction des notices
                    bibliographiques,  des regles  pour  la publication  des  recueils biblio­
                    graphiques  et la formation des repertoires  sur fiches, des  regl?s  pour
                    la cooperation  au Repertoire bibliographique  universel  et enfin des
                     conseils pratiques pour l'organisation  des bibliotheques,  la formation
                    de  leurs catalogues  et le  class.ement  des  ouvrages  sur  les  rayons  .
                     Ces manuels sont destines  aux specialistes de chaque branche des  con­
                    •
                    naissances. Ils  reproduisent, sous une forme pratique, les divers documents
                    deja parus ailleurs, pour la preparation et l'emploi des recueils et  _des
                    repertoires bibliographiques speciaux,
                      Les divers manuels de la collection  se vendent separement, Chaque
                    manuel forme lin numero specialdes publications de l'Institut Interna­
                    tional de Bibliographie.

                                     ·ONT PARU A CE JOUR:

                     Publication 1,d 26.  -  Manuel pour l'usage  du Repertoire bibliograpkique  des
                       Sciences  Physiques  [0.25.4:  53f      Pri? :  2 francs.
                     Publicati01, 1,° 40.  -  Manuel pour l'usage  des  Repertoires bibliograjkiques
                       [025·4]·                              •  Prix: 2 fr?ncs.

                    PztbliccdioJ> no14I.  -  Manuel pour l'usage  du  Repertoire bibliograPk?que  des
                       Sciences Agricoles [025'4: 63].         Prix : 5 francs ..
                     Public-ation nO  ?5.  -  Mal1uel  pour l'usage  du Repertoire bibliograpkique  des
                      SCiences Photogr.aphiques [025·4: 77].    Prix: 2 francs.
                     Publication no  48.  -  Manuel pour Tusag«  du Repertoire bibliograpkique  de la
                       Locomotion et des Sports [025.4: 629·IJ + [025·4: 79].
                                                               Prix: 2 francs.









































                                       L'AuXIl;IAIRE  BIBLI()GRAPHIQUE
                                       ·
                             IMPRIMERIE DE L'INSTITUt INTERNATIONAL DE BIBLIOGRAPHIE
                                        RUE V YDT, 70, BRUXEJ,LES.

