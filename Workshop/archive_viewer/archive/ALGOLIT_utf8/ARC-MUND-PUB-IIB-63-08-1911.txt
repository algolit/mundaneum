PROSPECTUS

PUBLIÂ­

CATION
NÂ° 6ï¿½
-Â·11"
-

,(.........

...

...

-,ï¿½....

.

,;::,

,l'1nstifut : <Perfactiom<ler, Â·de"'elo:Pji>elÂ·.e1Jl,ulÂ·i.flE\lJlesÂ· 'Netllo<iles'en matiere de Iiibliographie
et dï¿½Ld0cuh1ï¿½-Q:ta_tiGh!
Organiser la 'cï¿½QPï¿½:rï¿½t\on s9ienthfiqLl.e ï¿½.ternaï¿½ionalï¿½ entre g.rqupeï¿½ de
specialites di.verses en vue d'eIaborer, suivant un plan d'eiisemble, des tr-avaux embrassanf les
branches
de
Ia
documentation
et specialemen.t un Repertoire BibliographiqueUniversel,
diverses
Biit de

-

__

-

de
A

de

E,tabl'iI"tUl, centre .intematignal pO,ur la coordinatlon
thus 1es travai:lleilrs'intellectuals

par'

voie de

,tel.

travaux at la conservation

en or iÂ­

M7ettl'" l',J,sage de'heu!":.ci it'la disposition
cemmunicafjoh, de copie om â‚¬Ie publication.

gina;)'aï¿½s Cteperï¿½c>ire$ e't ael; c011ï¿½,ctions 'de' clbdumimts

-,

--='

-

c",tte fin multiplier en.tous p,ays Ies services de documcntacion e.t les n,ettr'l en relation perrnanerrte die-<:hï¿½nqe et de,,ï¿½ra':Tï¿½il ,,11 I'jrrtermed iaire d'Ulï¿½ service international.
,ï¿½
,

.

<,

,I.

Pr@g'ramme

et'

organisation geri.er-ale.

'Tra:valfï¿½, cï¿½l[ections, serv(ces', ï¿½ï¿½o'ï¿½'ration.
III. Methodes.

ï¿½'

IV. Classification.
,

t

.r

4,.
"

'.

-',

Â·ZUffiUCHÂ· :
Hof'snrasse.

4ï¿½

t,

Manuel de l'Institut International de Bibliographie
.

.

International

L'Institut

de
ler

Bibliographie et de
Congres International

Documentation
de

Bibliographie
developpe successivement par les Congres de 1897, 190.0. et 1910., --:- 11
a pour objet I'etude des questions relatives aux livres, l'organisation
internationale en cette matiere, I'etablissement et la generalisation des
a

13M

Ionde,

en

1895, par Ie

et

methodes unifiees et la formation des collections documentaires centrales.
A cet effet

:

a) L'Institut reunit par les liens d'une mutualite intellectuelle basee sur
la cooperation et I'eohange et s'etendant it tous les pays et it toutes les
branches du savoir, les organismes ayant des buts de science, d'etude,
d'enseignement ou de propagande: Societes savantes, Instituts, UniverÂ­
et en particulier les Associations internationales et
sites,
Bibliotheques
(etat

leur Office central

de la

cooperation it

ce

jour: 70.0. membres);

L'Institut elabore un Repertoire. Brbliographique Universel comÂ­
prenant l'inventaire de tous les impnimes (livres, brochures, articles de
periodiques), classe par nom.d.'auteur et par matiere et forme de la reunion
coordonnee des Bibliographies particulieres (ete.t du Repertoire BiblioÂ­
graphique Univeraef it ce jour: 10. millions de notices sur fiches);

b)

L?hrstitut forme et administre une Bibliotheque Collective IruernaÂ­
des collections deposees par les associations adheÂ­
rentes et tenue en relation avec les grand.es collections de tous les
Centrale
pays par la voie du pret iRternationaI (etat de Ia Bibliotheque

c)

tiorral:e, disposant

jam: 62, bibliotheques deposeesj:
d} L'Jnstitut concentre en une Encyclopedie Documeniaire Unioerselle,
formee de dossiers internationaux ouverts it chaque question d'actualiM,
Ies materiaux, textes et images, extraits in extenso des publications et
specialement ceux qui sont publies par les Editions et des Revues eoopeÂ­
rantes selon des methodes communes appropriees:
it

ce

e)
pour
leur

L'Institut formule des methodes internationales et unifiees ayant
objet la publication des livres et documents, leur classification,
catalogage ainsi que la formation et Ie service des collections.
*

*

*

general de l'Institut International de Bibliogmphie decrit
organisation, ses travaux, ses collections, ses services, ses methodes.
II ne comprend pas moins de 2,50.0. pages, dont Ia plus grande partie est
consaeree aux 'I'ablee de la Classification Bibliographique Decimals.
Le Manuel

son

instrument de travail it tous ceux qui ont a.
ou des dossiers d'etudes, des dossiers d'afÂ­
faires, des collections de Iivres, d'estampes, de photographies et, d'une
maniere generale, aux. institutions et aux particuliers qui veulent mettre
de l'ordre dans leurs papiers, imprimes ou manuscrits, et etablir leur
propre organisation ep connexion avec celle de l'Institut, tout en s'aidant
de ses travaux et de ses collections.
Le Manuel centralise les elements de caractere permanent qui ont 13M
publies dans le Bulletin de l'Institut ou dans ses publications separees
Ses diverses parties paraissent en editions successives tenues it jour. Elles
peuvent etre reliees en un seul volume ou divisees en plusieurs tomes
separes; elles peuvent etre acquises en bloc ou. en detail. Chaque fascicule
reeoit un numero invariable, lea editions successives sont indiquees pse
des lettres. (Ex. : fasc. nO 2a.) Eli outre, les feuilles sont datees,

II

.

se

recommande

comme

organiser une Bibliotheque

"

..

Dans Ie tableau suivant

on a

donne Ill. division des matieres qye

comÂ­

prendre, Ie _Manuel quand I'ceuvre aura recu son complet achevement,
On a indique actuellement les.partiea publiees et rassemblees en volume
et celles quiÂ· n'existent encore que SOhlS forme de publications separees ou
ir.'Ollt

encore paru que dans Ie Bulletin. Ce tableau
permettra, s'il y a-lieu,
de recourir a ces sources complementaires.
Le dl1anuel dans son ensemble, et certaines de ces parties, ont aussi
fait I'objet de resumes qu'on a mentionnes dans une coionne speoiale. En
les combinant avec les fascicules de certaines parties developpees on peut
former ae volonte, des manuels pour I'organisation de Ill. Bibliographie et
de Ill. Documentation de matieres speciales.
On a indique a Ill. suite du tableau les principales combinaisons de ce
genre .qu-il est loisible aux acheteurs de realiser,
Dans Ill. forme qui a ete donneo a I'edition, Ie Manuel et ses fascicules
separes offrerrt le maximum de facilites.
L'ouv'rage forme un volume d'environ 2,500 pages, relie en pleine perÂ­
caline, tranches avec entailles .et onglets facilitant la .consultation.
On peut. obtenir SUiI.' demande et moyennant nne majoration de
prix
de 10 francs, la reliure en volumes sepanes de Ill. Table methodique de la
Classification- et de I'Index alphabetique. de la Classification.
L'ouvrage complet se compose alors de trois volumes.
Les exemplaires sont relies au moment de la commande et
completes
alors a'C"ec les fascicules de Ill. dernieres edition parue.,
.

.

MANUEL GENERAL (Publication
..

port

nO

63.)

-

II

se

vend

au

prix

de 50

francs,

en-sus.

MANUEL' SPECiAL.

On formers des manuels speciaux en combinant
nO 63), nO 35: Organisation et methode;
nO 1 : Expose de Ill. Classification Decimale; nO 2 : Table des subdivisions'
communes: nO 34 : Index alphabetique general et (nOS divers) les fasciÂ­
cules relatifs a la classification de ehaque science.
les

De tels

de

-

fascicules (de Ill. Publication

precedes

manuels,

publications

.nO 45

:
;E?hotographie
(edition. 1897, .fr, 2)
..

1902, fr. 2ï¿½.,

d'introductions speciales, ont fait l'objet
reliees et preparees d'avance, Publication
(edition 190.0, 3 fr.). Publication n,0 24 : Physique
Publication nO 48: Locomotion et sports (edition

hors texte,

"

MANUEL ABREGE.
On forinera ad libitumÂ· des manuels abreges, dont
Ill. matiere resumee sera ,limitee a telle ou telle partie du Manuel general.
-

.

(Voir ta,ble;lue ci-dessus.)

On a reuni dans une publication brochee (Publication nO 65), les fasciÂ­
cules 35, la, 2a; une table abregee comprenant les 1,000
principalea
divisions (fasc. 16) et I'Index alphabetique general des 33,000 divisions.
(Fascicule 34.) Prix, broehe, 9 francs; relie, fro 10.50.

Tableau des matieres

comprises dans

Ie Manuel

(ou

a y

introduire).

FORME DE LA PUBLICATION
Paru en resume
Bulletin
comme publication
Nombre
separee
separee
de
Nombre
p"ges
Prix
NÂ°
de
Page
.

Publication

I

1
.Fasc.! Pl'i:l:J Prix' 1 Anneï¿½,1
Manuel

lVlATIERES

.

NÂ°

1'" PARTIE.
et organisation:
1.

-

Expose general.
Code pour I'organisa.....

-

.

pages

I

Programme

-

2.

I

tion de la bibliographie et de la documentation
ï¿½
.......

.

2' PARTIE.
Travaux.
Publications.
CollecSertions centrales.
vices.
Cooperation:
1.
Le Repertoire Bibliographique Universcl.
2.
Bibliotheque Collective
3.
Catalogue. des publications.Â·
4.
Liste de Ia Cooperatian
-

35

2.00

36

2.00

65

Ii

Epuise

176

106

78

2.00

98
T.II
fasc.5

1909

2.00

185

78

-

-

-

.

-

I'

-

II

if

-

...............

2.00

35

65

9.00

19

96

1.00

52

116

2.00

65

9.00

-

...........

-

...............

Methodes:
3" PARTrE.
1.
Expose general des
methodes documentaires
2.
Regles pour Ia publication
rationnelle;
a) des ouvr. scienti(iques; b) des Perio-

1911

140

60

-

-

.............

.

35

2.00

diques.
Materiel
3.

-

Regles

98
T.II
fasc.4

Regles,

-

.......

-

106

1908

72

1910

86

26

-1910

3

66

5

....

catalographi-

"lues internationales.
Classement par nom
dauteur
Regles pour la formation du Repertoire

98
T.II
fasc.2

...........

4.

176
I'

-

2.00

Bibliographique uni-"

n

Redaction
versel,
des notices. Publica;
-

ï¿½

tion
11.

-

...............

35

2.00

35

2.00

65

2.00

117

Regles pour l'organi-

sation des Bibliotheques

6.

-

..............

8

pour la forma-

Regles

..

tion 'du

Repertoire
Encyclopedrque de
documentation

des
blis

I'

et

etaconnexion

Repertoires
en

lui,

avec

II

II

.........â€¢.

7Â·. -.Regles pour I'organisation
tions

des

collec-

iconographi-

II

')_ues
Regles

8.

-

.pom l'organisation de la documentation adminis-

trï¿½tive publi,que

et

,

I'

privee
Regles pour I'organi.............

9.

-

Ii

I'

..............

II
II

..

98
T.Il
fa80.5

1910

112

21

1911

101

39

sation de la statisti-

internationale
que
des imprimes

II

109

.......

3.00
-

Classification
blbliographtque et documentaire
(classification

4Â· PARTIE.

-

decimale),

Voir

ci-eprea

tableau

special

des faaciculea.

78

2.00

NOTICE
SUR

LA CLASSIFICATION DOCUlVIENTAIRE DECIMALE
Pour Ie classement, par matieres ou sujets, des livres dans les biblioÂ­
theques, des notices bibliograpliiques ou catalographiques dans les
repertoires, index ou catalogues, de pieces et documents composant
les repertoires de documentation, les dossiers techniques ou adminisÂ­
tratifs, l'Institut International de Bibliographie a elabore des tables
generales sous Ie nom de Classification bibliographique decimale.
Cette classification est universelle, internatienale, encyclopedique,
'8, la fois paet.iculiere et generale elle est documentaire, en ce sens qu'elle
peut s'appliquer au 'classement de toute espece de documents ; elle
s'exprime en une notation concise; elle est fort etendue, comprenant
33,000 divisions dans les tables methodiques, representees par environ
38,000 mots classificateurs dans l'index alphabetique elle est indefiniÂ­
ment extensible. Les bases de cette classification ont ete .adoptees par les
Congres internationaux de Bibliographie en 1895, 1897, 1900 et 1908,
les principes en ont ete developpes et de larges applications en ont eM
faites tant en Europe que dans le Nouveau Monde.
La classification decimale consiste en une vaste table systematique
des ma.tieres, dans laquelle tous les sujets de connaissances sont repartis
par classes, sous-classes et divisions, 'en passant du general au particuli.er,
du tout a la partie, du genre a. I'espece.'
Chacune des rubriques de cette table est repreaentee .par un nombre
.classificateur compose d'un ou deplusieura chiffres, suivant le degre de
generalite, Ces nombres sont decimaux, en ce sens que chaque chiffre vel'S
la droite du nombre .ne modifie pas la valeur ordinaledes chiffres pre-.
cedents. L'ordro dans lequel les nombres se suivent est. aussi L'ordre
,

-

,

:
r
i

..

,

"

decimal.
La table systematique est completce par III index alphabetique des
matieres, dans lequel toutes les rubriques de la premiere table sont ranÂ­
gees en un.seul ordre alphabetique et sont suivies du nombre classificateur

correspondant.
Table

Exer)-'ples

.

:

Index

methodique:

..

..

...

.

alphabetlque

:

,

1
2
3
31
32
33

Philosophic.

Religion, :

Sciences sociales.

Statistique.
Politique.
Economie

Administration
Droit
Economie financiers
Economie politique
Monnaie
"

,

politique.

331.

Questions du tra'.[ail.

332
332.4'
34
35

Economie'financi,ere.

35
34
332
33
=:': 332.4
..

0

.

.....

.,

.

Philosophie
Politique, question
Religion

Monnaie:
Droit.

Sciences socia'les

Administration.

Travail

1Â·
.

>

du travail

331
2
3
31

Statistique

:
.

.

....

,

.....

33l

\"

.-."

L'indexation suivant qes tables (c'est-a-dire I'inscription sur lesÂ· docu-.
ments a. classer du numero classificateur correspondant, a .la matiere)
principale dont ils traitent) perrnet done de former des collections de
documents rangees dans un ordre methodique parfaip et susceptibies
d'accroissem.ents et dintercalabions COI1Jï¿½:nuels.
b'
.

.

'

..

â‚¬

,

)

cj

-

Manuel de l'Institut, International de Bibliographie
Tableau des "fascicules permettant de constituer a volonie

NÂ°
d'ordre:
du
fascic.
.

dfverses Â·sclences
dï¿½s Manu,els splfciaux ?our les

'

.

I

"INDWE
BIBLIOf>RAPill.QUE

I

b1'6

DENOMINATION

'de

'

.

pages

.

InterÂ­
Organisation, travaux, methodes de l'fustitut
nabionel de Bibliographie (expose.general)

35

,

..

-,

,

[0/9J

et

,

Bibliographie.

[0]

cietas savantes, etc

.............â€¢......

.....

'.'

Questions morales

:

'

:

[l]
[2ï¿½
[3]

Philosophie.
Sciences religieuses
Sciences sociales. Statistiqu.e. Econornie politique,
Enseignement. Assistance. Folklore

J34]

Droit

[35]

Administration

19
21
18

.

.

.

"

.

.

.

ï¿½

Astronomie

et

Mathemauiques

.

physiques (Mecanique ra.tiorrnelle et Physique): :
.:
Sciences chirniques : Chimie pure, Industries. chi[54 + 66]
.miques. Metallurgie
'[548 + 549 + 55r Mineralogic. Cristallographie .. GeologieA.nthropologie.
Sciences biologiques. Paleontologie.
[56 -1;5.7 :1-58 +59]
:
?'
.
:
:,.Botanique. Zool@g'ie
Sciences de' I'ingenieur : Mecanique. Elecbricite in[6 +.62]
dustrielle, Mines, Ponts et chaussees. Chemins de
-fer .et tramways. ;rravaux. maritimes et, hydrauÂ­
Locomotion en
li9-uï¿½s.Â· Tecluiologie sanitaire.
Sciences

'

-

'

._

.

32.

.

Sciences militaires
Philologie et Littera.ture

[355 + 623]
[4+8]
[51 +52]
[53]

"

:

:

'.

,

..

.

.

..

31

.

'.'

.

25
26

"

_

1

1

44

..

..

2

176
32

..

regles

,

17

,

.

de la Classification decimale
Resume des tables. Tables generales methodiques
:
abregees, Index alphabetique general abrege
Bibliotheconomie. SoGeneralitas.

Expose

la
16

8
14
23
22
24
3

â€¢

Nom-

..

.J

52
28
68

2
1
2

68
132
44
33
45
18

2
2
1
2
2
1

.46

1

223
,22

4
1

.74

2

j

.

_

,

27
12
28
9
10
11
13
31
6

"'.ï¿½:.

Physiologic

[612J .:3
[6I3 + 614]

p

:

â€¢â€¢â€¢â€¢

.:

:
:
.'. TIwrP,Reuj_;lque
[\H,5]
'
'l.
'Path61ogiE(,interne
"; .. [.616]
'\.".
Â·.:.'.:
Â·-Patho']ogieÂ·externe
[617]
Gynecologie, Pedia.trie. Medecine
[618 +619]
[,6l:l] -, ï¿½ c \ Voir [6 + 62l..
..

..

..

'.'

.

'"

,

Â·

...

..

"

â€¢

.....

..

.

-,

.

'

ï¿½

:ï¿½

'.Â·.-'

..

,Â·.:

ï¿½

c-

.'?

.

'.',

compareev-. ,"

.

r.Â·

"

a

,-

":

:-:-:

'

c

.

â€¢â€¢

:

c

C

,...

:

â€¢â€¢

,

HYl'iene pï¿½ivee et Hygiene publique

.

_

'j'

..

.

"

'.-.

[6.Â¥,9))' Cu.,' ':rp_dtistries',ae lï¿½ 'Locoi;notion [Locomotion p.aï¿½ terre,
.

,.

-et-par

eau,

AerostatlOn)

'

'.'

Agriculture. Agroflo,Q,lie: Sciences. Mric'o!es
Sciences appliqueesdiverses : Economie d'omestique.
Stenographie. Imprimerie et edition. Transports.
:
Comptabilite
Voir [54+66].
,

[63J

20
30

[64 + 65]

32
33

[66]
[67+68+69]

.

'

.

29

[(.),

=

.Â« Â»,

:

.

.

Table des subdivisions
I.ndex

communes

alphabetique general.

Code pour I'organisation de Ia

Supplement

nO 1

.

.

.

.

Bibliographie

Documentation

37

.

Sportei'I'ourisme, Cyclisme,.Auto;nobilisme)
Vou' [4+8].
Histoire. Geographie. Biographie. Gsnealogie

,Aï¿½Z]

",\

28

811

3

.96
30
10

2
1
1

100
84
340

2
3

et de la

2

j

.

aux

Tablesde Ia Clessification deci-

male

r

.

NOMBRE

J)

Peinture.

Sciences photographiques

[77]
[79]
[8]
[9]

7
22
15
2a
34
36

"

.8

461

Professions et metiers divers.

Industries diverses,

:
Construction
Beaux-Arts : Architecture, Sculpture.
.Photographie. Gravure. Musique

p]

4

3
1
1
1
1
1
1
1

A;:ï¿½ï¿½ï¿½ï¿½ï¿½::ï¿½: :.:.: .: :,:.:_::: :.:::::::::::: :.::::::::

[611]:

TOTAL

DES

PAGES

....

:.......

2249

!

