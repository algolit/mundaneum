///////////////////////////////////////////////////
// GENERAL FUNCTIONS //////////////////////////////
///////////////////////////////////////////////////

var text2image_data = undefined;
var loaded_png_id = -1;
var last_zoom = 1;
var last_width = 0;
var last_sort_field = undefined;
var opencv_visible = false;
var txt_zoom = 0.75;

function load_json(path, callback) {
	var httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200) {
				var data = JSON.parse(httpRequest.responseText);
				if (callback) callback(data);
			}
		}
	};
	httpRequest.open('GET', path);
	httpRequest.send(); 
}

function thumb_sort( field ) {

	if (text2image_data == undefined ) { return; }

	var tmp = text2image_data.slice(0);
	if ( field == undefined || field == '' ) {
		field = 'name';
	}

	var sort_returns = [ -1, 1 ];
	if ( field == last_sort_field ) {
		sort_returns = [ 1, -1 ];
		console.log( "invert sorting!" );
	}

	tmp.sort(function(x, y) {
	  if (x[field] < y[field]) {
		return sort_returns[0];
	  }
	  if (x[field] > y[field]) {
		return sort_returns[1];
	  }
	  return 0;
	});

	var container = document.getElementById( 'thumbs' );
	container.innerHTML = "";
	for (var i=0; i < tmp.length; ++i ) {
		var d = tmp[i];
		var w = d['width'];
		if ( d['thumb'] == "" ) {
			w = 100;
		}
		var block = '<div id="thumb_' + d['id'] + '" class="thumb" style="width:' + w + 'px">';
		if ( d['thumb'] != "" ) {
			block += '<img src="' + d['thumb'] + '" onclick="javascript:png_load(' + d['id'] + ');"/>';
		} else {
			block += '<div class="placeholder"></div>';
		}
		block += '<div>';
		block += '<a class="original" href="' + d['txt'] + '">txt</a>';
		if ( d['processed'] == 1 ) { block += '<a class="processed pok">ok</a>'; }
		else { block += '<a class="processed">!</a>'; } 
		block += '<a class="parts">' + d['png'].length + '</a>';
		block += '<div class="stats">' + d['char_valid'] + '/' + d['char'] + ',<br/>' +  d['width'] + '*' + d['height'] + '</div>';
		block += '</a>';
		block += '</div>';
		block += '</div>';
		container.innerHTML += block;
	}

	last_sort_field = field;

}

function thumb_display() {
	
	var d = text2image_data[loaded_png_id];
	var el = document.getElementById( "thumb_" + loaded_png_id );
	var as = el.getElementsByTagName( "a" );
	for ( var i = 0; i < as.length; ++i ) {
		var a = as[i];
		if (a.className.indexOf('processed') != -1 ) {
			if ( d['processed'] == 1 ) {
				a.innerHTML = 'ok';
				a.className = 'processed pok';
			} else {
				a.innerHTML = '!';
				a.className = 'processed';
			}
			break;
		}
	}
	
}

function opencv_toggle() {
	opencv_visible = !opencv_visible;
	opencv_visibility_adjust();
}

function png_zoom( ratio ) {
	
	if ( loaded_png_id == -1 ) {
		return;
	}
	var ww = window_size()[0];
	var d = text2image_data[loaded_png_id];
	if ( d['png'].length == 0 ) {
		return;
	}
	var w = ( d['width'] * ratio );
	document.getElementById('png_list').style.width = w + "px";
	document.getElementById('edition_canvas').style.width = w + "px";
	
	// adapt scroll top
	var scrolltop = document.getElementById('png_list').scrollTop / last_zoom;
	document.getElementById('png_list').scrollTop = scrolltop * ratio;
	
	
	if ( ww - ( w + 111 ) < 200 ) {
		document.getElementById('txt_source').style.visibility = 'hidden';
	} else {
		document.getElementById('txt_source').style.visibility = 'visible';
		document.getElementById('txt_source').style.marginLeft = ( w + 111 ) + "px";
		document.getElementById('txt_source').style.width = ( ( ww - ( w + 140 ) ) / txt_zoom ) + "px";
		console.log( document.getElementById('png_list').clientHeight );
		document.getElementById('txt_source').style.height = ( document.getElementById('png_list').clientHeight / txt_zoom ) + "px";
	}
	
	var imgs = document.getElementById('pngs').getElementsByTagName( "img" );
	for ( var i = 0; i < imgs.length; ++i ) {
		imgs[i].width = w;
	}
	document.getElementById('zoom' + last_zoom ).className = "submenu_link";
	document.getElementById('zoom' + ratio ).className = "submenu_link inactive";
	
	last_zoom = ratio;
	last_width = w;
	
	canvas_size();
	
	// black magic
	//q_inner_offset[1] = -document.getElementById( 'png_list' ).scrollTop;
	document.getElementById('png_list').scrollTop = document.getElementById('png_list').scrollTop + 1;
	document.getElementById('png_list').scrollTop = document.getElementById('png_list').scrollTop - 1;
	
	canvas_refresh();
	
}

function png_scroll( e ) {
	document.getElementById('png_list').scrollTop += e.deltaY * 10;
	canvas_sync_scroll();
}

function png_open() {
	if ( loaded_png_id == -1 ) {
		return;
	}
	document.getElementById('png_wrapper').style.visibility = "visible";
	document.getElementById('txt_source').style.visibility = "visible";
	// sending scroll to png div
	document.getElementById('edition_canvas').addEventListener( "wheel", function (e) { 
		png_scroll(e); 
		return false; 
	}, false );
	// hiding body scroll
	document.body.style.overflow = "hidden";
	//opencv_visibility_adjust();
}

function png_close() {
	/*document.getElementById('menu_a_png').className = "inactive";*/
	document.getElementById('png_wrapper').style.visibility = "hidden";
	document.getElementById('txt_source').style.visibility = "hidden";
	// document.getElementById('opencvs').style.visibility = "hidden";
	// removing scroll listener
	document.getElementById('edition_canvas').removeEventListener( "wheel", function (e) { 
		png_scroll(e); 
		return false; 
	}, false );
	document.body.style.overflow = "visible";
}

function png_click( e ) {
	
	console.log( "TO FINISH!!! > png_click" );
	return;
	
	var png_rect = e.target.getBoundingClientRect();
	var txt_rect = document.getElementById( 'txt_source' ).getBoundingClientRect();
	var psize = element_size( 'png_list' );
	var tsize = txt_size();
	var x = ( e.clientX - png_rect.left );
	var y = ( e.clientY - png_rect.top ) + parseInt( document.getElementById( 'png_list' ).scrollTop );
	
	var rx = ( x / psize[0] ) * tsize[0];
	var ry = ( y / psize[1] ) * tsize[1];
	
	var txt_src = document.getElementById( 'txt_source' );
	txt_src.contentWindow.document.body.scrollTo(rx,ry);
	
}

function png_load( id ) {

	if ( text2image_data == undefined ) {
		return;
	}

	var d = text2image_data[id];
	if ( d['png'].length == 0 ) {
		return;
	}
	
	document.getElementById('txt_source').src = d['txt'];
	
	var container = document.getElementById('pngs');
	//container.innerHTML = '<div id="opencvs"></div>';
	//opencv_visibility_adjust();
	container.innerHTML = '';
	for ( var i = 0; i < d['png'].length; ++i ) {
		//console.log( d['png'][i] );
		container.innerHTML += '<img src="' + d['png'][i] + '"/>';
		if ( i < d['png'].length - 1 ) {
			container.innerHTML += '<hr/>';
		}
	}

	var container = document.getElementById('png_info');
	container.innerHTML = ""
	container.innerHTML += 'name: <span class="info_field">' + d['name'] + '</span><br/>';
	container.innerHTML += 'char: <span class="info_field">' + d['char'] + '</span> <span class="info_separator">&#183;</span> ';
	container.innerHTML += 'valid char: <span class="info_field">' + d['char_valid'] + ' <span class="info_separator">&#183;</span> ';
	container.innerHTML += 'processed: <span class="info_field">' + d['processed'] + '</span> <span class="info_separator">&#183;</span> ';
	container.innerHTML += 'width: <span class="info_field">' + d['width'] + '</span> <span class="info_separator">&#183;</span> ';
	container.innerHTML += 'height: <span class="info_field">' + d['height'] + '</span>';

	loaded_png_id = id;
	document.getElementById('png_list').style.width = d['width'] + "px";
	document.getElementById('menu_a_png').className = "";
	png_zoom( last_zoom );
	png_open();
	
	adjust_manual_menu();
	adjust_iframe_display_result();
	
	if ( d['result'] != "" ) {
		load_manual();
	} else if ( d['opencv'] != "" ) {
		load_opencv();
	}

}

function element_size( id ) {
	var w = window,
	d = document,
	g = d.getElementById( id );
	return [g.scrollWidth, g.scrollHeight];
}

function txt_size() {
	var txt_src = document.getElementById( 'txt_source' );
	return [
		(txt_src.contentWindow.document.width !== undefined) ? txt_src.contentWindow.document.width : txt_src.contentWindow.document.body.offsetWidth,
		(txt_src.contentWindow.document.height !== undefined) ? txt_src.contentWindow.document.height : txt_src.contentWindow.document.body.offsetHeight
	];
}

function window_size() {
	var w = window,
	d = document,
	e = d.documentElement,
	g = d.getElementsByTagName('body')[0],
	x = w.innerWidth || e.clientWidth || g.clientWidth,
	y = w.innerHeight|| e.clientHeight|| g.clientHeight;
	return [x,y];
}

function load_opencv() {
	
	var d = text2image_data[loaded_png_id];
	if ( d['opencv'] != "" ) {
		// cleaning canvas while loading
		rects = [];
		canvas_refresh();
		// and loading
		load_json(d['opencv'], function(data){
			rects = data['blobs'];
			rect_process_all_center();
			canvas_refresh();
		});
		var me;
		me = document.getElementById('opencv_menu' );
		me.className = "submenu_link inactive";
		if ( d['result'] != "" ) {
			me = document.getElementById('manual_menu' );
			me.className = "submenu_link";
		}
	}
	
}

function load_manual() {
	
	var d = text2image_data[loaded_png_id];
	if ( d['result'] != "" ) {
		// cleaning canvas while loading
		rects = [];
		canvas_refresh();
		// and loading
		load_json(d['result'], function(data){
			rects = data['blobs'];
			rect_process_all_center();
			canvas_refresh();
		});
		var me;
		me = document.getElementById('opencv_menu' );
		me.className = "submenu_link";
		me = document.getElementById('manual_menu' );
		me.className = "submenu_link inactive";
	}
	
}

function adjust_manual_menu() {
	
	var d = text2image_data[loaded_png_id];
	var me = document.getElementById('manual_menu' );
	if ( d['result'] == "" ) {
		me.className = "submenu_link disabled";
	} else {
		me.className = "submenu_link";
	}
	
}

function iframe_display_ocr() {
	
	var d = text2image_data[loaded_png_id];
	document.getElementById('txt_source').src = d['txt'];
	var me;
	me = document.getElementById('display_ocr_menu' );
	me.className = "submenu_link inactive";
	if ( d['result_txt'] != "" ) {
		me = document.getElementById('display_result_menu' );
		me.className = "submenu_link";
	}
	canvas_sync_scroll();
	canvas_refresh();
	
}

function iframe_display_result() {
	
	var d = text2image_data[loaded_png_id];
	if ( d['result_txt'] && d['result_txt'] != "" ) {
		document.getElementById('txt_source').src = d['result_txt'];
		var me;
		me = document.getElementById('display_ocr_menu' );
		me.className = "submenu_link";
		me = document.getElementById('display_result_menu' );
		me.className = "submenu_link inactive";
	}
	canvas_sync_scroll();
	canvas_refresh();
	
}

function adjust_iframe_display_result() {
	
	var d = text2image_data[loaded_png_id];
	var me = document.getElementById('display_result_menu' );
	if ( !d['result_txt'] || d['result_txt'] == "" ) {
		me.className = "submenu_link disabled";
	} else {
		me.className = "submenu_link";
	}
	
}

// STARTING THE LOADING OF DATA

//document.getElementById('png_wrapper').addEventListener("mouseup", function (e) {
//png_click(e)
//}, false);

load_json('pngs/txt/data.json', function(data){
	// do something with your data
	//console.log(data);
	text2image_data = data;
	for (var i=0; i < text2image_data.length; ++i ) {
		text2image_data[i]['id'] = i;
	}
	thumb_sort();
});

///////////////////////////////////////////////////
// CANVAS FUNCTIONS ///////////////////////////////
///////////////////////////////////////////////////

var rects = [];
var rect_handle_size = 5;
var rect_handle_display_size = 10;
var rect_hover = -1;
var rect_sel = -1;
var rect_handle = -1;
var rect_drop = -1;

// rectangles display
var q_defaultcolor = "rgb( 0,255,255 )",
	q_defaultwidth = 1;
var q_hovercolor = "rgb( 255,130,10,0.9 )",
	q_hoverwidth = 1;
var q_selectcolor = "rgb( 255,0,255 )",
	q_selectwidth = 1;
var q_dropcolor = "rgb( 190,190,190 )",
	q_dropwidth = 1;
var q_seqcolor = "rgb( 255,255,0 )";

// canvas and mouse event related
var q_canvas, q_ctx, q_flag = false,
	q_absX = 0,
	q_prevX = 0,
	q_currX = 0,
	q_absY = 0,
	q_prevY = 0,
	q_currY = 0,
	q_dot_flag = false,
	q_mouse_in = false;
var q_size = [0,0];			// canvas size
var q_offset = [0,0];		// absolute position of tthe canvas on screen, therefore used to put mouse in relative position
var q_inner_offset = [0,0];	// influence position of the rectangle in the canvas

function canvas_size() {
	var w = window,
	d = document,
	g = d.getElementById('edition_canvas');
	q_size = [g.clientWidth, g.clientHeight];
	q_offset = [g.getBoundingClientRect().left, g.getBoundingClientRect().top];
	q_inner_offset = [0,0];
}

function canvas_init() {

	// generation of rect center
	rect_process_all_center();

	canvas_size();

	q_canvas = document.getElementById( "edition_canvas" );
	q_ctx = q_canvas.getContext("2d");

	q_canvas.addEventListener("mousemove", function (e) {
		canvas_findxy('move', 'mouse', e)
	}, false);
	q_canvas.addEventListener("mousedown", function (e) {
		canvas_findxy('down', 'mouse', e)
	}, false);
	q_canvas.addEventListener("mouseup", function (e) {
		canvas_findxy('up', 'mouse', e)
	}, false);	
	q_canvas.addEventListener("mouseout", function (e) {
		canvas_findxy('out', 'mouse', e)
	}, false);
	document.getElementById('png_list').addEventListener("scroll", function (e) {
		q_inner_offset[1] = -document.getElementById( 'png_list' ).scrollTop;
		canvas_refresh();
	}, false);
	document.addEventListener("keyup", function (e) {
		canvas_key(e);
		return false
	}, false);

	canvas_refresh();

}

function canvas_key(e) {
	
	if ( document.getElementById('png_wrapper').style.visibility != "visible" ) {
		return;
	}
	
	if ( e.key == 'Delete' && rect_hover != -1 ) {
		
		rects.splice( rect_hover, 1 );
		rect_hover = -1;
		canvas_refresh();
		
	} else if ( e.key == 'a' ) {
		
		var x = 0;
		var y = 0;
		
		if ( q_mouse_in ) {
			
			x = ( q_currX - q_inner_offset[0] ) / last_zoom;
			y = ( q_currY - q_inner_offset[1] ) / last_zoom;
			rect_hover = rects.length;
			
		} else {
			
			x = ( q_size[0] * 0.5 - q_inner_offset[0] ) / last_zoom;
			y = ( q_size[1] * 0.5 - q_inner_offset[1] ) / last_zoom;
			
		}
		
		var w = 20;
		var h = 20;
		x -= w * 0.5;
		y -= h * 0.5;
		
		if ( x < 0 ) {
			x = 0;
		} else if ( x + w > ( q_size[0] - q_inner_offset[0] ) / last_zoom ) {
			x = ( q_size[0] - q_inner_offset[0] ) / last_zoom - w;
		}
		if ( y < 0 ) {
			y = 0;
		} else if ( y + h > ( q_size[1] - q_inner_offset[1] ) / last_zoom ) {
			y = (q_size[1] - q_inner_offset[1]) / last_zoom - h;
		}
		
		var r = [ x, y, w, h, x + w * 0.5, y + h * 0.5 ];
		rects.push( r );
		canvas_refresh();
		
	}
	
	console.log( e );
	
}

function canvas_retrieve_pos( type, e ) {
	if ( type == 'mouse' ) {
		q_absX = e.clientX;
		q_absY = e.clientY;
		q_currX = e.clientX - q_offset[0];
		q_currY = e.clientY - q_offset[1];
	} else if ( type == 'touch' ) {
		q_currX = e.changedTouches[0].pageX - q_offset[0];
		q_currY = e.changedTouches[0].pageY - q_offset[1];
	}
}

function canvas_findxy(res, type, e) {
	
	if (res == 'out') {
		q_mouse_in = false;
	} else {
		q_mouse_in = true;
	}

	if (res == 'down') {
		
		q_prevX = q_currX;
		q_prevY = q_currY;
		canvas_retrieve_pos( type, e );
		q_flag = true;
		q_dot_flag = true;
		if ( rect_hover != -1 ) {
			rect_sel = rect_hover;
			canvas_refresh();
			q_canvas.style.cursor = "grabbing";
		}
		
	}

	if (res == 'up' || res == "out") {
		
		q_flag = false;
		
		if ( rect_drop != -1 ) {
			
			rects.splice( rect_drop, 1 );
			rect_hover = -1;
			rect_sel = -1;
			rect_handle = -1;
			rect_drop = -1;
			rect_sort();
			canvas_refresh();
			
		} else if ( rect_sel != -1 ) {
			
			var x1 = rects[rect_sel][0] + rects[rect_sel][2];
			var y1 = rects[rect_sel][1] + rects[rect_sel][3];
			var inverted = false;
			if ( rects[rect_sel][0] > x1 ) { 
				rects[rect_sel][0] = x1;
				inverted = true;						
			}
			if ( rects[rect_sel][1] > y1 ) { 
				rects[rect_sel][1] = y1;
				inverted = true;						
			}
			if (inverted) {
				rects[rect_sel][2] = Math.abs( rects[rect_sel][2] );
				rects[rect_sel][3] = Math.abs( rects[rect_sel][3] );
				canvas_refresh();
			}
			if ( last_zoom != 1 ) {
				for ( var i = 0; i < rects[rect_sel].length; ++i ) {
					rects[rect_sel][i] = Math.round( rects[rect_sel][i] );
				}
			}
			if (inverted || last_zoom != 1) {
				rect_sort();
				canvas_refresh();
			}
			rect_sel = -1;
			rect_handle = -1;
			
		}
		
		q_canvas.style.cursor = "default";
		
		if ( res == 'up' ) {
			rect_hover = -1;
			canvas_findxy('move', 'mouse', e);
		}
		
	}

	if (res == 'move') {
		
		canvas_sync_scroll();

		q_prevX = q_currX;
		q_prevY = q_currY;
		canvas_retrieve_pos( type, e );

		if ( rect_sel != -1 ) {

			var diffx = ( q_currX - q_prevX ) / last_zoom;
			var diffy = ( q_currY - q_prevY ) / last_zoom;
			
			switch( rect_handle ) {
				case 0:
					rects[rect_sel][0] += diffx;
					rects[rect_sel][1] += diffy;
					rects[rect_sel][2] -= diffx;
					rects[rect_sel][3] -= diffy;
					break;
				case 1:
					rects[rect_sel][1] += diffy;
					rects[rect_sel][2] += diffx;
					rects[rect_sel][3] -= diffy;
					break;
				case 2:
					rects[rect_sel][2] += diffx;
					rects[rect_sel][3] += diffy;
					break;
				case 3:
					rects[rect_sel][0] += diffx;
					rects[rect_sel][2] -= diffx;
					rects[rect_sel][3] += diffy;
					break;
				case 4:
					rects[rect_sel][1] += diffy;
					rects[rect_sel][3] -= diffy;
					break;
				case 5:
					rects[rect_sel][2] += diffx;
					break;
				case 6:
					rects[rect_sel][3] += diffy;
					break;
				case 7:
					rects[rect_sel][0] += diffx;
					rects[rect_sel][2] -= diffx;
					break;
				default:
					rects[rect_sel][0] += diffx;
					rects[rect_sel][1] += diffy;
					break;
			}

			rect_process_center();
			canvas_refresh();

		} else {

			var hr = rect_hit( q_currX, q_currY );

			if ( hr != rect_hover ) {
				rect_hover = hr;
				canvas_refresh();
			}
			if ( rect_hover != -1 ) {
				// updating selected handle
				if ( rect_sel == -1 ) {
					var prevh = rect_handle;
					rect_handle_identify( q_currX, q_currY );
					if ( rect_handle != prevh ) {
						canvas_refresh();
					}
				}
				if ( rect_handle == -1 ) {
					q_canvas.style.cursor = "grab";
				} else {
					switch( rect_handle ) {
						case 0:
							q_canvas.style.cursor = "nw-resize";
							break;
						case 1:
							q_canvas.style.cursor = "ne-resize";
							break;
						case 2:
							q_canvas.style.cursor = "se-resize";
							break;
						case 3:
							q_canvas.style.cursor = "sw-resize";
							break;
						case 4:
							q_canvas.style.cursor = "n-resize";
							break;
						case 5:
							q_canvas.style.cursor = "e-resize";
							break;
						case 6:
							q_canvas.style.cursor = "s-resize";
							break;
						case 7:
							q_canvas.style.cursor = "w-resize";
							break;
					}
				}
			} else {
				q_canvas.style.cursor = "default";
			}
			if (q_flag) {
				q_prevX = q_currX;
				q_prevY = q_currY;
			}
		}

	}

}

function rect_process_all_center() {
	for( var i =0; i < rects.length; ++i ) {
		while ( rects[i].length < 6 ) {
			rects[i].push( 0 );
		}
		rect_process_center(i);
	}
	rect_sort();
}

function rect_process_center( i ) {
	if ( i == undefined && rect_sel == -1 ) {
		return;
	}
	var ri = i;
	if ( ri == undefined ) {
		ri = rect_sel;
	}
	var r = rects[ri];
	r[4] = (r[0] + r[2] * 0.5);
	r[5] = (r[1] + r[3] * 0.5);
	if ( 
		r[4] < q_inner_offset[0] * last_zoom || 
		r[4] > ( q_size[0] - q_inner_offset[0] ) * last_zoom || 
		r[5] < q_inner_offset[1] * last_zoom || 
		r[5] > ( q_size[1] - q_inner_offset[1] ) * last_zoom 
	) {
		rect_drop = ri;
	} else {
		rect_drop = -1;
	}
}

function rect_handle_identify( mx, my ) {
	
	rect_handle = -1;
	if ( rect_hover == -1 ) {
		return;
	}
	
	var minx = ( rects[rect_hover][0] ) * last_zoom - rect_handle_size + q_inner_offset[0];
	var miny = ( rects[rect_hover][1] ) * last_zoom - rect_handle_size + q_inner_offset[1];
	var maxx = ( rects[rect_hover][0] + rects[rect_hover][2] ) * last_zoom + rect_handle_size + q_inner_offset[0];
	var maxy = ( rects[rect_hover][1] + rects[rect_hover][3] ) * last_zoom + rect_handle_size + q_inner_offset[1];
	
	// handle over
	if ( 
		// top left corner
		mx <= minx + rect_handle_size * 2 &&
		my <= miny + rect_handle_size * 2
		) {
		rect_handle = 0;
	} else if ( 
		// top right corner
		mx >= maxx - rect_handle_size * 2 &&
		my <= miny + rect_handle_size * 2
		) {
		rect_handle = 1;
	} else if ( 
		// bottom right corner
		mx >= maxx - rect_handle_size * 2 &&
		my >= maxy - rect_handle_size * 2
		) {
		rect_handle = 2;
	} else if ( 
		// bottom left corner
		mx <= minx + rect_handle_size * 2 &&
		my >= maxy - rect_handle_size * 2
		) {
		rect_handle = 3;
	} else if ( 
		// top side
		my <= miny + rect_handle_size * 2
		) {
		rect_handle = 4;
	} else if ( 
		// right side
		mx >= maxx - rect_handle_size * 2
		) {
		rect_handle = 5;
	} else if ( 
		// bottom side
		my >= maxy - rect_handle_size * 2
		) {
		rect_handle = 6;
	} else if ( 
		// left side
		mx <= minx + rect_handle_size * 2
		) {
		rect_handle = 7;
	}
	
}

function rect_hit( mx, my ) {
	
	var out = -1;
	var closest_center = -1;
	
	for ( var i = 0; i < rects.length; ++i ) {
		var minx = ( rects[i][0] ) * last_zoom + q_inner_offset[0];
		var miny = ( rects[i][1] ) * last_zoom + q_inner_offset[1];
		var maxx = ( rects[i][0] + rects[i][2] ) * last_zoom + q_inner_offset[0];
		var maxy = ( rects[i][1] + rects[i][3] ) * last_zoom + q_inner_offset[1];
		// larger area					
		if ( i == rect_hover ) {
			minx -= rect_handle_size;
			miny -= rect_handle_size;
			maxx += rect_handle_size;
			maxy += rect_handle_size;
		}
		// this rect's center is closest from the mouse
		closest_center = d;
		// inside aabb?
		if ( mx >= minx && mx <= maxx && my >= miny && my <= maxy ) {

			// distance to center
			var d = Math.sqrt( Math.pow( mx - rects[i][4], 2 ) + Math.pow( my - rects[i][5], 2 ) );
			if ( closest_center != -1 && closest_center < d ) {
				continue;
			}

			out = i;
			//console.log( out,rect_handle );
		}
	}
	return out;
}

function rect_sort() {
	
	rects.sort( function (a,b) {
		var diff = a[1] - b[1];
		if ( diff == 0 ) {
			return a[0] - b[0];
		}
		return diff;
	} );
	
}

function canvas_refresh() {

	q_ctx.canvas.width = q_size[0];
	q_ctx.canvas.height = q_size[1];
	q_ctx.clearRect(0, 0, q_size[0], q_size[1]);
	
	// drawing rectangles
	for ( var i = 0; i < rects.length; ++i ) {
		canvas_draw_rect( i );
	}
	
	if ( rect_sel == -1 ) {
		// drawing sequence
		q_ctx.beginPath();
		for ( var i = 0; i < rects.length; ++i ) {
			var r = rects[i];
			var x = r[4] * last_zoom + q_inner_offset[0];
			var y = r[5] * last_zoom + q_inner_offset[1];
			if ( i == 0 ) {
				q_ctx.moveTo( x, y );
			} else {
				q_ctx.lineTo( x, y );
			}
		}
		q_ctx.strokeStyle = q_seqcolor;
		q_ctx.lineWidth = 1;
		q_ctx.stroke();
		q_ctx.closePath();
	}
	
}

function canvas_draw_rect( i ) {

	var x = ( rects[i][0] + 0.5 ) * last_zoom;
	var y = ( rects[i][1] + 0.5 ) * last_zoom;
	var w = rects[i][2] * last_zoom;
	var h = rects[i][3] * last_zoom;
	var cx = rects[i][4] * last_zoom;
	var cy = rects[i][5] * last_zoom;

	var offx = q_inner_offset[0];
	var offy = q_inner_offset[1];
	
	var min = [ x + offx, y + offy ];
	var max = [ x + w + offx, y + h + offy ];
	if ( 
		min[0] > q_size[0] ||
		max[0] < 0 ||
		min[1] > q_size[1] ||
		max[1] < 0
	   ) {
		return;
	}

	//q_inner_offset				
	q_ctx.beginPath();
	q_ctx.moveTo( x + offx, y + offy );
	q_ctx.lineTo( x + offx + w, y + offy );
	q_ctx.lineTo( x + offx + w, y + offy + h );
	q_ctx.lineTo( x + offx, y + offy + h );
	q_ctx.lineTo( x + offx, y + offy  );

	// center cross
	q_ctx.moveTo( cx + offx - 2, cy + offy );
	q_ctx.lineTo( cx + offx + 2, cy + offy );
	q_ctx.moveTo( cx + offx, cy + offy - 2 );
	q_ctx.lineTo( cx + offx, cy + offy + 2 );

	if ( i == rect_hover ) {
		
		switch ( rect_handle ) {
			case 0:
				q_ctx.moveTo( x + offx - rect_handle_display_size, y + offy + rect_handle_display_size );
				q_ctx.lineTo( x + offx - rect_handle_display_size, y + offy - rect_handle_display_size );
				q_ctx.lineTo( x + offx + rect_handle_display_size, y + offy - rect_handle_display_size );
				break;
			case 1:
				q_ctx.moveTo( x + w + offx - rect_handle_display_size, y + offy - rect_handle_display_size );
				q_ctx.lineTo( x + w + offx + rect_handle_display_size, y + offy - rect_handle_display_size );
				q_ctx.lineTo( x + w + offx + rect_handle_display_size, y + offy + rect_handle_display_size );
				break;
			case 2:
				q_ctx.moveTo( x + w + offx + rect_handle_display_size, y + h + offy - rect_handle_display_size );
				q_ctx.lineTo( x + w + offx + rect_handle_display_size, y + h + offy + rect_handle_display_size );
				q_ctx.lineTo( x + w + offx - rect_handle_display_size, y + h + offy + rect_handle_display_size );
				break;
			case 3:
				q_ctx.moveTo( x + offx - rect_handle_display_size, y + h + offy - rect_handle_display_size );
				q_ctx.lineTo( x + offx - rect_handle_display_size, y + h + offy + rect_handle_display_size );
				q_ctx.lineTo( x + offx + rect_handle_display_size, y + h + offy + rect_handle_display_size );
				break;
			case 4:
				q_ctx.moveTo( x + w * 0.5 + offx - rect_handle_display_size, y + offy - rect_handle_display_size );
				q_ctx.lineTo( x + w * 0.5 + offx + rect_handle_display_size, y + offy - rect_handle_display_size );
				break;
			case 5:
				q_ctx.moveTo( x + w + offx + rect_handle_display_size, y + h * 0.5 + offy - rect_handle_display_size );
				q_ctx.lineTo( x + w + offx + rect_handle_display_size, y + h * 0.5 + offy + rect_handle_display_size );
				break;
			case 6:
				q_ctx.moveTo( x + w * 0.5 + offx - rect_handle_display_size, y + h + offy + rect_handle_display_size );
				q_ctx.lineTo( x + w * 0.5 + offx + rect_handle_display_size, y + h + offy + rect_handle_display_size );
				break;
			case 7:
				q_ctx.moveTo( x + offx - rect_handle_display_size, y + h * 0.5 + offy - rect_handle_display_size );
				q_ctx.lineTo( x + offx - rect_handle_display_size, y + h * 0.5 + offy + rect_handle_display_size );
				break;
			default:
				break;
		}
		
	}

	if ( i == rect_hover ) {
		if ( rect_drop != -1 ) {
			q_ctx.strokeStyle = q_dropcolor;
			q_ctx.lineWidth = q_dropwidth;
		} else if ( rect_sel != -1 ) {
			q_ctx.strokeStyle = q_selectcolor;
			q_ctx.lineWidth = q_selectwidth;
		} else {
			q_ctx.strokeStyle = q_hovercolor;
			q_ctx.lineWidth = q_hoverwidth;
		}
	} else {
		q_ctx.strokeStyle = q_defaultcolor;
		q_ctx.lineWidth = q_defaultwidth;
	}
	q_ctx.stroke();
	q_ctx.closePath();
}

function canvas_sync_scroll() {
	
	var div = document.getElementById( 'png_list' );
	var iframe = document.getElementById( 'txt_source' );
	var divsi = [ div.clientWidth, div.clientHeight, div.scrollWidth, div.scrollHeight ];
	var divoff = [div.getBoundingClientRect().left, div.getBoundingClientRect().top];
	var ifrsi = [ iframe.clientWidth, iframe.clientHeight, iframe.contentDocument.body.scrollWidth, iframe.contentDocument.body.scrollHeight ];

	lmouse = [
		q_absX - divoff[0],
		q_absY - divoff[1]
	];

	var pcx = ( lmouse[0] / divsi[2] * 1.0 ) * ifrsi[2] - ifrsi[0] * 0.5;
	var pcy = ( ( lmouse[1] + div.scrollTop ) / divsi[3] * 1.0 ) * ifrsi[3] - ifrsi[1] * 0.5;
	iframe.contentDocument.body.scrollTo( pcx, pcy );
	
}

canvas_init();

///////////////////////////////////////////////////
// SERVER INTERACTIONS FUNCTIONS //////////////////
///////////////////////////////////////////////////

var PORT_APP = 25001;

function rect_render() {
	
	if ( rects.length == 0 ) {
		return;
	}
	
	var d = text2image_data[loaded_png_id];
	var sdata = "{";
	sdata += '"txt":"' + d['txt'] + '",'
	sdata += '"name":"' + d['name'] + '",'
	sdata += '"blobs":['
	for ( var i = 0; i < rects.length; ++i ) {
		if ( i > 0 ) {
			sdata += ",";
		}
		sdata += "[";
		for ( var j = 0; j < 6; ++j ) {
			if ( j > 0 ) {
				sdata += ",";
			}
			sdata += rects[i][j];
		}
		sdata += "]";
	}
	sdata += "]}";
	
	var url = window.location.href.substring( 0, window.location.href.indexOf( ':', 6 ) );
	url += ":" + PORT_APP;
	
	var http = new XMLHttpRequest();
	http.open('POST', url, true);
	http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	http.onreadystatechange = function() {
		
		if(http.readyState == 4 && http.status == 200) {
			
			var data = JSON.parse(http.responseText);
			//console.log( data );
			if ( data['success'] ) {
				
				var td = text2image_data[loaded_png_id];
				td['result'] = data['json'];
				td['result_txt'] = data['txt'];
				td['processed'] = 1;
				thumb_display();
				adjust_manual_menu();
				load_manual();
				adjust_iframe_display_result();
				iframe_display_result();
				
			} else {
				
				alert(http.responseText);
				
			}
		}
		
	}
	
	http.send( sdata );

}