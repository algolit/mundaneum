import cv2
import imutils
import numpy as np
import os
import copy
import json
import re

f_root = 'pngs/txt'
f_target = 'pngs/opencv'
trail_digits = 5
last_name = None
json_data = {}
general_json = None
general_json_path = os.path.join( f_root, 'data.json' )
offsety = 0

#################
# OPENCV CONFIG #
#################

cv_dilate = 1
cv_blur = (1,0)

if not os.path.isdir(f_target):
	os.makedirs(f_target)

def reset_json():
	
	global offsety
	global json_data
	
	offsety = 0
	json_data = {}
	json_data['name'] = ''
	json_data['pngs'] = []
	json_data['blobs'] = []

def dump_json( name ):
		
	jpath = os.path.join( f_target, name + '.json' )
	json = open( jpath, 'w' )
	
	json.write( '{\n\t"name": "' + json_data['name'] + '",' )
	json.write( '\n\t"blobs":[' )
	bi = 0
	for blob in json_data['blobs']:
		if bi > 0:
			json.write( ',' )
		json.write( '[' )
		for bii in range( 0, 4 ):
			if bii > 0:
				json.write( ',' )
			json.write( str( blob[bii] ) )
		json.write( ']' )
		bi += 1
	json.write( '],' )
	json.write( '\n\t"pngs":[\n\t\t' )
	pi = 0
	for p in json_data['pngs']:
		json.write( '["' + p['png'] + '","' +  p['cv'] + '"]' )
		if pi < len( json_data['pngs'] ) -1:
			json.write( ',\n\t\t' )
		pi += 1
	json.write( '\n\t]\n}' )
	json.close()
	
	i = 0
	for d in general_json:
		if d['name'] == name:
			general_json[i]['opencv'] = jpath
			return
		i += 1
	
	print( "Failed to store reference to json " + jpath + " no data in related in " + general_json_path )
	
def fast_blob_detection( name, ipng, opng ):
	
	global last_name
	global json_data
	global offsety
	
	if last_name != None and last_name != name:
		dump_json( last_name )
		reset_json()
		
	json_data['name'] = name
	jdata = {
		'png' : ipng,
		'cv' : opng
	}
	
	im = cv2.imread(ipng, cv2.IMREAD_GRAYSCALE)
	height, width = im.shape[:2]
	im_result = np.zeros((height, width, 4), dtype=np.uint8)
	
	if cv_dilate > 0:
		im = cv2.dilate(im, None, iterations=cv_dilate)
	if cv_blur[0] > 0 and cv_blur[1] > 0:
		im = cv2.blur(im,(cv_blur[0],cv_blur[1]))
	
	cnts = cv2.findContours(im.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)

	for c in cnts:
		# if the contour is too small, ignore it
		if cv2.contourArea(c) < 0:
			continue
		(x, y, w, h) = cv2.boundingRect(c)
		json_data['blobs'].append( (x, y + offsety, w, h) )
		cv2.rectangle(im_result, (x, y), (x + w, y + h), (255, 255, 0, 255), 1)
	
	cv2.imwrite( opng, im_result )
	
	offsety += height
	
	json_data['pngs'].append( jdata )
	
	last_name = name
	
pngs = [f for f in os.listdir(f_root) if f.endswith('.png')]
pngs.sort()

gjs = open( general_json_path, 'r' )
general_json = json.load(gjs)
gjs.close()

logger = open( os.path.join( f_target, 'log.txt' ), 'w' )

reset_json()
for p in pngs:
	if p.find( ".thumb." ) == -1:
		name = os.path.splitext(p)[0][:-(trail_digits+1)]
		ipng = os.path.join( f_root, p )
		opng = os.path.join( f_target, p )
		fast_blob_detection( name, ipng, opng )
		logger.write( name + ' - process finished\n'  )
		print( '***************\nfile processed: ' + name + '\n\tin: ' + ipng + '\n\tout: ' + opng )

logger.close()

if last_name != None:
	dump_json( last_name )

gjs = open( general_json_path, 'w' )
#dump = json.dumps(general_json, indent=4)
#tabs = re.sub('\n +', lambda match: '\n' + '\t' * int(len(match.group().strip('\n')) / 2), dump)
gjs.write( json.dumps(general_json, indent=4) )
gjs.close()

'''
def draw_keypoints(vis, keypoints, color = (0, 255, 255)):
	for kp in keypoints:
		x, y = kp.pt
		cv2.circle(vis, (int(x), int(y)), 2, color)
im=cv2.bitwise_not(im)
params = cv2.SimpleBlobDetector_Params()
detector = cv2.SimpleBlobDetector_create(params)

# Detect blobs.
keypoints = detector.detect(im)
im=cv2.bitwise_not(im)
# Draw detected blobs as red circles.
# cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
# im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
im_with_keypoints = copy.copy(im)
draw_keypoints(im_with_keypoints, keypoints, (0,0,255) )

# Show keypoints
cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.imshow("image", im_result)
cv2.waitKey(0)
cv2.destroyAllWindows()
'''
