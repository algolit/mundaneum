﻿# fix encdong with https://stackoverflow.com/questions/436220/determine-the-encoding-of-text-in-python#436299

import sys
import argparse
import codecs
import re
import glob
import os

# using script in command line:
# $ python columner.py [arguments]
# $ python columner.py -h for full list of arguments

###### GENERAL CONFIGURATION ######

### output related ###
#
folder = None
#path = '../mundaneum_archive/ALGOLIT/ARC-MUND-PUB-UAI-CONGRES-OFFICE-INT-TRAVAIL-1910.txt'
path = '../mundaneum_archive/ALGOLIT_utf8/ARC-MUND-PUB-UAI-CONGRES-PROSTITUTION-1910.txt'
path_suffix = '.columned.txt'
path_export = ''
# comment char in the generated text
comment_char = '#'
# set to False to remove all comment from generated text
print_info = True
# generation of a an html representation of the layout
generate_html = False

### debug related ###
#
# terminal print related (~debug)
dump_lines = False
dump_pages = False
dump_visualise_columns = False

### process related ###
#
# all strings to consider as empty lines
empty_line = [ '\r', '\r\n', '\n' ]
# number of consecutive white spaces to consider as one space
# used for columns splitting
consider_as_one_space = 3
# number of consecutive empty line to consider as return,
# used for page splitting
consider_as_simple_return = 5
# enable removal of leading spaces, triple & double spaces in output (safety: ++)
enable_clean_spaces = True
# enable removal of spaces between consecutive single letters (safety: ~)
enable_glue_characters = True

# removal of leading spaces, triple & double spaces
def clean_spaces( txt ):
    txt = txt.strip()
    txt = txt.replace( '   ', ' ')
    txt = txt.replace( '  ', ' ')
    return txt

# enable removal of spaces between consecutive single letters
# might break the text, be carefull with it
def glue_characters(txt):
    m = re.findall( '\s(\w(?:\s\w){2,})\s', txt )
    out = txt
    for sb in m:
        needle = sb
        repl = ' {} '.format(re.sub(r'\s', '', sb))
        out = out.replace( needle, repl, 1 )
    return out

# generation of a dictionnary representing a line of text
# basic manipulations are made here, columns generation
# is processed later 
def line_data( txt, id ):
    
    first_non_space_char = 0
    for l in txt:
        if l != ' ':
            break
        first_non_space_char += 1 
    
    stripped_txt = txt.lstrip().rstrip()
    empty = len( stripped_txt ) == 0

    for el in empty_line:
        if el == stripped_txt:
            empty = True

    return {
        'id': id,
        'page': -1,
        'txt': txt.rstrip(),
        'start': first_non_space_char,
        'empty': empty,
        'columns': []
        }

# generation of a dictionnary representing a page of text
# nothing is done here except creation of keys
def page_data( id ):
    
    return {
        'id': id,
        'lines': [],
        'aabbs': [],
        'columns': [],
        'column_success': [],
        'column_failed': [],
        'char_width': 0,
        'empty': True
        }

# generation of a bounding box (aabb) representing a area of text
# defined by min and max position vertically and horizontally
# can be setup via arguments
def boundingbox_data( start = None, end = None, lineid = None ): 
    
    if start == None or end == None:
        return {
            'v_min': None,
            'v_max': None,
            'h_min': None,
            'h_max': None,
            'area': 0
            }
    else:
        aaab = boundingbox_data()
        aaab['v_min'] = start
        aaab['v_max'] = end
        aaab['h_min'] = lineid
        aaab['h_max'] = lineid
        boundingbox_area( aaab )
        return aaab

# computation of bounding box areas
def boundingbox_area( aabb ):
    
    v = aabb['v_max'] - aabb['v_min']
    h = 1 + aabb['h_max'] - aabb['h_min'] # think to the one line boxes...
    aabb['area'] = v * h

# test if the bounding box aabb is inside the bounding box aabb_src
# horizontal check can be deactivated (enabled by default)
def boundingbox_inside( aabb_src, aabb, check_h = True ):
    
    ok = False
    if aabb[ 'v_min' ] >= aabb_src[ 'v_min' ] and aabb[ 'v_max' ] <= aabb_src[ 'v_max' ]:
        ok = True
    if ok and check_h:
        ok = (aabb[ 'h_min' ] >= aabb_src[ 'h_min' ] and aabb[ 'h_max' ] <= aabb_src[ 'h_max' ])
    return ok

# main process on a page (pg)
# detects the bounding boxes and split lines in columns
def detect_boundingboxes( pg ):
    
    if pg[ 'empty' ]:
        return

    lines = pg['lines']

    # longest line
    first_line = None
    last_line = None
    for l in lines:
        lt = len( l['txt'] )
        if pg['char_width'] < lt:
            pg['char_width'] = lt
        if not l['empty']:
            if first_line == None or first_line > l['id']:
                first_line = l['id']
            if last_line == None or last_line < l['id']:
                last_line = l['id']

    #print( 'page ' + str( pg['id'] ) + ' width: ' + str( pg['char_width'] ) )
    
    # creation of a line of free characters, having the width of the
    # longest line of the page
    free_char = []
    for i in range( 0, pg['char_width'] ):
        free_char.append( 1 )

    # removing columns of free characters
    for l in lines:
        for c in l['columns']:
            for i in range( c[0], c[1] ):
                free_char[ i ] = 0
    
    # visualisation of columns
    if dump_visualise_columns:
        lo = 'page ' + str( pg['id'] ) + ' columns:\n'
        for l in lines:
            for i in range( 0, pg['char_width'] ):
                if free_char[ i ] == 0 and l['id'] >= first_line and l['id'] <= last_line:
                    lo += '#'
                else:
                    lo += '-'
            lo += '\n'
        print( lo )

    # detection of bounding boxes, based on the line of characters
    aabbi = -1
    for i in range( 0, pg['char_width'] + 1 ):
        if ( i == pg['char_width'] or free_char[ i ] == 0 ) and aabbi == -1:
            # starting a new bounding box            
            pg['aabbs'].append( boundingbox_data() )            
            aabbi = len( pg['aabbs'] ) - 1
            # setting all available info
            pg['aabbs'][aabbi]['v_min'] = i
            pg['aabbs'][aabbi]['h_min'] = first_line
            pg['aabbs'][aabbi]['h_max'] = last_line
            # preparation of a column to store the lines
            pg['columns'].append( '' )
        elif ( i == pg['char_width'] or free_char[ i ] == 1 ) and aabbi != -1:
            # end of a column
            pg['aabbs'][aabbi]['v_max'] = i
            aabbi = -1

    # rendering areas, useful for merging (not implemented yet)
    for aabb in pg['aabbs']:
        boundingbox_area( aabb )
        #print( 'aabb ' + str( pg['id'] ) + '\n\t' + str(aabb['v_min']) + ':' + str(aabb['v_max']) + '\n\t' + str(aabb['h_min']) + ':' + str(aabb['h_max']) )
    
    # fill columns of the page, based on bounding boxes
    for l in lines:
        cid = 0
        for c in l['columns']:
            txt = l['txt'][ c[0]:c[1] ]
            aabb = boundingbox_data( c[0], c[1], l['id'] )
            inside = False
            #print( '\tline ' + str( l['id'] ) + ' aabb: ' + str(aabb['v_min']) + ':' + str(aabb['v_max']) )
            for i in range( 0, len( pg['aabbs'] ) ):
                inside = boundingbox_inside( pg['aabbs'][i], aabb, True )
                h_match = boundingbox_inside( pg['aabbs'][i], aabb, False )
                if inside or h_match:
                    if ( len( pg['columns'][i] ) != 0 ):
                        pg['columns'][i] += '\n'
                    #print( '\t\tin aabb[' + str(i) + '], certainty: ' + str( inside ) )
                    pg['columns'][i] += txt
                    inside = True
                    break
            if inside:
                pg['column_success'].append( { 'line': l, 'column': cid } )
            else:
                pg['column_failed'].append( { 'line': l, 'column': cid } )
            cid += 1

    if enable_clean_spaces or enable_glue_characters:
        for i in range( 0, len( pg['columns'] ) ):
            if enable_clean_spaces:
                pg['columns'][i] = clean_spaces( pg['columns'][i] )
            if enable_glue_characters:
                pg['columns'][i] = glue_characters( pg['columns'][i] )


# process of a line, detects blocks of texts based on white spaces
def detect_columns( ld ):
    
    consecutive_space = 0
    cid = -1

    for lid in range( 0, len( ld[ 'txt' ] ) ):
        l = ld[ 'txt' ][ lid ] 
        if l == ' ':
            consecutive_space += 1
        else:
            if consecutive_space > consider_as_one_space or cid == -1:
                ld[ 'columns' ].append( [ lid, lid ] )
                cid += 1
            ld[ 'columns' ][ cid ][ 1 ] = lid + 1
            consecutive_space = 0
   
# utils to print line data in the terminal
# has no effect on the text output
def dump_line( ld ):

    print( 'line ' + str( ld[ 'id' ] ) )
    print( '\tstart: ' + str( ld[ 'start' ] ) )
    print( '\tpage: ' + str( ld[ 'page' ] ) )
    print( '\tempty: ' + str( ld[ 'empty' ] ) )
    print( '\ttxt: ' + ld[ 'txt' ][ld[ 'start' ]:] )
    print( '\tcolumns: ' + str( len( ld[ 'columns' ] ) ) )
    for c in ld[ 'columns' ]:
        print( '\tstart: ' + str( c[0] ) + ', end: ' + str( c[1] ) )

# utils to print page data in the terminal
# has no effect on the text output
def dump_page( pg ):

    print( 'page ' + str( pg[ 'id' ] ) )
    print( '\tempty: ' + str( pg[ 'empty' ] ) )
    print( '\tlines: ' + str( len( pg[ 'lines' ] ) ) )
    '''
    for ld in pg[ 'lines' ]:
        print( '\tline ' + str( ld[ 'id' ] ) )
    ''' 
    print( '\taabbs: ' + str( len( pg[ 'aabbs' ] ) ) )
    for aabb in pg[ 'aabbs' ]:
        print( '\t\taabb: ' + str( aabb['v_min'] ) + ',' + str( aabb['h_min'] ) + ' <> ' + str( aabb['v_max'] ) + ',' + str( aabb['h_max'] ) + ', area ' + str( aabb['area'] ) )
    print( '\tcolumns: ' + str( len( pg[ 'columns' ] ) ) )
    cid = 0
    for c in pg[ 'columns' ]:
        print( '###### ' + str( cid ) )
        print( c )
        cid += 1

# utils to repeat the comment character
def comment( n = 1 ):

    out = ''
    for i in range( 0, n ):
        out += comment_char
    return out

# main function to process the file
def process():

    global path
    global path_suffix
    global path_export
    
    # specifying the file encoding is important to
    # avoid breaking characters
    f = codecs.open( path, 'r', encoding='utf-8' )

    # turning each line of text into a line data object
    lid = 0
    lines = []
    for l in f:
        # print( 'line ' + str( lcount ) )
        lines.append( line_data( l, lid ) )
        lid += 1

    f.close()

    # detection of pages based on empty lines repetition
    consecutive_return = 0
    pid = 0
    pages = []
    pages.append( page_data( pid ) )
    
    for l in lines:
        
        if l['empty']:       

            consecutive_return += 1
        
        else:

            detect_columns( l ) 

            if consecutive_return > consider_as_simple_return or pid == -1:
                pid += 1
                pages.append( page_data( pid ) )
            consecutive_return = 0

        l['page'] = pid
        pages[ pid ]['lines'].append( l )
        if not l['empty']:
            pages[ pid ]['empty'] = False
            
    # bounding boxes generation
    for p in pages:
        detect_boundingboxes( p )

    # enable terminal dump of line data
    if dump_lines:
        for l in lines:
            dump_line( l )
            pass

    # enable terminal dump of page data
    if dump_pages:
        for p in pages:
            dump_page( p )
            pass

    # generation of output file
    if len( path_export ) == 0:
        path_export = path + path_suffix

    f = codecs.open( path_export, 'w', encoding='utf-8' )

    for p in pages:
        
        # if enabled, the output will contains the data dump
        if print_info:

            f.write( comment( 20 ) + '\n' )
            f.write( comment( 1 ) + ' page ' + str( p['id'] ) + '\n' )
            f.write( comment( 1 ) + ' lines: ' + str( len( p['lines'] ) ) + '\n' )
            f.write( comment( 1 ) + ' columns: ' + str( len( p['columns'] ) ) + '\n' )

            f.write( comment( 1 ) + ' aabbs: ' + str( len( p[ 'aabbs' ] ) ) + '\n' )
            for aabb in p['aabbs']:
                f.write( comment( 1 ) + '\tmin: ' )
                f.write( str( aabb[ 'v_min' ] ) + ',' )
                f.write( str( aabb[ 'h_min' ] ) + ', max: ' )
                f.write( str( aabb[ 'v_max' ] ) + ',' )
                f.write( str( aabb[ 'h_max' ] ) + ', area: ' )
                f.write( str( aabb[ 'area' ] ) + '\n' )

            if len( p[ 'column_failed' ] ) > 0:
                f.write( comment( 20 ) + '\n' )
                    
                f.write( comment( 1 ) + ' failed columns ' + str( len( p[ 'column_failed' ] ) ) + '\n' )

                for cf in p[ 'column_failed' ]:
                    col = cf['line']['columns'][ cf['column'] ]
                    f.write( comment( 1 ) + ' line ' + str( cf['line']['id'] ) ) 
                    f.write( '[' + str( col[0] ) + ':'  + str( col[1] ) + '] = ' )
                    f.write( cf['line']['txt'][ col[0]:col[1]+1 ] )
                    f.write( '\n' )

            f.write( comment( 20 ) + '\n' )
            f.flush()

        # important part of the process: writing columns in the output file
        cid = 0
        for c in p[ 'columns' ]:
            if print_info:
                largest_column = 0
                for aabb in p['aabbs']:
                    w = aabb['v_max'] - aabb['v_min']
                    if largest_column < w:
                        largest_column = w
                f.write( comment( largest_column ) + ' column ' + str( cid ) + '\n' )
            f.write( c + '\n' )
            f.flush()
            cid += 1
            
    f.close()

    # end of the process
    print( path_export + ' file generated' )

    if generate_html:

        f = codecs.open( path_export + '.html', 'w', encoding='utf-8' )

        f.write( '<html><head><meta charset="UTF-8">' )
        f.write( '<style>' )
        f.write( 'html, body { padding: 0; margin: 0; }' )
        f.write( 'table { width: 1024px; }' )
        f.write( 'table, td { vertical-align: super; padding: 0; margin: 0; background: #f00; }' )
        f.write( 'td { width: 50%; padding: 5px 25px 5px 5px; }' )
        f.write( '.info { margin: 0px 10px; padding: 0px; }' )
        f.write( '.columns { border-bottom: 1px solid #000; margin: 5px 10px; background: #f0f; }' )
        f.write( '.columns p, .info p { margin: 0px; padding: 0px; }' )
        f.write( '.columns p { padding-right: 5px; padding-left: 5px; }' )        
        f.write( '.columns p:hover { background: #fff; }' )
        f.write( '</style>' )
        f.write( '</head><body>' )
        
        for p in pages:
            
            f.write( '<div class="info page" id="info_page' + str( p['id'] ) + '">' )
            f.write( '<p>page ' + str( p['id'] ) + '</p>' )
            f.write( '</div>' )

            f.write( '<div class="columns" id="columns_page' + str( p['id'] ) + '">' )
            f.write( '<table><tr>' )
            for c in p[ 'columns' ]:
                f.write( '<td>' )
                lines = c.split( '\n' )
                for l in lines:
                     f.write( '<p>' + l + '</p>' )  
                f.write( '</td>' )
                f.flush()
            f.write( '</tr></table></div>' )


        f.write( '</html>' )

        f.close()
        
        print( path_export + '.html' + ' file generated' )

################### LET'S BEGIN! ###################

# getting commandline arguments
try:
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--folder", help="folder to process", type=str)
    parser.add_argument("-i", "--input", help="input path to text", type=str)
    parser.add_argument("-s", "--suffix", help="suffix for output, if no output path is specified", type=str)
    parser.add_argument("-o", "--output", help="output path", type=str)
    parser.add_argument("-pi", "--print_info", help="print info in output, [0 or 1]", type=int)
    parser.add_argument("-cc", "--comment_char", help="comment char", type=str)
    parser.add_argument("-dl", "--dump_lines", help="dump line data in terminal, [0 or 1]", type=int)
    parser.add_argument("-dp", "--dump_pages", help="dump page data in terminal, [0 or 1]", type=int)
    parser.add_argument("-dc", "--dump_columns", help="dump columns visualisation, [0 or 1]", type=int)
    parser.add_argument("-os", "--one_space", help="consider_as_one_space, must be an int", type=int)
    parser.add_argument("-sr", "--simple_return", help="consider_as_simple_return, must be an int", type=int)
    parser.add_argument("-cs", "--clean_spaces", help="enable_clean_spaces, [0 or 1]", type=int)
    parser.add_argument("-gc", "--glue_chars", help="enable_glue_characters, [0 or 1]", type=int)
    args = parser.parse_args()
    if args.folder != None:
        folder = args.folder
    if args.input != None:
        path = args.input
    if args.suffix != None:
        path_suffix = args.suffix
    if args.output != None:
        path_export = args.output
    if args.comment_char != None:
        comment_char = args.comment_char
    if args.print_info != None:
        print_info = bool( args.print_info )
    if args.dump_lines != None:
        dump_lines = bool( args.dump_lines )
    if args.dump_pages != None:
        dump_pages = bool( args.dump_pages )
    if args.dump_columns != None:
        dump_visualise_columns = bool( args.dump_columns )
    if args.one_space != None:
        consider_as_one_space = args.one_space
    if args.simple_return != None:
        consider_as_simple_return = args.simple_return
    if args.clean_spaces != None:
        enable_clean_spaces = bool( args.clean_spaces )
    if args.glue_chars != None:
        enable_glue_characters = bool( args.glue_chars )
except:
    pass

if folder != None:
    for (dirpath, dirnames, filenames) in os.walk(folder):
        filenames.sort()
        for f in filenames:
            if os.path.splitext(f)[1] == '.txt':
                path = os.path.join( folder, f )
                print( 'processing ' + path )
                process()
                path_export = ''
else:
    process()
