from pattern.fr import parsetree
# from pattern.en import parsetree
from collections import namedtuple
from colored import fg, bg, attr
import webbrowser
import random
import re
import time
import sys


def delay_print(a):
    for c in a:
        sys.stdout.write(c)
        sys.stdout.flush()
        time.sleep(0.02)

MarkovModel = namedtuple('MarkovModel', ['keys', 'data', 'history'])

def as_key (str):
  return str.lower()

def make_key (inp):
  return tuple([as_key(w) for w in inp])

def make_markov_model (corpus, history=3):
  model = MarkovModel(keys = [], history=history, data = {})

  for i in range(history, len(corpus)):
    key = make_key(corpus[(i-history):i])
    if not key in model.keys:
      model.keys.append(key)
      model.data[key] = []

    model.data[key].append(corpus[i])

  return model

def generate (model, text_length = 50, seed = None):
  if seed is None:
    key = random.choice(model.keys)
  else:
    key = make_key(seed)

  text = list(key)

  while (len(text) < text_length):
    if (key in model.keys):
      text.append(random.choice(model.data[key]))
      new_key = list(key[1:])
      new_key.append(text[-1])
      key = make_key(new_key)
    else:
      return text

  return text

def get_model_statistics (model):
  statistics = {
    'size': len(model.data),
    'alternatives': { '1': 0, '2': 0, '3': 0, '4': 0, '5': 0, '5+': 0 }
  }

  for key, entries in model.data.items():
    l = len(entries)

    if l == 1:
      statistics['alternatives']['1'] += 1
    elif l == 2:
      statistics['alternatives']['2'] += 1
    elif l == 3:
      statistics['alternatives']['3'] += 1
    elif l == 4:
      statistics['alternatives']['4'] += 1
    elif l == 5:
      statistics['alternatives']['5'] += 1
    else:
      statistics['alternatives']['5+'] += 1

  return statistics

def make_text (words):
  text = ''
  first_word = True
  captilize = True

  for word in words:
    if re.match(r'\W', word) is not None:
      text += word

      if re.match(r'[!?\.]+$', word) is not None:
        captilize = True

      if re.match(r'\n+$', word) is not None:
        first_word = True

    else:
      if not first_word:
        text += ' '

      text += word.capitalize() if captilize else word
      captilize = False
      first_word = False

  return text
delay_print("\n")
time.sleep(0.25)
delay_print("\n")
time.sleep(0.25)
delay_print("\n")
time.sleep(0.25)
delay_print("""%s%s
   _____ __             __
  / ___// /_____ ______/ /_
  \__ \/ __/ __ `/ ___/ __/
 ___/ / /_/ /_/ / /  / /_
/____/\__/\__,_/_/   \__/

%s""" % (fg(118), bg(0), attr(0)))
time.sleep(1)
delay_print("\n")
time.sleep(0.25)
delay_print("\n")
time.sleep(0.25)
delay_print("\n")
time.sleep(0.25)
f = open ('data.txt', 'a')

# while True:
choicefile = open("question_test.txt","r")
linelist=["\nQuels sont les principaux fondateurs de l'oulipo ?\n","\nSi vous recevez cette phrase, il s'agit d'un test, que savez-vous sur Paul Otlet ?\n","\nBonjour, comment allez vous ?\n"]
for line in choicefile:
    linelist.append(line)
choice=random.choice(linelist)
i = input(choice + '\n')
i = i.strip()
if len(i) > 0:
    f.write(i+'\r\n')
    f.flush()

delay_print('%s%s\nRéponse enregistrée dans le fichier data !\n%s' % (fg(118), bg(0), attr(1)))
time.sleep(1)
delay_print('%s%s\n↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓\n%s' % (fg(226), bg(0), attr(0)))
time.sleep(1)
delay_print('%s%s\nConfiguration du fichier html\n%s' % (fg(225), bg(0), attr(0)))
time.sleep(1)
delay_print("""%s%s
\|/
-o------.
/|\     |
 |      '-|
 |        |
 |        |
 |        |
 |        |
 |    html|
 '--------'
%s""" % (fg(14), bg(0), attr(0)))
time.sleep(0.5)

fichierhtml = open("markovweb.html",'w')
fichierhtml.write('<!DOCTYPE html>' + '\r\n' + '<html lang="en" dir="ltr">' + '\r\n')
fichierhtml.write('\t' + '<head>' + '\r\n\t\t' + '<meta charset="UTF-8">' + '\r\n\t\t' + '<link rel="stylesheet" type="text/css" href="main.css" media="all">' + '\r\n')
fichierhtml.write('\t' + '</head>' + '\r\n' + '<body>' + '\r\n\t')
fichierhtml.write('<div class="box_result">' + '\r\n')

delay_print("%s%s\n...\n%s" % (fg(225), bg(0), attr(0)))
delay_print("%s%s\nFichier HTML ouvert et configurer, prêt à l'écriture\n%s" % (fg(118), bg(0), attr(0)))
time.sleep(2)

# while True:
with open('data.txt','r') as h:
    raw_text = h.read()
    parsed_text = parsetree(raw_text)
# tokens = [[word.string for word in sentence.words] for sentence in parsed_text.sentences]
    tokens = []

for sentence in parsed_text.sentences:
  tokens.extend([word.string for word in sentence.words])

model = make_markov_model(tokens, history = 1)

delay_print("%s%s\nMarkov process\n%s" % (fg(14), bg(0), attr(0)))
time.sleep(1)
print("\n")

fichierhtml.write('\t\t' + '<h1 class="model">'+make_text('Monogram Model')+'</h1>' + '\n\r')
for i in range(4):
  delay_print(make_text(generate(model, 50))+ '\n')
  fichierhtml.write('\t\t\t\t' + '<p class="text_generate">'+make_text(generate(model, 25))+'</p>' + '\n\r')

delay_print("%s%s\n*****\n%s" % (fg(14), bg(0), attr(25)))
print("%s%sMaking model (monogram)%s" % (fg(14), bg(0), attr(0)), flush=True)
time.sleep(1)
model = make_markov_model(tokens, history = 1)
delay_print('%s%s\n______________________________________________________\n%s'% (fg(124), bg(0), attr(0)))
time.sleep(0.5)
delay_print("\n")

fichierhtml.write('\t\t\t' + '<h1 class="model">'+make_text('Bigram Model')+'</h1>' + '\n\r')
for i in range(4):
    delay_print(make_text(generate(model, 50))+ '\n')
    fichierhtml.write('\t\t\t\t' + '<p class ="text_generate">'+make_text(generate(model, 25))+'</p>' + '\n\r')

delay_print("%s%s\n*****\n%s" % (fg(14), bg(0), attr(25)))
print("%s%sMaking model (bigram)%s" % (fg(14), bg(0), attr(0)), flush=True)
model = make_markov_model(tokens, history = 2)
delay_print('%s%s\n______________________________________________________\n%s'% (fg(124), bg(0), attr(0)))
time.sleep(0.5)

fichierhtml.write('\t' + '</div>' + '\r\n' + '</body>' + '\r\n' + '</hmtl>')
fichierhtml.close()
delay_print("%s%s\nEnregistrement du fichier HTML ... \nFermeture...%s" % (fg(226), bg(0), attr(0)))
time.sleep(0.25)
delay_print("\n")
time.sleep(0.25)
delay_print("\n")
time.sleep(0.25)
delay_print("\n")
time.sleep(0.25)
delay_print("""%s%s
    ______          __
   / ____/___  ____/ /
  / __/ / __ \/ __  /
 / /___/ / / / /_/ /
/_____/_/ /_/\__,_/

%s""" % (fg(196), bg(0), attr(0)))
delay_print("\n")
time.sleep(0.25)
delay_print("\n")
time.sleep(0.25)
delay_print("\n")
time.sleep(0.25)
