#!/usr/bin/env/ python

#Copyright (C) 2018 Constant, Algolit

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details: <http://www.gnu.org/licenses/>.

import nltk

# élimine la ponctuation et sépare des unités comme c est / qu il

# déclare word_tokenizer en nltk
# Possibly better regex: http://fabienpoulard.info/post/2008/03/05/Tokenisation-en-mots-avec-NLTK
word_tokenizer = nltk.RegexpTokenizer(r'\w+')

# déclare sentence tokenizer en nltk
sent_tokenizer = nltk.data.load('nltk:tokenizers/punkt/PY3/french.pickle')

# ouvre le fichier texte
with open("input/lelivre_extrait.txt", 'r') as g:
	# lit le texte
	text = g.read()
	# divise en phrases
	sentences = sent_tokenizer.tokenize(text)
	# imprime phrase, mots d'une phrase, lettres d'une phrase
	for sent in sentences:
		print(sent)
		words = word_tokenizer.tokenize(sent)
		print(words, '\n')
		print(list(str(sent)))
