#    Copyright (C) 2018 Constant, Algolit

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details: <http://www.gnu.org/licenses/>.

from pattern.fr import parsetree
import os
# https://www.clips.uantwerpen.be/pages/pattern-en#tree

#   Pattern is trained on this dictionary for French tagging
#   http://pauillac.inria.fr/~sagot/index.html#lefff

#   tokenize = True,         # Split punctuation marks from words?
#       tags = True,         # Parse part-of-speech tags? (NN, JJ, ...)
#     chunks = True,         # Parse chunks? (NP, VP, PNP, ...)
#  relations = False,        # Parse chunk relations? (-SBJ, -OBJ, ...)
#    lemmata = False,        # Parse lemmata? (ate => eat)
#   encoding = 'utf-8'       # Input string encoding.
#     tagset = None         # Penn Treebank II (default) or UNIVERSAL.
files = os.listdir('input/mundaneum/CLEAN')
textfiles = []
for file in files:
    filename, file_extension = os.path.splitext(file)
    if file_extension == '.txt':
        textfiles.append('input/mundaneum/CLEAN/'+file)

f = open('sentence.txt','w')
for textfile in textfiles:
    with open(textfile, 'r') as h:
      raw_text = h.read()
      parsed_text = parsetree(raw_text, tags=False, chunks=False, relations=False, Lemmata=False)

      for sentence in parsed_text.sentences:
      #parsed_text is in fact a Text object
        ## A sentence object is actually holds more information
        s = sentence.string
        if '?' in s:
            s = s.strip()
            if '#' in s:
                continue
            if 'III' in s:
                continue
            if '!' in s:
                continue
            if '%' in s:
                continue
            if ':' in s:
                continue
            if ';' in s:
                continue
            if '""' in s:
                continue
            if '^' in s:
                continue
            if '¨' in s:
                continue
            if '*' in s:
                continue
            if '<' in s:
                continue
            if '>' in s:
                continue
            if '.' in s:
                continue
            liste = ['qui', 'quoi' , 'où', 'quand', 'quel', 'comment']
            sl = s.lower()
            listefound = False
            for word in liste:
                sindex = sl.find(word)
                if sindex > -1 and sindex < 5:
                    listefound = True
                    break
                    #print( "#####################################", word, sl )
            if not listefound:
                #print( "beeeeeeeeeekkkkkkkkkkk..." )
                continue
            else:
                print("++++++++++++++++++++++++++++++++++++++++++")
            if not s[0].istitle():
                continue
            if not s[-1:] == '?':
                continue
            if s[1].istitle():
                continue
            if s[2].istitle():
                continue
            words = s.split(' ')
            if len(words) < 3:
                continue
            motdelongueurun = 0
            for word in words:
                if len(word) == 1:
                    motdelongueurun+=1
            if motdelongueurun >= len(words)/3.0:
                continue
            f.write(s + '\r\n')
            f.flush()
            print(s, '\n')

        ## We can easily loop through the individual words
        #for word in sentence.words:
          #print(word)

      #  print()
