# from pattern.fr import parsetree
from pattern.en import parsetree
from collections import Counter
import re

def notStopWord(word):
    stopWords = ['the','on','.','in','a']

    return word in StopWords

def count(key, counter, filter=None):
    if filter is None or filter(key):
        if key not in counter:
            counter[key] = 1
        else:
            counter[key] += 1

counter = Counter()
lemmaCounter = Counter()
posCounter = Counter()

with open('input/cat.txt', 'r') as h:
  raw_text = h.read()
  parsed_text = parsetree(raw_text, lemmata=True)

  for sentence in parsed_text.sentences:
    for word in sentence.words:
      if word.string not in counter:
        counter[word.string] = 1
      else:
        counter[word.string] += 1

    for word in sentence.words:
        count(word.string, counter, notStopWord)
        count(word.lemma, lemmaCounter)
        count(word.type, posCounter)


print('*********')
print('3 Most Common:')
print(counter.most_common(3), '\n')
print(counter)

print('*********')
print('3 Most Common:')
print(counter.most_common(3), '\n')
print(lemmaCounter)

print('*********')
print('3 Most Common:')
print(counter.most_common(3), '\n')
print(posCounter)
