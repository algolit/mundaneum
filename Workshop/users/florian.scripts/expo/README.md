# Florian script for expo

markbot chain script for exhoibition

## note

*FDVW*, 02/03/2019:

- (1) J'aimerais qu'au moment où il affiche une partie du contenu du fichier data.txt, il print le fichier à l'envers, de manière à ce que la dernière phrase enregistrée dans le fichier soit print en premier.
- (2) J'ai aussi modifier le fait que les phrases générées soient en lowercase, mais j'aimerais que la première lettre soit à chaque fois en uppercase. (Le str.title() ne fonctionne pas).

*FZ*, 11/03/2019

- (1)

il me semble que ce dont tu parles se situes à la ligne 138:

```python
    for i in range(0,70):
        print(tronconnage(tabulation + linelist[i]))
        time.sleep(0.1
```

de toute façon le principe est le même: pour afficher une liste "à l'envers", plutôt que de faire 0,1,2,3,4,etc, tu fais :

```python
l = ['ddfd','ddfd',...,'ddfd','ddfd','ddfd','ddfd']
for i in range( 0,50 ):
	l[ len(l-1) - i ]
```

on part du dernier, *len(l)-1*, et on remonte en fonction de *i*, donc -0, -1, -2, -3, etc...

- (2)

si *.title()* ne suffit/marche pas, tu fais ce qu'il fait à la main, càd:

```python
s = 'sfvdgfgdfgdf'
s_cap = s[0].upper() + s[1:] #premier char en majuscule + reste de la chaîne, sans la première lettre
print(s_cap)
```

ça t'affichera:

```
Sfvdgfgdfgdf
```