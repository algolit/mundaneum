from pattern.fr import parsetree
from collections import Counter
import re

stringCounter = Counter()
lemmaCounter = Counter()
tagCounter = Counter()

def notStopWord (word):
  stopWords = ['Une', ']', '↑', '[', 'Elle', 'etc', 'elles', 'Dans', 'Un', '[', 'ex.', 'c&rsquo', '3', '3°', 'sans', '2', 'sous', 'donc', 'C&RSQUO', 'après', 'entre', 'leurs', 'En', 'où', 'celle', 'ainsi', '1', 'doit', '1°', 'b', 'chaque', 'aussi', 'dont', '1 ', 'ils', '2°', 'toute', 'comme', 'On', 'autres', 'deux', 'tous', 'c’', 'peut', 'ensemble', 'autre', 'avoir', 'pouvoir', 'autre', 'l&rsquo', 'comme', 'faire', 'devoir', 'fait', 'cette', 'donner', 'ensemble', 'plus', 'qu’', 'Les', '»', 's’', '«', 'Il', 'sciences', 'L&RSQUO', '—', 'Le', 'n’', 'tout', 'toutes', 'être', 'l’', 'd’', ')', ';', 'a', '(', ':', 'La', 'les', ',', '.', 'au', 'aux', 'avec', 'ce', 'ces', 'dans', 'de', 'des', 'du', 'elle', 'en', 'et', 'eux', 'il', 'je', 'la', 'le', 'leur', 'lui', 'ma', 'mais', 'me', 'même', 'mes', 'moi', 'mon', 'ne', 'nos', 'notre', 'nous', 'on', 'ou', 'par', 'pas', 'pour', 'qu', 'que', 'qui', 'sa', 'se', 'ses', 'son', 'sur', 'ta', 'te', 'tes', 'toi', 'ton', 'tu', 'un', 'une', 'vos', 'votre', 'vous', 'c', 'd', 'j', 'l', 'à', 'm', 'n', 's', 't', 'y', 'été', 'étée', 'étées', 'étés', 'étant', 'étante', 'étants', 'étantes', 'suis', 'es', 'est', 'sommes', 'êtes', 'sont', 'serai', 'seras', 'sera', 'serons', 'serez', 'seront', 'serais', 'serait', 'serions', 'seriez', 'seraient', 'étais', 'était', 'étions', 'étiez', 'étaient', 'fus', 'fut', 'fûmes', 'fûtes', 'furent', 'sois', 'soit', 'soyons', 'soyez', 'soient', 'fusse', 'fusses', 'fût', 'fussions', 'fussiez', 'fussent', 'ayant', 'ayante', 'ayantes', 'ayants', 'eu', 'eue', 'eues', 'eus', 'ai', 'as', 'avons', 'avez', 'ont', 'aurai', 'auras', 'aura', 'aurons', 'aurez', 'auront', 'aurais', 'aurait', 'aurions', 'auriez', 'auraient', 'avais', 'avait', 'avions', 'aviez', 'avaient', 'eut', 'eûmes', 'eûtes', 'eurent', 'aie', 'aies', 'ait', 'ayons', 'ayez', 'aient', 'eusse', 'eusses', 'eût', 'eussions', 'eussiez', 'eussent']
  return word not in stopWords

def isWord (word):
  return re.match(r'\w', word) is not None

def count (key, counter, test = None):
  if test is None or test(key):
    if key not in counter:
      counter[key] = 1
    else:
      counter[key] += 1

def printCounter (counter, name, most_common=3, print_full=False):
  print('**{:*<18}'.format(name))
  print('{} Most Common:'.format(most_common), '\n')
  for key, count in counter.most_common(most_common):
    print('{:<12}{}'.format(key, count))
  
  if print_full:
    print('\n', counter, '\n\n')

with open('traite.txt', 'r') as h:
  raw_text = h.read()
  parsed_text = parsetree(raw_text, lemmata=True)
  
  for sentence in parsed_text.sentences:
    for word in sentence.words:
      count(word.string, stringCounter, notStopWord)
      count(word.lemma, lemmaCounter, notStopWord)
      count(word.type, tagCounter, notStopWord)

printCounter(stringCounter, 'Token', 50, False)
printCounter(lemmaCounter, 'Lemma', 50, False)
#printCounter(tagCounter, 'Tag', 40, False)