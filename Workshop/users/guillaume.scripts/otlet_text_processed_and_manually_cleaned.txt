

0,1 Front
0,11 degré de saillie des arcades sourcilières.
0,111 - - très petites
0,112 — —  petite.
0,113 - - (petites)
0,114 - - intermédiaires
0,115 - - (grandes)
0,116 - - grandes
0,117 - - très grandes.

0,12 l'inclinaison de la ligne de profil du front.
0,121 - - très fuyante.
0,122 — - fuyante.
0,124 — - intermédiaire,
0,125 - -(verticale).
0,126 — - verticale.
0,127 — - proéminente.

0,13 hauteur du front.
0,131 - - très petite
0,132 — —  petite
0,133 - - (petite)
0,134 - - intermédiaire
0,135 - - (grande)
0,136 - - grande
0,137 - - très grande

0,14 largeur du front.
0,141 - - très petite
0,142 — —  petite
0,143 - - (petite)
0,144 - - intermédiaire
0,145 - - (grande)
0,146 - - grande
0,147 - - très grande


0,15 particularités du front
0,151 sinus frontaux
0,152 bosses frontales.
0,153 profil courbe.
0,154 fossette frontale

0,2 nez
0,21 racine du nez
0,211 - - très petite
0,212 — —  petite
0,213 - - (petite)
0,214 - - intermédiaire
0,215 - - (grande)
0,216 - - grande
0,217 - - très grande

0,22 dos du nez.
0,221 - - très cave
0,222 — —  cave
0,223 - - (cave)
0,224 — —  rectiligne
0,225 — —  (vexe)
0,226 — —  vexe.
0,228 — —  busgue.
0,229 - - très busqué

0,23 base du nez.
0,231 - - très relevée
0,233 — - (relevée)
0,234 — - horizontale
0,235 — - (abaissée)
0,236 — - abaissée
0,237 — — très abaissée

0,24 hauteur du nez.
0,241 - - très petite
0,242 — —  petite
0,243 - - (petite)
0,244 - - intermédiaire
0,245 - - (grande)
0,246 - - grande
0,247 - - très grande

0,25 saillie du nez.
0,251 - - très petite
0,252 — —  petite
0,253 - - (petite)
0,254 - - intermédiaire
0,255 - - (grande)
0,256 - - grande
0,257 - - très grande

0,26 largeur du nez.
0,261 - - très petite
0,262 — —  petite
0,263 - - (petite)
0,264 - - intermédiaire
0,265 - - (grande)
0,266 - - grande
0,267 - - très grande



0,27 particularités du nez. 
0,271 particularités du dos du nez.
0,2711 dos du nez en S.
0,2712 méplal du dos,
0,2713 dos mince,
0,2714 dos large,
0,2715 dos écrasé, 
0,2716 incuryé à droite, 
0,2717 incurvé à gauche. 

0,272 particularités du bout du nez.
0,2721 bout du nez bilobé.
0,2722 méplat du bout.
0,2723 - - effilé
0,2724 - - gros
0,2725 - - pointu.
0,2726 - - dévié à droite.
0,2727 — — dévié à gauche.
0,2728 nez couperosé,

0,273  particularités de la cloison.
0,2731 - - découverte.
0,2732 — -  non apparente. 

0,274 — des narines.
0,2741 - - empatées
0,2742 — - dilatées.
0,2743 — - pincées.
0,2744 - - récurrentes.
 
0,275 — de la racine.
0,2751 - - racine du nez étroite,
0,2753 — — a haut. petite.
0,2754 — — a haut. grande.

0,3 oreille.
0,31 bordure ou hélix de l'oreille
0,311 bordure originelle de l'hélix
0,3111 - - très petite
0,3112 — —  petite
0,3113 - - (petite)
0,3114 - - intermédiaire
0,3115 - - (grande)
0,3116 - - grande
0,3117 - - très grande

0,312 bordure supérieure
0,3121 - - très petite
0,3122 — —  petite
0,3123 - - (petite)
0,3124 - - intermédiaire
0,3125 - - (grande)
0,3126 - - grande
0,3127 - - très grande

0,313 bordure postérieure
0,3131 - - très petite
0,3132 — —  petite
0,3133 - - (petite)
0,3134 - - intermédiaire
0,3135 - - (grande)
0,3136 - - grande
0,3137 - - très grande

0,314 particularités de l'hélix.
0,3141 particularités darwiniennes

0,31411 nodosité darwinienne
0,31412 élargissement darwinien
0,31413 saillie darwinienne.
0,31414 tubercule darwinien

0,3142 particularités de la bordure
0,31421 bordure échancrée.
0,31422 —  froissée
0,31423  — post. fendue.
0,31424 —  cicat. et gelée.

0,3143 particularités du contour supérieur de l'oreille
0,31431 contour sup. aigu
0,31432 contour super-antérieur aigu
0,31433 contour supéro-postérieur en équerre
0,31434 contour supérieur bicoudé
0,31434 contour supérieur obtus-aigu


0,32 le lobe.
0,321 forme du bord libre du lobe
0,3212 descendant
0,3212 descendant-équerre.
0,3213 équerre.
0,3214 intermédiaire.
0,3215 golfe.

0,322 adhérence.
0,3221 très fendu.
0,3222 fendu.
0,3223 (fendu).
0,3224 intermédiaire
0,3225 (sillonné),
0,3226 sillonné.
0,3227 isolé,

0,323 modèle de sa surface externe.
0,3231 très traversé,
0,3232 traversé.
0,3233 (traversé).
0,3234 intermédiaire.
0,3235 uni
0,3236 uni
0,3237 éminent.

0,324 dimensions.

0,3241. Très petit.
0,3242 petit.
0,3243 (petit)
0,3244 moyen.
0,3245 (grand).
0,3246 grand.
0,3247 très grand.

0,325 particularités

0,3251 particularité de la forme du lobe
0,32511 lobe fendu.
0,32512 lobe pointu.
0,32513 lobe carré
0,32514 lobe oblique interne.
0,32515 lobe oblique externe.
0,32516 lobe a torsion antérieure.

0,3252 particularités des rides du lobe
0,32521 lobe à fossette
0,32522 lobe a virgule.
0,32522 ride oblique postérieure.
0,32524 lobe à îlot.

0,3253 particularités de la peau du lobe
0,32531 lobe poilu


0,33 l'antitragus.
0,331 inclinaison de l'antitragus
0,3311 très horizontale
0,3312 horizontale. 
0,3313 (horizontale),
0,3314 intermédiaire.
0,3315 (oblique).
0,3316 oblique.
0,3317 très oblique.

0,332 profil da l’antitragus.
0,3321 très cave
0,3322 cave.
0,3321 rectiligne.
0,3324 intermédiaire.
0,3325 (saillant).
0,3326 saillant.
0,3327 très saillant.

0,333 renversement de lantitragus.
0,3331 très versé.
0,3332 verse.
0,3333 (versé).
0,3335 (droit).
0,3336 droit.
0,3337 très droit.

0,334 volume de l'antitragus.
0,3341 nul.
0,3342 très petit.
0,3343 petit. —
0,3344 intermédiaire.
0,3345 (grand).
0,3346 grand.
0,3347 très grand.

0,335 particularités.
0,3351 antitragus fusionné avec l'hélix
0,3352 tragus pointu.
0,3353 tragus bifurqué.
0,3354 tragus et antitrag. poilus.
0,3355 incisure postantitragienne.
0,3356 Canal étroit

0,34 bordure ou hélix de l'oreille.

0,341 saillie du pli inférieur.
0,3411 très cave
0,3412 cave
0,3413 (cave)
0,3414 intermédiaire,
0,3415 (vex.)
0,3416 vex.
0,34417 très vex.

0,342 particularités du pli inférieur.

0,3421 fossette naviculaire en pointe.

0,35 le pli supérieur.
0,351 saillie du pli supérieur
0,3511 nul. 
0,3512 effacé.
0,3514 intermédiaire.
0,3516 accentué.
0,3516 très accentué.

0,352 particularités.
0,3522 sillons sépares.
0,3524 pli supérieur joignant la bordure.
0,3525 hématome du pli supérieur.

0,36 forme générale de l'oreille
0,361 forme triangulaire de l'oreille.
0,362 rectangulaire,
0,363 ovale.
0,364 ronde.

0,37 écartement du pavillon
0,371 — supérieur de l'oreille
0,372 — postérieur. —
0,373 — inférieur. —
0,374 — Toal. —
0,375 oreille collée supérieurement
0,376 cassé à l'antitragus

0,38 insertion de l'oreille
0,381 insertion verticale
0,382 — intermédiaire.
0,383 - oblique


0,4 les lèvres, la bouche, le menton.

0,41 les lèvres.
0,411 hauteur absolue de la lèvre supérieure
0,415 particularités des lèvres

0,4111 bordure très petite
0,4112  - - petite
0,4114  - - intermédiaire
0,4116  - - grande
0,4117  - - très grande

0,412 proéminence des lèvres
0,4121 lèvre sup. très proéminent.
0,4122 —.. sup très proéminente.
0,4123 lèvre inf très proéminent.
0,4124 —.. — inf proéminente.

0,413 largeur de la bordure
0,4131  - - très petite
0,4132  - - petite
0,4134  - - intermédiaire
0,4136  - - grande
0,4137  - - très grande

0,414 l'épaisseur des lèvres
0,4141 -- très minces,
0,4142 — minces.
0,4143 —  intermédiaires.
0,4144 —  épaisses.
0,4145 —  très épaisses.
0,4146 — lév. sup. retroussée
0,4146 — lév. inf. pendante

0,4151 lèvres lippues
0,4152 lèvres gercées.
0,4153 bec de liévre.
0,4154 sillon médian accentué


0,42 la bouche. 
0,421 dimensions de la bouche.
0,4211 très pelite.
0,4213 — petite.
0,4213 — (petite).
0,4214 — intermédiaire.
0,42145 — (grande).
0,4216 — grande.
0,4217 — très grande.

0,422 ouverture de la bouche.
0,4221 bouche pincée.
0,4222 bée.——

0,423 particularités.
0,4231 coins abaissés.
0,42311 — droit abaissé. 
0,42312 — gauche abaissé. 
0,4232 coins relevés.
0,42321 — droit relevé,
0,42322 — gauche relevé,
0,4233 bouche oblique.
0,42331 bouche oblique à droite. 
0,42332 bouche oblique à gauche.
 
0,4234 bouche en coeur.
0,4235 incisives découvertes.
0,4236 perte des incisives

0,42361 perte de ja première incisive
0,42362 perte de la seconde incisive
0,42364 perte de la première et seconde
0,42365 perte de la première et troisième
0,42366 perte de la seconde et troisième

0,43 le menton.
0,431 inclinaison du menton


0,4311 inclinaison très fuyante.
0,4312 — fuyante.
0,4314 - verticale
0,4316 — saillante
0,4317 — très saillante,


0,432 hauteur du menton.
0,4321 hauteur très petite.
0,4322 —  petite.
0,4324 - intérmédiaire
0,4326 — — grande.
0,4327 —  très grande.

0,433 largeur du mentor. 
0,4331 largeur petite,
0,4332 — petite.
0,4334 —  intermédiaire.
0,4336 — grande,
0,4337 très grande

0,434 forme de la saillie inférieure.
0,4341 — menton plat
0,4342 — a houppe.
0,4343 — a trés forte houppe.

0,435 particularité
0,4351 menton a fossette.
0,4352 — à fossette allongée.
0,4353 —  bilobé. 
0,4354 sillons sus-mentonniers.

0,51 contour général du profil
0,511 profil continu
0,513 -- brisé
0,513 paralléle
0,514 anguleux
0,515 arqué 
0,516 ondule.
0,517 semilunaire,

0,52 contour nasobuccal.
0,521 prognathisme,
0,522 orthognathisme.
0,523 naso-prognathe.
0,524 machoire inférieure proéminente.
0,525 prognathisme avec machoire inférieure proéminente
0,526 face rentrée en dedans,

0,61 profil du crane proprement dit
0,611 crane bas.
0,612 crane haut.
0,613 téte en bonnet a poil
0,614 téte en besace 
0,615 occiput plat.
0,616 occipul bombé.
0,617 bourrelet occipital
0,618 téte en carène

0,62 contour général de la face
0,620 particularités des parties du contour
0,621 l'ensemble du contour de la face
0,6211 face en pyramide.
0,6212 — en toupie.
0,6213 en losange.
0,6214 biconcave.
0,6215 carrée,
0,6216 ronde,
0,6217 rectangulaire,
0,6218 — longue.
0,6219 asymétrie de la face.

0,6221 face à pariétaux écartés.
0,6222 face à pariétaux rapprochés.
0,6223 face à zygomes écartés.
0,6224 face à zygomes rapprochés.
0,6225 face à mâchoire écartées.
0,6226 face à mâchoire rapprochées.
0,6227 face à Paulette saillantes.

0,623 particularités des chairs de la face.

0,6231 face pleine.
0,6232 — osseuse.
0,6233 - flacidité des chairs


0,71 les sourcils.

0,711 emplacement des sourcils
0,7111 rapprochés.   
0,7124 sourcils obliques internes, 
0,7112 écartés.
0,7113 sourcils bas
0,7114 sourcils hauts.

0,712 direction des sourcils 
0,7121 sourcils obliques internes
0,7122 — —  externes.

0,713 forme des sourcils
0,7131 sourcils arqués
0,7132 sourcils rectilignes
0,7133 sourcils sinueux
0,7134 rapprochement ou écartement nerveux des sourcils  

0,714 dimension des sourcils
0,7141 sourcils courts
0,7142 sourcils longs
0,7148 —  étroits.
0,7144 —  larges.

0,715 particularités
0,7151 sourcils clairsemés.
0,7152 — fournis.
0,7154 — à maximum en queue
0,7155 .— en brosse.
0,7156 — noirs et barbe blanche

0,716 nuance
0,7161 - - blond clair 
0,7162 — - blond foncés.
0,7163 — - chatains clairs.
0,7164 - - châtains foncés
0,7165 — - roux.
0,7166 — — noirs.
0,7167 - - blancs.

0,72 les paupières.
0,721 ouverture des paupières
0,7211 très peu ouvertes
0,7212 peu ouvertes
0,7213 intérmédiaires
0,7214 ouvertes
0,7215 largement ouvertes

0,722 inclinaison de la fente palpébrale. 
0,7221 à angle externe relevé. 
0,7222 à angle externe abaissé.

0,723 modéle dela paupiére supérieure. 
0,7231 paupiére supérieure recouverte
0,7232 paupiére supéricure découverte,


0,724 particularités de la paupiére supérieure.
0,7241 débordement entier des paupiéres supérieures
0,7242 paupiéres rentrantes,
0,7243 débordement externe. 
0,7244 débordement interne.
0,7245 paupiére supérieure droite tombante.
0,7246 paupiére supérieure gauche tombante
0,7247 yeux bridés.

0,725 particularités de la paupiére inférieure
0,7251 paupière inférieure à bourrelet
0,7252 paupière inférieure à poches
0,7253 paupière inférieure à rides
0,7254 paupière inférieure renversées

0,726 particularités des deux paupières.
0,7261 paupièrés chassieuses
0,7262  — larmoyantes.
0,7263 cils trés longs.
0,7264 cil très courts.
0,7264 cil très abondants.
0,7264 cil très rares.

0,73 globes oculaires.
0,731 saillie du globe.
0,7311 yeux enfoncés
0,7312 — intermédiaires.
0,7313 —  saillants.

0,732 particularités du globe.
0,7321 strabisme convergent.
0,7322 — divergent.
0,7323 — vertical.
0,7324 intraoculaire petit
0,7325 intraoculaire grand

0,74 orbites
0,741 orbites basses.
0,742 orbites  hautes.
0,743 orbites encavées. .


0,81 le cou. 
0,811 cou court
0,812 cou long 
0.813 cou mince
0,814 cou large
0.815 larynx saillant 
0,816 goitre

0,82 les rides.
0,821 rides frontales
0,8211 ridé unique totale. 
0,8212 ride unique  médiane.
0,8213 ride double.
0,8214 rides multiples. 

0,822 rides oculaires
0,8221 intersourcilière verticale médiane, .
0,8222 intersourcilière  verticale
0,8223 intersourciliére unilatérale.
0,82231 intersourcilière unilatérale droite
0,82232 intersourcilière unilatérale gauche,
0,8224  intersourcilière oblique unilatérale.
0,82241 intersourcilière oblique droite
0,82242 intersourcilière oblique gauche
0,8225 sillon horizontal de la racine du nez
0,8226 circonflexe intersourcilier.
0,8229 rides tragiennes.
0,82291 ride tragienne unique.
0,82292 ride tragienne double.
0,823 rides buccales.
0,8231 sillon nasolabial accentué.
0,8232 sillon jugal.
0,8233 sillon susmentonnier.
0,8234 rides verticales du cou.
0,8235 joues à fossettes. — 

0,83 la carrure.
0,831 largeur des épaules
0,8311 - très petite.
0,8312 — petite.
0,8314 - moyenne,
0,8315 (oblique),
0,8316 — grande,
0,8317 — très grande.
0,832 chute des épaules
0,8321 - horizontales.
0,8322 — intermédiaires.  
0,8323 — tombantes.

0,84 la ceinture.
0,841 - très petite.
0,842 — petite.
0,843 — (petite).
0,844 —  intermédiaire.
0,845 — (grande).
0,846 — grande.
0,847 — très grande


0,85 l’ attitude.
0,851 le port de la tête et l’inflexion du cou
0,8511 tête penchée en avant.
0,8512 tête penchée en arriére 
0,8513 tête inclinée à droite
0,8514 tête inclinée à gauche

0,852 le degré de rotondité du dos.
0,8521 dos vouté.
0,8522 dos droit.
0,8523 épaules saillantes.
0,8524 épaules  effacées.

0,853 la posture habituelle des bras et des mains et attitude.
0,8531 mains sur les hanches
0,8532 mains dans les poches de pantalon
0,8533 mains dans l’entournure du gilet
0,8534 mains croisées sur la poitrine
0,8535 attitude raide.
0,8536 attitude nonchalante. 

0,86 l’allure générale.
0,861 la démarche
0,8611 — très lente
0,8612 - très rapide
0,8613 — à petits pas.
0,8616 — à grands pas.
0,8614 —  légère;
0,8615 — lourde.
0,8617 — boiteuse.
0,8618 - déhanchée
0,8619 — sautillante. 

0,862 le geste.
0,8621 gesticulation abondante.
0,8622 absence de gesticulation.

0,863 le regard
0,8631 - droit. 
0,8632 — oblique.
0,8633 — fixe.
0,8634 — mobile.
0,8635 — franc.
0,8636 - fuyant
0,8637 — lent,
0,8638 —  rapide.

0,864 mimique physionomique.
0,8641 tics de la bouche
0,8642 tics des yeux
0,8643 clignement des yeux

0,87 la voix.
0,871 - grave
0,872 - aigue
0,873 — féminine,
0,874 — masculine,
0,875 zézaiement.
0,876 chuintement.
0,877 bégaiement.
0,878 accent étranger.

0,9 yeux, cheveux, barbe, pigmentation
0,91 yeux
0,911 - impigmentés.
0,912 - pigmentation jaune
0,913 - orangés
0,914 - châtain
0,915 — marron en cercle.
0,916 — marron verdatre,
0,917 - marron pur


0,913 particularités des yeux.
0,9131 oeil truité
0,9132 yeux à secteurs.
0,9133 yeux à vairons.
0,9134 oeil truité.
0,9135 yeux à zone concentrique grisatre.
0,9136 yeux à pupille dilatée,
0,9137 pupille pisiforme ou excentrique. .
0,9138 amputé d’un oeil,
0,9139 porteur d’un oeil de verre.

0,92 cheveux.
0,921 nature des cheveux
0,9211 - droits
0,9212 - ondés
0,9213 - bouclés
0,9214 - frisés
0,9215 - crépus
0,9216 - laineux

0,922 insertion des cheveux. 
0,9221 - circulaire.
0,9222 — rectangulaire. 
0,9223 — en pointe. 

0,923 abondance des cheveux 
0,9231 cheveux abondants
0,9232 cheveux clairsemés
0,9233 calvitie frontale
0,9234 calvitie tonsurale
0,9235 calvitie alopécie

0,924 nuance des cheveux, 
0,9241 cheveux blonds. 
0,92411 très blonds
0,92412 blonds
0,92413 blond foncé.

0,9242 cheveux châtains. 
0,92421 chatain clair.
0,92422 chatains,  
0,92423 chatain foncés, 

0,9243 — noirs. 
 
0,9244 — roux. 
0,92441 roux vif.
0,92442 roux.
0,92443 roux chatain clair.

0,9245   blonds albinos
0,9246 — grisonnants.
0,9247 — blancs.

0,93 barbe
0,931 nature des poils de la barbe et abondance
0,9311  droits
0,9312 — ondés.
0,9313 — bouclés.
0,9314 — frisés.
0,9315 - très frisés.:
0,9316 abondants.
0,9317 clairsemés, 

0,932. forme de la barbe. 
0,9321 la moustache.
0,9322 les favoris.
0,9323 la mouche.  
0,9324 barbe de bouc.
0,9325 le collier 
0,9326 la barbe entière.
0,9327 face glabre.
0,9328 joues glabres et moustaches fortes

0,933 nuance des poils. 
0,9331 - blonds.
0,9332 — châtains.
0,9333 - noirs
0,9334 — roux,
0,9335 — blonds albinos.
0,9336 — grisonnants.
0,9337 — blancs.

0,94 pigmentation as la peau, 
0,941 coloration pigmentaire
0,9411 petite
0,9412 moyenne
0,9413 grande

0,942 coloration sanguine
0,9421  petite.
0,9422 moyenne. 
0,9423 grande. 
