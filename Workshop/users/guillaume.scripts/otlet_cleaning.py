import re
import pprint
import json
import time

from spellchecker import SpellChecker
spell = SpellChecker( language ='fr')


# create pretty print variable in order to be able to have a nicely formatted output
pp = pprint.PrettyPrinter(indent=2)

#strikethrough
def strike(text):
    result = ''
    for c in text:
        result = result + c + '\u0336'
    return result

word_previous = "species"


#open the file
with open("output_third_ocr+regex.txt", "r") as h: 
	#rsubstitution of weird characters
	raw = h.read()
	raw= re.sub(r'•', 'i',raw)
	raw= re.sub(r'•', 'i',raw)
	raw= re.sub(r'o,', '0,',raw)
	raw= re.sub(r'°', '0',raw)
	raw= re.sub(r'º', '0',raw)
	raw= re.sub(r'Ù', 'u',raw)
	raw= re.sub(r'Î', 'i',raw)
	raw= re.sub(r'·', '',raw)
	raw= re.sub(r'[\-\_]', '',raw)
	raw= re.sub(r'[\-]', '',raw)
	raw= re.sub(r'\|', '',raw)
	raw= re.sub('-', '',raw)
	raw= re.sub(r'Ž', 'é',raw)
	raw= re.sub(r'\~', 'é ',raw)
	raw= raw.lower()

	#regex patterns if necessary
	pattern_a = r'\b\w+\b'
	pattern_b=r'\d+\a\d+'
	pattern_c = r'\w+'
	pattern_d= r'[a-z]+\.+\n'
	pattern_e= r'\b\w+\b'
	i = 0

	for word in raw.split():
	# z=re.match(pattern, lines)	
	# print(z)
		print(word, end=" ",flush=True)
		if spell.unknown(word):	
			word_correct= spell.correction(word)			
			re.sub(word, word_correct , raw)
			print(strike(word), end=' ',flush=True)
			print (word_correct, end=' ',flush=True)
		if re.match(r'\n',word):
			print(word, end=r'\n', flush=True)
		if re.match(linea,word):
			print(r'\n',flush=True)
		else:
			pass

	with open('output_third_ocr+regex+correct.txt', 'w') as out_file:
		out_file.write(raw)
	print(raw)





