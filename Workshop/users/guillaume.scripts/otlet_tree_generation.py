import re
import pprint
import json

#modules
pp= pprint.PrettyPrinter(indent=2)

#functions to create the tree
def make_child ():
	return { 'data': [] }


def register (data, address, tree):
	key = address.pop(int(0))

	if key not in tree:
		tree[key] = make_child()

	if address:
		tree[key] = register(data, address, tree[key])
	else:
		tree[key]['data'].append(data)

	return tree

tree = make_child()

#open text file and last cleaning
with open("output_third_ocr+regex+correct_3_manual_2.txt", "r") as h:
	raw = h.read()
	raw= re.sub(r'\n', '',raw)
	raw= re.sub(r'\-', '',raw)
	raw= re.sub(r'\-\-', '',raw)
	raw= re.sub(r'\- \-', '',raw) 
	raw= re.sub(r' \- \-', '',raw)
	raw= re.sub('-', ' ',raw)
	raw= re.sub(r'\.', '',raw)
	raw= re.sub(r'\;', '',raw)
	raw= re.sub(r'\,', '',raw)
	raw= re.sub(r'0', '',raw)
	parts = re.split(r'(\d+)', raw)
	# print(parts)




#create tree
	tree['data'].append(parts[0])

	for i in range(1, len(parts), 2):
		code = [ int(n) for n in list(parts[i]) ]
		snippet = parts[i+1]
		snippet= re.sub(r'\n','',snippet)
		register(snippet, code, tree)

# pprint(tree)
with open('data_tree_5th.txt', 'w') as outfile:
    print(tree, outfile)

with open('data_tree_6th.json', 'w') as outfile:
    json.dump(tree, outfile)

