# from pattern.en import parsetree
from collections import namedtuple
import random
import re

with open('input/CCNY_recipe_book.txt', 'r') as h:
    # Read text file
    raw_text = h.read()

    recipes = re.split(r'(^[A-Z ]+$|^Ingredients:|^Directions:)', raw_text, flags=re.M)
    while '' in recipes:
        recipes.remove('')

    names = []
    description = []
    country = []
    ingredients = []
    directions = []

    ### Splitting the recipes into parts
    for e in range(0, len(recipes),2):
        # print(recipes[e+1])
        # print("----------")
        if "Directions:" not in recipes[e] and "Ingredients" not in recipes[e]:
            desc_orig = str(recipes[e+1]).replace("\n\n","")
            for word in desc_orig.split("\n"):
                # print(repr(word))
                if word.startswith("("):
                    country.append(word)
                elif len(word) > 0:
                    description.append(word)
            names.append(recipes[e])
        if "Ingredients:" in recipes[e]:
            ingr = recipes[e+1]
            for sentence in ingr.split("\n"):
                if len(sentence) > 0:
                    ingredients.append(sentence)
            # print(ingr)
            # ingredients.append(recipes[e+1])
        if "Directions:" in recipes[e]:
            dir = recipes[e+1]
            for sentence in dir.split("\n"):
                if len(sentence) > 0:
                    # print(sentence)
                    # print("--------")
                    directions.append(sentence)
            # directions.append(recipes[e+1])

    # parsed_ingredients = parsetree(ingredients[0])
    # print(directions)
    # for d in directions:
    #     print("----------")
    #     print(d)
    #     print("0000000000")
    print("\n\nIngredients:\n\n")
    for i in range(random.randrange(2,15)):
        print(random.choice(ingredients))
    print("\n\n-----------\n\n")
    print("Directions:\n\n")
    for i in range(random.randrange(2,15)):
        print(random.choice(directions))
