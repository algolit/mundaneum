import os
import time
import nltk
from nltk.corpus import movie_reviews
import random 
import re
import numpy as np

# Load functions from machine learning library scikit-learn
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split

# classifiers
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC, NuSVC, LinearSVC # other evaluation methos, see doc

# evaluation methods
from sklearn.metrics import classification_report, confusion_matrix, f1_score

# save model
from sklearn.externals import joblib


def feminin (sentence):
    fem = ['prostituée','femme','femmes','féminin','féminine','féminins',' elle',' elles','Elle','Elles','courtisane','fille','amante','escort','filles','adolescente','adolescentes','maîtresse','maîtresses','pute','catin','escorte','péripatéticienne','péripatéticiennes','tapin','vierge','travailleuse']
    
    for word in fem :
        if word in sentence:
            return True
    return False
    
    
def masculin (sentence):
    masc = ['homme','hommes','masculin','masculine',' il',' ils','Il','Ils','spécialistes','spécialiste','proxénète','client','amant','adolescent','viril','marié','mariés','garçon','garçons','citoyen','citoyens','monsieur','mari','maris','adultère','époux','désir','désirs',]

    for word in masc :
        if word in sentence:
            return True
    return False

######## CONFIGURATION ########

txt_file = 'L-CONGRES-PROSTITUTION-1910-C.txt'
buffer = ''
phrases=[]
pid= 0
phrases.append('')
f = open( txt_file )


for sentence in f:
    pospoint=-1
    lcount=0
    for letter in sentence:
        if letter == '.':
            pospoint=lcount
            break
        lcount+=1
    if pospoint==-1:
        phrases[pid]+=sentence#+'\n'
    else: 
        phrases[pid]+=sentence[0:pospoint+1]
        phrases.append('')
        pid+=1
        phrases[pid]+=sentence[pospoint+1:]
f.close()

fichierhtml = open ("testcolor2.html",'w')
fichierhtml.write( '<html><head><meta charset="UTF-8">\n <link rel="stylesheet" type="css" href="stylepros.css">' )
fichierhtml.write( '<style>' )
fichierhtml.write('body{font-family:"courier";color:#EBFCE1;font-size:11pt;margin:50px 30px 20px 30px;padding:2px;}\n .fem{background:#C50536;} .masc{background:#0B7A75;} .neutre{background:#3D2B3D;}')
fichierhtml.write( '</style>\n' )
fichierhtml.write( '</head><body>\n' )

for p in phrases:
#    l = l.strip()
#    if len( l ) == 0:
#        continue
    
#    X_new = vectorizer.transform(sentence)
#    Y_new = loaded_model.predict(X_new)
#    prediction = ''.join((enc.inverse_transform(Y_new)))
        #print( '\n\n##### start' )
    frankie=(p.replace('\n','<br/>'))

    if  feminin(p) :
        buffer += '<span class="fem">' + frankie +'</span>'
    elif masculin(p) :
        buffer += '<span class="masc">' + frankie +'</span>'
    else:
        buffer += '<span class="neutre">' + frankie +'</span>' 
        #time.sleep(1)
        
fichierhtml.write(buffer)

fichierhtml.write( '</body>\n' )
fichierhtml.write( '</html>\n' )
fichierhtml.close()