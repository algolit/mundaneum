import random

WORD = ('mundaneum', 'otlet', 'lafontaine', 'litterature')
word = random.choice(WORD)
correct = word
clue = word[0] + word[(len(word)-1):(len(word))]
letter_guess = ''
word_guess = ''
store_letter = ''
count = 0
limit = 10

print('Welcome to "Guess the Word Game!"')
print('Bienvenue au jeu de devinette - Le Pendu !')
print('You have 10 attempts at guessing letters in a word')
print('Tu as 10 tentatives pour deviner le mot, indique lettre par lettre')
print ('The theme is Paul Otlet.')
print ('Le thème est Paul Otlet.')
print('Let\'s begin!')
print('Commençons!')
print('\n')

while count < limit:
    letter_guess = input("Guess a letter/ Devine une lettre ")

    if letter_guess in word:
        print('yes!')
        store_letter += letter_guess
        count += 1

    if letter_guess not in word:
        print('no!')
        count += 1

    if count == 2:
        print('\n')
        clue_request = input('Would you like a clue? / Veux-tu un indice? y/n  ')
        if clue_request == 'y':
            print('\n')
            print('Indice/ La premiere et la derniere lettre du mot est /  CLUE: The first and last letter of the word is: ', clue)
        if clue_request == 'n':
            print('You\'re very brave! / Wow tu es brave!')

print('\n')
print('Il est maintenant temps de me donner ta réponse / Now its time to guess. You have guessed',len(store_letter),'letters correctly.')
print('Les lettres que tu as trouvé sont /These letters are: ', store_letter)

word_guess = input('Devine le mot / Guess the whole word: ')
while word_guess:
    if word_guess.lower() == correct:
        print('Bravo!  Congrats!')
        break

    elif word_guess.lower() != correct:
        print('Pas de chance ! Unlucky! The answer was,', word)
        break

print('\n')
input('Press Enter to leave the program')