#    Copyright (C) 2018 Constant, Algolit

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details: <http://www.gnu.org/licenses/>. 

# WARNING: This script is a demo, it has many problematic issues and is not suitable for practical use!

# Following this tutorial: https://pythonprogramming.net/text-classification-nltk-tutorial/ because of confusion matrix implementation
# Using NLTK corpus movie_reviews data set, and they are labeled already as positive or negative.
# Related paper on the creation of corpus of movie reviews: 
# Minqing Hu and Bing Liu. "Mining and summarizing customer reviews". Proceedings of the ACM SIGKDD International Conference on Knowledge Discovery & Data Mining (KDD-04), 2004.

import nltk
import random
# import matplotlib.pyplot as plt
from nltk.corpus import movie_reviews
from nltk.metrics import ConfusionMatrix
import pickle

#### COLLECT LABELED DATA
# import movie reviews and their category (pos/neg label), word tokenization is already included
# In each category (we have pos or neg), take all of the file IDs (each review has its own ID), 
# then store the word_tokenized version (a list of words) for the file ID, 
# followed by the positive or negative label in one big list. 
documents = [(list(movie_reviews.words(fileid)), category)
             for category in movie_reviews.categories()
             for fileid in movie_reviews.fileids(category)]

# Shuffle because we're going to be training and testing. 
# If we left them in order, chances are we'd train on all of the negatives, some positives, 
# and then test only against positives. We don't want that.
random.shuffle(documents)

# example of 1 document:a big list, where the first element is a list the words, 
# and the 2nd element is the "pos" or "neg" label.
# print(documents[1])
# print(len(documents))


#### PREPROCESS THE REVIEWS
# collect all words that we find, so we can have a massive list of typical words.
all_words = []
for w in movie_reviews.words():
    all_words.append(w.lower())

#### BAG OF WORDS: FREQUENCY COUNT
# perform a frequency distribution, to then find out the most common words
all_words = nltk.FreqDist(all_words)

# Frequency Distribution Plot
# all_words.plot(30,cumulative=False)
# plt.show()

# return 15 most common words
#print(all_words.most_common(15))

# return occurences for 1 particular word
#print(all_words["stupid"])

# vocabulary reduction: create word features with the top 3,000 most common words
word_features = list(all_words.keys())[:3000]
#print('word features:', word_features)


#### CREATE VECTORS
# function that will find these top 3,000 words in our positive and negative documents, 
# marking their presence as either positive or negative
def find_features(document):
	words = set(document)
	features = {}
	for w in word_features:
		#print('word:', w)
		features[w] = (w in words)
		#print('features:', w in words)
#print(features)
	return features

featuresets = [(find_features(review), category) for (review, category) in documents]
# for featureset in featuresets: 
# 	print('featureset:', featureset, '\n')

#### SPLIT SAMPLE DATA IN TRAIN & TEST DATA
# set that we'll train our classifier with (80% of the sample data)
training_set = featuresets[:1600]
training_reviews = [featureset[0] for featureset in training_set]
training_labels = [featureset[1]for featureset in training_set]

# set that we'll test against (20% of the sample data)
testing_set = featuresets[1600:]
testing_reviews = [featureset[0] for featureset in testing_set]
testing_labels = [featureset[1]for featureset in testing_set]

#### TRAIN
# define classifier & train
classifier = nltk.NaiveBayesClassifier.train(training_set)

#### TEST

# calculate accuracy
print("Classifier accuracy percent:",(nltk.classify.accuracy(classifier, testing_set))*100)

# create confusion matrix
cm = nltk.ConfusionMatrix(testing_labels, label_probdist)
print(cm.pretty_format(sort_by_count=True, show_percents=True, truncate=9))

# find most valuable words
#classifier.show_most_informative_features(15)

#### PREDICT NEW TEXT
#classifier.classify(find_features('This is a test for a movie review. It was great. It was fantastic. The acting was really good.'))

#### SAVE THE MODEL
# save_model = open("naivebayes.pickle","wb")
# pickle.dump(classifier, save_model)
# save_model.close()

# -------------------------------
# Once your model is trained & tested & saved
# # open model
# model_f = open("naivebayes.pickle", "rb")
# model = pickle.load(model_f)
# sentence = "This film is absolutely one of marvels best movies, but that does not mean that it is a child friendly film . This film has things ranging from violent stabbings to (at times) quite a sad story . There are many over the top fight scenes in this film with many stabbings but these are done without great detail , but still show realistic blood wounds . There is a lot of blood in this film but all of it is dried so nothing to shocking with the blood is shown . The fight scenes can also be very violent with lots of bashing in them and lots of dried blood shown and the baddie in one scene repeatedly kicks the hero even when he's given up , but this scene is very short and no blood is shown in this part . There are many deaths by guns in this film including a scene where a man shoots through his girlfriend to get to his enemy and then repeatedly shoots him , but again no great detail is shown . There's a part where someone's neck is cut , but no detail at all is shown and it is very quick . In another scene someone takes a bullet and quite a detailed wound is shown . The film has quite a lot of swear words in it , but it's only the s word and a middle finger is shown at one point."
# print(model.classify(find_features(sentence)))
# model.show_most_informative_features(15)

# model_f.close()

