# from pattern.fr import parsetree
from pattern.en import parsetree

def make_ngrams (sentence, n=3, stringify=False):
  return [[word.string if stringify else word for word in sentence[i:i+n]] for i in range(len(sentence)-n)]

def make_ngrams_for_tree (tree, n=3, stringify=False):
  return [make_ngrams(sentence, n, stringify) for sentence in tree.sentences]

with open('input/lelivre_extrait.txt', 'r') as h:
  print("Reading text file", flush=True) 
  # Read text file
  raw_text = h.read()

  print("Tokenizing", flush=True)
  parsed_text = parsetree(raw_text)

  print(make_ngrams_for_tree(parsed_text, stringify=True))
