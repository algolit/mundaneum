# https://www.tutorialspoint.com/python/python_multithreading.htm
# pip install opencv-python

from PIL import Image
import codecs
import os

f_root = '../mundaneum_archive/ALGOLIT_utf8/'
f_target = 'txt2image'
im_maxheight = 800
im_thumbheight = 500
img_data = []

# all strings to consider as empty lines
empty_line = [ '\r', '\r\n', '\n' ]
# all chars to consider as invalid
invalid_chars = [ '\t', '\r', '\n', ' ' ]

def line_data( txt, id ):
	
	first_non_space_char = 0
	for l in txt:
		if l != ' ':
			break
		first_non_space_char += 1 
	
	stripped_txt = txt.lstrip().rstrip()
	empty = len( stripped_txt ) == 0

	for el in empty_line:
		if el == stripped_txt:
			empty = True

	return {
		'id': id,
		'page': -1,
		'txt': txt.rstrip(),
		'start': first_non_space_char,
		'empty': empty,
		'length' : len( stripped_txt ),
		'columns': []
		}

def txt2image( name, txt_path, png_path ):

	f = codecs.open( txt_path, 'r', encoding='utf-8' )
	lid = 0
	lines = []
	for l in f:
		# print( 'line ' + str( lcount ) )
		lines.append( line_data( l, lid ) )
		lid += 1
	f.close()

	# ready to generate image
	im_parts = 1
	im_width = 0
	im_height = len( lines )
	for l in lines:
		if im_width < l['length']:
			im_width = l['length']
	im_yoffset = 0
	im_parts = 1 + int(im_height) / int(im_maxheight)
	im_th = im_thumbheight
	if im_th > im_height:
		im_th = im_height
	#print( im_width, im_height )
	im_ty = 0
	im_thumb = Image.new( 'RGB', (im_width,im_thumbheight) )
	thumb_path = png_path + ".thumb.png"

	idata = { 
		"name": name, 
		"width":im_width, 
		"height": im_height, 
		"txt": txt_path,
		"opencv": "",
		"png": [],
		"thumb": thumb_path, 
		"char" : 0, 
		"char_valid": 0,
		"processed": 0	
		}
	
	if im_width < 1 or im_height < 1:	
		idata["parts"] = 0
		idata["thumb"] = ""
		img_data.append( idata )
		return
	
	for part in range( 0, im_parts ):
		ih = im_maxheight
		if ih + im_yoffset > im_height:
			if im_yoffset > 0:
				ih = im_height % im_yoffset
			else:
				ih = im_height
		if ih == 0:
			break
		im = Image.new( 'RGB', (im_width,ih) )
		for y in range( 0, ih ):
			txt = lines[y+im_yoffset]['txt']
			max_x = lines[y+im_yoffset]['length']
			idata[ 'char' ] += max_x
			add_2_thumb = False
			if y+im_yoffset < im_th:
				add_2_thumb = True
			for x in range( 0, im_width ):
				c = ' '
				if x < max_x:
					c = txt[x]
				color = (0,0,0)
				if not c in invalid_chars:
					idata[ 'char_valid' ] += 1
					color = (255,255,255)
				im.putpixel( (x,y), color )
				if add_2_thumb:
					im_thumb.putpixel( (x,im_ty), color )
			if y+im_yoffset < im_th:
				im_ty += 1
		ppath = png_path + "."
		if part < 10:
			ppath += "0000"
		elif part < 100:
			ppath += "000"
		elif part < 1000:
			ppath += "00"
		elif part < 10000:
			ppath += "0"
		ppath += str( part ) + ".png"
		im.save( ppath )
		idata["png"].append( ppath )
		im_yoffset += im_maxheight

	im_thumb.save( thumb_path )
	img_data.append( idata )

# starting the process

if not os.path.isdir(f_target):
	os.makedirs(f_target)

f_to_process = []

for (dirpath, dirnames, filenames) in os.walk(f_root):
	filenames.sort()
	for f in filenames:
			st = os.path.splitext(f)
			if st[1] == '.txt':
				ipath = os.path.join( f_root, f )
				opath = os.path.join( f_target, st[0] )
				f_to_process.append( ( st[0], ipath, opath ) )
				#print( opath, path )
				#txt2image( st[0], ipath, opath )

# sorting results
def f2sort_sort( a, b ):
	if a[0] > b[0]:
		return 1
	elif a[0] == b[0]:
		return 0
	else:
		return -1

f_to_process.sort( f2sort_sort )

i = 0
for f in f_to_process:
	txt2image( f[0], f[1], f[2] )
	print( '***************\nfile processed: ' + f[0] )
	print( str( i + 1 ) + ' out of ' + str( len( f_to_process ) ) )
	i += 1


# json generation

json = open( os.path.join( f_target, 'data.json' ), 'w' )

json.write( '[\n' )

h = 0
for d in img_data:
	json.write( '\t{\n' )
	l = len( d )
	i = 0
	for k in d:
		if k == 'png':
			json.write( '\t\t"' + k + '":[' )
			j = 0
			for pp in d[k]:
				if j > 0:
					json.write( ',' )
				json.write( '"' + d[k][j] + '"' )
				j += 1
			json.write( ']' )
		elif isinstance(d[k],str):
			json.write( '\t\t"' + k + '":"' + d[k] + '"'  )
		else:
			json.write( '\t\t"' + k + '":' + str( d[k] ) )
		if i < l - 1:
			json.write( ',' )
		json.write( '\n' )
		i += 1
	json.write( '\t}' )
	if h < len( img_data ) - 1:
		json.write( ',' )
	json.write( '\n' )
	h += 1

json.write( ']\n' )
json.close()
print( img_data )
