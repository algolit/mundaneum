#import 
import os.path
import glob
from nltk.corpus import gutenberg

from pattern.vector import Document, Model

# https://www.clips.uantwerpen.be/pages/pattern-vector#model

# model.documents             # List of Documents (read-only).
# model.document(name)        # Yields document with given name (unique).
# model.inverted              # Dictionary of (word, set(documents))-items. 
# model.vector                # Dictionary of (word, 0.0)-items. 
# model.vectors               # List of all Document vectors. 
# model.features              # List of all Document.vector.keys(). 
# model.classes               # List of all Document.type values.  
# model.weight                # Feature weights: TF | TFIDF | IG | BINARY | None
# model.density               # Overall word coverage (0.0-1.0).
# model.lsa                   # Concept space, set with Model.reduce().
# model.append(document)
# model.remove(document)
# model.extend(documents)
# model.clear()


model = Model()
documentIndex = {}

# https://www.clips.uantwerpen.be/pages/pattern-vector#document

# document = Document(string, 
#        filter = lambda w: w.lstrip("'").isalnum(), 
#   punctuation = '.,;:!?()[]{}\'`"@#$*+-|=~_', 
#           top = None,       # Filter words not in the top most frequent.
#     threshold = 0,          # Filter words whose count falls below threshold.
#       exclude = [],         # Filter words in the exclude list. 
#       stemmer = None,       # STEMMER | LEMMA | function | None.
#     stopwords = False,      # Include stop words?
#          name = None,
#          type = None,
#      language = None, 
#   description = None)

# document.id                 # Unique number (read-only).
# document.name               # Unique name, or None, used in Model.document().
# document.type               # Document type, used with classifiers.
# document.language           # Document language (e.g., 'en').
# document.description        # Document info.  
# document.model              # The parent Model, or None.
# document.features           # List of words from Document.words.keys().  
# document.words              # Dictionary of (word, count)-items (read-only).
# document.wordcount          # Total word count.
# document.vector             # Cached Vector (read-only dict).

for fileid in gutenberg.fileids():
  raw_text = gutenberg.raw(fileid)
  print('Loading: {}'.format(fileid), flush=True)
  document = Document(raw_text, name=os.path.splitext(fileid)[0])
  model.append(document)

print()
print('Loaded {} documents'.format(len(model.documents)))
print()

# for document in model.documents:
#   print(document.name)
#   print('Latent: {}'.format(document.tfidf('latent')))
#   print('Beauty: {}'.format(document.tfidf('beauty')))
#   print('Forest: {}'.format(document.tfidf('forest')))
#   print('War: {}'.format(document.tfidf('war')))

while True:
  lookup = input('Word to lookup: ')
  print()
  print('************')
  print('{:<25}{}'.format('TFIDF', lookup))
  print('************', '\n')
  for document in model.documents:
    print('{:<25}{}'.format(document.name, document.tfidf(lookup)))
  print()
  print('************', '\n')