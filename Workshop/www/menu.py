import os

html_folder = 'data/'

menu = open( 'menu.html', 'w' )
menu.write('''
<html>
    <head>
        <title>menu</title>
        <style>
        html, body, p { padding:0; margin: 0; font-size: 11px;  }
        html, body { background: #888;  }
        p { clear: both; }
        a { display:block; float:left; white-space: nowrap; text-decoration: none; color: #fff; padding: 3px; background: #555; }
        a:hover { color: #000; background: #fff; }        
        </style>
    <head>
    <body>
''')

for (dirpath, dirnames, filenames) in os.walk(html_folder):
    filenames.sort()
    count = 0
    for f in filenames:
        id = str( count )
        while( len( id ) < 4 ):
            id = '0' + id
        menu.write('<p><a title="' + f[:-5] + '" href="' + os.path.join( html_folder, f ) + '" target="display">[' + id + '] ' + f[:-5] + '</a></p>' )
        count += 1

menu.write('''
    </body>
<html>
''')

menu.close()
