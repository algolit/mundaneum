#!/bin/bash
#iconv -f original_charset -t utf-8 ARC-MUND-PUB-BULLETIN-IIB-76-BCAISC-1906.txt > ARC-MUND-PUB-BULLETIN-IIB-76-BCAISC-1906-UTF8.txt
#iconv -f CP1252 -t utf-8 ARC-MUND-PUB-BULLETIN-IIB-76-BCAISC-1906.txt > ARC-MUND-PUB-BULLETIN-IIB-76-BCAISC-1906-UTF8.txt

target=../mundaneum_archive/ALGOLIT_utf8/

for path in ../mundaneum_archive/ALGOLIT/*.txt; do
    echo $path
    filename=$(basename "$path")
    iconv -f CP1252 -t utf-8 $path > $target$filename
done;
