from collections import Counter
import nltk
import re
import pickle


# VARIABLES

source = open("Why_Was_the_Sea_Stormy.txt", "r")
destination = open("Why_Was_the_Sea_Stormy_trigrams.txt", "w")
destination.write("MOST FREQUENT TRIGRAMS with Penn's TREEBANK\n\n\n")

stopwords = ["the", "a", "to", "of", "in", 'is', "with", "on", "for", "at", "from", "about",\
 "are", "an", "up", "out", "have", "be", "this", "one", "says", "as", "all", "just", "was", "so",\
 "her", "his", "it", "its", "their", "me", "our",\
 "and", "that", "but", "like", "what", "if", "then", "there", "they", "us", "my", "your", "theres", "theyre", "or", "not",\
 "which", "by", "who", "them", "into", "while", "been", "dont", "where", "youre", "has", "when", "over", "him", "were", "doesnt",\
 "did", "thats", "how", "had", "these", "would", "could", "because", "didnt"]

# FUNCTIONS
## sort words by frequency (import module)
def sort_dict(frequency_d):
	c=Counter(frequency_d)
	frequency = c.most_common()
	return frequency

## MAKE SURE ALL VARIABLES ARE DECLARED WITHIN THE LOOPS		

# 1. Create dictionary of trigrams
trigrams = {}
for line in source:
	# remove punctuation
	clean_tri = []
	words = line.split(" ")
	for word in words:
		if word not in stopwords:
			cleaning = re.compile(r"[A-Za-z0-9]")
			if cleaning.match(word):
				clean_tri.append(word)
			else:
				pass
	# find trigrams
	tricount = nltk.trigrams(clean_tri)	
	# count frequency of each trigram and add trigram + value in dictionary			
	for trigram in tricount:
		if trigram in trigrams:
			trigrams[trigram] += 1
		else:
			trigrams[trigram] = 1	

trigrams_sorted = sort_dict(trigrams)
first100pairs = trigrams_sorted[:100]


with destination as text:
	for tri, frequency in first100pairs:
		text.write("{} : {} \n".format(tri, frequency))




