#!/usr/bin/env/ python

#Copyright (C) 2018 Constant, Algolit

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details: <http://www.gnu.org/licenses/>.

# From this manual: https://treetaggerwrapper.readthedocs.io/en/latest/
# Abbréviations TAGS: https://www.sketchengine.eu/french-treetagger-part-of-speech-tagset/

import pprint   # For print of sequences on separate lines.
import treetaggerwrapper

# build a TreeTagger wrapper
tagger = treetaggerwrapper.TreeTagger(TAGLANG='fr')

# tag text
with open("lelivre_extrait.txt", 'r') as g:
	texte = g.read()
	tags = tagger.tag_text(texte)

# print tags list (list of string output from TreeTagger).
#pprint.pprint(tags)

# # transform it into a list of named tuples Tag, NotTag (unknown tokens) 
# # TagExtra (token having extra informations requested via tagger options - like probabilistic indications)
tags_meta = treetaggerwrapper.make_tags(tags)
pprint.pprint(tags_meta)

# select elements
words = []
pos_tags = []
lemmas = []
for tag in tags_meta:
	word = tag[0]
	if word:
		words.append(word)
	pos = tag[1]
	if pos:
		pos_tags.append(pos)
	lemma = tag[2]
	if lemma:
		lemmas.append(lemma)

print('words:', words, '\n', 'part-of-speech tags:', pos_tags, '\n', 'lemmas:', lemmas)