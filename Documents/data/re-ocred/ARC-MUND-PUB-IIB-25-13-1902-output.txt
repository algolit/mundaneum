 
Institut International de Bibliographie
PUBLICATION N° 25 Ù INDICE BIBLIOGRAPHIQUE [025.4]

CLASSIFICATION BIBLIOGRAPHIQUE

DEÉCIMALE

——. > —

 

 

 

 

 

TABLES GÉNÉRALES REFONDUES de
établies en vue de la publication du
Répertoire Bibliographique Universel

EDITION FRANÇAISE
publiée avec le concours du

BUREAU BIBLIOGRAPHIQUE DE PARIS

 

FASeLCEU CE NN - 19
Tables des divisions [618] et [619]

_ Gynécologie. Pédiatrie. Médecine comparée

 

 

INSTITUT INTERNATIONAL DE BIBLIOGRAPHIE

BRUXELLES, 1, rue du Musée, PARIS, 44, rue de Rennes.
ZURICH, 30, Eidmattstrasse.

2 se

1902: . 4

 

 

 

 

 
 
 
 

EE

 

RO A
EX

A
jrs

 
 

 

 

 

 

GYNÉCOLOGIE. — PÉDIATRIE 618.1 (0)

 

618 Gynécologie, Obstétrique, Pé-
diatrie.

DIVISIONS GÉNÉRALES :

618.1 GYNÉCOLOGIE. Maladies des organes génitaux de la femme.
618.2 Grossesse normale.

618.3 Pathologie de la grossesse.

618.4 Travail.

618.5 ‘Pathologie du travail.

618.6 Suites de couches.

618.7 Accidents des suites de couches. Hygiène du nouveau-né.
618.8 Opérations obstétricales.

618.9 PÉDIATRIE. Maladies des enfants.

618.1 Gynécologie et Obstétrique.

En général, la plupart des affections, ne présentant aucune particularité qui
les distingue chez l'homme et chez la femme, sont classées à 616 et 617.
Lorsque ces particularités méritent d'être signalées, on dispose des subdivisions
communes :

05 Formes spéciales.
053 Affections suivant les sexes. Femmes.

Les affections médicales et chirurgicales propres aux organes génitaux de la
femme età leurs fonctions et plus spécialement l’obstétrique sont classées à 618.1.

Le classement par subdivisions communes est fait ici de la même manière
que pour la pathologie interne[616], à savoir :

1° Classement par organes et par systèmes, — Les nombres qui correspon-
dent à ces subdivisions sont précédés de deux zéros: Ex. :

005 Hémorragies.
618,15 Maladies de l'utérus.
618.15,005 Hémorragiesutérines.

2° Classement par maladies. — Les nombres qui correspondent à ces subdi-
visions sont précédés d’un zéro. Ex. :

03 Sémiologie. »
618.15 Maladies de l'utérus.
618,15.03 Sémiologie des maladies de l'utérus.
618.15.005 Hémorragies utérines.

à 618.15.005.03 Sémiologie des hémorragies utérines.

La manière de combiner les subdivisions communés avec les subdivisions
principales a été exposée en détail sous 616.0

618.1 (0) Généralités. Ouvrages généraux.

La gynécologie, et toutes ses divisions, peuvent être combinées avec les
subdivisions de généralités, de forme, de lieu et de temps, Voir les tables
générales des subdivisions communes, dont les principales ont été reproduites
sous 616 (0). Ex. :

618.1(02) Traités de gynécologie. .

 

 
 

 

 

 

 

 

618.11 GYNÉCOLOGIE. — PÉDIATRIE
618.11 Ovaires.
.12 Trompes de Fallope.
519 Organes péri-utérins.
1372 Ligaments péri-utérins.
HOTEL Ligaments larges.
.137.12 — ronds.
19/2193 = sacrés.
.137.14 = tubo-ovariens.
19722 Tissu cellulaire et péritoine.
19729 Muscles.
See Vaisseaux.
“13720 Nerfs. .
.137.8 Cul-de-sac de Douglas.
.I4 Utérus et col utérin.
A7 Col de l'utérus.
.147.8 Corps de l'utérus.
.147.9 Utérus gravide.
E9 Vagin.
.16 Vulve.
.17 Troubles fonctionnels. Maladies de la menstrua-
tion.
ALL Troubles de la période d'établissement de la mens-
truation.
.172 Troubles de la période d'activité de la menstruation.
179 Troubles de la ménopause.
.174 Ménorrhagie.
17 Dysménorrhée.
.176 ‘ Aménorrhée.
7 Stérilité.
.178 Leucorrhée.
“170 Autres troubles.
.18 Affections du périnée chez la femme.
.19 Glande mammaire.
618.2 Obstétrique. Grossesse.
Classer à 618.2 (02) les Traités d'accouchement et d'obstétrique et tout ce qui a
trait à l'accouchement en général.
2T Physiologie de la grossesse.
.22 Signes de la grossesse.
.23 Durée de la grossesse.
-24 Hygiène de la grossesse.

 
LR Perse

 

LE — —

 

GYNÉCOLOGIE. — PÉDIATRIE 618.52

 

618.25
.28

618.3
31

.317
312
319
.314
319
*310
.318

192
39

.34
39
.36

‘37
.38

.39

618.4

.4I
.42
.43
44

618.5
DT

p2

Grossesses multiples. Jumeaux.
Grossesse chez des femmes ayant subi des opé-
rations gynécologiques.

Pathologie de la grossesse.

Grossesse extra-utérine.

On traitera la grossesse extra-utérine et chacune de ses subdivisions
comme une maladie. On aura, par conséquent, pour le traitement chirur-
gical de la grossesse abdominale, 6r8.3r8.08095 et on mettra à 618.3r.o7
l'anatomie pathologique de la grossesse extra-utérine.

Grossesse ovarienne.
— tubaire.
—  péri-utérine.
— anormale.
—  yaginale.
— intestinale.
— abdominale.

Pathologie de l'œuf.
— du fœtus. Mort et rétention.
E des annexes du fœtus.
_ de la caduque.
== du placenta.
— _ de l’amnios.
— du cordon ombilical.

Avortements. Fausse-couches. Accouchements
prématurés.

Accouchement. Travail (Physiologie).

Voir aussi 173.4 Infanticide (Morale), 340.615 Avortement (Médecine légale)
343.621 Avortement (Droit pénal). :

Mécanisme du travail.
Présentations. Positions.
Evolution clinique du travail.
Direction du travail normal.

Pathologie du travail.

Anomalies du travail dépendant de l'impuissance
des forces expulsives.
Obstacles mécaniques.

 
 

 

 

618.53! GYNÉCOLOGIE. — PÉDIATRIE
618.53 Anomalies du fœtus.
.54 Hémorragies de l’accouchement.
.55 Rupture et dilacération des voies génitales.
.56 Rétention du placenta.
07 Inversion de l'utérus.
.58 Prolapsus du cordon.
59 Autres complications pathologiques.
618.6 Etat puerpéral (Physiologie, Hygiène).
Soins à donner, y compris ceux intéressant l’enfant.
618.7 Pathologie de l’état puerpéral.
71 Maladies de la lactation. Fièvre de lait.
72 Fièvre puerpérale.
79 Métrite. Péritonite.
.74 Septicémie.
.75 Eclampsie puerpérale.
.76 Manie puerpérale.
71 Phlébite. Thrombite. Phlegmatia.
.78 Autres affections puerpérales.
.79 Mort subite après la délivrance.
618.8 Opérations obstétricales.
.81 Levier. Forceps.
.82 Version.
.83 Embryotomie.
.84 Dilatation du col.
8) Symphyséotomie.
.86 Opération césarienne.
.87 Ablation du placenta.
.88 Provocation de l'accouchement.
.89 Autres opérations obstétricales.
.89I Antisepsie.
.892 Anesthésie obstétricale.
.893 Réduction de l'utérus gravide.
.895 Incision du col.
.897 Opération de Porro.

VERRE
 

GYNÉCOLOGIE. — PÉDIATRIE 618.9

 

618.9

Pédiatrie. Maladies des enfants.

On classe à 618.9 les ouvrages généraux relatifs aux maladies des en-
fants, Ex. :
618.9 (o2) Traité de Pédiatrie.

La plupart des affections médicales ne présentent aucune particularité qui les
distingue chez l’enfant et on les classe à la Pathologie interne et externe, 616
et 617. Lorsque ces particularités méritent d’être signalées, on dispose de
a subdivision commune : +

053 Affection selon les âges. Enfants.

Toutefois, on pourra réunir sous 618.0 toutes les questions de physiologie,
d'hygiène, de thérapeutique, de pathologie concernant l'enfance, en combinant
par : la division 618.9 Pédiatrie avec toutes les autres divisions de la méde-
cine 611 à 617. Ex. :

618.0 : 616.312.0024 Stomatite gangréneuse chez l'enfant.

 

 

 
 

 
 

 

m—)

MÉDECINE COMPARÉE 619.19

 

619 Médecine comparée. Art vété-
rinaire.

On ne classe ici que la médecine comparée en général et tout ce qui concerne
l'anatomie, la physiologie, la pathologie et la thérapeutique des animaux utiles
ou domestiques. Ces mêmes questions étudiées au point de vue des autres
animaux sont classées avec la Zoologie [59].

Voir aussi les divisions suivantes :
636 Zootechnie, élevage.
682.1 Maréchalerie.
614.9 Police sanitaire des animaux.
614.317 Inspection des viandes.

619 (0) Généralités. Ouvrages généraux.

L'art vétérinaire et toutes ses divisions peuvent être combinées avec les
subdivisions de généralités, de forme, de lieu et de temps.
Voir les subdivisions communes, dont les principales ont été reproduites
sous 616 (0). Ex. :
619 (05) Revues de médecine vétérinaire ï

619 : Médecine comparée générale.

Ces questions sont classées ici à 6r9 : suivi ‘du nombre propre à chacune de
ces questions, conformément aux divisions de la médecine générale 61r à
618. Ex. : :
619:616,5 Les maladies du sang en médecine comparée.

619,1à.09 Art vétérinaire.

Les questions d'art vétérinaire propres à chaque espèce d'animaux domes-
tiques sont classées de 610.1 à 6r9.9. Ex. :
619.4 Médecine vétérinaire du porc.
Ces divisions concordent avec celles de 636 Zootechnie. Elles peuvent être
subdivisées à leur tour comme la médecine comparée générale. Ex. :
619.4 :616.5 Les maladies du sang chez le porc.
Les maladies propres aux animaux et qui ne sont pas inscrites à la patho-
# logie interne ni externe, à 616 et à 617, sont classées à 616.999 Autres maladies
générales. Ainsi on classera à
619.5:616.099 Choléra des poules (qui n’a aucun rapport avec le
choléra humain).
619.4: 616.990 Rouget du porc.

619.1 Les équidés domestiques.

IL Cheval. . =

12 Ane.
19 Mulet.
.19 Autres équidés domestiques.

 
 

|
|
1
|
|

 

 

619.2 MÉDECINE COMPARÉE

 

619.2 Les grands ruminants. Bétail.
-21  Bœuf. Vache.

619.3 Mouton. Chèvre.

ST Mouton =
.32 Chèvre.

619.4 Porcs.

619.5 Oiseaux de basse-cour.

ST Coq. Poule.
.92 Dindon.
.93 Pintade.

.4 Faisan.
.55 Paon.
.56 Pigeon domestique.
Voir 610.62 Pigeons voyageurs.
07 Canard.
.58 Oie.
.99 Autres oiseaux de basse-cour.

619.6 Oiseaux exploités pour leurs plumes, leur
travail. Oiseaux d'agrément.

.61 Oiseaux exploités pour leurs plumes. Autruches.
.62 Pigeons voyageurs.

.68 Oiseaux d'agrément et d’ornementation.

.681 Cygnes.

.682 Oiseaux de cage et de volière.

.683 Perroquets.

619 7 Chien,
619.8: Chat

619.9 Autres animaux domestiques ou utiles
(cobayes, lapins, etc.).

 

on
ver

 

 
 

 

 
 
 
  
 

 

 
 
 

 
