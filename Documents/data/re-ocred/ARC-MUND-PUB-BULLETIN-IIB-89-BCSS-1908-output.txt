 

INSTITUT INTERNATIONAL
DE BIBLIOGRAPHIE PUBLI-

Bur ps L'Ixsriter : Améliorer, développer et unifier les méthodes en matière CATION
de bibliographie et de documentation. Organiser la coopération scientifique
internationale entre groupes de toutes spécialités en vue d'élaborer, suivant N° 89
an plan d'ensemble, des travaux embrassant la documentation universelle,
Établir un centre pour la conservation des répertoires et collections de
documents.

 

 

 

 

 

 

 

Bibliothèque Collective

des Sociétés Savantes.

BRUXELLES. — PALAIS DES BEAUX-ARTS
RUE DE LA RÉGENCE

OUVERTE TOUS LES JOURS NON FÉRIÉS
DE 9 À 12 ET DE 2 A 6 HEURES

Indice bibliographique : BRUXELLES

{o27 (493.2)] 1, RUE DU MUSÉE

 
 

 

Le But et le Fonctionnement de la Bibliothèque Collective

Sa corrélation avec les autres services

de documentation de l'Institut International de Bibliographie

La Bibliothèque collective des Sociétés savantes est un organisme
tout nouveau, de nature coopérative et fédérative. Son ohjet est de
grouper les collections de livres et de périodiques appartenant aux
associations scientifiques et aux rédactions des grands périodiques
qui sont domiciliés à Bruxelles ; de mettre ces collections à la dispo-
sition de tous les affiliés et d'en confier l'administration à l'Institut
International de Bibliographie, agissant en concert avec un Comité
de délégués.

Vingt-cinq groupes ont constitué la nouvelle Bibliothèque et mis
en commun l'usage d'environ 40,000 unités biblicgraphiques.

Les collections de livres de l'Institut international de Bibliographie
sont jointes à la Bibliothèque Collective.

+
* *

Une administration centralisée va permettre d'effectuer dans de
bonnes conditions, les opérations complexes que nécessite l'organi-
sation des callectinns de livres et faute desquelles les bibliothèques
corporatives restent inutilisées ou ne s'accroissent point : c'est la
possibilité de consulter des cuvrages à toute heure du jour dans des
locaux chauftés, éclairés et gardés, c'est l'inventaire et le cataloguage,
l'envoi à domicile, la demande des ouvrages aux auteurs, la sollicita-
tion continue de dons, de livres et d'échanges de périodiques, de
manière à constituer des ensembles bien systématiques et tenus à
jour de la production contemporaine.

Les sociétés savantes possèdent en leurs Bulletins de puissants
moyens d'accroitre sans frais leur bibliothèque à l'intervention du
Service international des échanges, en les échangeant avec les bulle-
tins et périodiques similaires du monde entier. D'autre part, la publi-
cation régulière d’'analyes et comptes-rendus bibliographiques, voire
de simples listes classées d'ouvrages spéciaux, peut faire affluer
l'envoi des nouveautés par les auteurs et Îles éditeurs. Certaines des
sociétés affiliées ont de longue date organisé leur service de biblio-
thèque, Elles peuvent être fières des résultats de leurs persévérants
efforts. Avec l'organisation nouvelle il est à espérer qu'elles serviront
d'exemple aux autres et qu'ainsi la bibliothèque centrale ne se bor-
nera pas seulement aux ouvrages actuellement existants, mais fonc-
tionnera à l'avenir comme un organisme approprié au collectionne-
ment systématique des livres et des revues.

+
+ +

Envisagée au point de vue général, la Bibliothèque Collective est
destinée à devenir l'utilé compiément et l'auxiliaire de la Biblio-
thèque royale, laquelle fatalement ne peut pas pénétrer dans toutes
les spécialités de la science moderne, tandis qu’il appartient aux
sociétés savantes constituées de le faire. Pour constituer les vastes
collections de livres nécessaires à notre époque, la coopération se
présente comme la formule économique appropriée aux facultés des
petits pays, dont les ressources sont nécessairement limitées.

Envisagée au point de vue des sociétés scientifiques, la Biblio-
thèque Collective est un essai de travail concerté qui ne peut man-
quer de porter ses fruits. Il répond, en effet, aux tendances de la
science moderne d'organiser de plus en plus le travail et d'unifier les
méthodes en vue de marcher plus rapidement dans la Voie des décou-
vertes et du progrès.

Envisagée au point de vue de l'Institut International de Biblio-
graphie, à qui elle fournit une source abondante de documents pour
ses travaux, la Bibliothèque Collective constitue une des cinq sec-
tions de son organisation, laquelle comprend aussi, avec des degrés
différents de développement, la Section bibliographique, la Section
iconographique, la Section de documentation et le Service central de
renseignements relatifs aux institutions et collections belges et con-
cernant les sciences, les lettres, le livre de l'enseignement,

Les livres de la Bibliothèque Collective ont fait l'objet d'une pre-
mière mise en place. Iis ont été groupés par fonds dont ils pro-
viennent et les fonds se succèdent dans l'ordre de la Classification
décimale d'aprés la matière à laquelle ils se rapportent et qui est
celle-la même dont s'occupe chaque société affiliée. Ainsi apparaît
nettement le caractère encyclopédique qui préside à notre œuvre et
le but proposé aux efforts communs. La classification fait connaître
à tout moment les branches non encore représentées ét ce sera la
tâche de demain de négocier de nouvelles affiliations ou de susciter
la création de groupes nouveaux. Toutes les sciences ne sont-elles
pas représentées en Belgique par des sociétés ou par des revues spé-

 

ciales et, s'il n'en est pas ainsi, n'y at-il pas à faire que cela
devienne?

Le catalogue — on a pu dire avec raison que le catalogue est à
la Bibliothèque ce que le cerveau est aux organes — le catalogue des
diverses collections réunies jusqu'à ce jour est en bonne voie d’éla-
boration. Il est dressé d’abord un inventaire des ouvrages de chaque
société, et celui-ci constitue comme le relevé comptable de la prise
en charge des collections. [1 y a donc autant d'inventaires que de
groupes affiliés. Quant au catalogue proprement dit, formé par
duplicata de l'inventaire, il n'y en a été établi qu'un seul comprenant
en une seule série les ouvrages de toutes les collections. Les lecteurs
auxquels l'usage de tous les fonds est offert indistinctement, n'auront
donc à consulter qu'un seul guide, Le cataloguc est à double entrée,
alphabétique par nom d'auteurs et décimal par matières. [1 est établi
sur fiches mobiles du format international, et à l'état de manuscrit.
Îl est à espérer que chaque société aura à cœur de publier dans son
Bulletin le catalogue de son propre fonds et d'en faire connaître par
la même voie les accroissements. Une telle liste remplacera avanta-
geusement les accusés de réception publiés déjà dans maints bulle-

tins,
%

+ à

Dans les locaux mêmes de la Bibliothèque Collective a été installé
le Service de renseignements sur la Belgique scientifique, artistique et lilféraire.
À la suite d'une enquête étendue, l'Institut a réuni de nombreux
documents sur presque tous les organismes nationaux en ce domaine.
Il a en outre commencé l'élaboration d’un Catalogue collectif des Biblio-
fhèques de Belgique, En créant cette organisation nouvelle l'Institut a
été guidé par cette idée : permetiré aux membres des sociétés
scientifiques de s'adresser ici dés le début de leurs recherches
bibliographiques. Ils connaîtront par la consultation du catalogue
de la Bibliothèque Collective si les ouvrages désirés existent ici, et
alors pourront en ce cas les consulter dans la salle de lecture; s'ils ÿ
font défaut, ils pourront connaître par la consultation du Catalogue
collectif des Bibliothèques belges, dans quelle autre bibliothèque
is les trouveront.

Ce dernier catalogue comprend à cette heure environ 660,060 titres
d'ouvrages possédés par 54 des grandes bibliothèques, tant de Bru-
xelles que de la province.

Ce premier catalogue est complété par le Catalogue général des
bibliothèques populaires en Belgique.

Il comprend, lui, à ce jour environ un million de titres d'ouvrages
déposés dans 800 bibliothèques populaires. Il est destiné à servir de
base aux envois des livres que leur fait le gouvernement. Ces envois
constituent un mode d'encouragement aux bibliothèques populaires

 
réellement efficace. Ils pourront à l'avenir s'effectuer d'nne manière
de plus en plus systématique.

Faute de place, pour les concentrer en un même bâtiment, les
autres services et collections de l'Office demeureront momentanément
au local no 1 {rue du Musée) et au local ne 2 (Chapelle Saint-Georges,
Montagne de ia Cour). Par le téléphone on pourra, en partie, les
utiliser d'ici même,

Ces services comprennent d'abord les Réfertoires bibliographiques.
Ceux-ci constituent comme le catalogue général de toutes les grandes
bibliothèques du monde ou, encore, le catalogue de la bibliothèque
idéale, réellement encyclopédique, mondialeet universelle, que forme-
rait la réunion de tout ce qui a été imprimé depuis l'invention de l’im-
primerie. |

Les répertoires bibliographiques de l'Institut, établis sur fiches,
sont actuellement riches de 8 millions de notices. Celles-ci sont
ordonnées en trois séries, de manière à répondre directement à ces
trois questions principales :

Qu'est-ce qui a été publié par tel auteur (Répertoire des auteurs) ?

Qu'est-ce qui a été publié sur telle question (Répertoire des
matières) ?

Qu'est-ce qui a été publié à telle époque dans tel périndique
{Répertoire chronologique des articles de Revues ou Table générale
des Revues) ?

Poursuivant donc leurs recherches, les membres des sociétés aff-
liées qui n'auront pas trouvé ce qu'ils désiraient dans la Bibliothèque
Collective, ou qui auraient constaté que les ouvrages ne sont pas
renseignés dans le catalogue des autres bibliothèques de Belgique,
pourront donc être avertis de l'existence d’autres ouvrages en con-
sultant les répertoires bibliographiques. Il leur sera toujours possible
de se procurer les livres renseignés, soit en librairie, soit en deman-
dant à nos bibliothèques de les obtenir en prêt de l'étranger. Grâce
aux répertoires bibliographiques, qui comprennent non seulement
l'inventaire des livres, mais encore celui des articles de revue, les
travailleurs pourront aussi connaître le contenu des précieuses collec-
tions de périodiques possédé par nos bibliothèques. Ceci leur sera
particulièrement utile, car il est au-dessus des forces catalographiques
actuelles de n'importe quelle bibliothèque isolée d'inclure les dépouil-
lements des revues dans leurs catalogues ordinaires.

*
 *

Ainsi, la Bibllothèque Collective, tout en ayant son existence auto-
nome et en permettant à chaque société savante de conserver la pro-
priété et la libre gestion de ses livres, ne constitue cependant pas
uné institution à part et sans lien avec les autres sections de l’Institut.

 

Elle est, au contraire, étroitement rattachée à l’ensemble de son orga-
nisation et, d'autant plus naturellement, que celle-ci, en toutes ses
parties, est elle-même l'œuvre d’une vaste coopération, à la fois belge
et internationale. Bibliothèque collective est ainsi synonyme de groupe-
ment des sociétés savantes, travaillant de concert avec l'Institut au
progrès de l’organisation documentaire et à la mise en commun de
l'usage des collections.

C'est ce qui a permis l’affiliatiôn de certaines institutions dont les
livres doivent demeurer dans d’autres locaux, tels Institut de Socio-
logie Solvay, la Société de Géologie. C'est aussi ce qui permet
d'énumérer parmi les membres qui font partie du corps moral de cette
institution et ont adopti les méthodes communes, tous les groupes,
services et bibliothèques qui publient des parties du Répertoire
Bibliographique Universel, et dont la liste trop longue à énumérer
figure en tête des annuaires de l'Institut.

Bibliothèque, Catalogue, Office de renseignements, tous ces ser-
vices s'intégrent les uns les autres et constituent un outillage dont
bénéficieront les travailleurs.

#
Æ *#

Cependant, il y a lieu de les compléter d’une autre manière encore
pour répondre aux vrais besoins de la documentation moderne et
réaliser la conception du rôle que les imprimés peuvent jouer dans
notre civilisation.

La production scientifique devient plus intense, les publications
se multiplient, et le nombre des personnes qui peuvent ou qui doivent
utiliser les informations contenues par milliers et par milliers dans
les livres, les revues et les journaux. Conséquence obligée : 11 faut
rendre de mieux én mieux accessible au grand public ces masses
énormes de documents.

La consultation pour chaque question d'un dossier unique conte-
nant par extrait tout ce qui a paru sur la question, et tenu rigoureu-
sement à jour, la réunion en un ensemble systématique de tous les
dossiers ainsi constitués, tel apparait ceci l'idéal à poursuivre.
Le caractere utopique de cet idéal disparaît dés que l’on considère
qu'on peut procéder graduellement à Ha formation de semblables
dossiers en s'attachant d'abord aux documents de l'époque actuelle:
qu'il existe de nombreux groupes intéressés à la constitution de sem-
blables collections et que l'exemple du Répertoire Bibliographique
Universel, commencé depuis 1895 seulement, est là pour stimuler les
énergies créatrices.

Aussi, l'ardeur de tous les groupes aidant, avec le concours des
dirigeants des sociétés scientifiques, des rédactions de revues, des

directeurs de journaux, avec l’aide de généreuses donations, l'Institut

 
International de Bibliographie a commencé le Réberioire Universel de
Documentation, nom donné à la collection systématique des dossiers,
comprenant deux parties, les textes dans l’uneet les images photo-
graphiques dans l’autre. Après divers tâtonnements, la méthode a été
arrêtée et elle est en corrélation étroite avec celle qui préside à
l'organisation des autres travaux et spécialement de la Bibliothèque
Collective. Élle sera considérablement aidée, d'une part, par les pro-
cédés de reproduction photomicroscopique; d'autre part, par la
réforme des publications documentaires. Celle-ci apparaît nettement
comme le corollaire dé cette imtiative : ce sont elles qui devront à
l'avenir alimenter de tels dossiers, qu'ils soient constitués en notre
Office central ou ailleurs, par duplicata ou en corrélation. La carac-
téristique des nouvelles publications documentaires, livres ou revues,
est l'impression sur fiches détachables portant chacune un seul élé-
ment, indexé en concordance avec la classification internationale et
par suite directement et mécaniquement intercalable dans les dossiers
respectifs. Déjà la Société belge de sociologie est entrée dans cette
voie avec ses enquêtes et le succès a répondu à son initiative. On
peut, par la pensée, entrevoir Le jour où les publications scientifiques,
gräce à l'unité de classement et à la divisibilité extrême de tous leurs
éléments, se solidariseront de plus en plus Les unes avec les autres.
Alors chacune, dans sa forme, comme elle le sera dans sa substance
même, ne formera plus qu'une partie, un chapitre, voire un simple
paragraphe du Livre Universel, formé au jour le jour et réalisant
une vaste Encyclopédie documentaire à la taille de notre xxt siècle scien-
tifique.

 

 

Liste des Bibliothèques affiliées

Institut International de Bibliographie.

Société de Médecine mentale.

Société belge d'Astronomie.

Société belge de Neurologie.

Association internationale des Médecins-Experts des Compagnies
d'Assurances.

Associations médicales des Accidents du Travail.

Cercle belge de la Librairie.

Union de la Presse périodique belge.

Société belge d'Otologie, etc.

Société centrale d'Agriculture.

Société de Médecine légale.

Société chimique de Belgique.

Institut de Sociologie Solvay.

Association internationale des Auteurs et Compositeurs.

Société belge de la Paix.

Club alpin belge.

Journal des Brevets.

Société royale belge de (réographie.

Cercle belge des Collectionneurs de Journaux.

Syndicat des Agents de Brevets de Belgique.

Ligue belge du Droit des Femmes.

Commission internationale de l'Enseignement agricole.

L'Indépendance Belge.

Commission permanente de l'Association internationale du Congrès
des Chemins de Fer.

Comité central du Travail industriel.

 
Réglement de la Bibliothèque Collestive

T. Tous les membres des associations affiliées à la Bibliothèque
peuvent y consulter les ouvrages. Les personnes connues ou recom-
mandées, habitant la province ou l'étranger et poursuivant des
recherches scientifiques, peuvent être autorisées à fréquenter la
Bibliothèque.

2. La Bibliothèque est ouverte tous les jours non fériés de 9 à
18 heures.

3. Les ouvrages sont remis en consultation sur place. Les lecteurs
dressent, à cet effet, un bulletin de demande. Lorsque les ouvrages
leur sont remis, ils contresignent le bulletin. Les ouvrages doivent
être remis au bibliothécaire de service avant la sortie de la salle.
L'accès des rayons est interdit,

4. Les membres des associations qui, d'après leur propre règle-
ment, autorisent le prêt au dehors, peuvent recevoir des livres sur
demande, en se conformant au règlement spécial,

5. Les ouvrages, revues et journaux remis en communication,
sont placés sous la responsabilité des détenteurs. 1} est absolument
interdit d'y faire aucune marque, de les souiller ou de les mutiler.

 

 

 

INSTITUT INTERNATIONAL DE BIBLIOGRAPHIE

But de l'Institut.

Perfectionner, développer et unifer les méthodes en matière de
bibliographie et de documentation ; organiser la coopération scientifique
internationale entre groupes de spécialités diverses en vue d'élaborer,
suivant un plan d'ensemble, des travaux embrassant les diverses
branches de la documentation et spécialement un Répertoire Bibliogra-
phique Universel; établir un centre international pour la coordination
de tels travaux et la conservation en original des répertoires et des col-
lections de documents ; mettre l’usage de ceux-ci à la disposition de tous
les travailleurs intellectuels par voie de communication, de capie ou de
publication ; à cette fin muitiplier en tous pays les services de documen-
tation et les mettre en relation permanente d'échange et de travail à
l'intermédiaire d'un Office international.

Organisation.

L'Institut International de PBibliographie est une organisation libre
exclusivement scientifique. Les affiliés particuliers où collectifs (biblio-
thèques, universités, sociétés, instituts, administrations) acquittent leur
cotisation soit par une somme annuelle de 10 franes, soit par des tra-
vaux ou des publications.

Par arrêté royal du 12 septembre 1895, les services de l'Institut ont
été érigés en office public et installés dans les Musées royaux.

Répertoires. Coilections. Services.

Bibliographie : Répertoire Bibliographique Universel. — Réu-
nion des notices bibliographiques relatives aux écrits de toutes natures,
livres et articles de revues, publiés dans les divers pays, sur toutes les
matières. Les notices sont établies sur fiches du format international
1250 x 75m et classées par duplicata en deux séries, l’une par ordre
alphabétique de nom d'arteur, de manière à répondre à la quéstion :
x Quel ouvrage à publié tel auteur?», et lautre par matières (Classi-
fication bibliographique décimale), de manière à répondre à la question :
« Qu'estce qui à été publié sur tel sujet? » Le Répertoire Bibiiogra-
phique Universel comprend plus de 7 millions de fiches, La consulta-
tion en est gratuite dans les locaux de l'Institut. Envoi de copies sur
demande au prix de 5 centimes la fiche. Abonnement à tout ee qui
parait sur une question dannée.

Catalogne colicetif des Bibliothèques de Belgique.

Iconographie : Répertoire Iconographique Universel. — Réunion
en une seule collection, classée systématiquement par ordre des matières
et cataloguée, des illustrations photographiques de toute nature et sur
tous sujets provenant de sources variées (environ 100,000 documents) ;
Centre international de la documentation photographique, en suite de
la résolution du Congrès de Marseille, octobre 1906.

Documentation : Répertoires divers de documentation en voie
d'élaboration. — Collection systématique de dossiers sur les questions
d'actualité.

 
Bibliothèque : Bibliothèque bibliographique internationale et Biblio-
thèque collective centrale organisée avec le concours des institutions et
associations scientifiques et internationales.

Institution et collection de la Belgique scientifique, artistique
et littéraire. — Dossiers de renseignements émanant des institutions
elles-mêmes. Enquête permanente. Publication de l'Annuaire de la Bel-
gique scientifique, artistique et littéraire.

Publications : Bibliographia Universalis, collection de contributions
imprimées au Répertoire Bibliographique Universel publiées en coopé-
ration sous la direction de l’Institut; Manuel du Répertoire Bibliographique
Universel, règles catalographiques internationales, méthode pour la tor-
mation de répertoires sur fiches, table de classification universelle ou
classification bibliographique décimale; Bulletin de l'Institut International
de Bibliographie; publications diverses relatives aux méthodes et au mou-
vement bibliographique et documentaire.

Matériel de la documentaire : Matériel (classeurs, rayons et fiches)
établi conformément aux méthodes de l’Institut. Musée des méthodes.

Locaux : HEURES D'OUVERTURE. — Les Répertoires de l’Institut sont
installés à Bruxelles, 1, rue du Musée (Musées Royaux, deuxième étage).
La Bibliothèque collective est installée au Palais des Beaux-Arts, rue
de la Régence, 3a. Heures d'ouverture : de g à 12 et de 14 à 18 heures.

 

 

Publications de l'institut International de Bibliographie

BIBLIOGRAPHIA UNIVERSALIS

Description sommaire. — La Büibliographia Universalis, ou partie
imprimée du Répertoire Bibliographique Universel, forme une collec-
tion de bibliographies distinctes, rédigées par divers groupes de spécia-
listes et publiées en coopération, selon un plan uniforme et une méthode
commune arrêtés à cette fin par l’Institut International de Bibliographie.

Les diverses contributions sont identifiées par un numéro d'ordre
invariable qui en facilite la désignation. Leurs éditions différentes s’in-
diquent par des lettres de la manière ci-après :

Edition À — en volume ordinaire;
» B — en volume imprimé recto, blanc au verso;
» € = sur fiches imprimées du format international.

Total des notices parues à ce jour : 638,472.

Numéro Abonnement — Edition
de contrib. A
Bit ++ % a 31 =
hs 3 =
Apr Cd — ot 39
j ; EL. Ü+ 49 =
385 - 625] Chemins de fer. . 8 — 10.—
A  — Variable —
18.75 25.— 10 le mille
TG — — —
120.— — —
10.— 14.50 Variable
— o.5olefasc. Variable
= 12.— _

Salle des Réepes toires.

=
AR
à
FE
—

 
MANUEL OÙ RÉPERTOIRE BIBLIOGRAPHIQUE UHIVERSEL

[(Punzicarton x° 63 DE L'Ixsvicur ISTERNATIONAL P8 HiptioGRAPrte]

Cette publication, aujourd'hui entièrement terminée, résume et condense
en un seul volume mis à jour les diverses publications fragimentaires que
l'Institut International de Bibliographie a fait paraitre jusqu'à ce jour sur
son organisation, ses méthodes ct scs travaux. Elle contient les matières
détaillées ci-après.

L'ouvrage comporte un volume de plus de 2,259 pages. relié en pleine
percaline, avec disposition spéciale d'onglets facilitant sa consultation. Le
prix de souscription est fixé à 5o francs par exemplaire, port en plus. Les
tables de classification, contenues dans le Manuel, reproduisent in exfenso
les tables publiées séparément dans la publication n° 25, Elles com-
prennent plus de 32,000 divisions systématiquement ordonnées et dont
les rubriques figurent sous 40.008 mots dans l’Index alphabétique général,
On peut se procurer séparément les divers fascicules composant l'ouvrage
et dont le détail est donné dans le tableau ci-après.

ORGANISATION, TRAVAUX, MÉTHODES

Organisation.

I. Le Répertoire bibliographique universel de l'Institut Interna-
tional de Bibliographie.
IT. Statistique sénérale des services et des travaux.
III. Actes officiels, statuts, etc.

Catalogue des travaux,

IV, Catalogue du Répertoire bibliographique universel, (Prototype
du manuscrit}

V. Catalogue de la « Bibliographia Universalis », {Contributions
imprimées.)

VI. Catalogue des publications diverses.

Règles et méthodes.

VII. Etablissement des répertoires sur fiches : Répertoires des
matières, répertoires des auteurs. autres répertoires.
VIII. Rédaction des notices bibliographiques.
IX, Publication des notices bibliographiques en recueils et sur
fiches.
X. Modes divers de coopération au Répertoire bibliographique
universel.
XI, Organisation des diverses collections de documents.
XII. Matériel et accessoires à l'usage des répertoires.

TABLES DE CLASSIFICATION BIBLIOGRAPHIQUE

Exposé et règles de la Classification décimale,
Table principale,

Tables auxiliaires,

Index alphabétique général,

Tableau des fascicules composant le Manuel du Répertoire Bibliographique Universel
et pouvant être obtenus séparément, aux prix indiqués

N° d'ordre
des
fascicules

 

MATIÈRE

 

 

INDICE

HIBLIOGR A PITIQUE

DÉNOMINATION

Nombre de

piges
du fascicule

EN
FRANCS

 

 

 

 

Cu ND
a fn

 

»

[ii =,c Dis À-Z]

[53]

[77]
[620.1]

Ga
34
{615
616

617]
Gi2|
[618+6r9]
35
lo]

à

le]
(3]

i
G
(353 | 623]
[5x +52]
[5484540 +55]
[56-+55+58+59]l
[611]
[6134614]
(71
[64-463]

[62]

[54-166]
[67-+68+60]

»
»

 

Exposé et rôgles de la Classificalion décimale .
Table des subdivisions communes, .
Sciences physiques (Mécanique rationne le ct Phy-

sique) . . .

Sciences photographiques .

Industries de la Locomotion (Locomotion par
terre et par eau, Aérostation).

Sports (Tourisme, Cyclisme, Automobilisme) :

Droit . don ee ou a ee +

Thérapeutique.

Pathologie interne

Pathologie externe

Physiologie.

Gynécologie. Pédiatrie. Médecine comparée

Administration

Histoire. Géogr aphie. Biogr apbie. Généalogie

Résumé des tables. Tables ; générales méthodiques
abrégées, Index alphabétique général abrégé .

Généralités. Bibliographie. Bibliothéconomie. 5o-
ciétés savantes, etc,

Sciences sociales. Statistique. Héconomie politique.
Enseignement. Assistance, Foiklore .

Philosophie. Questions morales,

Agriculture. Agronomie. Sciences agricoles

Sciences religieuses .

Philologie et Littérature

Sciences militaires

Mathématiques et Astronomie .

Minéralogie. Cristallagraphie. Géologie

Sciences biologiques. Paléontologie. Anthropo-
logie, Botanique. Zoologie - .

Anatomie .

Hygiène privée et Hye giène publique .

Beaux-Arts : Architecture. Sculpture. Peinture.
Photographie. Gravure. Musique

Sciences appliquées diverses : Economie domes-
tique. Sténographie. Imprimerie et édition. Tran-
sports. Comptabilité . dou ou ee ee

Sciences de l'ingénieur : Mécanique. Électricité
industrielle, Mines, Ponts et chaussées. Che-
mins de fer et tramways. Travaux maritimes et
Bbydrauliques. Technologie sanitaire. Locomo-
tion en généra!. dou ee à

Sciences chimiques : Chimie pure.
chimiques. Métallurgie .

Industries diverses. Professions et métiers divers.
Construction.

Index alphabétique général

Organisation. travaux, méthodes de l'Institut Inter-
national de Bibliographie

Inclustries

NOMBRE TOTAL DES PAGES

 

 

HD Di EH ei ei HO Hi ei

n

B

HHEE ER 4 HR

 

 

 

 

 
eau

 

MÉTHODES DOCUMENTAIRES

RÉPERTOIRES SUR FICHES

On entend par répertoires sur fiches, une collection de renseigne-
ments, dont chacun est établi sur un feuillet séparé ou fiche et dont
tous les feuillets, disposés dans un ordre de classement constant, sont
réunis dans des meubies classeurs spéciaux. Les méthodes à suivre
pour Ja formation des répertoires sur fiches, font l'objet du cha-
pitre VII du Manuel du Répertoire Bibliographique Universel.

Classification bibliographique décimale

La Classification bibliographique décinale est universelle, internatio-
nale, encyclopédique, à la fois particulière et générale; elle est documen-
taire, elle s'exprime en une notation concise, elle est fort étendue {environ
32,000 divisions) et indéfiniment extensible. Les bases en ont été adoptées
par le Congrès international de Bibliographie en 1893 et en 1897 à
Bruxelles et en 1900 à Paris, et elle a déjà reçu une large application,
tant en Europe que dans le Nouveau Monde,

Cette classification consiste en une vaste table systématique des matiéres,
dans laquelle tous les sujets de connaissances sont répartis par classes,
sous-classes et divisions, en passant du général au particulier, du tout à la
partie, du genre à l’espèce.

Chacune des rubriques de cette table est représentée par un nombre
classificateur composé d'un ou de plusieurs chiffres, suivant le degré de
généralité. Ces nombres sont décimaux, en ce sens que chaque chiffre vers
la droite du nombre ne modifie pas la valeur ordinale des chiffres précé-
dents, mais correspond à une subdivision de la matière représentée par les
chiffres précédents, L'ordre dans lequel les nombres se suivent est aussi
l'ordre décimal,

La table systématique est complétée par un index alphabétique des
matières, dans lequel toutes les rubriques de la première table sont
rangées en un seul ordre alphabétique et sont suivies du nombre classi-
ficateur correspondant, Exemples :

1 Philosophie, Economie financière 332
g2 Religion. 5 Econvmie politique 33
F3 Sciences sociales. = Monnaic 332.4
8 3: Statistique. 3 Philosophie I
5 32 Politique. & Politique, question du travail 331
5 33 Economie politique. # Religion 2
& 331 Questions du travail. || # Sciences sociales 3
É 332 Économie financière. || € Statistique 31

332.4 Monnaie. | Travail 331

  

 

ep

L'indexation suivant ces tables {c'est-à-dire l'inscriplion sur les docu-
ments à classer du numéro classificateur correspondant à Ja matiére
principate dont ils traitent} permet donc de former des collections de
documents rangées dans un ordre méthodique parfait et susceptibles
d'accroissements et d’intercalations continus.

D'une manière générale, comme classification des matières uniforme et
internationale, la classification bibliographique universelle est susceptible
d'être appliquée au classement des diverses espéces de documents et
matériaux dont les travailleurs intellectuels ont à se servir, et elle fournit,
à cet effet, des cadres tout prêts, tracés d'avance : classement des réper-
toires bibliographiques et des catalogues; classement des ouvrages eux-
mêmes dans les bibliothèques ; classement des notes, observations, extraits
et documents divers destinés à des études et à des travaux personnels;
classement des tables de matière dés recueils périodiques; classement de
documents graphiques, illustrations et photographies, de clichés, de
brevets, de specimens, de catalogues industriels, de circulaires commer-
ciales et toutes autres applications à la decumentalion, prise dans le sens
le plus large.

Le jour où la classification documentaire universelle se séra répandue,
où son application aura été généralisée, au lieu d'avoir à se familiariser
avec vingt clés diférentes, variant d'aprés les institutions qui conservent
et qui classent les documents, le public des chercheurs pourra, à l'aide
d'une seule clé, c'est-à-dire d'une mème table de classification des matières,
se faire ouvrir Les trésors de tous les dépôts de documents. Une économie
considérable de temps pourra être réalisée ainsi, et le Chercheur bénéf-
ciera des avantages de la connexion étroite établie entre toutes les sources
documentaires de nos connaissances. La classification bibliographique
universelle permettra enfin de créer l'entente et la coopération dans les
travaux. Au point de vue des collaborations internationales, elle pourra
jouer un rôle similaire à celui qu'on attend de la langue internationale,
qui ne cherche pas à contrarier les langues particulières ni à s'y substituer,
mais bien à servir d'auxiliaire et de complément pour Les relations exté-

rieures,

   
  

  
Méthodes et Matériel de la Documentation ”

LES FICHES

pour répertoires bibliographiques et catalogues,

Le type adopté par l'Institut International de Bibliographie est la fiche
du format de 6,125 X 0,075 posée dans le sens de sa plus grande dimension.

 

 

 

O

 

 

 

Ces fiches ont les dimensions qu'une langue expérience a démontré étre
les plus pratiques, Pas trop hautes, elles peuvent être lues parfaitement et
sans dérangement quand elles sont placées dans les tiroirs; assez larges,
elles ne multiplient pas inutilement les alinéas et permettent la formation
de répertoire à l'aide de découpures d'imprimés à justification large. Leur
surface totale est proportionnée à l'étendue moyenne des renseignements
à y inscrire. Ces fiches sont perforées au talon et maintenues en place
dans les tiroirs par une tringle mobile,

L'emploi des fiches pour les répertoires bibliographiques et les cata-
logues permet l'intercalation indéfinie de nouveaux titres, leur assure un
classement parfait et facilite la réparation des erreurs et des omissions.
Les fiches détériorées ou devenues inutiles sont remplagçables par d'autres.

{al Demander le Catalogue détaillé illustré des meubles classeurs et des fiches.

Les fiches qui portent les titres d'ouvrages sont blanches; d’autres
fiches, de couleur celles-ci, plus hautes et diversement découpées [becs ou
encochesl, servent à marquer les divisions à introduire dans les fiches de
renseignements, soit au moyen des lettres de l'alphabet, soit par un numé-
rotage, soit à l'aide de mots de classement.

Réunies avec les fiches bibliographiques dans les meubles classeurs
décrits ci-après, ces fiches divisionnaires servent à former, peut-on dire,
de véritables livres dont la reliure est mobile, Les fiches blanches consti-
tuent les feuillets du livre, les fiches divisionnaires, diversement combi-
nées, en marquent les chapitres, les sections et les paragraphes, et celivre
peut être lu et feuilleté dans le tiroir lui-même sans déplacement des fiches,

Voici les prix de ces divers iypes de fiches ; les commandes peuvent

ètre adressées directement à l'Institut de Bibliographie, 1. rue du Musée,
Bruxelles :

CATALOGUE DES FICHES
DÉSIGNATION TYPE PRIX

FICHES EN BLANC

 

 

 

Fiches pour renseignements :
fortes (simples blanches, similijapon). .! Aa 3. mille.

» doubles, blanches, simil japon] . | Ba . mille.
minces, simples blanches. , . . . | Ab .75 le mille.

« doubles » , . . . | Be . mille.
divisionnaires (de couleurs variées) : :

À becs . . , . . . . . . . . C . mille.

À encoches, . , , 4 , , , . . D . mille,

Nembre

FICHES AVEC de
Numéros

des fiches. FORMULES IMPRIMÉES | fties

ï Fiche divisionnaire « Répericire Biblio- Fetes.
grafhique Universel » se plaçant en
tète des répertoires . 0.20 la

Fiches divisionnaires de la partie ami.
sistralie des répertoires bibliogra-
phiques {voir & Manuel du Réper-
toire Bibliographique Universel »,
ch. VII, p. 79) .« . sou . série.

Fiches divisionnaires pour répertoires
classés méthodiqiement d'après la clas-
sification décimale :

Séric des groupes principaux de
Ja classification sous forme de
table des matières . ., a.40 la série.

 

 

Fiches pour subdivisions conmames de
formes et de lieux . . 0,20 la douz.
Fiches divisionnaires pour répertoires
diprabéiqutes, fiche titre et fiches À,
B, C.. dou eos eo , 0.50 la série.

 

 

 

 

 
LES MEUBLES CLASSEURS

Pour la conservation et la consultation de fiches et de documents, organi-
ses en dossiers, l'Institut a fait établir divers tvpes de meubles classeurs. LES MEUBLES CLASSEURS A FICHES

Nota. — Même en dehors de la Bibliographie, les classeurs à fiches ont
des applications fort multiples :

Ils sont d'un usage précieux pour les administrations publiques, établis-
sements d'enseignement, bibliothèques, archives, assurances, chemins de
fer, tramways, banquiers, agents de change, industriels, commerçants,
ingénieurs, avocats, médecins, notaires, etc., classement des fiches par
ordre alphabétique et méthodique des adresses, localités, professions,
plans, documents techniques et scientifiques, etc.

 

Les classeurs existent en deux formats.

A. Classeurs pour fiches du format || B. Classeurs pour fiches format
international de o"125 X o"0%5. carte postale, oMI4 X oM0g.

 

PRIX PRIX

 

Types. Avec SE: Sans Avec

serrure. serrure, serrure.

 

Meuble à 2 tiroirs. | A: 2|Ab fr. Meuble à 2tiroirs.| Aa fr.
» 4 B: 38|Bb : 4 Ba
» & Cb É » » Ca

9 9

16 Gb 5 » 16 » Ga
24 Eb » 24 Ea
36 4 Fb 36 » Fa
re Db 72 » Da

 

 

 

 

 

 

 

 
 

RÉPERTOIRES À DOSSIERS

Le principe des classeurs à dossiers est ie même que celui des
classeurs à fiches de petit format, dont la description précède.

Le classeur à dossiers se compose d'un meuble à grands tiroirs,
dans lequel sont déposées verticalement les chemises qui servent à
réunir par groupes les pièces ct documents traités sous la forme de
fiches grand format et à constituer des dossiers portant les marques
ou rubriques de classement; des fiches divisionnaires émergeantes
servent à faciliter les recherches.

Voir description et fonctionnement dans le Bulletin de lTustitué
Tuternational de Bibliographie, 1967.

PRIX DES MEUBLES CLASSEURS Pour Dossiers

Typen*s Sans serrure Avec serrure
2 tiroirs Fr. go Fr, 95
4 D 5 175 » 199
8 » 345 »  3ÿo

12 D » 505 » 540

16  n » 655 »  7Q0

Un meuble spécial comportant à La fois des tiroirs pour dossiers et
des tiroirs pour fiches de om075 X amr25 a été établi, Il se compose
dans sa partie inférieure de huit grands tiroirs et, dans sa partie
supérieure, de trente petits tiroirs. Voir cliché ci-contre.

RÉFÉRENCES. — Les Ministères des Affaires étrangères, des Chemins
de fer, de la fustice, de l'Intérieur, de l'Industrie et du Travail de Bel-
gique. La Bibliothèque royale de Belgique. Les musées du Cinquante-
naire, lés bibliothèques des Universités de Bruxelles, Gand, Louvain,
Lille, la Sorbonne, bibliothéque Nobel de l'Académie suédoise, l'Ins-
titut de sociologie, de physiologie et de commerce, à Bruxelles, l’Icole
dés Sciences politiques et sociales, les Collèges du Pape et du Saint-Esprit
à Louvain,les Collèges Saint-Louis, Saint-Michel et les Bollandistes à
Bruxelles, Le grand Séminaire de Malines, ainsi qu'un grand nombre
d'Admivistrations communales, Banyues, Assurances, Avocats, Ingé-
nieurs, Médecins, Notaires, etc, etc.

 

 

 

 
Oscar LAMBERTY
Imprimeur de l'Institut International de Bibliographie
RUE VEYDT, 70, BRUXELLES
