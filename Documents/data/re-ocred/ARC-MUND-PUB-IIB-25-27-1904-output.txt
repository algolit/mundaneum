 
 

 

 
67 ANATOMIE.

Divisions principales.

6r1(0) * Généralités. Ouvrages généraux.
OPTIOH RE Anatomie analytique.

\6II.012 ’ D

611.013. Ontogénie. Embryologie.

GIL.014. Anatomie anthropologique.
6I1:016. Anatomie paléontologique.
GIL.018 _ Histologie. Le
GTÉOTO F Refiale des animaux inférieurs.

661.1 à.9 Anatomie systématique.
ORISTA er Angéiologie. ee

One Organes respiratoires.
611.3 : _ Organes de la nutrition.
‘611.4 Système lymphatique.

611.6 Appareil génito-urinaire.
611.7, |  » Organes locomoteurs. Peau.»
611.8 : Système nerveux. Organes des sens.
611.9 À Anatomie topographique.

12

Observation générale.

Chacne des batisions de l'ARato E. D bris
être subdivisée par combinaison au moyen du signe de.
4 relation : suivi des autres divisions de l'anatomie. Ex. : :

ÉTTT2 NN Le cœur,

611.12 : 61r.013 Embryologie du cœur.
611.12: 611.018 Histologie du cœur. L
611.12 : 611.13 | Circulation artérielle du cœur.

 
ue

HGTT 420 ircu
- : 611.460  Ganglions lymphatique:
Os. LA nl
Articulations.
:611.73 * Muscles, RÈA |
: 611.741  Tendons. ..
: 611-742 Aponévroses.
: 611.743 | Gaines tendineuses, fbreuses.
“Orrerda se ligaments :
_:611.75  Gaïnes synoviales. Bourses séreuses. à

 
611.07. Anatomie analytique,

 

412. d Tératologie. Anomalies. LE

Ne sont classées ici que 1 de relatives
nerfs, aux vaisseaux et aux muscles.
Voir aussi Gan.3 Chirurgie orthopédique.

EL \

Ontogénie. Embryologie.
À subdiviser comme 611 (o). L'embryologie des divers

organes s'exprime par combinaison. EE
6rt. 018 G6rr. r2 Embryelogie du cœur.

_ Cellules germinatives.
Sperme.
Spermatogenèse. :
Voir aussi 6rr.63 Organes génitaux de one
CUT ue RTE
Ovogenèse.
Voir aussi 611.65 Organes génitaux de la Le

Maturation, corps jaune.

Co Fécondation.
Voir aussi 576. 37 Fécondation, en biologie. De
are germinatives.
Segmentation, blastula.
Gastrula. i
Lo primitive.
_ Blastopore.
Archenteron.
Canal neurentérique.
Endoderme.
Ectoderme.
_Mésoderme. :
. Mésenchyme.

Organes endodermiques. 5
AD Corde dorsale.

.5 Organes ectodermiques.
2010 Lames médullaires. 5
Ê Voir aussi 617.8 Système nerveux
46: Organes mésodermique
61  Protovertèbres.
“OA _ Somatopleure.
PC _ Splanchnopleure.
POœlome ss ee 2
- Voir aussi 611.389 Cœlome, .
HiSansieent nee

Voir: aussi 6x1.018. 5. Sang, en Moose

 
 

 

 

 

 

 

 
| Hématoblastes. Miorooytes. Gtobuine. Plaquettes, etc.
Leucocytes. |
ee leucocyte.
. Lymphocytes.
_ Mononucléés.
‘Polynucléés.
 Éosinophiles.
Mastzellen. Chlasmatocytes.
: _Plasmazellen.
Plasma sanguin.
 Salive. ;
Voir aussi 6x2 313.7 Composition de la salive.
 Chyle.
Voir: aussi 612.332 Action du sc intestinal.
Lymphe.. es :
Voir aussi 672. de Su imphatique ;

- Mucus. es Sie #3
Voir aussi 612. 1 lucas, e

 
611.018.822.
HAS 620 me
.:824 Membrane cc
825 Division cellulaire.
Baie Prolongements ‘Chneriss Men
.837 . Prolongement cylindre- axile.… ET
:832.  Cylindreaxe. =
.833 ‘ Gaine de Schwann.
834 HAS Myéline. Etrangléments de Ranvie ;
OSDuene Gaine a dventice, périnèvre, etc. “
PS7  Prolongements protoplasmiques.
Appendices. Varicosités si
] Myélocytes. ;
842 Cellules. en
843 Cellules de névroglie.
.S44 Fibres de névroglie,

BAS tte. Corps amyloïdes. Cane

079 Anatomie. des animaux inférieurs.

 

 
 

 

 

 

 

 

 

   

Artères.

611.13
.137 Artères pulmonaires.
.132 Aorte.
0 Crosse de l'aorte. Artères et veines branchiales.
72 Artères coronaires:-
55 ‘Tronc brachio-céphalique:
195 Artères carotides. .
Si Artères carotides primitives.
2 Artères carotides externes.
21 Artère thyroïdienne supérieure.
22) Artère linguale.
29 Artère faciale.
.24  Artère pharyngienne ascendante,
129 Artère auriculaire postérieure.
.26 Artère occipitale.
27 Artère temporale supérieure.
.28 Artère maxillaire interne.
+29 Artère méningée moyenne.
59° Artère carotide interne.
51 Branches collatérales.
32 Artère ophtalmique.
Dre Artères sous-clavières. «
TL Artère thyroïdienne inférieure. .
2 Artère cervicale ascendante.
4 Artère scapulaire supérieure.
5 Artère mammaire interne.
.6 Artère cervicale profonde.
7 Artère intercostale supérieure.
.8 Artère scapulaire postérieure.
.OI Artère vertébrale.
.02 Artère axillaire.
.93 Artère humérale:
.94 Artère radiale.
.95 Artère cubitale.
.96 Arcade palmaire supérieure.
07 Arcade palmaire profonde.
.98 ) Artère des doigts.
TOO Rameaux thoraciques de l'aorte.
“0 Artères bronchiques.
é :2 Artères œsophagiennes.
s 3 Artères médiastines.
de Artères intercostales aortiques.
.136 Rameaux abdominaux.

& À H

Artères diaphragmatiques inférieures.
Artères lombaires.
Artère sacrée moyenne, caudale.

 
  
 
 
  
 
 
 
 
 
 
 
 
 
  
 
 
 
 
 
 
 
 
 
  
 
 
 
 
 
 
 
 
 
 
 
 

 

 

 

 
(Tronc cœliaque.
Artère hépatique.
ec splénique.

Artèr é ‘omphalo-mésent
Artère ou ou

 Artères apsblanes moyennes,
ès ee

de _ Artères iliaques Drm ves. De
; ï Artère i iliaque interne, ou byporastique. ae

| Artère ee
Artère utérine.s

Na a honteuse interne.
: Artère allantoïdienne. ve
! Fu ee exter

 
Veine cave supérieure. Troncs : veineux brachiocéphaliques
etleurs branches thoraciques. ” HAAA :
 Veine jugulaire interne. à
Voir aussi 611.218 Sinuscrâniens.
Veines diploïques.
Veine jugulaire externe.
Veines sous-clavière et axillaire.
Veines profondes du membre supérieur.
Veines superficielles du membre supérieur.
_ Veines jugulaire postérieure ee vertébrale.
_ Veines AZYEOS) lombaire re iléolombaire, sacrée.

 Veine cave supérieure. D A
Veines lombaires. :

. Veines rénales. ; ;

. Veinés capsulaires, moyennes.
Veines diaphragmatiques inférieures.
Veines diaphragmatiques supérieures.
Veines po tURe :
Veines ovariennes.

k

Veines iliaques.
Veines sous-cutanées do le
 Veines profondes du membre inférieur.
 Veines superficielles du membre inférieur.
Veine saphène interne. 7
 Veine saphène externe.

_ Veiïne! porte.
_ Grande veine mésentérique.
_ Petite veine mésentérique.
Veines sus-hépatiques.
Veines ombélicales.

_ Vaisseaux capillaires.

Nez Ceue

Fosses nasales.

Cloison. ni ;
_ Plancher des fosses ee

 
POTI- 217. Mat rem
D Méat inférieur.
.2  Méat moyen.
.3 .  Méatsupérieur.
DO NAASIQUS AUS ee
«1  Sinus maxillaire.
2  Sinusfronta.
«3  Sinussphénoïdal.
rh Cellules ethmoïdales.
22000 Lans
220 Epiglotte.
.222 Isthme du larynx. :
22229 . Fausses cordes vocales.
.224  Ventricules. ie
225 Glotte ER
.226 Cordes vocales vraies.
.228 Région interaryténoïdienne.
.229 Région sous-glottique.

.23 Trachée et bronches.

29 . Trachée. “:
230 Bifurcation de la trachée.
-233  Bronches en général.
.234 Grosses bronches.
1290 _ Petites bronches.

.24 Poumon. ‘

243 Lobes.

.246  Lobules. . ;
2470 MCAlVÉOlES eee
248  Trame pulmonaire.
.25  Plèvre.
253 Plèvreinterlobaire.

j

260. Diaphragme.

27 Médiastin.
28  Branchies.

29 Autres organes, entre autres vessie natatoire.

 
 

 

    
     
       
       

S3T

JT
“319
.314.

DID UE © D x

“3TS
.316

H

317:
.318

292

4327
922
1323
324
1329
.326
327

in & b à

.329

& À H

39

5333
.334
335
.336
337
.338.
.339

 

611.3 Organes de la nutrition.

Bouche.

Cavité buccale et plancher de la bouche.
Langue.
Dents.

Lèvres.

Pharynx. Œsophage. j

Estomac.

_ Caillette.

  

Incisives.

Canines.

Molaires.

Dents de lait.

Dents permanentes.

Gencives.

Gencives supérieures.

Gencives inférieures.
Voûte palatine. Voile du palais. Luette.
Glandes salivaires. ;

Glandes sous-maxillaires et sublinguales.

Glandes parotides.

Canal de Sténon.

      
    
   
    
     
    
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
    
    
    
     
    
 
     
    
   
   

Joues.

Pharynx.
Amygdales.
Amygdales palatines.
Amygdale pharyngée.
Amygdale linguale.
Autres amygdales.
Pharynx par régions.
Epipharynx.
Mésopharynx.
Hypopharynx.
Espace rétro-pharyngien. |
Espace latéro-pharyngien.
Œsophage.
611.329 : 611.383 Méso-œsophage.
Portion thoracique.
 Portion diaphragmatique.
Portion abdominale.

611.33 : 611.383 Mésogastre.

Cardia.

Pylore.

Grande courbure.
Petite courbure.
Jabot:

Panse.
6rt. 342 : 61.383 éronaténu, £ fossetes d
do

ec

+344 . ; À néom.

. 6rr. 344: 6rx. 383 Méso- iléon.
Cæcum.
6x. 344 : Grr.383 Mésocæcum.

_ Valvule iléo-cæcale..
. Appendice iléo-cæcal

 
: 61r.4 | Système lymphatique. :

AT _ Rate.
DHATB Capsule de la es

.42 Vaisseaux lymphatiques.

. Pour les lymphatiques! des. organes et des RE voir à chacun de ces
organes et de ces issues f ss À !

Tête.
Face.
Cou
_Thorax.
Abdomen.
Région pelvienne.
Membre supérieur.
Bras.
Avant-bras.
Main.
Membre inférieur.
Cuisse.
 Jambe.
Pied.
Queue:
| Citerne de Pécquet.
Canal thoracique. )
Grande veine lympathique.
Vaisseaux chylifères. ;

. Thymus.
Thymus accessoire.
Thyroïde

| Thyroïdes. accessoires ou aberrantes. :
_ Glandules paräthyroïdes.

: Capsuies surrénales.

 Ganglions parotidiens. sr
à. s. ous-maxillaires.

 
Eire

 

 

RUE

611.464 Ganglions du thorax.
T Ganglions diaphragmatiques.
2 Ganglions mammaires (internes, rétro-sternaux).

3  Ganglions intercostaux. É

4 Ganglions médiastinaux antérieurs.

.5 Ganglions péritrachéo-bronchiques.

6 Ganglions médiastinaux postérieurs.

.465 Ganglions de l'abdomen.
de . Ganglions iliaques et hypogastriques.
2 Ganglions juxta-aortiques.
.467 Ganglions du membre supérieur.
É Ganglions axillaires.
7 Ganglions épitrochléens et autres.
.468 Ganglions du membre inférieur.
7 Ganglions inguinaux.
ds Ganglions poplités.
4 * Ganglions tibials antérieurs.
47 Ganglion carotidien. :
472 Glande coccygienne.

611.6 Appareil génito-urinaire. sn

.6I Rein.

.6117  Glomérule.
.612 Lobes et lobules.
.613 Canaux excréteurs. !
614. Calices. Bassinet.
.615 Enveloppe du rein.
SO17 Uretère. <
.618 Pronephros.
.619 Mesonephros. k

.62 Vessie urinaire, Urètre.

.621 Vessie.

“AT Trigone.
2 Bas-fond. ë \
: 23 Orifices urétéraux. :
622 Ouraque. Pédicule de l'allantoïde.
.623 Urètre de l’homme.
“D Urètre prostatique. Sphincter lisse et strié,
2 Urètre membraneux. è
+3 Urètre spongieux. .
4. Lacunes de Morgagni. :
.627 Glandes bulbo-urétrales.

.629 Sinus urogénital,

 
 

 

 

 

611.63
.631

.64
.647

.652
.653

.654
.655
.656
.658

Organes génitaux de l'homme.

Testicule.
Canalicules spermatiques.
Tubes droits.
-Cônes afférents. es
Corps d'Highmore. Rete de Haller.
Voir aussi 6rr.013.12 Spermatogenèse.
Épididyme.
Vasa aberrantia.
Corps paratesticulaires.
Tuniques du testicule.
Canal déférent.
Canal éjaculateur.
Vésicules séminales.
Prostate.
Scrotum.
Dartos.
Cordon spermatique.

Voir aussi 611.689 : 611.013 Canal péritonéo-vaginal. Migration du:

testicule.

Pénis.
Racine.
Prépuce.
Gland.
Méat.
Corps caverneux..
Corps spongieux.
Bulbe.
Fourreau.

Organes génitaux de la femme.

Ovaires.
Substance médullaire.
Follicules.
Substance corticale.
Voir aussi 61r.013.16 Ovogenèse,.
Corps paraovariens.
Organes péri-utérins.
Ligament large.
Ligament rond.
Ligaments utéro-sacrés et utéro-lombaires.
Ligaments tubo-ovariens. ï É
Espace rétro-péritonéal.
Parametrium. Gaine hypogastrique.
Petit bassin. Si
‘Trompes.
Cul-de-sac de Douglas.

Ne

 

 

 
 
 

 

 

 

 
72

.721

Ë

Os du tarse.
Astragale.
Calcanéum.
Cuboïde.
Scaphoïde.
Cunéiformes.

- Métatarsiens.

Os des orteilles.

 Phalanges.
Phalangines.
phenneenee

| Symdcsmologie. Arthrologie.

Articulations des vertèbres entre elles de avec le crâne.
Articulations occipito-atloïdienne et occipito-axoïdienne.

Articulations atloïdo-axoïdienne a Re

Articulations intervertébrales.
Articulations sacro-vertébrales.
Articulations sacro-coccygiennes. Ù
Articulations coccygiennes. s 0

Articulations costo-vertébrales. ET Tes en
Articulations chondro-costales, chondro- ie, inter-

_chondrales et sternales. j

Articulation temporo-maxillaire.

| Articulations du membre supérieur. -
Articulations sterno-claviculaire et Le
ao de l'épaule. Sn
_Articulation scapulo humérale. a
Articulation acromio-claviculaire et coraco-claviculeire.
_ Articulations du coude, ë ï
Articulation huméro-cubitale.
Articulation radio-cubitale supérieure.
Articulations du poignet.
_ Articulation radio-cubitale inférieure.
Articulation radio- -Carpienne. À
Aïticulation carpienne. ee
/ Articulation médio-carpienne. Ë
Articulations carpo-métacarpiennes. j
Articulations intermétacarpiennes.
Aïticulations métacarpo-pha langiennes.
Articulations phalangiennes de la: main.

À

Abolatone du membre nibdeurs
Articulations sacro-iliaque et pubienne.
 Articulation coxo-fémorale. FRA
Articulations du’genou. ee

Articulation fémoro-tibiale. A

1 Articulation tibio-péronière supérieure.

 
611.728.4 Articulations du es |
HT . Articulation tibio-péronière inférieure.
ha _ Articulation tibio-tarsienne.
0 Articulations tarsiennes. ê '
-e5T Articulations astragalo- nées scaphoïdienne.
0 Articulation calcanéo- cuboïdienne. +
.54 Articulation médio-tarsienne.
55  Aïticulation cuboïdo-cunéiforme.
( Articulations “ phoNe Dhabi, scaphoïdo-cunéi- À
forme. : ù é
Articulations intercuné iformes.
Articulations. tarso-métatarsiennes. ;
Articulations intermétatarsiennes.
Articulations métatarso-phalangiennes. ce
Articulations phalangiennes du pied.

f<

 Myologie.

_ Muscles do : 7
* Musoles _spino-huméraux Ho Aa rhomboïde, ru
- angulaire de l'omoplate, lévator scapulæ). M
aie spino- -costaux (dentelés postérieurs). a
. Muscles: spino-dorsaux (trapèze, cucullaris). è
Muscles splenius etcomplexus. D
. Muscle sacro-lombaires (Sacrospinalis, osent lis), one
_ dorsal (longissimus dorsi), épi-épineux. (spinalis). Ra
. Muscle transversaire épineux (transversospinalis, semi- ei
: | spinalis, multifidus, rotatores). : ï
. Muscles So sous transversaire ce
du cou.
Muscles obliques de la tête, droits postérieurs ( t droit |
_ latéral de la tête. See Ë

é ni la. tête. je
Peaucier du cou. nc s
Muscles ee , oceipital, frontal, enporat super:
_ ficiel. : :

_ Muscles de nez (emidel. “transverse du nez, mr 5
_ forme, constricteur des: re dilatateur des | Ne
| Muscles de l'oreille. Se
_ Muscles orbiculaires de d'œil. 1 (sourier, . cuire à des
Does Se we

teur. propre de la lèvre supérieure, a ;

muscle de la houppe du menton, carré du menton, é

Ft angulaire des lèvres, risorius, transverse du ‘menton).
F Muscles, masticateurs (nasséter, ou PSE ;

 
Muscles du cou.
Sterno-cléido-mastoïdien.
Sterno-cléido-hyoïdien.
Omo-hyoïdien.
Sterno-thyroïdien:
Thyro-hyoïdien.

Long du cou.

Grand droit antérieur.
Petit droit antérieur.
Scalène. :

Muscles hyoïdiens (digastrique, stylo-hyoïdien,
dien, génio-hyoïdien).

. Muscles thoraciques.

_ Grand pectoral.

Petit pectoral.
Sous-claviculaire.
Surcostaux.

Grand dentelé.
Intercostaux externes.
Intercostaux internes.
Présternal.

Muscles abdominaux et coccygiens.
. Droit de l'abdomen.
- Pyramidal de l'abdomen.

Obliques de l'abdomen.
Crémaster.
‘Transverse de l'abdomen.
Carré des lombes.

. Muscles coccygiens.

. Muscles des membres supérieurs.

Muscles de l'épaule (deltoide, sous- cpu sus-
épineux, sous-épineux, petit rond, grand rond).

Muscles du bras (biceps, brachial, coraco-brachial,
brachial antérieur, triceps). F

Muscles de l'avant-bras. the

Muscles pronateurs de l'avant-bras (rond pronateur,
Carré pronateur).

Muscles fléchisseurs de l’'avantbras (grand palmaire,
petit palmaire, cubital Sais re eos des doigts,
-fléchisseur du pouce).

Muscles extenseurs et supinateurs de l’avant-bras (long
supinateur, Court supinateur, radiaux, extenseurs des
doigts, extenseur du petit doigt, cubital postérieur,
anconé, long abducteur du pouce, court et long Rise

PSeurs du pouce, extenseur de l'index).

_Muscles de la main (lombricaux de la main, : ‘interroseux
de la main, thénar, court abducteur du pouce, court
féchisseur du pouce, opposant, adducteur du pouce,
hypothénar, adducteur du petit doigt, court. fléchisseur
du petit doigt, opposant, palmaire cutané). Sr

 
 

 

 

 

 

611.738
mr

 
 
  
 
  
 
 
 
 
 
  
  
 
  
  
 
 
 

Muscles des membres inférieurs.

Muscles de la hanche et de la fesse (psoas-iliaque, petit

psoas, fessier, pyramidal de la hanche, jumeaux de la
hanche, obturateur carré, crural). À ÿ

Muscles de la cuisse (couturier, droit antérieur de la
cuisse, triceps fémoral, quadriceps fémoral, tenseurs
de la synoviale du genou, biceps fémoral, demi-tendi-
neux, demi-membraneux, tenseur du facia lata, vaste
externe, pectiné, adducteurs).

Muscles de la jambe.

Muscles antérieurs de la jambe (jambier antérieur, tibial
antérieur, extenseur des orteils, extenseur des gros
orteils, péroniers).

Muscles postérieurs de fa: jambe (jumeaux CTUTAUX,
gastrocnémiens, soléaire, plantaire grêle, poplité,
jambier postérieur, tibial postérieur, fléchisseur du
gros orteil).

Muscles du pied.

Muscles dorsaux du pied (pédieux).

Muscles plantaires (adducteur du gros orteil, court
fléchisseur du gros orteil, abducteur du petit orteil,
court fléchisseur du petit orteil, court fléchisseur plan-
taire, accessoire du long fléchisseur, lombricaux du
pied, interosseux du pied, abducteurs ie gros orteil).

‘Tendons.
A diviser comme les muscles.

 

Aponévroses.

Aponévroses de la tête.

Aponévroses de la face.

Aponévroses du cou.

Aponévroses du thorax.

Aponévroses de l'abdomen.

Aponévroses de la région pelvienne et Die

Aponévroses du membre supérieur.
Aponévroses de l'épaule.
Aponévroses du bras.
Aponévroses du coude.
Aponévroses de l’avant-bras.
Aponévroses du poignet.
Aponévroses de la main.
Aponévroses des doigts.

Ron du membre inférieur.
Aponévroses de la hanche et de la fesse.
Aponévroses de la cuisse. ;
Aponévroses du genou.
Aponévroses de la jambe.
Aponévroses du cou-de-pied.
Aponévroses du pied. AN
Aponévroses des orteils. DN

 

 

 

 
Gaines date ei fibreuses.  Ligaments.
À diviser ‘comme les apon!

one séreuses. DNA

è diviser, comme De aponévroses.

Glandes à cérumen.

_ Derme.

AE "6x. 778 : Es 78 Muscies du derme.
_ Pigment.
Hypoderme.

78 Poils, ongles, écailles, plumes, ete.

 
611.812. Prosencéphale.
Br Télencéphale. Cerveau antérieur.
Le Écorce ee Lobes. Sillons. Circonvolutions.
Lobe frontal. / à
212 _ Lobe pariétal et sillon de Rolando.
: Iobe temporo- -sphénoïdal. Scissure de Sylvius.
14 Corne d’Ammon, Corps godronné. Corps ones.
Fe Noyau amygdaloide.
Lobe occipital. Sillon pariéto-occipital.
Lobule paracentral.
.Cuneus etscissure calcarine.
Lobe quadrilatère.
Insula de Reil.
Corps strié. .
Noyau caudé.
Noyau lenticulaire.
Avantmur.
Septum lucidum, cinquième ventricule.
Bandelette demi-circulaire.
 Rhinencéphale, bulbe olfactif, bandelettes on
substance perforée antérieure, etc. : î
Chiasma optique, bandelettes optiques, récessus due
L'lame, terminale, commissures de Gudden et de Mey- G.
mert, etc. . ts A
Hypophyse. ;
Corps mamillaires. Faisceau de Vica-d’ AZÿr.
Centre ovale. Substance blanche cérébrale.
. Ventricules latéraux. ;
Corps calleux. |

#

 Diencéphale. Cerveau intermédiaire.
‘Hypothalamus. Corps. de Luys. Anse pédonculaire.
1FAnse: lenticulaire.  Ganglion basal de None Com-
| missure postérieure. . ;
Couche optique. Commissure grise. |
_ Épiphysé Glande ne Habenuta:

| Capsule ee. ë re
. Troisième ventricule. Fe de Moro.

| Mésencéphae: Cerveau moyen. Fe
Dee Leone (bijumeaux) et bras d ces.

en
Noyau rouge.
: Système de l’oculo-moteur commun e de oculo-moteurs

1 Fe
l

 

 

 
 6rt.816

3
4
5
6

.817

“
DL
T9

Isthme de l’encéphale.
Valvule de Vieussens. : ei
Pédoncules cérébelleux supérieurs.
Système du pathétique. .
Ganglion interpédonculaire.

- Mésencéphale. Cerveau postérieur.

Cervelet.
Lobes, sillons, valvule de Tarin, etc. -
Noyaux gris centraux, olives cérébelleuses, noyaux
du toit, noyaux dentelés, etc.
Substance blanche du cervelet.
Protubérance annulaire. Pont de Varole.
Calotte. Formation réticulaire.

Noyaux du pont. Pédoncules cérébelleux moyens.
Fibres unissantla rose au cerveletetrécipro-
quement.

Système du trijumeau.

Système de l’oculomoteur externe.

Système du facial et du nerf intermédiaire de he

Système de l’acoustique. Olive supérieure. se (ras
pézoïde. :

Quatrième ventricule. f :

_Olives inférieures et accessoires et  e. de fibres
_ qui en dépendent.
Système du glosso- En et du pheurno- gastique.
Système du spinal. eee

Système de lhypoglosse.

Pédoncules cérébelieux inférieurs. Corps Re re

Novaux de Goll et de Burdach. Voies spino-cérébel- :

leuses et cérébello-spinales.
Substance réticulaire et autres parties du bulbes

Me ‘en général et méninges cérébrales en particulier.

 Pie-mère. : i
_ Toile choroïdienne. Plexus choroiïdes. < '
. Arachnoïde. ‘
Cavité sous-arachnoïdienne, Confluents sous- arachnoï-
diens. ; k
Dure-mère. Tente du cervelet. Faux du cerveau.
Liquide encéphalo-rachidien.
‘Trous de Magendie et de Luschka.
 Granulations de Pacchioni.

o

 

 

Myélencéphale. Arrière-cerveau. Bulbe. Moelle allongée.

 
 

   

611.82
.821

Y Oo + & D

ao un + Co

©

.826

.83
.831

 Moelle épinière.

Substance grise.

Renflements, sillons, structure générale, etc.
Moelle cervicale.
Moelle dorsale.
Moelle lombaire.
Moelle sacrée
Cône terminal.
Queue de cheval.

Cornes antérieures.
Cornes postérieures.
Colonne de Clarke. Noyaux de Stilling.
Autres cellules.
Névrolgie.

Substance blanche.

Cordon antéro-latéral.
Faisceau pyramidal.
Faisceau cérébelleux direct.
Faisceau de Gowers.
Faisceau sulco-marginal.
Faisceau intermédiaire du cordon latéral.
-Fibres courtes.
Commissure antérieure. î
Pour les fibres cérébello-spinales etspino-cérébelleuses, voiraussi611.818.7.
Cordons postérieurs.
Fibres ascendantes.
Fibres descendantes.
Fibres radiculaires.
Commissure postérieure.
Racines des nerfs.
Racines postérieures.
_ Racines antérieures.
Ganglions spinaux.

Canal rachidien. Ependyme. Sinus rhomboïdal. Fibres de
Reissner.
Méninges spinales.

- A subdiviser comme 611.810.

Nerfs périphériques.

Nerfs crâniens et leurs ganglions.
Nerfolfactif.
Nerfoptique. :
Nerf oculomoteur commun.
_ Nerf pathétique.
Nerftrijumeau.
Ganglion de Gasser.
Nerf ophtalmique de Willis.
Ganglion ophtalmique.

 

 

 

ET Sn . —

 

 

 

   

 
GLS Lo © Nerf Hoatiote oct
541 Ganglionde Meckel.
542 Nerf sus-orbitaire.
.56. Nerf maxillaire inférieur.
HOGL Ganglion otique-
571 Nerf dentaire inférieur.
572 : Nerf buccal.
Nerf lingual.
D 7AN Nerf auriculo-temporal.
.59 _ Nerf masticateur. |
.6 . Nerf oculomoteur externe.
ANSE facial.
Nerfs pétreux supérieurs.
Corde du tympan.
Autres branches collatérales.
Branches terminales.
Nerf intermédiaire de Wrisberg.
Ganglion géniculé.
. Nerf auditif acoustique.
Nerf cochléaire.
Ganglion spiral de Corti.
_ Nerf vestibulaire.
Ganglion vestibulaire de Scarpa.
Nerf glosso-pharyngien.
_Ganglions d'Andersch et d'Ehr
Nerf de Jacobson. Nerfs pétre
Nerf pneumogastrique. eee
_ Ganglions jugulaire et plexiforme.
_Nerfs et plexus pharyngiens..
_Nerfs cardiaques et nerf de Go?
Nerf laryngé supérieur:
Nerf récurrent.
Branches pulmonaires.
Branches œsophagiennes.
Branches péricardiques.
Branches abdominales. SE
Nerf spinal, accessoire de Willis.
Nerf hypoglosse.
Nerfs rachidiens.
ÿ Branches postérieures.
Branches antérieures. ï
.… Nerfs cervicaux. RE TAs
Branches postérieures des nerfs cervicaux. Peti /
nerfs sous-occipitaux.
_ Branches antérieures des nerfs cervicaux.
 Plexus cervical. : ANNE
_ Plexus cervical superficiel.
 Plexus cervical CR

; A :
Nerbphrenique ALgArre
à Branches anastomotiques. | Ë

 
 

611.833.4
AIT

.412
.413
.4T4.
TS)
.42

‘437
.432

 

 

 

       

Plexus brachial.
Nerf de l’angulaire et du rhomboïde. Nerf dorsal de
l’omoplate.
Nerf du grand dentelé. Nerf long thoracique.
Nerf sous-scapulaire.
Nerf du grand rond.
Nerf du grand dorsal,
Nerf axillairé, circonfiexe. :
Nerf sus-scapulaire.
Nerf du sous-clavier.
Nerf du grand pectoral.
Nerf du petit pectoral. \
Nerf musculo-cutané.
Nerf médian.
Nerf cubital.
Nerf radial.
Nerf brachial cutané interne.
* Nerf accessoire du brachial cutané interne.

_Nerfs thoraciques. pee :

Branches postérieures.
Branches antérieures. Nes intercostaux.

Nerfs barres et sacrés.
Branches postérieures.
Branches antérieures. :

_Plexus lombo-sacré. :
Plexus lombaire. i
Grand nerf abdomino- en ilio- ds
_ Petit nerf abdomino-génital, ilio-inguinal. è
Génito-crural, génito-fémoral.
Fémoro-cutané.
Nerf crural, fémoral. 1
Nerf musculo (fémoro)-cutané externe.
Nerf musculo (fémoro)-cutané interne.
Nerf du quadriceps.
Nerf saphène interne.
Nerf obturateut.
Nerf obturateur accessoire.
.Nerf lombo-sacré.

. Plexus sacré.

Nerf fessier supérieur.
‘Nerf fessier inférieur.

Nerf cutané postérieur de la cuisse.
Autres nerfs.

Nerf sciatique.
Nerf sciatique poplité externe, péronier commun.
Nerf musculo-cutané, péronier superficiel.

Nerf tibial antérieur, péronier profond.
Nerf sciatique poplité interne, tibial.
N erf saphène externe, cutané interne de la jambe.

 

 

 

   

 
Plexus génital, honteux.
Branches collatérales.
Nerf honteux interne.

Plexus sacro-coccygien. Nerfs coccygiens.

Système du grand sympathique.
_Sympathique cervical.
Ganglion cervical supérieur.
Nerf carotidien interne.
Ganglion cervical moyen.
Ganglion cervical inférieur.

: Nerfs cardiaques. Plexus cardiaque.
Sympathique thoracique.
Nerfs æœsophagiens.
Nerfs trachéens, bronchiques et pulmonaires.
Nerfs vertébraux. à ke
Sympathique abdominal.
- Plexus solaire. :
Ganglions semi-lunaires.
Nerf grand splanchnique.
Nerf petit splanchnique.
_Plexus et nerfs diaphragmatiques.
Plexus et nerfs hépatiques. .
Plexus et nerfs (cholé) cystiques.
Plexus et nerfs pyloriques.
Plexus et nerfs gastro-épiploïques.
Plee et nerfs spléniques.
Plexus et nerfs coronaires stomachiques.
Plexus et nerfs mésentériques.
Plexus etnerfs surrénaux.
Plexus et nerfs rénaux.
Plexus et nerfs spermatiques.
 Plexus et nerfs utéro-ovariques.
Plexus et nerfs lombo-aortiques.
Plexus et nerfs hypogastriques. à
 Plexus et nerfs hémorroïdaux moyens.
Plexus et nerfs vésicaux.
Plexus et nerfs prostatiques.
Plexus et nerfs vaginaux.
Plexus et nerfs utérins….

RES

UN

Organes de la vue.

uniques fibreuses de l'œil.
 Conjonc tive.
Cornée.
Sclérotique.

 

 
   

. 611.842

: SE
+3
à

Tuniques vasculaires de l'œil. Tractus uvéal. : ‘
Iris. Pupille.…
Choroïde. ee :
Corps ciliaire. Muscle ciliaire. Procès ciliaire. Zone de
Zinn. ue
Canaux de Petit, de Fontana, de Schlemm, d'Hovius..
Chambre antérieure. Humeur aqueuse.
Chambre postérieure.
Rétine. Nerf optique.
Nerf optique.
PREUNEMEN
Appareil dioptrique de l'œil.
Cristallin.
Capsule cristalline.
Humeur vitrée.
Organes accessoires.
Muscles extrinsèques de l'œil.
Appareil lacrymal.
Glande lacrymale.
Points et conduits lacrymaux.
Sac lacrymal et canal nasal.
Paupières.
Cils.
Sourcils.
Orbite.
Capsule de Ténon.

Organes de l’ouie.

Oreille interne.
Labyrinthe membraneux.

'Limaçon. j At

Organe de Corti. :
Vestibule.
Saccule.
Utricule.

Canaux semi-circulaires. !
Nerf acoustique.
Endolymphe.

Labyrinthe osseux.

Oreille moyenne.

Attique.

Cellules mastoïdiennes. qe FR ‘

Tympan.
Trompe! d'Eustache.
Osselets de l'oreille.
Oreille externe.

 

   

 
RÉ

Organes tactiles.

Ganglions. *

_ Ganglions des nerfs crâniens.
sous 67r.38r.
Ganglions des nerfs rachidiens.
Classer plutôt à 6rr.827.3.
Ganglions sympathiques.
Classer plutôt à 611.830.

 
ie
LA

14

pe
Fe,

to

 
 
