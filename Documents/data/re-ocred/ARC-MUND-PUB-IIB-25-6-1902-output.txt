 
 
 

 

Institut International de Bibliographie

PUBLICATION N° 25 INDICE BIBLIOGRAPHIQUE [025.4]

CLASSIFICATION BIBLIOGRAPHIQUE

DÉCIMALE

TABLES GÉNÉRALES REFONDUES
établies en vue de la publication du

Répertoire Bibliographique Universel

SZ
GS

ÉDITION FRANCAISE
publiée avec le concours du

BUREAU BIBLIOGRAPHIQUE DE PARIS
et du
“ TOURING CLUB DE FRANCE

PAS CRE UE ER Ne 6
Tables de la division [620.1]

Industries de la Locomotion
(LOCOMOTION PAR TERRE ET PAR EAU, AÉROSTATION)

 

 

 

 

 

INSTITUT INTERNATIONAL DE BIBLIOGRAPHIE

BRUXELLES, 1, rue du Musée. | PARIS, 44, rue de Rennes
ZURICH, 39, Eidmattstrasse,

1902

 

 

 

EN AE

 

 
 

 

 

 

 
 

EOCOMOTION 629.1.023.4

 

620.1 Locomotion. Industrie des
. Transports.

La locomotion se subdivise en trois grandes subdivisions :

{ 629:17 l-ocomotion sur routes.
629.12 Locomotion par eau.
629.13 Navigation aérienne.

Les tables des subdivisions communes de forme, lieu, langue, temps etc.(Tables
I, II, I, IV et V) sont applicables dans toute l'étendue de la division 620.7,
ainsi que la table des divisions de formes spéciales caractérisées par le
signe (oo) qui est applicable à toute la division 6; mais on ne doit utiliser
ces subdivisions communes que lorsque des divisions spéciales équivalentes
n’ont pas été prévues.

On pourra faire usage aussi, dans toute l'étendue de la division 629.1, des
subdivisions analytiques spéciales dont la table est donnée ci-après, 629.10
et qui sout caractérisées par le zéro simple, sans parenthèses. On les em-
ploiera pour spécifier certains points de vue sous lesquels on peut envisager
les sujets traités, ou certains détails des organes de construction.

La désignation des matériaux entrant dans la composition des organes, ainsi
que celle des matières premières utilisées dans l'emploi du matériel pourront
être spécifiées à l'aide des subdivisions communes 0023 et 0024.

629.1.0 Questions générales. [Subdivisions analytiques spé-
ciales.]
OT Organes élémentaires entrant dans la construction des

appareils de transport.

Ces organes sont différents suivant le but que les appareils ont pour objet
de réaliser. On trouvera aux trois subdivisions principales 629:77,
629.12 et 629.13 les subdivisions analytiques spéciales relatives aux
organes des appareils propres à chaque genre de locomotion.

«02 Moteurs mécaniques spéciaux aux véhicules de toute

nature. Différents types.
Les moteurs mécaniques, autres que ceux destinés aux véhicules, sont
classés sous l'indice 627 et ses subdivisions.

021 Moteurs à vapeur.
pa Moteurs proprement dits.
2 Générateurs.
3 Foyers et bouilleurs. «
4 Accessoires spéciaux.

:022 Moteurs à gaz ou à air comprimé.
TL Moteurs proprement dits.

pr 2 Réservoirs.
& 5 Appareils de compression.

4 Accessoires spéciaux.

.023 Moteurs à pétrole ou à combustible liquide.
+ Moteurs à essence ou à alcool.

7) Moteurs à huile lourde. :

53 Carburateurs etbrüleurs.

Accessoires spéciaux.

;
3
4
i
#|

%

 

 

 
 

 

629.1.024

 

LOCOMOTION

 

629.1.024
TL

629.11

“IT OT

.OIT

-012

.013

.OI4

.OI5
.016
.017
«II.018

.O19

629.111
“LIT
ITLICIT

-111.12

TILS
.I11.31

TIT32

“LITES

Moteurs électriques.
Dynamos.
Accumulateurs.
Commutateurs. .
Accessoires spéciaux.

Locomotion sur routes. Construction de véhicules

terrestres.
Moyens de transport par terre autres que les chemins de fer et les tram-
ways; pour ces derniers, voir 625.

629-110 Organes élémentaires entrant dans la contruction des
appareils.

629.111 Moyens de transport primitifs.

629.112 Voitures à traction animale.

629-113 Voitures à traction mécanique. Automobiles.

629.117 Véhicules légers à plus de deux roues. Voiturettes.

629.118 Véhicules légers à deux roues au plus. Cycles. Véloci-
pèdes.

Organes élémentaires entrant dans la construction des
appareils de transports par terre.
Ex.: 620.113.6.016 Freins pour voitures à moteurs électriques.
Organesformant la charpente constitutive des appareils
Chässis, Cadres, Entretoises, Côtés, Fonds, etc.
Organes de support et de roulements.
Essieux, Roues, Boîtes de roues, Jantes, Raies, Moyeux, Bandages,
Glissières, Patins, Rouleaux.
Organes d’attelage et de traction.
Brancards, Limons, Traits, Palonniers.
Organes moteurs.
Leviers, Pédales, Manivelles, Chaînes, Engrenages®
Pour les moteurs mécaniques, voir 629.1.02.
Organes d'appui, de conduite et de direction.
Rênes, Guides, Selles, Sièges, Guidons, Volants.
Organes de retenue et d’enrayage.
Sabots d’enrayage, Freins, Cliquets, Béquille:
Organes d’entretien et de propreté.
Clés, Burettes, Carters, Garde-boue, etc.
Organes accessoires, d’avertissements ou d'appel.
Signaux, Lanternes.
Autres organes.
Moyens de transport primitifs.
Appareils de transport portés à bras ou à dos d'hommes.
Pour fardeaux.
Hottes, Crochets, Brancards.
Pour les personnes.
Civières, Palanquins, Chaises à porteurs.
Appareils de transport à roues, tirés à bras.

Pour les fardeaux.
Brouettes, Diables, etc.
Pour les personnes.
Vinaigrettes, Pousse-pousse, Fauteuils roulants.
Appareils detransport portés directement parles essieux.

 

 
LOCOMOTION 629.113.41

 

629.111.7

.II1.8
2

STTOAL
112.9
LUZ ST
‘112.32
TL2:0

.112.6

 

LE2:y

LS

*TISSE

ALI 29
ATOS 2T

“119:22
21929

.113.4
TS AT

Appareils de transport par glissement.
Traîneaux.

Appareils de transport à rouleaux.

Véhicules ou voitures de transport à traction animale.

On pourra dans les divisions ci-dessous spécifier la destination spéciale des
voitures par l'emploi du signe de relation suivi du numéro indiquant
l'objet considéré.

Ex. : 629.112.5 : 343.8 Voitures cellulaires.

Voitures à deux roues.

Voitures à plus de deux roues.
Pour le transport des fardeaux ou des marchandises.
Pour le transport des personnes.

Voitures spéciales pour le transport en commun des

personnes:
Omnibus, Diligences, Breaks.

Voitures spéciales pour le transport des fardeaux et

marchandises.
Haquets, Camions.

Voitures pour services spéciaux.
Roulottes.

Voitures à traction mécanique. Automobiles.
Voitures automobiles sur routes en général. Voitures à vapeur et autres
et leurs moteurs.
Pour les véhicules allant sur rails, chemins de fer et tramways, voir 625 et
656.
Pour les voiturettes, cycles et motocycles, voir plus loin 620.rr7 et 629.118.
On pourra dans cette division ainsi que dans les suivantes, jusqu'à 629.116 in-
clus, établir à l’aide du tiret les subdivisions suivantes d’après la destination
et la nature des véhicules :
— 1 Locomotives routières.
— 2 Tracteurs.
—3 Trains sur route.
— 4 Voitures lourdes pour marchandises.
— 5 Voitures pour transport en commun
— 6 Voitures de famille.
— 7 Voitures légères.
— 8 Voitures pour courses,
— 9 Autres voitures.

p-

: Premières voitures à moteurs mécaniques. Moteurs
bras, à ressorts, etc. Premiers essais de moteurs
‘vapeur.
Voitures à vapeur.
Voitures à vapeur avec foyer au charbon.
Leblant, de Dion, Bollée, Scott, Rowen, Serpollet,
Voitures à vapeur avec foyer au pétrole.
Serpollet, etc.
Voitures à vapeur sans foyer, à eau surchauffée.
Lamm, Francq, ete.
Voitures. à air et à gaz comprimés et leurs moteurs.
Voitures à air comprimé et à réservoir indépendant.
Mekorski, etc.

pp

 

 

 
 

629.113.42

LOCOMOTION

 

629.113.42

.113.43

113.44
“H13-9

LOT
902
113.6

.113.61

.113.62
2113.03
113.64
.113.65
TS

TJ 7T

ST.
yo T
.117.2

T7.

“IT 7-4
.I118

.118.I

.118.2

ETTOS
.118.4

e .118.5
.118.6

629.12

-13.01I

“2 OFI

TT

s .12.012

 

 

Voitures à air comprimé à réservoir alimenté en route
par canalisation spéciale.
Popp, Conte, etc.
Voitures à gaz d'éclairage comprimé.
Voitures à acide carbonique comprimé.
Voitures à pétrole ou à combustible liquide.
Voitures avec moteurs à essence de pétrole.
Voitures avec moteurs à huile lourde.
Voitures à moteur électrique.
Voitures de différents systèmes à prise de courant
extérieur. :
. Voitures à conducteurs aériens ettrolleys.
Voitures à conducteurs souterrains:
Voitures à contacts superficiels.
Voitures à accumulateurs.
Voitures mixtes à moteurs combinés.
Voitures à pétrole et à électricité.
Véhicules légers à plus de deux roues.
Premiers essais de véhicules légers à plus de deuxroues.

Véhicules légers mus par l’homme.
Tricyles, Quadricycles.

Véhicules légers à moteurs à pétrole.
Motocycles, Voiturettes, Tricycles, Quadricycles
Véhicules légers à moteurs électriques.
Véhicules légers à deux roues au plus.
Cycles, Vélocipèdes.
Premiers essais de vélocipèdes.
Célérifères, Draisiennes, etc.

Vélocipèdes à une seule place mus par l'homme.
Cycles, Bicycles.
Bicyclettes.

Vélocipèdes à plusieurs places mus par l’homme.
Tandem, Sociables, Triplettes, etc.

Vélocipèdes à une seule place et moteur mécanique.
Vélocipèdes à plusieurs places et moteur mécanique.

Locomotion par eau. Moyens de ‘transport par voie

- fluviale èt par voie de mer. :
Pour la théorie de lanavigations voir 527; pour la pratique commerciale,
voir 656, : :
Organes élémentaires entrant dans la construction des
appareils de transport par eau.
Organes formant la charpente constitutive des ap-
pareils.

Coque, Ossature, Bancs, Carlingues, Bordages, Doublages
” Pour les blindages des navires, voir 623.0.

Organes de stabilité.

Flotteurs, Balanciers, Quilles.

 

 

 
 

 

 

 

 

 

 

 

 
 

 

 

 

| 629.123.24 LOCOMOTION.
| :
é
F 629.123.24. Bâtiments spéciaux pour les marchandises.
il Cargo-boats à vapeur, Pétroliers.
1293-29) Bâtiments de servitude.
| Remorqueurs.
É 129 Bâtiments légers pour la navigation de plaisance et
| embarcations. :
| 3 “120 À rames et à voiles.
| -L20217 Bâtiments légers de types anciens ou primitifs.
| Pirogues. °
l .125.12 Yachts à voiles:
12019 Canots, Chaloupes.
| .129.2 À moteurs mécaniques.
l 29521 Types anciens de bâtiments légers à motétrs méca-
| niques.
| 125.22 Vachts et petits bâtiments à vapeur.
| 129.23 Chaloupes à vapeur.
L .125.24 Chaloupes à pétrole.
L 125225 Chaloupes électriques.
| — 629.13 Navigation aérienne. Aéronautique. Moyens de trans-

 

port par l'air.

Pour la théorie, voir 533.6 Aéronautique.

 

ë .13.01 Organes élémentaires entrant dans la construction des
L : appareils de transport aérien.
Fe .OIT Organes formant la charpente constitutive des aéros-
D tats.
| Enveloppes, Ballons, Nacelies, Filets.
.O12 Organes formantlacharpente constitutive desappareils
le | 7 : d'aviation ou aéroplanes.
È Ê Cadres, Châssis, Ailes. à
2 .OT4 * Organes de propulsion et d'orientation.
Hélice, Gouvernail, Stabilisateurs, Equilibrantes.
2 .0I5 Organes de manœuvres et d'atterrissage.
- 5 Freins, Ancres, Soupapes.

ê .O19 Appareils accessoires divers.
LOL Premiers essais de navigation aérienne.
Ë Aérostats, Montgolfières.

.132 Appareils plus légers que É Ballons libres à gaz

hydrogène.

.132.1 : Ballons captifs.

.132.2 Ballons dirigeables. à

“IDE Appareils dits plus lourds que l'air.

HOUS Appareils d'aviation.
1992 Aéroplanes.

1999 Cerfs-volants, :

 

 
 

 
 
 
 
 
 
