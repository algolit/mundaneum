 

 

INTERNATIONAL BIBLIOGRAPHICAL INSTITUTE

Publication AN 62

Bibl. Jndex [ox (064) (æ)]

 

 

ABSTRACT OF THE CATALOGUE
OF THE INTERNATIONAL
BIBLIOGRAPHICAL INSTITUTE

JUNE 1904

SUMMARY :

I.

IT.

‘The International Bibliographical Institute
and the Universal Bibliographieal Index;
Organization of the International Bibliogra-

phical Institute ;

. Bibliographical Departments ;
IV.
V.
VI.
. Universal Bibliographicat Classification by

Catalogue of Publications ;
General Statistics of the Institute ;
General Inventory of the Indexes ;

Decimal Notation ;

. Equipment and Bibliographical Accessories.

 

INTERNATIONAL

 

 

 

BRUSSELS, 1, rue du Musée.
ZURICH, 39, Eidmattstrasse.

BIBLIOGRAPHICAL INSTITUTE

PARIS, 44, rue de Rennes.

 
INTERNATIONAL BIBLIOGRAPHICAL INSTITUTE,

THE INDEX ROOM

IN BRUSSELS

 

I. The International Bibliographical Institute

and Universal Bibliographical Index

The work of the Universal Bibliographical Index, which is identical
with that which the International Bibliographical Institute of Brussels has
undertaken since 1895 (1), aims at gathering and continually bringing up
to date the elements of a first General Index, placed on cards, bringing
together bibliographical notices which refer to every kind of intellectual
work published in every country. It also aims at facilitating the esta-
blishment of indexes of a particular type, reproducing, either as a whole,
or only in certain parts, this Index which is to serve as a prototype.

These particular indexes may be useful to men who confine themselves
to the study of sciences as well as to those who make use of their appli-
cation in a practical manner.

Science progresses, in fact, by the labours of scientific men in every
country and in every branch of science. It behoves the student therefore
to keep himself in touch with the labours of his predecessors and his con-
temporaries, both in order to make use of them and to carry on farther
scientific investigation, by avoiding inevitable repetitions and loss of time.

Practical men, on the other hand, need to be guided in the world of
books, in order to find easily in them documents which may be useful to
them, and which may sometimes bring to their aid valuable elements of
success in the commercial battle. There is the same need for those who
have to interest themselves in the administration of public affairs, such as
legislators, heads of departments or officials.

It is therefore a matter of general interest to establish a Universal Bibli-
ographical Index, in such a manner that more or less complete duplicates
or partial reproductions limited to certain branches of exact sciences or
special subjects, should be established so as to answer readily to the
requirements of educational establishments or government offices, public
or private libraries, scientific or commercial associations, or even t9 the
needs of certain classes of persons, such as librarians, civil engineers,
medical men, lawyers or even private individuals.

This is the programme which the International Bibliographical Insti-
tute has set before itself. In order to carry it into effect the Institute calls
for the cooperation of different nationalities and scientific specialists of
all kinds; it also solicits the help of individual initiative in those coun-
tries where the assistance of public authorities cannot be obtained at
the outset.

{x} Sce Report cf the International Bibliagraphical Institute, Brussels, at the offices of the
Institute, rue du Musee. All documents, researches, and information bearing upon the Index of
Universal Bibliography, its organization and its progress, are published periodically in the Æeforé,

 
— 2 —

In order to secure the success of such international cooperation, the
Institute has adoptéd a scheme of homogeneous work, allowing of the
regular grouping together of all the materials collected, of whatever
origin or kind.

This uniform plan necessitates, for the methodical arrangement of the
materials, a system of methodical classification which can be applied to
the total amount ot human knowledge, and which can assign to every
bibliographical notice at éhe las! determinate plate and onc easy to find
among the continually increasing series of noticeswhich have been
already classified.

In fact we must be able to consider every thing that is published, from
whatever place it may have come, when or how it may have been printed,
as one of the elements of à gigantic encyclopedia, at once theoretical, his-
torical and practical, and of this the Universal Bibliographical Index
farms the general table of contents.

For the classification of subjects, the International Bibliographical
Institute has adapted the Decimal Bibliographical Classification, the tables
of which will receive special notice farther on. They are suggested by
the indexes af the best treatiscs, and are drawn up in different languages
and treat of different sciences.

The tables of Decimal Classification which are to be used are drawn up
by the Brussels International Institute in order to secure the accuracy
of the work.

‘The principles of the method according to which the Universal Biblio-
graphical Index is established and kept up to date, as well as the
special indexes which form part of it, may he summed up as follows :
summary or bibliographicai notice is made of every document or intellec-
tual work (book, review article, proceedings of learned societies, official
or periodical publication, maps, pictures, stalistics, &c.)

These bibliographical notices are set down on movable cards of a
uniform size, each of which is reserved to the notice of one single intel-
lectual work, and which are arranged in regular order in cases set
apart for them.

These notices arc drawn up in strict conformity to certain fixed rules
and the classification of them is carried out by the regular working of
other equaily fxed rules.

The carrying out of the work of collecting and drawing up these notices
is intrusted in each country, according to a scheme of cooperative organi-
zation, to groups of specialists who undertake the preparation of particular
branches of bibliography, in conformity to a general programme drawn
up by the International Bibliographical Institute.

For this preparation, existing bibliographical collections are utilized in
the largest measure possible; but for the unification and coordination of
the materials which they supply, appeal is also made to the assistance of
the editors of all kinds of publications, in order that they may facilitate, by
the. adoption of certain arrangements, the obtaining of printed bibliogra-

 

_— 3 —

phical notices which maÿ be directly made useful for the formation of
indexes,

The original copy or prototype of the Index is kept in Brussels, at the
offices of the International Bibliographical Institute, The multiplication
of this prototype for the formation of the general and of the particular
indexes which are taken from it, is carried out, according to cases, by
means of special publications or manuscript copies.

As to the classification of the cards, in each copy of the Index the prin-
ciple is admitted that there arc to be two identical cards, nne of which is
intended to take its place in an index classifiel according to the names
of authors, and the other in an index classified according to the order of
subjects. .

The frst index answers the question : & What works has a certain author
published ? » The second answers the question : « What works have been
published on a certain subject?»

The classification of the index according to names of authors is made
according to the alphabetical order of these names, completed, if neces-
sary, to certain rules.

The classification of the index according to the order of subjects is made
with reference to the subjects treated of, following the order of the
classifying numbers allotted to these subjects in the tables of the Biblio-
graphical Decimal Classification.

If only one single card for each biblicgraphical notice is possessed,
the index according to order of subjects is as a rule alone made out, and
an abbreviated index supplies the plate of the complete index of names
of authors.

IL. Organization of the International
Bibliographical Institute

The International Bibliographical Institute is an exclusively scientific
association. .

It was established in 1895 to promote bibliographical studies and ta
frame rules of cooperation with a view to form a universal bibliographical
Index by unifying and centralizing bibliographical fabours,

The International Bibliographical Institute has assembled its mem-
bers in three general meetings, two in Brussels in 1895 and 1897 and one
in Paris in 1900.

The Institute invites all associations, congresses, scientific institutes,
the staffs of reviews, as well as libraries, to enrol themselves amongst
its members, and thus give solid assistance to the work of the Uni-
versal Bibliographical Index. (Annual subscription, 10 francs.)

The members of the Institute receive the Report and the Directory Îree.

 

 
JET. Bibliographical Departments

The International Bibliographical Institute is preparing a Universal
Pibliograhhical Tuer, a general catalogue intended to include notices of all
the books and review.articles published in the various countries and on
the various subjects.

This index is divided in two parts, one according to order of subjects, the
other according to names of authors. The classification thus answers
these twoquestions : « What are the works which treat of a certain subject?
What are the works published by a certain author?»

The Universal Bibliographicai Index is encyclopedic in its nature and
aims at embracing the total amount of human knowledge — Bibliography
and literature relating to Libraries, Journalism, Manuscripts, Philosophy,
Theology, Economic and Social Science, Lasr, Education, l'olklore, Philo-
logy, the science and Study of Languages, Mathematics, Physical and
Natural Science, Medicine, Science in relation to engineering and Manu-
facturing, Agriculture, Building and Architecture, line Arts, Literature,
Geography, Biography and Historv.

In order to place these bibliographical collections at the disposal of
the members offhe Institute and also of the public and in order to assist
in the formation of special bibliographical indexes in libraries and other
centres of study, the Institute has arranged the followirg departments :

1. Consultation of the Index, — Tlie Index can be consultéd at the offices
of the Institute, 1, rue du Musée, Brussels, every day {sundays and
horidays excepted), from 10 to 12 and from 3 to 5. No charge is made for
consulting the Index. -

2, Extracts from the Index. — Jxiracts from the Universal Index relating
to any particular question may be sent when asked for. They are sent in
the form op type-written cards at a charge of 5 centimes for each biblio-
graphical notice copied. Lest their correspondents might unexpectedly
haveto pay for more than they want, the Institute informs them when
the number @f cards relating to the subject asked for cxcceds fifty and, in
such case, the Institute asks for confirmation of the order,

3. Special bibliographical indexes. -— For the benefit of scientific esta-
blishments, public departments and private individuals, the Institute
prepares bibliographicai indexes by means of duplicates of the Universal
Bibliographical Index relating to the entirety af the questions connected

 

 

 

si —

to any particular branch of science. These duplicates are drawn up at
the price of 20 francs per thousand cards.

4. Subscription to the Universal Bibliographical Index. -- Subscribers
to the Institute receive periodically copies of the notices of the Universal
Bibliographical Index relating to new works which may come out on any
subject which they may have pointed out to the Institute. Subscribers
must pay an entrance fee of 5 francs, from the payment of which members
of the Institute are free. Accounts are opened to subscribers, who are
charged at the rate of 5 centimes cach. Accounts are made up half
vearly, with the price of all notices sent, and the sums due are col-
lected through the post-oilice.

5. Bibliographical Publications. — ‘The Institute, with the cooperation
of specialists, has arranged for the publication of a collection of special
bibliographies, by means of which new works which have appeared in the
different branches of science will be made known periodically and which
will form contributions printed in the Universal Bibliographical Index.
Several of these bibliographies are in course of publication, and are a
matter of separate subscription. (See n° IV Catalogue of Publications.)

6, Bibliographical Cooperation, — The Institute sallicits the help of
bibliographers, authors, students, publishers, libraries, learned societies
and public depariments. It entrents them to cooperate in the formation
of the Universal Bibliographical Index by sending to the Institute a
list of their works and their bibliogranhical publications, such as catalo-
gues, bibliographies, lists of periodicals, &c, ‘The Institute also entreats
publishers, to bring outin futur these publications in accordance with
the few wenerai regulations which the Institute has drawn up 16 allow
of these publications being considered as printed contributions to the
Universal Bibliographical Index.

7. Exchange of Bibliographical Information, — When inembers of
the Institute send bibliographical notices relating to determinate matters,
and established according to the rules laid down to that effect, they are
entitled to receive gratuitously copy of an equivalent number of notices
of the Universal Index relating to other matters.

8, Various Bibliographical Works, — The Institute possesses an cx-
perienced échnical skif and à private printing office, by means of which
they are able to undertake for others than members, on terms to be agreed
upon, every kind of bibliographical work, especially the organization of
bibliographical departments in scientific establishments, or public or pri-
vate offices. Private bibliographical indexces can be revised at their
owners’ personal residences, library-catalogues can be drawn up and
printed, as well as liste of misceilaneous periodicals, special bibliogra-
phies, &c.

 

 
_— 6 —

g. Accessories and materials for Bibliographical Indexes, — The Institute
has arranged various types of cards and cases for cards and has had them
made in large numbers by qualified furnituremakers. Subscribers can be
supplied with these bibliographical accessories on advantageous terms.
{See the illustrated catalogue of cases and cards.)

IV. Catalogue of Publications

(ABSTRACT)

Bibliographia Universalis, — This collection is made up of a series of
separate bibliographies, compiled by specialists, and published in coope-
ration according to a uniform plan and a familiar method. It includes some-
times the bibliography of books, pamphlets, the communications from
léarned societies and review articles, sometimes the bibliography of oniy
some of these classes of works. They are drawn up in accordance with the
Decimal Bibliographical Classification, and are generally printed either
directly on cards of this type (125 millimetres by 75, taken length-wise) or
in separate numbers of such a type that each bibliographical notice, after
being cut out and gummed separately on a card cf any size, may serve to
complete and bring up to date the indexes and catalogues arranged
according to the method recommended by the Institute or according to
some other method.

ND, Éréion À — fn ordinary dock,
» BE = in books sit obverse pages priniet and blink
reverse,
.2 C = on printed cards, Anprimate
number o
» D = on carés fosimcé by micaus of notices eut ouf and notices which
granite apbearcd up
to January
1904

Part 2. Dibliographia Zoolagica Universalis, from 1896.
Edition À, in weekly numbers, fr. 18.75 a year.
» BB, 3 » 25.00 a year. 102,952

» €, on cards ffr. ro per 1,000 cards).
Sent out in series of about $o cards,

Part 3, Bibliogvaphia Philosophica Universalis, from 1805.
Edition B, in quarterly numbers, fr, 5.00 a year.

Part 4. Bibliographia Physiologica Universalis, from 1893.
Edition B, 3 or 4 numbers a year, fr. 0.50 a number.
»  C, on cards, price varying,

 

Approximate |
namber of
notices which
appearcdupto
January 1"
1904
Part 6. Bibhographia Analomica Universalis, from 1897,
Edition A, 24 numbers a year, fr. 10.00.
» B,24 » » 14.50.

» C,on cards, price varying.

Part 8. Monthly Bibliograbhy of Raïlways, from 1897.
Edition B, 12 numbers à year, fr, 10.00.

Part 13. Bibliography of Eure-et-Loir, [rom 1808.
Edition A, in monthly numbers, fr. 4.00 a year.
a ©, in printed cards, annually : in France,
fr, 4.00 ; abroad, fr, 5.00.

Part 15. Bibliography of Bdgnn, from 1897.

Edition À, bi-monthiy numbers. 93,915

Part 16. Pibliographin Geologica Universalis, from 1856.

Edition À, in annual volumes, price varying. 38,172

Part 17. Pibliographia Furidica Portugaleusis, from 1898.
Edition B, appearing irregularly, 1,800 reis a year. 1,106
» ©, per 100 cards, 300 reis.

Part 30. Pibliographia Medica Universalis, from 1500.

. 108,000
Edition A, about 36,000 notices a year, Îr. 120.00. '

Part 31, Bibliographia Bibliographies Universalis, from 1898.
Edition B, in an annual pamphlet, fr. 4.00.
»  C, on printed cards, fr. 12.00 a year.
» D, on gummed cards, fr. 7.00 a year.

2,146

Part 30. Bibliographia Economica Universalis, from 1902.

Edition B, in an annual volume, fr. 6.00, 3,375

Part 40, Bibliographie Agronomica Universalis, from 1903.

o
Edition B, in quarterly numbers, fr. 12.00 a year. 77094

Partar. Bibliographia Technica Universalis, fram 1903.
Édition B, in monthly numbers, annuaïly : Belgium, 15,064
fr. 10,00; abroad, fr, 14.25,

‘Total number of notices, about 425,730

Manuals for the use of special bibliographical indexes. — ‘l'hc Institute
has drawn up a collection of MANUALS for the formation and use of
bibliographical indexes of every special science.

 

 
_8g—

These manuals include, besides the tables of bibliographical classif-
cation peculiar to the science treated of à general exposition of the prin-
ciples of classification, rules for drawing up bibliographical notices, rules
far the publication of bibliographical collections and for forminz indexes
on cards, rules for cooperating with the Universal BibHographical Index,
and finally practical advice as to the formation of libraries, drawing up
their catalogues and the arrangement of the works on their shelves.
These manuals are Intendect for specialists in every branch of science.
They reproduce, in & practical form, the different documents which
have already appeared elsewhere, for the preparation and the use of
collections and special bibliographical indexes. ‘The various manuals in
the collection are sold separately, and each manual forms a special
number of the publications of the International! Bibliagraphical Institute.

The following have appeared up te the present time in french edition :

Publication 26, Manual for the use of the Bibliographicai Index of Physical
Science [025.4 : 531, Price : 2 francs.

Publication 40. Manual for the use of Bibliographical Indexes [025.4].
Price : 2 francs.

Publication 41. Manual for the use of the Bibliographical Index of
Agricultural Science [025.4 : 63]. Price : 5 francs,

Publication 45, Manual for the use cf the Bibliographical Index of
Photographic Science [025.4 : 37]. Price : 2 france.

Publication 48. Manual for the use of the Bibliographical Index of
Locometion and Sports [025.4 : G2g. 1] }-[o25.4 : 79]. Price : 2 francs.

General_tables of Bibliographical Decimel Classification. — [Pumi-

CATION NÔ 25 OF THE INTERNATIONAL BipLiocrapricaL [xeriTuTe.] — The
french cdition, rerast and enlarged, ef re GENRRAL TABLES of the Biblio.
graphical Decimal Classifieation, has been prepared by the International
Bibliographical Institute with the assistance of a large number of eclla-
borators and more especially with that of the Paris Bibliegraphical
Bureau. This edition brings together the various fragmentary editions
published up to the present time, and appears in successive numbers.
In order not to interfere with the printing arrangements, the order
followed in the publication is that in which the manuscripts prepared by
the collaborators find their way to the Institute,

When the collection shall be comyplete, it will be easy to have the
numbers bound in one volume, in the regular order of subjeels, and accor-
ding tothe table of arrangement which will be published subsequently.
In order to facilitate this arrangement, the tables have not been paged in
regtular order,

Beginning from the present time, the complete edition can be subscribed
to, atthe price of 25 francs, carriage paid. Nummer 16, containing a sum-
mary of the tables, may also be bought separately, at the price of 1 franc.

The complete work bears n° 25 of the publications of the International
Institute of Bibliography ; morcover, the various parts are numbered.

They are sent to subscribers as soon as they are published.

Up tothe present lime, the following parts of the General Tables have
been published :

Number
of piges

No r. General introduction to the Tables of Decimal Classification,
and Summary of the Rules of Decimal Classification . . . 38
No 2, Table of Common Subdivisions . . 18

No 3. Table of division [531 Physical Science ( (Rational and Phy sical
Mechanics]. . . Don eo ou ne 46
N° 4. Table of division [77] Photographic Science ou ou os + + + 30
. Table of division 621.3] Industrial Electricity . . . . + 10

. L'able of division [629.1] Locomotion industries (Locomotion
by land and water, aerostation . . . . … 8
. Table of division [79] Sports (Touring, Cycling, Motoring) . . 10

. Table of division [341 Law .

9. Table of division [615] Therapeutics . . . . . . . + . . 26
. Table of division [616] Internal Pathology . . . . . . . 28
. Table of division [617] External Pathology . . . . . + . 34
. Table of division [ér2) Physiology , . . 38

. Table of divisions [618 and 619] Gy necology: Pediaty Compa.
rative Medicine. . . . 4 os EE
. Table of division [35] Administration . . 44

. Table of division [q] Fstons, Géography, Piography, Genea-
logy . 4 , , . + . + 100

. Summary of ‘Tables. General methodical 'ables sbridgod
General alphabetical Index abridged . . . . + 44

Tabte of division [o] Various. Bbogaphy. Libraries. Learned
Societies, &e, . . . » + + 92

. Table of division [3] Social Science. Statistics, “Political
Economy. Teaching. Assistance. Folklore . . . . . . 68
. L'able of division [r] Philosophy. Moral questions . . . . 28

Total . . 746

Bulietin of the International Bibliographica!l Institute, — [Statements of
the labours of the Institute; record of the studies and inquiries relating to
Bibliography; analyses of bibliographical works; spécial information on
all that concerns the international organization of scientific bibliography
and the formation of the Universal Bibliographical Index.] This report
comes out in six octavo numbers annually (0.25 by 0.16), paper covers.

 

 
Subscriptions reccived for one year, beginning from the first of January. —

Price, within the Postal Union, 10 fr. (series from 1895). It is forwarded
gratuitousiy to members of te Institute. Various parts taken from im-
portant articles published in the Report are also published separately,

and form part of the collection of publications of the Institute.

Directory of the International Bibliographical Institute for the year 1902.

 

— Brussels, at the offices of the Institute, large octavo {0.25 by 0.16), in

paper, 106 pp.

Price : r franc.

Report on the position and work ofthe Institute down to December 31$t

1903. — Brussels, offices of the Institute, large octavo (0.25 by 0.16),

paper covers, 25 p.

V. General Statistics of the Institute

Members :

Total number down to January ft 1904. .
Report of I. B. I. :

Number of pages down to January 15t 1904.
Bibliographia Universalis :

Number of notices published down to January 15t 1904
Publications of I. B. I. besides contributions :

Number down to May 151904 .

Indexes in manuscript, accessible to the public, (Brought down
to July 15t 1903) :
1. Index of subjects (notices; . . . . . 2,483,750
II. Index arranged according to names
{notices} . 4 + + «+ « + « 3,061,000
III. Other indexes (notices) . . . . . . 725,000

Total. , . ————— 6,269,750

Bibliographical library. (Brought down to January 15t 1904) :
Number of works . . . . . . . . . 4 4 , ,
Number of collections of periodicals. . . . . . .

2,205
102

— Il —

VL General Inventory of Accessories

— INDEX
or

SUBJECTS

 

 

Numbers
oi
Bibliogra-
phical
Classification

——

Classed according
to abridged tables

NUMBER OF NOTICES

_ Pa EE

Classed according
to full tables

 

 

General,

Bibliography .
Libraries

Encyclopedias and Col-

lections of Essays.

Reviews and General

Societies.
Political Journals

Miscellanies, Polygraph.
Manuscripts, Rare books

Fhilosophy
Religion . . . .
Sociology and Law.

General.

Statistics

Politics . .
Political Economy .
Law... . . .
Administration
Artof War. . .
Charity and Thrift
Education .
Commerce. . .
Customs, Folklor

Fhilology, Languages .
Pure Sciences

General .
Mathematics
Astronomy .
Physics .
Chemistry .
Geology. .
Paleontology .
Biology .
Botany .
Zoology .

CARRIED FOR WARD -

24,000

35,000

174,000

96,000

29,000
5,000
16,000
2,000
21,000
2,000
1,000

26,000

337,000

35,000

23,000
1,900

50

800

8,00n
$00
850

8,500

282,000

28,500

619,000

 

 

 

 

 

1,236,000 |

 

 
 

 

IL — INDEX Numbers NUMBER OF NOTICES |

of

OF Phogra 7 ToraLs
arure plica Classcd according lassed acecrdin
SUBJECTS Classification] to abridged tubes si to al Fbtes $ VII Universal Bibliographical Classitication

 

 

BROUGHT FORWARD. . 590,060 646,000 1,236,000 ‘ by Decimal Notation
Applied Sciences . . , 200,000 564,000 764,000

General. . . . .. 2,000
Medicine . . ,. . . 390.000
Physiology. . . . . | 38,000
Pngineering Los ee - 74,000 At this moment the International Bibliographical Institute is carrving
Sgriculture. ou ne . 16,000 into effect one of the desiderata most oflen expressed in he world of
omestic Economy. . 4 2,000 L - . . Le : : :
Commerce. , : 10,000 books, viz: the establishment of a universal bibliographical classification.
Chemical Technology . 16,000 This classification is now in course of publication, and its completion is

Various Industries . . 10,000 expected during the year 1904.

Builäing. .
Ë 3,000 Fhis classification #8 universal, international, encyclopedic, and both

Fine Arts . . . . ,. 91,000 " 138,000 general and particular; it is carefully thought out, it is expressed
Various. . . . . 19,000 in a thoroughly exact system of notation : it is very extensive (com-
Music , , . . .. 72,000 . prising about 25,000 divisions) and it is capable of indefinite further

extension” The bases on which it has been founded were adopted by

| tbe International Bibliographical congresses in Brussels in 1895 and 1897

History and Geography . 195,750 250,750 and in Paris in 1900, and it has been already widels applied both in

Europe and in the new svorld,

 

Literature . . , . . . 45,000 .  g3,000

Gencral sous no ue : ü.,000
Gcography. nor ee 60,900 The Universal Bibliographical Classification consists of à vast svste-
Biography . . . . . 36,000 -  matic table of subjects, in which every branch of knowledge is divided into

ancient . , . .., 6.000 none : | k
nf Europe . . . 2.000 classes, sub-classes and divisions, passing from the general to the parti.

of Great Britain . . 3,000 cular, from the whole lo the part, from the genus to the species. Each of
of Germany . . . . 12,000 the headings of this table is represented by à classifying number compo-
of France. ot ot 13,600 sed of onc or more figures, according to the divisions. ‘These numbers

Forccc 71090 are decimal, in this sense that every figure to the right corresponds to a

of Spain . . : . . 2,000 .
of Russia and the Scan- subdivision of the subjectrepresented by the preceding figures. The order

dinavian Countries. : 1,000 in which the numbers follow each other is also of the nature of decimals.
small Lurop. States . 6,000 The following will be taken as an example :
of Belgium. . . . 16,000
Of Asia . , . 5 . 2,000
Of Africa . .., . . ‘ 550
Of America , , . . 2,000

 

z Philosophy.
2 Religion,

3 Social Science.
31 Statistics.

IL ONOMASTIC INDEX, OR ACCORDING TO NAMES OF Politics.
AUTTIORS, — Number of cases of drawers for each letter of the ‘ 33 Financial Economy.
D DOTE to es oi te : À (218), B (380), C {264), 331 Labour Questions.

174), 108), EF (r4o}, x (150), I (161), TI (19), J (6x), K (66), - ti r
L (181), M (261), N (75, O (50), P (225), Q (7), R (176), S (289), 333 Politicat Economy.
T (ag), U (13), V (68, W 1135), X (2), Ÿ (9), Sea). à | . , : | 3,061,000 3324 Money.

IIT. OTHER INDEXES , . . . 4 4 4 725,000

 

Torars, , 942,000 1,541,750

 

 

 

 

 

‘The systematic table is completed by än alphabetical index of subjects,
in which all the headings of the first table are arranged in one single

 

 

Torar. oF ALL INDExES, . , | G6,269,750

 

 

 

 
alphabetical order and are followed by the corresponding classifying
number. ÂÀsan example :

Financial Economy 7 332
Political Economy 33
Money 332.4
Philosophy I
Politics, Labour Questions 331
Religion 2
Sacial scicnec 3
Statistics 37
Labour 337

These tables are followed by an indexation, 1. e., a record of the docu-
ments that are to be classified, by means of a classifying number corres-
ponding to the principal subject of which they treat. ‘This method renders
it possible ta form collections of documents arranged in a perfectly
methodical order, capable at the same time of being enlarged and of
receiving fresh insertions.

In a general way, considered as a classification of subjects which are
at ance uniform in their nature and international in their interest, the uni-
versal bibliographical classification is capable of being applied to the
classing of different kinds of documents and other materials of which
intellectual workers may wish to makc use, and with this view, it supplies
a framework ready to the hand and marked out beforchand. Among the
purposes for which it is suitable, may be enumerated the following,
classification of bibliographical indexes and catalogues, of works already
contained in libraries, of notes, observations, extracts and various
documents intended for studies and private researches, of indexes of
collections of periodicals, of papers Containing sketches, illustrations,
photographs, stereotyne plates, palents, specimens, manufacturing cata.
logues, commercial circulars, and of every conceivable kind of document
in the most extensive sense of the word.

When thé universal classification of documents shaïl be made use of
everywhere, and its application shall have become general, then, instead
of having to get familiar with twenty different methods, varying according
to the institutions which keep and classify the documents, the publie will
be able, by the aïd of one single key, i. e., a single table of classification
of contents; to unfold for itself the treasures contained in all the deposi-
tories of documents. À considerable saving of time will thus be effected,
and the inquirer will benefit by the advantages of a direct communication
established between all the documentary sources cf knowledge. The
universal Bibliographical Classification will in the end allow of the esta-
blishment of mutual understanding and cooperation in all kinds cf intel-
léctual work. From the point of view of international coadjutorship, it
will play a part similar to that which is expected from tlie international
language, which does not seek to interfere with or to supplant living
languages, but rather to be used as an auxiliary and à complement with a
view to international relations.

 

— 15 —

VIIL, Materials and Bibliographical Accessories

{Ask for detailed itlustrated Catalogue)

CARD CASES

 

 

AVITHQUT LOCK AVITH LOCK

 

TYPE

 

‘Evpe | Price Type | Price

Case with 2 drawers . . . . . P2 Ab . 27»
» » on os ‘ 38 Bb 45 »
» ou à 4 72 Cb 95 »
» sou Gb 155 »
n Lou un 210 »
» so se Fb 290 »
» ne + Db 520 »

Qne drawer holds about a thousand cards.

 

PRICE PER THOUSAND
DESCRIPTION | Printed

inted : :
Unprinte on one side jes buth sides

 

 

“White cards in imitation Japan . . Fr. 3 50 | Fr. 5 »|X7r. 6 50
” » » » double » Ho »] » 13 5»

Variously coloured { With handles | C
divisions cards With notches » 10 50! » 12

 

ÿ

 

VARIOUS FORMS OF CARDS

Type Price
Francs

Cards with alphabetical divisions, the set of twenty six letters... . . . . Xaa Ô 50
Dito {four letters À, D, L, R). 4444444. Eab
Cars divided according to subjects, with décimal divisions D to , w sith : a
summary of the general tables of classificution, the set o[ten, - . . . . Eba
Dit, with geographical decimal divisions, the setoften. :
Cards for lists of partodicats, the number of divisions varying & accordinte to the
periodicity, per thousand , 44... sue.
List of members... . 4, .. ..

Eards for comittees of { ° .
learned societies, per | »  subscriberstoihe Ærgorf, , . .

thousand »  exchanges ofthe Æeporé. .

 

 

 
— 1) —

The division cards are of the same width as those in white, but are
larger in height and coloured. They are to be used for grouping
together the white cards concerning information of the same kind, for
exemple : letters of the alphabet or names of countries, towns, &c.

The white cards may be supplied ruled at an increased price of 1 franc
per thousand for the obverse, and fr. 1.75 per thousand for obverse and
reverse sides. For double white cards the prices are double.

Cards may also be supplied ruled in squares, or with any kind of indica.
tions printed on them, price according to quantity.

These different types of cards may be delivered unperforated if wished,
at a slightly diminished price.

Classifying Cases of the International Bibliographical Institute

(PATENT)

Type Aa, without lock, 22 fr.

Classifyi ith 2 d
assifying case with 2 drawers Tres Ab, will lock, Si

Type Ba, witho )Ck
Classifying case with 4 drawers } HS pe ith ut lock, 38 fr.
| Type Bb, with lock, 45 fr.

ORDERS

Al publications, copies of bibliographical cards and bibliographical
accessories, may Le procured by applying direct to the International
Bibliographical Institute, in Brussels, x, rue du Musée (Royal Picture
Galeries). Telephone 5487.

 

INTERNATIONAL
BIBLIOGRAPHICAL INSTITUTE

BRUSSELS, 1, RUE DU MUSÉE.

 

APPLICATION FOR MEMBERSHIP

(Member of the Institute)

The undersigned (name, profession, address)

 

requesis you to propose his name as a member of the International Bibliogra-
Phical Institute, and, if elected, to forward him the Report published by the

Institute.
, the IQ —

(Signature|

SUBSCRIPTION FORM

(Publications)

The International Bibliographical Institute is requested to enter my name as
{ Its report for the year )
subscriber to ETES for the year \ for which & enclose
{ Publication n° }
the amount, fr. by Post-Office order.
=, UE ai L Di

(Name)
(Profession)
(Address)

(Signature)

 

 

 

ORDER FORM

(Bibliographical Materials and Accessories)

The International Bibliographical Institute is requested to forward me

classifying cases, type

V for wuch &. enclose the amount

. cards, type
by Post-Ofce order.
, the

 

(Name)
(Profession)
(Address)

(Signature)

 

 

 
 

SUBSCRIPTION FORM

 

 

 

 

International

Bibliographical Institute

1, rue du Musée

BRUSSELS

rte ie

 

 

 

 

 

 

 
Oscar LAMBERTY

PAINTER OF THE INTERNATIONAL BIBLIOGRAPHICAL INSTITUTE

VEYDT STREET, 70, IN BRUSSELS

 
