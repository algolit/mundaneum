CONGRÈS MONDIAL

DES

ASSOCIATIONS INTERNATIONALES N° 39
Deuxième session : GAND-BAUXELLES, 15-18 juin 1913

Organisé par l'Union des Associations Internationales

Office central : Bruxelles, 3bis, rue de la Régence

 

1913.06

 

 

 

Actes du Congrès. — Documents préliminaires.

 

 

 

Le Progrès

Résumé des discussions
à l’Institut International de Sociologie

PAR

RENÉ WORMS
Secrétaire général

[3(o1)]

Au premier Congrès Mondial des Associations Internationales,
le regretté professeur Hector Denis à présenté une note sur l’Ins-
titut International de Sociologie, dont il avait été président.
Depuis cette date, cette association scientifique, fondée il y a
vingt ans,a tenu son huitième Congrès, dans des conditions qui
méritent d’être ici relatées brièvement. Les précédents Congrès
s'étaient réunis aux Universités de Paris, Londres et Berne.
Celui-ci a siégé à l'Université de Rome, du 7 au 12 octobre 1912.
Ses séances ont été présidées tour à tour par MM. le député Fer-
dinand Buisson (Paris), le sénateur Raffaele Garofalo (Rome) et
le professeur Ludwig Stein (Berlin). de manière à lui assurer un
caractère pleinement international.

Le Gouvernement italien lui a fait le plus obligeant accueil.
Cinq ministres assistaient à sa séance inaugurale. Des réceptions
ont été données en son honneur par le ministre des Affaires

 
étrangères et par la municipalité de Rome. Un Comité local
d'organisation, constitué par les soins de la Société italienne de
Sociologie, a multiplié pour les congressistes, les invitations et les
excursions les plus agréables.

Le sujet unique de la session était : « Le Progrès ». Cette vaste
question devait être examinée sous ses aspects les plus divers.
11 était entendu qu'on envisagerait les formes particulières du
progrès, et qu'on csquisserait ensuite sa théorie générale. La
première partie des travaux fut donc analytique ; la seconde,
synthétique.

La partie analytique porta sur : le progrès anthropologique,
le progrès économique, le progrès politique, le progrès mental.
Nous ne saurions signaler ici que les plus importants d'entre
les rapports qui furent présentés. Sur le progrès anthropologique,
M. L. Manouvrier montra que l'organisme humain, s’il présente
un perfectionnement certain depuis les temps préhistoriques,
n'en montre pas un tangible aux époques historiques. M. G. Pa-
pillault opposa l'hygiène lamarckienne à l'eugénique darwinienne,
et M. G.-L. Duprai signala certaines contre-sélections sociales.
Sur le progrès économique, le Congrès écouta tour à tour des rap-
ports dus à des représentants de quatre écoles opposées : M. René
Maunier, de l'école historique, et M. Vves Guyot, de l'école libé-
rale, traitèrent du progrès dans la production : M. Eugène Four-
nière, de l’école socialiste, envisagea le progrès dans la réparti-
tion ; M. Charles Gide, de l’école solidariste, parla du progrès dans
la consommation. Sur le progrès politique, M. Maxime Kova-
lewsky, M. Ferdinand Buisson et le prince Roland Bonaparte, se
trouvèrent d'accord pour constater qu'il conduit à des institu-
tions de plus en plus démocratiques. Sur le progrès mental,
M. Grimanelli exposa la thèse positiviste de l’évolution intellec-
tuelle, M. Mackenzie chercha les principes d’un perfectionnement
social par l'éducation morale, et M. Léon Philippe examina
l'évolution esthétique, spécialement dans la musique des derniers
siècles. |

La partie synthétique du Congrès s'ouvrit par un résumé que
nous fimes personnellement des vues émises sur le progrès par
les fondateurs de la sociologie (Auguste Comte, Herbert Spencer,
Quetelet}, et des problèmes généraux qui se posent sur la marche
des sociétés. Puis, plusicurs tentatives furent faites pour donner

— 3 —

des solutions à ces problèmes. Deux d'entre elles émanaient de
penseurs qui ont été récemment enlevés à la science : M. T. No-
vicow, pour qui tout progès consiste dans une association plus
intime des éléments humaïns, et M. Lester Ward, qui s'attache
à distinguer le progrès spontané du progrès volontaire. Une note
de M. Wilhelm Ostwald, esquissait une expression énergétique
des lois générales du progrès. Une autre, de M. Alessandro Chiap-
pelli, voyait dans le progrès une substitution des valeurs.
MM. Ludwig Stein et Raoul de la Grasserie se rangeaient parmi
les optimistes. M. Robert Michels parlait, au contraire, du carac-
tère partiel et contradictoire du progrès. MM. E. de Roberty,
G. Sergi, J.-K. Kochanowski, Rudolph Goldscheid, etc., pre-
naient aussi part au débat. Il n’est guère possible d'analyser,
dans une aussi brève note, tant d’études d’un caractère philo-
sophique. Elles vont, du reste, paraître en leur intégralité dans
le tome XIV des Annales de l'Institut International de Sociologie
(Paris, Giard et Brière, 530 pages), par lequel on pourra juger des
résultats scientifiques de la session.

Sur l'invitation de la Société de Sociologie de Vienne, le Congrès
de Rome a décidé que la capitale de l'Autriche serait le siège
de la prochaine réunion. L'Institut International de Sociologie
compte donc tenir son neuvième Congrès à l'Université de
Vienne, au printemps de 1915. Il aura pour sujet : « L’Autorité
et la Hiérarchie sociale ».

 
