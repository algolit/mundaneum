 
 
 
 

 

 

 

 

 

 

 

 
 

 

Office International de Bibliographt SLOTHENL

HOTEL RAVENSTEIN, BRUXELLES

DECHAAL CLASNIFICATION

Tables générales

General summaries

Hauptabtheilungen

 

BRUXELLES

IMPRIMERIE VEUVE FERDINAND LARCIER
26-28, RUE DES MINIMES

1805

  

#

gs

 

Een a en RE

CRE

 
 
 

 

+

 

9

bi
.

CHALET Où on AS

PREMIÈRE TABLE

CLASSES

OUVRAGES GÉNÉRAUX
PHILOSOPHIE
RELIGION

SOCIOLOGIE
PHILOLOGIE

SCIENCES

SCIENCES APPLIQUÉES
BEAUX-ARTS
LITTÉRATURE
HISTOIRE

 

 

 

 
SECONDE

TABLE

DIVISIONS

000 Ouvrages généraux.

010 BIBLIOGRAPHIE.

020 BIBLIOTHÉCONOMIE.

030 ENCYCLOPÉDIES GÉNÉRALES.
040 COLLECTIONS GÉNÉRALES.
050 PÉRIODIQUES GÉNÉRAUX.
060 SOCIÉTÉS GÉNÉRALES.

070 JOURNAUX.

080 BIBLIOT.SPÉCIALES, POLYGRAPHIE,
090 Livres RARES.
100 Philosophie.

110 MÉTAPHYSIQUE.

120 SUJETS SPÉCIAUX DEMÉTAPHYSIQUE.
130 L'ESPRIT ET LE CORPS,
140 SYSTÈMES PHILOSOPHIQUES.

150 FACULTÉSMENTALES, PSYCHOLOG,
160 LOGIQUE.

170 ÉTHIQUE.

180 PHILOSOPHES ANCIENS.

190 PHILOSOPHES MODERNES,

200 Religion.

210 THÉOLOGIE NATURELLE.

220 BIBLE.

230 THÉOL.DOCTRINALE, DOGMATIQUE.
240 DéÉvorTion. PRATIQUE, ŒUVRES,
250 SERMONS. CLERGÉ. PAROISSE.
260 L'ÉGLISE : SES INSTITUTIONS,

270 HISTOIRE RELIGIEUSE,

280 RELIGIONS ET SECTES CHRÉTIENNES.
290 RELIGIONS NON CHRÉTIENNES.

300 Sociologie.

310 STATISTIQUE.

320 SCIENCE POLITIQUE.

330 ECONOMIE POLITIQUE ET SOCIALE.
340 DROIT.

350 ADMINISTRATION,

360 ASSISTANCE. ASSURANCE.

370 ENSEIGNEMENT.

380 COMMERCE. TRANSPORT.

390  CourumEs, CosTumes. FOLKLORE.

400 Philologie.

410 COMPARÉE.

420 ANGLAISE.

430 GERMANIQUE.

440 FRANÇAISE.

420 ITALIENNE.

460 EsPAGNoLE.

470 LATINE.

480 GRECQUE.

490 LANGUES SECONDAIRES.

 

500

510
520
530
540
550
560
570
840
590

600

610
620
630
640
650
660
670
680
690

700

710
720
730
749
720

779
780

192

800

810
820
830
840
850
860
870
880
890

900

910
920
930
940

960
979
980
990

 

 

Sciences.

MATHÉMATIQUES.
ASTRONOMIE.
PHYSIQUE,
CHIMIE,
GÉOLOGIE,
PALÉONTOLOGIE.
BioLoGrE.
BOTANIQUE,
ZOOLOGIE.

Sciences appliquées.

MÉDECINE.

ART DE L'INGÉNIEUR.
AGRICULTURE,
ECONOMIE DOMESTIQUE.
TRANSPORT. COMMERCE,
CHIMIE APPLIQUÉE.
MANUFACTURES.
INDUSTRIE MÉCANIQUE.
CONSTRUCTION.

Beaux Arts.

PAYSAGES DE JARDINS,
ARCHITECTURE.
SCULPTURE.

Dessin. DÉCORATION.
PEINTURE.

GRAYURE.
PHOTOGRAPHIE.
MUSIQUE.
RÉCRÉATIONS.

Littérature.

AMÉRICAINE.

ANGLAISE.

GERMANIQUE.

FRANÇAISE.

ITALIENNE,

ESPAGNOLE.

LATINE.

GRECQUE.

LITTÉRATURES SECONDAIRES.

Histoire.

GÉOGRAPHIE.
BIOGRAPHIE.
HISTOIRE ANCIENNE.
EUROPE.
ASIE,
AFRIQUE,
AMÉRIQUE pu Norp.
AMÉRIQUE DU SUD.
OcÉANtE. RÉGIONS POLAIRES.

Hisr. Mon.

 

 
 

tee re das

 

 

000

001
002
003
004.
00
006
007
008
009

(op do)

o11

012
013
014
019

016

017
018

019

020

o21
022
023
024
02)
026
027
028
029

030
031
032
033
034.
035
036
037
038
039

040

041
042
043
044
045
046
04

O4:

049

TROISIÈME TABLE

OUVRAGES

Laïssé en blanc pour usages parti-
culiers.

Bibliographie.

Bibliogr. générales,
» spéciales d'auteurs,
n par classes d'auteurs,
» d’anon.etde pseudon.
» par pays.
» de sujets spéciaux.
Catalogues par ord.de matières,
» par nom d'auteur,
» par ordre alphabét.

Bibliothéconomie.

But des bibliothèques.
Constructions etaménagements,
Administration et personnel.
Règlements pour les lecteurs.
Organisation des bibliothèques.
Biblioth. de matières spéciales.
» générales.
Lecture : assistance aux lecteurs,
L’art d'étudier.

Encyclopédies générales.

Américaines.
Anglaises,
Allemandes.
Françaises.

Italiennes
Espagnoles.

Slaves,

Scandinaves.
Langues secondaires.

Collect. gén. de manuels.

Américains,

Anglais.

Allemands.
Français,

Italiens.

Espagnols.

Slaves.

Scandinaves.
Langues secondaires.

 

 

SUBDIVISIONS

GÉNÉRAUX

050 Périodiques généraux.

051
052
053
054
055
056
05

056
059

060

061
062
063
064.
065
066
067
068
069

070

071

979

080

o81
082
083
084.
085
086
087
088
089

090
001

099

Américains.

Anglais.

Allemands.
Français.

Italiens.

Espagnols.

Sacs.

Scandinaves.
Langues secondaires,

Sociétés générales.

Américaines.
Anglaises.
Allemandes,
Françaises.
Italiennes.
Espagnoles.

Slaves.

Scandinaves,
Langues secondaires.

Journaux.

Américains.

Anglais.

Allemands.

Français.

Italiens.

Espagnols.

Slaves.

Scandinaves.
Langues secondaires.

Bibl, spéc. Polygraphies.

Laiïssé en blanc pour usages parti-
culiers.

Livres rares.

Manuscrits.

Block Books.

Premiers livres. Incunables.
Impressions rares.

Reliures rares.

Illustrations ou matières rares,
Ex libris. Book Plates.

Livres prohibés ou supposés.
Autres raretés. Curiosa.

 

 

og en

 
 

gr nee APRES

presse

 

 

TRoIsiÈME TABLE

PHILOSOPHIE

xoo Philosophie en général.

101 Utilité.

102 Manuels,

103 Dictionnaires.
104 Essais.

105 Périodiques,
106 Sociétés.

10 Enseignement.
10 Polygraphie,
109 Histoire.

110 Métaphysique.

111 Ontologie.
112 Méthodologie.
119 Cosmologie.

114 Espace,

115 Temps.

116 Mouvement,

11 Z Matière.

11 Force.

119 Quantité etnombre.

120 Sujets métaphysiques.

121 Connaissance : origine et limites.
122 Causalité : cause et effets.

123 Liberté et nécessité,

124 Téléologie. Causes finales,

125 Infini et fini.

126 Conscience. Personnalité,

127 Inconscience. Automatisme.
128 L'âme.

120 Origine de l’âme individuelle.

130 L'Esprit et le Corps.
131 Physiol. et hygiène mentales.

132 Troubles mentaux. ’
133 Apparitions. Sorcellerie. Magie.
134 Mesmerisme. Double vue. |

135 Sommeil. Songes. Somnamb.
136 Caractéristiques mentales.

137 Tempéraments.

138 Physiognomonie.

139 Phrénolog. Photograp. mentale,

140 Systèmes philosophiques.

141 Idéalisme. Transcendantalisme.
142 Philosophie critique.

143 Intuition.

144 Empirisme.

145 Sensualisme.

146 Matérialisme. Positivisme.

14: Panthéisme. Monisme.

14 Eclectisme.

149 Autres systèmes philosoph,

 

SUBDIVISIONS

150 Facultés mentales.

151 Intelligence.

152 Sens.

153 Entendement.

154 Mémoire.

155 Imagination.

156 Raison.

157 Sensibilité. Emotions.
158 Instinct, Appétit.

159 Volonté.
160 Logique.

161 Inductive.

162 Déductive.

163 Assentiment. Foi.

164 Symbolique, Algébrique.

165 Sources d'erreur. Sophismes.
166  Syllogisme, Enthymème.

167 Hypothèse,

168 Argument. Persuasion.

169  Analogie, Correspondance.

170 Éthique.

171 Philosophie de l'éthique.

172 Ethique de l'Etat.

173 Ethique de la famille.

174. Ethique des professions.

175 Ethique du plaisir,

176 Ethique des sexes.

177 Ethique des relations sociales.
178 Tempérance.

179 Autres sujets de l'éthique.

 

180 Philosophes anciens.

181 Orient.

182 Grèce primitive.

183 Sophistes et socratistes.

184 Platoniciens.

185  Aristotéliciens.

186 Pyrrhoniens. Neoplatoniciens,
18 Epicuriens.

18 Stoiciens.

189 Chrétiens primitifs.

190 Philosophes modernes.

191 Américains.
102 Anglais,

193 Allemands.
104 Français.
192 Italiens.

196 Espagnols,
19 Slaves.

19) Scandinaves.
199 Autres,

 
 

 

 

 

g meme

l
:
|
|
TRrorsième TABLE SUBDIVISIONS |
RELIGION |
200 Religion. 250 Homélie. Clergé. |
201 Philosophie. Théories. 251 Homélies. Prêches. |
202 Manuels. 252 Sermons. . !
à 203 Dictionnaires, Encyclopédies. 253 Visites pastorales. Evangélisat, |
J 204 Essais. Conférences. 254 Vie religieuse. Célibat, |
205 Périodiques. 255 Confr. d'hommes et de femmes. |
206 Sociétés. 256 Sociétés paroïissiales.
; 207 Enseignement. Ecoles théolog. | 257 Ecoles et biblioth. paroïssiales. |
208 Polygraphie, Collections, 258 Entret, paroiss. des malades, etc.
J 209 Histoire de la Théologie. 259 Autres travaux de la paroisse. |
1
210 Théologie naturelle. 260 Eglise : institut. ét trav. |
211  Déisme. Athéisme. 261 L'Eglise. |
212 Panthéisme. Théosophie. 262 Politique de l'Eglise, |
213 Création. Evolution. 263 Sabbat. Dimanche. |
214 Providence. Fatalisme. 264 Culte public. Rituel. |
215 Religion. Science. 265 Sacrements. |
216 Mal. Réparation. 266 Missions intérieures et étrangèr. |
21 Prière, 267 Associations. Y. M. C. A., etc. |

21 Vie future. Immortalité. 268 Ecoles du dimanche.
210 Analogies. Correspondances. 269 Refuges. Retraites. |
220 Bible. 270 Histoire religieuse. {
221 Vieux Testament. 271 Ordres monastiques. |
222 Livres historiques. 272 Persécutions. |
223 Livres poétiques. 275 Doctrine. Dogmes. Hérésies. |
224 Livres prophétiques. 274 Europe. |

225 Nouveau Testament. 279 Asie.
226 Evangiles. Actes des Apôtres. | 276 Afrique. |
22 Epitres. -277 Amérique du Nord. i
22 Apocalypse. 278 Amérique du Sud.
229  Apocryphes. 279 Océanie. 1
230 Doctrines. Dogmatique. | 280 Eglises chrétiennes. Â
231 Dieu. Unité. Trinité, 281 Eglise primitive et orientale, À
232 Christ. Christologie. 282 Eglise catholique romaine. {
233 Homme. Chute origin Péché, | 283 Eglise anglicane et américaine, ï
234 Salut, Soteriologie. 284 Eglise protestante continentale, À
235 Anges, Démons. Satan. 285 Eglise presbytér. rationnelle. À
236 Echatologie. Mort. Jugement. | 286 Baptistes. 4
237  Viefuture. 287  Méthodistes, }
238 Credo. Confessions. 288 Unitairiens. ‘
239 Apologétique. 289 Sectes chrétiennes secondaires. |
{ : : à |
240 Pratique. Dévotion. 290 Relig. ethnol.Sc.des relig. {
241 Didactique. Catéchisme, 291 Mythologie compar. et génér. |
242 Méditation: 292 » grecque et romaine, À
243 Exhortation. 203 » german. et septentr. fl
244 Mélanges. Romans religieux. 204 Brahmanisme. Bouddhisme. |
245 Hymnologie. Poésie religieuse, | 205 Parséisme. {
246 Ecclésiologie. Symbolisme, 290 Judaïsme. ï
.247. Objets sacrés. Vases, etc. 297 Mahométanisme. k
248 Religionindividuelle, Ascétisme | 2098 Mormonisme.
249  Dévotions familiales, 299 Religions secondaires, 4
fi
1
i
#1
à
À
1:
1!
k
t 5 FT à
MERS si

 

 
TROISIÈME TABLE

SUBDIVISIONS

SOCIOLOGIE

 

 

 

 

300 Sociologie en général. 350 Administration.
301 Philosoph. des sciences sociales, | 351 Droit administratif en général.
302 Manuels. 352 Gouvernem. local, Communes.
303 Dictionnaires et encyclopédies. | 353 Organisation du gouv. central.
304 Essais. 354 Organisation du gouv. central.
305 Revues, 355 Armée. Science militaire.
306 Sociétés. 356 Infanterie.
30 Enseignement. Etude. 357 Cavalerie,
30 Polygraphie. 358 Artillerie.
309 Histoire de la sociologie. 359 Marine, Science navale,
310 Statistique. 360 Assist. Assur. Associat
311 Théories, Méthodes. 361 Assistance.
312 Population. Démographie. 362 Etablissement de bienfaisance.
313 Sujets spéciaux. 363 Associations politiques.
314 Europe, 364 Maisons de réforme.
315 Asie, 365 Prisons.
316 Afrique. 366 Sociétés secrètes,
317 Amérique du Nord. 367 Clubs sociaux.
318 Amérique du Sud. 368 Assurance,
319 Océanie. 369 Autres associations.
320 Science politique. 370 Enseignement.
321 Forme de l'Etat. 371 Pédagogie.
322 Eglise et Etat. 372 Enseignement primaire.
323 Politique interne. 373 Enseignement moyen.
324 Elections. 374 Enseignement personnel.
329 Migration et colonisation. 372 Programmes scolaires.
326 Esclavage. 376 Enseignement des femmes.
32 Politique internationale. 377 Enseignement religieux et mor.
328 Parlements et parlementarisme. | 378 Enseignement supérieur.
329 Partis politiques. 379 Intervention de l'Etat.
330 Economie politique. 380 Commerce. Transport.
331 Travail et travailleurs. 381 Commerce intérieur.
232 Banque. Monnaie. Crédit. 382 Commerce extérieur.
333 Propriété immobilière, 383 Postes,
334 Coopération. 384  Télégraphe. Téléphone.
335 Socialisme, 385 Chemins de fer.
336 Finances publiques. 386 Canaux et grandes routes.
337 Protection. Libre échange, 387 Transport fluvial et maritime.
338 Production. Manufactures. 388 Commerce local. Voirie.
339 Paupérisme. Capitalisme, 380 Poids et mesures. Métrologie.
340 Droit. 390 Coutumes. Vie populaire.
341 Droit international, 391 Costume et soins dela personne.
342 Droit constitutionnel, 392 Naïssance. Intérieur. Mariage.
343 Droit pénal. 303 Traitement des morts.
344 Droit militaire. 394. Coutumes publiques et sociales.
545  Législat. et jurispr. en général. | 395 Etiquette. Politesse,
346 306 Position de la femme.
347 Droit privé, 397 Nomades. Tziganes.
348 Droit canonique et ecclésiastiq. | 308 Folklore. Proverbes, etc.
349 Histoire du droit, Droît romain. 309 Coutumes de guerre,
“4
l
f
l
l
&
u
EX

 

 

 

 

 
 

he ——————— 0"  ——— —

ti. cd

 

TROISIÈME TABLE

SUBDIVISIONS

PHILOLOGIE

400 Philologie en général.

401 Philosophie, Théories,
402 Manuels.

403 Encyclopédies. :

404 Essais,

405 Périodiques.

406 Sociétés.

407 Enseignement, Etude.
408 Polygraphie.

409 Histoire.

410 Philologie comparée,

411 Orthographe.
412 Etymologie.
413 Dictionnaires.
414 Phonologie.
415 Grammaire.
416 Prosodie.
417 Inscriptions.
418 Textes.
419 Hiéroglyphes.

420 Philologie anglaise.

421 Orthographe.

422 Etymologie,

423 Dictionnaires.
424 Synonymes.

429 Grammaire,

426 Prosodie,

427 Dialectes.

428 Textes classiques.

429 Anglo-saxon.

430 Philologie germanique.

431 Orthographe,

432 Etymologie.

433 Dictionnaires.

434 Synonymes.

435 Grammaire.

436 Prosodie.

437  Dialectes.

438 Textes classiques.

439 Langues germaniques second.

440 Philologie française.

441 Orthographe.

442 Etymologie.

443 Dictionnaires,
444 Synonymes.

445 Gammaire.

446 Prosodie.

447 Dialectes.

448 Textes classiques.

449 Provençal.

 

450 Philologie italienne.

451 Orthographe.

452 Etymologie,

453 Dictionnaires,

454 Synonymes.

455 Grammaire.

456 Prosodie,

457  Dialectes,

458 Textes classiques.

459 Roumain. Vallaque.

460 Philologie espagnole.

461 Orthographe.

462 Etymologie.

463 Dictionnaires.
464 Synonymes.

465 Grammaire.

466 Prosodie,

467  Dialectes.

468 Textes classiques.
469 Portugais.

370 Philologie latine.

A71 Orthographe.

472 Etymologie.

473 Dictionnaires.

A7A Synonymes.

475 Grammaire.

476 Prosodie,

477 Dialectes,

478 Textes classiques.

479  Languesitaliques secondaires,

480 Philologie grecque.

481 Orthographe.

482 Etymologie.

483 Dictionnaires.

484 Synonymes.

485 Grammaire.

486 Prosodie.

487  Dialectes.

488 Textes classiques.

489 Langues helléniq. secondaires.

490 Autres langues.

491 Langues indo-européennes.
492 Sémitique,.

493 Hamitique.

494  Scythique.

492 Asie orientale,

490 Afrique.

497 Amérique du Nord.

408 Amérique du Sud.

499 Malaisie. Polynésie.Autres lang.

 

À
|
1
Î

 

A TRE Te
 

 

 

 

TROISIÈME TABLE SUBDIVISIONS
SCIENCES
500 Sciences en général. 550 Géologie.
5o1 Philosophie des sciences. 551 Géologie physique et dynam.
502 Manuels. 552 Lithologie. Pétrographie.
503 Dictionnaires. Encyclopédies. 553 Géologie économique.
5o4 Essais. 554 Europe.
505 Périodiques. 555 Asie.
506 Sociétés. 556 Afrique.
567 Enseignement. Etude. 557 Amérique du Nord,
508 Polygraphie. Collections. 558 Amérique du Sud.
5og Histoire. 559 Océanie.
510 Mathématiques. 560 Paléontologie.
bit Arithmétique. 561 Plantes.
512 Algèbre. 562 Invertébrés.
513 Géométrie. 563 Protozoaires. Radiés.
514  Trigonométrie. 564 Molliusques.
515 Géométrie descriptive. 565 Articulés.
516 Géométrie analytique. 566 Vertébrés.
517 Calcul différent. et intégral, etc. 567 Poissons. Batraciens.
518 - 568 Reptiles. Oiseaux.
519 Probabilités. 569 Mammifères.
520 Astronomie. 570 Biologie. Ethnologie.
521 Théorique, 571 Archéologie préhistorique.
522 Pratique et sphérique. , 572 Ethnologie.
523  Descriptive. : ee
524 Cartes et observations. 7 an de l'homme.
22 ee 575 Evolutions. Espèces.
_ Nasraton 576 Origine de la vie.
528 Ephén “ee Dr Propriété de la matière vivante,
5 Chr logi 578 Microscopie,
29 rono1og1e. 579 Manuels des collectionneurs.
530 Physique. 580 Botanique.
531 Mécanique, i i
532 Liquide. Hydrostatique. er PR
533 Gaz. Pneumatique. 583 Dicotylédonées.
534 Son. Acoustique. 584 Monocotylédonées.
535 Lumière. Optique. 595 Gymnospermes.
536 Chaleur. 586  Cryptogames.
537 Electricité. 587 Ptéridophytes.
538  Magnétisme. 588 Bryophytes.
539 Physique moléculaire. 589 Thallophytes.
540 Chimie. 590 Zoologie. |
541 Chimie théorique. É ee |
542 Chimie pratique. ne, Tiers e |
543 os 503 Protozoaires. Radiés. |
344 ne 594 Mollusques,
545 Quantitative. 595 Arbeules:
220 Elauere hour 596  Vertébrés.
| 547 Chimie organique. 507 Poissons, Batraciens.
+ 548 . Cristallographie. 508 Reptiles. Poissons.
j 549 Minéralogie. 599 Mammifères,

 

een nen* oran

 

_ ARE

 

 
 

 

 

BR 2

TROISIÈME TABLE

SUBDIVISIONS

SCIENCES APPLIQUÉES

600 En général.

6oi
602
ee
04
605
606
60
60
609

, 610

Gii
612
613
614
615
616

6
16
619

620

621
622
623
G24
625
626
627
628
629

630
631

633
634
635
636
63

63

639

640
G41

642
643

645
646

647
648

649

Philosophie,

Manuels.

Dictionnaires. Encyclopédies.
Essais,

Périodiques.

Sociétés. Expositions,

Ecoles techniques.
Inventions. Patentes.
Histoire des sciences appliq.

Médecine.

Anatomie.

Physiologie.

Hygiène, Gymnastique.
Santé publique.
Thérapeutique.

Pathologie. Maladies. Traitem.

Chirurgie. Art dentaire.

Maladies des femmes et enfants.

Médecine comp. Art vétérin.

Art de l'ingénieur.

Machines,

Mines.

Art militaire.

Ponts ettoitures,

Routes et chemins de fer,
Canaux.

Rivières et ports. 5
Travaux sanit. et hydraul.
Autres branches.

Agriculture.

Sol. Engrais, Drainage,
Pestilences. Insectes.
Graines. Herbes, Fibres,
Fruits, Vergers. Vignobles,
Culture maraîchère.
Animaux domestiques.
Laiterie. Lait.
Abeilles. Vers à soie.
Pêche. Chasse.

Economie domestique.

Cuisine. Gastronomie.
Confiseries. Glaces.
Alimentation,

Combustible, Lumière.
Ameublement. Tapisserie.
Habillement. Toilette,
Domesticité, Devoirs. Gages.
Blanchissage, :

Enfants. Chambres de malades.

Beurre. From.

 

650 Transport. Commerce.

651
652
653
654
655
Be

57
658
659

Chiffres, Machines à écrire.
Employés. Ustensiles, Méthod..
Abréviations, Sténographie.

Télégraphie. Câbles, Signaux.
Imprimerie. Publications.
Transports. Chemins de fer.
Tenue de livres. Comptabilité.

Manuels de commerce. Tables.

Publicité. Autres matières.

660 Technologie chimique.

661
662
663
66

66

.
668
669

Produits chimiques. Sel, Acides.
Pyrotechnie, Explosifs.
Boissons. Vins.L'iqueurs.Bières,
Aliments. Sucre. Amidon.
Lumière. Gaz. Huiles. Bougies.
Céramique. Glaces. Porcelaines.
Blanchiment. Teinture. Encre.
Autresindustr, de chimie organ.
Métallurgie.

670 Manufactures.

671
672
673
674
675
676
“
7
679

Articles de métal,

Fer et acier. Poëlerie, Coutell,
Cuivre et bronze, Cloches.
Bois, articles de bois,

Cuirs et articles de cuir.

Papiers et articles de papier.
Coton, Laine. Soie. Lin.
Caoutchouc et art. en caoutch.
Celluloïde et autres produits,

680 Industries mécaniques.

681
082
683
684
685
686
687
688
689

690
Go1
602
693
:
9
696
_
(9)
699

Instruments de précision.
Forge. Maréchal ferrant.
Serrurerie, Armes.
Carrosserie, Ebénisterie.
Sellerie. Cordonnerie.
Reliure,

Vêtements. Chapellerie.

Autres métiers,

Construction.

Matériaux. Bois, Pierres.
Plans. Devis.

Maçonnerie, Plafonnage.
Charpenterie Menuiserie.
Tuiles. Ardoises.

Plomberie, Appareils.
Chauffage. Ventilation.
Peinture, Vitrerie.

Constr, de navires, Carrosserie.
 

|
;
R

700

7o1
702
703
704
705
706
TO
708
#00

710

711
712
713
7x4
A1
716
717
718
729
720

721
722
723

124
726
m2
72
129

730

R_

TRoOïSIÈME TABLE

SUBDIVISIONS

BEAUX-ARTS

Beaux Arts en général.

Philosophie. Esthétique,
Manuels,

Dictionnaires,

Essais.

Périodiques.

Sociétés.

Enseignement. Etude.
Galeries artistiques.
Histoire de l'Art.

Paysage de jardin.

Parcs publics.
Jardin privé. Pelouses,
Chemins. Avenues.
Eaux. Fontaines. Lacs,
Arbres. Haies. Arbustes,
Plantes. Fleurs. Serres.
Sièges, Perspectives.
Monuments.
Cimetières.

Architecture.

Construction architecturale,
Antiquité. Orient.

Moyen âge. Art gothique.
Moderne.

Monuments publics.
Monuments religieux.

Ecoles et institutions scientifiq.

Maisons privées.
Dessin et décoration,

Sculpture.

Matériaux. Méthodes.

Epoque ancienne.

Grèce. Rome,

Moyen âge.

Epoque moderne.

Sceaux. Coins, Pierreries.
Numismatique. Médailles.
Potteries, Porcelaines.
Bronzes. Antiquités.

Dessin. Décoration.

Esquisses,

Perspective,

Anatomie,

Dessin linéaire.

Dessin ornemental,
Ouvrages à l'aiguille.
Décoration intérieure.
Verres et vitraux.

Fournitures artistiques.

 

750 Teinture.

751 Matières premièreset méthodes,
752 Couleur.

753 Genre épique, idéaliste.

754 Peinture de genre.

752 Genre religieux.

756 Peinture historique.

7 Portraits.

758 Paysage. Marine.

759 Ecoles diverses de peinture.

760 Gravure.

761 Bois.

762 Cuivre. Acier,

763 Lithographie.

764  Chromolithographie.
765 Taille douce.

766  Mezzotinte. Aquatinte.
70 Eau-forte. Pointe sèche,
768 Billets de banque.

769 Collections de gravures,

770 Photographie.

771 Chimie photographique.

772 Procédé à l'argent.

773 Procédés à la gélatine,

774  Albertype. Encre d'impression,
77? Photolithographie.

776  Photozincographie,

777 Photogravure. Electrophotogr.
778 Applications particulières.

779 Collections de photographies.

780 Musique.

781 Théorie.

782 Dramatique.

783 Sacrée.

784  Mocale,

785 Orchestrale.

786 Piano et Orgue.

787 Instruments à cordes,
788 Instruments à vent.
789 Autres instruments.

790 Amusements. Jeux.

791 Récréations publiques.

702 Théâtre, Opéra.

703 Jeux d'intérieur.

704. Jeux d'adresse. Echecs.
702 Jeux de hasard, Cartes.
706 Sports extérieurs.

707 Canotage. Jeu de balle.
708 Equitation.

709 Pêche. Chasse, Tir,

 

 

 
 

 

pu

 

TROISIÈME TABLE

SUBDIVISIONS

LITTÉRATURE

800 Littérature en général.

801
802
803
804
805
806
807
808
809

810

811
812
813
814
815
os
1
816
819

820

821
822
e

24
825
826
827
828
829

830

831
832
833
834
835
#37
836
839

Philosophie.

Manuels.

Dictionnaires.

Essais.

Périodiques.

Sociétés.

Etude. Enseignement.
Traités. Réoriques,
Histoire de la littérature.

Littérature américaine.

Poésie.

Drame.

Roman.

Essais.
Eloquence.
Lettres.

Satire. Humour.
Mélanges.

Littérature anglaise.

Poésie.

Drame,

Roman.

Essais.

Eloquence.

Lettres,

Satire. Humour,
Mélanges.

Littérature anglo-saxonne.

Littérature germanique.

Poésie.

Drame,

Roman.

Essais.

Eloquence.

Lettres.

Satire. Humour.

Mélanges.

Littératures germaniques sec,

840 Littérature française.

841
842
843
ou
4.
846
847
848
849

Poésie.

Drame,

Roman.

Essais,
Eloquence,
Lettres.

Satire. Humour.
Mélanges.

 

850

851
852
.

4
855
856
857
858
859

870

871
872
873
:
7
876
877
878
879

880

881
882
883
884
885
886
887
888
879
890
801
892
803
804
802
806
807
808
899

Littérature italienne.

Poésie.

Drame,

Roman,

Essais,

Eloquence.

Lettres.

Satire. Humour.

Mélanges.

Littérat. roumaine et vallaque.

Littérature espagnole.

Poésie.

Drame.

Roman.

Essais.

Eloquence.

Lettres,

Satire. Humour.
Mélanges.

Littérature portugaise.

Littérature latine.

Poésie,
Dramatique.
Epique,
Lyrique,
Eloquence.
Lettres.
Satire, Humour.
Mélanges.
Littérat, italiques secondaires.

Littérature grecque.

Poésie,
Dramatique.
Epique.
Lyrique.
Eloquence.
Lettres,
Satire. Humour.
Mélanges.
Littérat. helléniq,. secondaires,

Littératures secondaires.

Indo-européennes secondaires.
Sémitique.

Hamitique.

Scythique.

Asie orientale,

Afrique.

Amérique du Nord.

Amérique du Sud.

Polynésie, Autres.
 

 

  
   
 
 
 
    
 
 
   
  
 
   
 
 
 
 
   
   
  

 

SAR TS

 

900

go1
902
903

‘go 4
90
906
07
908
999

910

g11
912
013
rs
91

916
9
918

920

921
922
923
2e
92
926
027
928

_929

930

031
932
933
034

036
93

036
939
940

941
942
943
944

946
047
948

919

TROISIÈME TABLE

HISTOIRE

Histoire en général.

Philosophie.

Manuels, Chronologie.
Dictionnaires.

Essais,

Revues, Périodiques.
Sociétés.

Etude. Enseignement.
Polygraphie.

Histoire universelle,

Géographie et descript.

Géographie historique.
Cartes. Atlas,

Antiquités. Archéologie.
Europe.

Asie,

Afrique.

Amérique du Nord,
Amérique du Sud.

Océanie et Régions polaires.

Biographie.

Philosophie.
Théologie.
Sociologie.
Philologie.
Sciences.

Sciences appliquées.
Beaux-Arts,
Littérature.

Généalogie, Héraldique.

Histoire ancienne.
Chinois.
Egyptiens,
Juifs,
Indiens,
Mèdes et Perses,
Celtes.
Romains, 2
Grecs.
Peuples secondaires,

Europe.

Ecosse. Irlande,

Angleterre. Pays de Galles,
Allemagne. Autriche.
France,

Italie.

Espagne. Portugal.

Russie,

Norwége. Suède. Danemark.
Petits pays.

  

 

 

950 Asie,

951 Chine.

952 Japon.

953 Arabie.

954 : Indes.

95 Perse.

Turquie d'Asie.

ge Sibérie.
Turkestan. Afgahnist. Beloutc.

de Inde orientale.

960 Afrique.

961 Afrique du Nord.

962 Egypte. Nubie,

063 Abyssinie,

064 Maroc.

965 Algérie.

966 Afrique centrale du nord.
967 Afrique centrale du Sud.
968 Afrique méridionale,

969 Madagascar.

970 Amérique du Nord.

971 Amérique anglaise, Canada.
972 Mexique. Amérique centrale.
973 Etats-Unis.

07 Etats de l'Atlantique, Nord.
97 Etats de l'Atlantique, Sud,
976 Etats du Centre, Sud.

077 Etats du Centre, Nord-Est,
078 Etats du Centre, Est.

979 Etats du Pacifique,
980 Amérique du Sud.

981 Brésil.

982 ReRABBARe Argentine.

983 Chili.

084 Bolivie,

985 Pérou.

986 Colombie, Equateur.

987 Venezuela.

988 Guyane.

989 Paraguay. Uruguay.
990 Océanie, Régions polaires.
991 + Malaisie,

992 Iles de la Sonde.

993 Australasie.

994 Australie.

90? Nouvelle Guinée.

906 Polynésie,

99 Iles isolées.

99 Régions arctiques,

999

 

SUBDIVISIONS

   

Régions antarctiques.

 
 

PP

  
  
 
