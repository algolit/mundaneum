 

 

CONGRÈS MONDIAL

ASSOCIATIONS INTERNATIONALES

Deuxième session : GAND-BAUXELLES, 15.18 juin 1913
Organisé par l'Union des Associations Internationales
Office central : Bruxelles, 3bis, rue de la Régence

 

 

 

 

Actes du Congrès, — Documents préliminaires.

 

 

 

Réponse de l'Association Internationale pour
la Protection légale des Travailleurs au
questionnaire de l’Union des Associations
Internationales ‘”

(351.83 (063) ! )]

I, — COOPÉRATION, ENTREPRISES COMMUNES

À.— Coopération.

1. L'Association Internationale pour la Protection légale des
Travailleurs (A. TL. P. L. T} coopère avec les associations sui-
vantes :

Comité permanent des Assurances sociales (objets : Économie
des frais de composition d'imprimerie et de traduction de cer-

(1) Cette réponse est l’unc des plus complètes faites au ques-
tionnaire de l'Union, qui avait pour but de préparer les discussions
du Congrès. Klle montre que toutes les questions, ou à peu près toutes,
peuvent Concérner, par quelque sspect, toutes les Associations inter-
nationales. Tlle fait comprendre aussi comment dans la réalité d'une
association, elles s'enchaïînent et se précisent. C’est pourquoi il a paru
utile de la publier ici 14 exfenso. Cette réponse constitue un type qui
pourra aider les autres associations à rédiger la leur, ou à la préparer pour
les discnssioris orales.

 
— 2 —

taines lois sur les assurances. Statistique internationale des
accidents peut-être aussi de la mortalité et morbidité).

Association Internationale pour la Lutte contre le chômage
{objet : Protection légale des émigrants).

Office international du Travail à domicile. (Objet : Propa-
gande pour les Comités de salaires minima.)

Union des Associations Internationales. (Objet : Organisation
générale. Statut juridique des Associations Internationales) (1).

2. Les hvgiéniste de notre Association collaborent surtout avec
le Congrès international des maladies professionnelles. Plusieurs
de nos membres sont membres de l'Institut international de
Statistique, de l’Institut international de droit comparé, du
Bureau international de Ja paix. Nous sommes en relation pour
la question de la protection des ouvriers de chemins de fer avec
le Congrès international des chemins de fer. Nos présidents sont
membres de l'Union interparlementaire.

3. Les relations entre les organisations internationales off:
cielles et les organisations litres s’établissent par des renseigne-
ments réciproques. Nous sommes de cette façon en relation avec
les bureaux officiels de Berne. Nous échangeons également nos

documents ét renscignements avec l'Institut international

d'Agriculture à Rome.

4. Nos vuës sur la coopération entre Associations Internatio-
nales ressortent des réponses données sous les n°° 1, 2, 3. Une
division du travail ne peut se produire que sur les questions dans
lesquelles une parfaite unité de vues a été établie ou pour des
tâches purement techniques ou des questions financières : pour
la propagande collective; pour l'écoulement des publications ;
pour une action collective auprès des pouvoirs publics.

5. À notre connaissance des doubles emplois d'organismes
internationaux n'existent pas dans notre domaine. Quelques
tentatives d'établir des associations rivales ont régulièrement
échoué. Ces doubles emplois ne sont pas du reste toujours con-

{x} Cfx. : Collaboration de l'A. I. P. L. T. avec d'autres Associations
internationales. Rapport présenté par M. Ernest Mahaim, Publication
de l'Association Internationale pour la Protection lésale des Travail-
leurs, n° 8, p. 208. °

 

 

 

— 3 —

damnables. Quelquefois une association internationale peut
réussir, par les méthodes qu'elle emploie, là où une autre a
échoué. |

S'il est à souhaiter de voir « coordonner des efforts », il est aussi
à redouter qu'une grande concentration, dans un domaine comme |
le nôtre, qui touche à la politique n’amène à la création d'orga-
nismes dangereux parleur parti-pris où leur exclusivisme. Notre
Association a ce privilège de grouper des hommes de bonne
volonté appartenant à tous les partis et à toutes les confessions.
La « concentration » s’y est opérée tout naturellement ; maïs elle
n'aspire nullement au monopole et le voisinage d'autres 4ssocia-
tions dans le très vaste domaine de la politique sociale est par
elle considéré comme une bonne fortune.

B. — Vœux, Publications, Code.

1. Les vœux et résolutions de notre Association se trouvent
publiés en français, en allemand et en anglais dans les
comptes rendus de ses assemblées. (Publications de l'Association
Mmternationale, n% 1-8.) Un essai de codification se trouve dans
le rapport présenté au Congrès mondial des Associations Inter-
nationales, Bruxelles, 1910, p. 48-78.

2. La question de la méthode de codification n’a pas été dis-
cutée,

3. Parmi les résolutions on peut distinguer :

a) 1. Mesures pratiques relatives à l’organisation (Finances,
publications, extension, relations avec d'autres Asso-
ciations internationales, bibliothèque, prochaines
assemblées, etc.) ;

2. Résolutions s'adressant aux gouvernements :
a) Pour obtenir des mesures d'ordre international ;
b} Pour obtenir des mesures d'ordre national.

3. Résolutions s'adressant aux sections nationales :

b) Conclusions et principes en matière de législation

sociale ;

c) Problèmes et désiderata proposés à l'étude des sec-

tions, du Conseil permanent d'hygiène sociale, des
sous-commissions d'experts, du bureau et des sections.

 
— 4 —

4. I cst désirable que les associations, par une sage économie
de leurs forces concentrent leurs efforts sur les questions les plus
urgentes et les mieux préparées.

53. Une codification générale des résolutions etvœux des
associations serait utile du point de vue de l’économie de travail
et du point de vue historique {changement d’attitude, dévelop-
pement des revendications}.

6. Les méthodes indiquées en à. €. d., avec un index chrono-
logique, nous paraissent très recommandables.

7. Les limites indiquées en a et « sont désirables ; celles indi-
quées en à ne le sont pas.

8. Il ne nous paraît pas possible de proposer comme un modèle
général à suivie, une codification quelconque existante. Celles
citées (p. 3 de l'Enquête-Refcrendum) ne conviennent nullement
à notre Association.

g. L'exposé sommaire des points, thèses, et principes fonda-
mentaux se trouve dans la monogarphie citée sous le point B. I.
et dans l'ouvrage de M. E. Mahaim, Le Droit international du
Travail, Paris, 1013.

10. La publication des conventions internationales du tra-
vail et des assurances s'effectue dans le Bulletin de l'Office Tier-
national du Travail, revue mensuelle publiée en allemand, en
français et en anglais.

C. — Union des Associations Internationales.
Centre International. — Participation des États.

Union des Associations Tnlernationales.

1. Nous n'avons pas d'observations à faire ni de modifications
à proposer en ce qui concerne les statuts.

2. Voir sous II, C, ra.

3. Une fédération des Associations Internationales ne peut à
notre avis qu'être composée de membres délégués de ces ass0-
ciations. Les personnes avec voix consultative, n'appartenant
à aucun organisme international, seront en général insuffisam-
ment préparées et ne feront qu'obstacle ; cesont dans la règle des
congressistes amateurs.

 

 

— 5 —

4. Les Associations officielles constituées uniquement de fonc-
tionnaires sont liées par des traités et par leur organisation à
une réserve qui leur rend assez difficile la collaboration avec
des Associations libres. Tel n’est pas le cas des Associations
mixtes dont la représentation comprend des délégués des Gou-
vernements et de groupements nationaux privés et auxquels des
subsides des pouvoirs publics sont alloués sans autre condition.

5. a) Oui.

h} On pourrait attribuer à toutes les Associations, ayant un
but analogue et qui collaborent, une représentation proportion-
nelle au nombre de leurs délégués. On donnerait une voix à cha-
cune des autres associations.

c) Idem.

4) Il est peu probable qu'une corporation se soumette à F'ar-
bitrage d'un tiers pour se fusionner avec une autre association
dont les méthodes, les buts et les moyens d'action sont dispa-
rates. Ce serait unc bien dangereuse tentative, pour l'Union 4e
s'offrir comme médiatrice ou arbitre en vue d'amener des fusions
d'Associations Internationales. D’après notre expérience, celles-ci
sont des organismes extrêmement délicats, auxquels il est même
malaisé de paraître donner des conseils.

I] faut toujours des preuves d'un avantage supérieur librement
accepté pour obtenir une concentration pareille.

Centre internahonal.

1. Une Association mixte ne peut disposer des moyens finan-
ciers qui lui parviennent en grande partie de la part des gouver-
nements en faveur d'une autre Association, sauf pour des
abonnements aux publications ou leur échange, mais elle peut
bien lui consacrer des travaux et des renseignements.

2, Il nous semble que le Centre international devrait obte-
nir les moyens pour développer la documentation internatio-
nale.

3. Pourobtenir un optimum sous ce rapport, il s'agirait
plutôt de créer des sections internationales sur chaque genre de
questions de science et de technique rattachées au Centre inter-
national et non pas des sections nationales.

 
6 —

Participation des États.

1. Les États seraient invités à déléguer leurs représentants
aux sections précitées.

II. — RÉGLEMENTATION, LÉGISLATION

A. Régime juridique des Associations Internationales,

1-6. Du point de vue de notre Association (mixte), le besoin
d'un statut juridique ne s’est pas fait sentir jusqu'à présent. Le
seul cas concevable dans lequel il y aurait avantage d'obtenir
le droit d'ester en justice s'offrirait lorsqu'une section nationale
pendant plusieurs années aurait obtenu des publications sans
payer la cotisation. Dans un cas analogue, le gouvernement du
pays auquel appartient cette section lui est venu en aide ; c'est
un procédé infiniment préférable à une poursuite judiciaire. Le
haut contrôle financier a par courtoisie été accordé au pays dans
lequel l'Association a son siège (Département fédéral suisse de
l'Industrie). Les États ont en acceptant le règlement de l'Office
International du Travail, reconnu cette institution quasi comme
un établissement d'utilité publique internationale sans lui attri-
buer toutefois les droits mentionnés sous 6, a, &, cet d.

Remarque de M. Mahaim :

Cette question du régime juridique des Associations Inter-
nationales est extrêmement complexe. Personnellement, je ne
suis pas l'adversaire de l'étude d'un régime juridique supra-
international ; mais celui-ci ne pourrait jamais s'établir que par
convention entre les États. Le besoin ne s’en fait pas sentir
pour notre Association, surtout parce que son siège ne quitte
pas la Suisse. Il en serait différemment si nous changions de
territoire.

B. — Droit international conventionnel.

1. Notre Association a préparé des projets de Conventions
internationales (traités de travail) et de réglementation d'indus-
tries dangereuses, en laissant le soin aux gouvernements de trou-

 

— Ÿ —

ver les formules juridiques. En général, elle se borne a émettreles
piincipes de la réglementation dont elle s'inspire. Le Gouverne-
ment suisse les propose par circulaire et par voix diplomatique
comme base de discussion aux Conférences officielles interna-
tionales et c'est ainsi plutôt que par des contrats-types interna-
tionaux qu'il convient de réaliser les buts internationaux.

2. Ces projets et contrats contribuent à développer le droit
international social à l'étude duquel se livre les Associations
nommées sous I, À, r.

3. a) Convention de Berne du 26 septembre 1906, sur l'inter-
diction de J’emploi du phosphore blanc (jaune) dans l’industrie
des allumettes.

b) Convention internationale sur l'interdiction du travail de
nuit des femmes employées dans l’industrie, de la même date.

c) Traités bilatéraux (trois) concernant le traitement des
ouvriers étrangers en matière d'assurance sociale,

dj Nomination d'une Commission internationale pour la
réforme de la statistique de l'inspection du travail.

c) Interdiction de l'emploi des enduits à la céruse à l'intérieur
des bâtiments privés en France et en Autriche, et de la céruse
dans les travaux publics en Belgique, France, Basse-Autriche,
dans l'administration fédérale suisse et dans trois cantons
SUISSES.

f) Prescriptions s'inspirant des principes énoncés par les péti-
tions ‘de l'Association : Autriche (fonderies de plomb, impri-
meries) ; Grande-Bretagne (fabriques d'accumulateurs} ; Prusse
(confection de franges, fabrication de limes).

En préparation :

Interdiction du travail de nuit des jeunes gens dans l’indus-
trie ;

Journée maxima de dix heures pour les femmes et les jeunes
ouvriers employés dans l'industrie.

Équipe de huit heures pour les ouvriers de la grosse métal-
lurgie du fer et de l'acier ; °

Semaine maxima de 36 heures dans la verrerie.

Journée maxima de dix heures et interdiction du travail de
nuit dans la broderie à fil continu.

Restriction de l'emploi des composés de plomb dans l'industrie
céramique.

 
_ 8 —

Prescriptions internationales pour combattre l’ankylosto-
miase pour les ouvriers des mines et des tunnels.
Semainc anglaise dans l'industrie.
4. Les procédés mis en œuvre pour obtenir ces réalisations sont :
a) Enquête de l'Office international du Travail et des sections
nationales basées sur les questionnaires élaborés par des
experts:

Projet d’un mémoire soumis à une sous-commission de
5 membres ;

Refonte de ce mémoire et projet de résolution de la sous-
commission ;

Discussion par une commission spéciale ;

Discussion par l’assembléc générale ;

Rédaction des résolutions de l'assemblée générale par le
Bureau et la Sous-Commission et pétition au Conseil
fédéral suisse lui demandant de convoquer une confé-
rence internationale ;

g) Demande de propagande aux sections nationales.

&. Dans les Conventions précitées on s’est plutôt inspiré des
textes les plus clairs des lois nationales ouvrières que de conven-
tions existantes dans d’autres domaines. La forme extérieure du
traité de Bruxelles 1904, concernant l’abolition des primes dans
l'industrie sucrière, a été d’une certaine utilité, Mais au fond, les
conventions de Berne sont complètement nouvelles et sus generis.

C. — Conception et rôle des Associations Internationales,

1. a) La définition nous paraît exacte.

b) Il convient d’exclure la coexistence d'organismes distincts
pour les seuls motifs de race, de nationalité ct de religion, mais
on ne peut pas demander l'anicité, puisque les mêmes questions
peuvent être traitées sous différents points de vue par diffé-
rentes Associations Internationales. 11 vaudrait mieux préconiser
la neutralité ethnique, religieuse et politique. Dans ce cas seraient
exclues les Associations, telles que celle des Sionistes, des Pansla-
vistes, etc., qui ne sont au fond que des associations nationales
établies sur un terrain mondial :

c) Oui;

4) Non;

e) Non.

— 9 —

2. Nous ne croyons pas qu’il faille donner une signification
différente aux expressions internationales, universelles, mon-
diales. Évidemment une société nationale qui a quelques corres-
pondants étrangers n'est pas une Association Internationale par
ce seul fait.

3. Il y a lieu de distinguer entre les Association mixtes et
les Associations privées. Ces dernières sont définies sous la
lettre a). Les Associations mixtes sont de facto des conseils
consultatifs. Tant que le principe de la représentation politique
subsiste, tant que le droit électoral n'est pas attaché à une cor-
poration, ces associations ne pourront pas former des corps
représentatifs.

4. Notre Association se considère comme un conseil consul-
tatif libre dans le domaine de la législation ouvrière.

5. Nous croyons qu'à l'heure actuelle un conflit entre le corps
des fonctionnaires d'État qui ont la responsabilité de l'exécu-
tion des conventions internationales et un nouvel organisme
officiel permanent qui se superposerait à eux et aux associations
aboutirait à une fin de non recevoir,

IT. -. UNIFICATION DES SYSTÈMES D'UNITÉS

1-3. En matière d'unification notre Association n'a rien
réalisé.

IV. — ORGANISATION DES ASSOCIATIONS
ET DES CONGRÈS

À. — Moyens divers d'accroître l’unité et le rendement
des Associations Internationales et des Congrès Inter-
nationaux.

1. Notre Association est une fédération de sections natio-
nales ; celles-ci possèdent des secrétariats généraux et des sièges
permanents qui obtiennent des cotisations des sections et quel-
quefois des subsides de leurs États. Les sections nationales sont
priées par le Bureau de former des sous-commissions pour les
questions techniques, le Bureau s'adresse à des experts ou à des

 
— JO —-

correspondants. La propagande se poursuit dans les pays
nouveaux par ces derniers. Les délégués des sections se réunis-
sent tous les deux ans an Suisse. L'Association a le droit de con-
voquer des Congrès ; elle n’a jamais fait usage de ce droit.

2. Notre Association a entrepris des travaux scientifiques
en collaboration ; elle publie mensuellement le Bulletin Tnterna-
tional de l'Office du Travail ; elle vise à la comptabilité des statis-
tiques du travail; elle a obtenu un encouragement sous forme
de prix à distribuer aux auteurs des meilleures ouvrages sur le
saturnisme ; elle entretient une bibliothèque, publie une biblio-
graphie, soutient la propagande par des sections et participe
de temps à autre à des expositions d'économie sociale.

3-4. L'Office international du Travail représente ce centre.
Le règlement pour l'Office international du Travail se trouve
page 45 de la monographie de l'Association internationale pour
la Protection légale des Travailleurs, et l'Office international du
Travail, Bruxelles, 1910.

5. Toutes les Associations Internationales sauront gré à
l'Union internationale si elle organisait un mouvement pour
obtenir une réforme radicale du système des échanges interna-
tionaux des publications officielles dont les retards sont insup-
portables et constituent une véritable parodie du mouvement
international du xx® siècle. Il vaudrait mieux abolir ce service
inutile et obtenir à sa place une réduction des tarifs de la poste,
du télégraphe et des libres parcours de chemin de fer sous cer-
taines conditions.

B. — Réalisation des vœux du Congrès.

1-2. Cette question a déjà obtenu une réponse sous II, B, 4,
page 8.

3. Notre Association ne dispose que d’une seule sanction :
Celle de l'opinion publique représentée par 7,000 membres,
parmi lesquels les membres corporatifs représentant 8 mil-
lions d'ouvriers syndiqués et parmi lesquels, d'autre part, se
trouvent des membres de parlements appartenant à tous les
partis.

4. La propagande de notre Association est organisée par les

 

 

— Il —

Sections nationales par la voice de pétitions, par des conférences
ct des expositions ; dans les pays nouveaux par des correspon-
dants et par des conférences de propagande.

5. Des communiqués sont adressés tantôt directement aux
journaux les plus influents dans chaque pays, tantôt par les
agences télégraphiques. Un bon service de correspondance reste
encore à organiser. Les différentes agences de coupure sont éga-
lement peu satisfaisantes et nous avons renoncé à leur service,

C. — Règlement type des Congrès.

1. Notre Association possède un règlement pour ses assem-
blécs générales.

3. Notre Association ne formule que des résolutions, soit pour
entreprendre une action internationale, soit pour obtenir une
mesure d'ordre national, soit enfin pour inviter les sections
nationales et l'Office intemational à entreprendre des enquêtes
d'ordre scientifique. :

D. — Participation des Etats et des diverses nationalités.

I. — 1. La participation officielle des États a été obtenue d'abord
par pétition de la section suisse et du comité provisoire interna-
tional au Conseil fédéral suisse ; par des démarches personnelles
du Bureau auprès du Gouvernement de la République française ;
ensuite par des pétitions des sections nationales belge, allemande,
autrichienne, hollandaise, italienne, etc., auprès de leur gou-
vermement respectif. Dans les pays sans section nationale, par
des démarches personnelles et des pétitions réitérées du Bureau.

2. Le rôle de l'État où l'Association a son siège est d’ins-
truire ses représentants à l'étranger du caractère de l'Association
et de lui faciliter ainsi l'obtention des renseignements aux pays
auprès desquels ils sont accrédités.

IL. — 1. La participation des États à leur représentation dans
notre comité avec voix délibérative, ou, si les gouvernements
le préfèrent, avec voix consultative, en est la conséquence.

2. Nous possédons [a représentation proportionnelle aux
cotisations pour les sections nationales, combinée avec un

 
 

maximum de dix délégués ; il y a représentation égalitaire pour
les États. Les subsides des États ne sont pas basés sur un sys-
tème conventionnel. Les grands pays industriels et le pays du
siège social accordent naturellement des subsides bien supé-
rieurs.

E. — Budget idéal.

1. Notre budget actuel s'élève à 88,450 francs, dont
66,250 francs sont couverts par des subsides des gouvernements.
Les sections versent une contribution annuelle de 1,000 à
1,500 francs chacune et de 15,000 francs au total. Le produit
de la vente de nos publications s'élève annuellement à 4,000 francs.

2. Un budget idéal de 100,000 francs a été proposé au début
de la formation de l'Association Internationale. Nous espérons
arriver on quelques années à une augmentation des recettes de la
part des pays américains ct des colonies anglaises, ainsi que des
pays balkaniques. Ces ressources financières permettraient de
donner plus détendue au Bulletin de l'Office international du
Travail, dont l'espace ne permet plus d'imprimer &x fofo les
grandes codifications des lois protectrices du travail ; d'organiser
peut-être des publications spéciales pour les pays de langue espa-
gnole : de disposer de sommes suffisantes pour des publications

spéciales, rapports comparatifs sur l'inspection du travail, sur

le droit ouvrier qui font actuellement défaut. Le rôle du mécénat
serait utile surtout pour : a) l'achat des publications et leur dis-
tribution aux bibliothèques centrales du monde; b} octroi de
prix pour concours à organiser par des Associations Internatio-
nales ; c) l'octroi de bourses de voyage pour la propagande et
pour les enquêtes.

V. — DOCUMENTATION ET PUBLICATIONS
EXPOSITION ET ENSEIGNEMENT

A. 1. Nous possédons deux genres de publications : 19 celles
de l'Association Internationale — publications qui visent direc-
tement à l'action — ; 2° celles de l'Office international
du Travail, publications d'ordre technique et scientifique ayant
un caractère documentaire.

 

 

2. Pour des motifs d'économie de place dans les biblio-
thèques, il y aurait intérêt à unifier les formats, en adoptant
peut-être, ceux recommandés par l'Association « Die Brücke ».
Nos formats du Bulletin de l'Office international du Travail
sont conformes à ceux du Bulletin de l'Office du Travail du
Gouvernement français qui lui a servi de modèle. La Resue
belge du Travail, lc Bulletin américain, la Soziale Rundschau
autrichienne ont le même format. Pour tous ceux qui tra-
vaillent dans le même domaine, cette unité des formats est
d'un certain intérêt.

3. a) Répondu sous 1.

b) La publication de Sfandard Works, dans notre domaine
par exemple, d'un code international comparé du droit ouvrier,
dépend surtout des moyens financiers, dont disposent les Asso-
ciations. Certains Shzndard W'orks ont été publiés par la collabo-
ration internationale des gouvernements eux-mêmes, tel que ie
Répertoire technologique des professions, œuvre franco- anglo-
allemande.

c) Les périodiques internationaux publiés par les Associations
Internationales doivent être : exacts, complets et rapides.

B. — Documentation.

1. Notre bibliographie répond aux besoins de la documen-
tation par les divisions :

a) Publications officielles; 8) Publications privées d'ordre
documentaire.

Chacune de ces grandes divisions est classée en livres et
articles de revue. Le système de classification d’après lequel
ces dermières sont subdivisées a été créé par l'Office international
du Travail. Ce système de bibliographie sert de base à notre sys-
tème bibliothéconomique. Bien que les livres d'ordre privé soient
rangés dans l'ordre alphabétique et les publications officielles
d’après les pays des auteurs, un catalogue classé dans l'ordre de
notre bibliographie permet de travailler selon les matières. Les
catalogues alphabétiques, ainsi que celui d'après des matières,
sont rangés en cartothèques. Les dossiers sont rangés d'après les
questions à préparer pour chaque assemblée dans des classeurs
Zeiss et Stolzenberg « Progrès». ‘

 
ue

2. Notre Association est disposée à accepter toute améliora-
tion de son outillage technique, pourvu que des réformes pa-

reilles s'adaptent à ses besoins, à ses moyens et au personnel dont |

elle dispose.

D. — Université et enseignement.

1. Notre Association compte parmi ses 160 délégués, 52 pro-
fesseurs d'économie politique et d'hygiène sociale. C’est donc
en bonne partie aussi une association internationale du corps
enseignant. Plusieurs de ces membres ont donné des conférences
à des Universités étrangères.

2. Les membres nommés sous 1 ont déjà donné des cours
de droit ouvrier international et d'hygiène sociale. Notre Asso-
ciation peut contribuer à la prapagnade de ces cours (encartage
du Bullctin en trois langues), et s'adresser sur demande à ses
membres pour collaborer à une œuvre pareille.

3. Le projet d'une Université mondiale ne peut être développé
que conformément à l’évolution universitaire historique. La
facultas ubiaue docendi accordée par Grégoire IX en 1233, a offi-
ciellement confirmé le caractère imternational de la corporation
universitaire du moyen âge. À Paris, les étudiants organisés par
nations ayant marqué des préférences pour leurs compatriotes,
Robert de Sorbonne les exhorta de ne pas aller son ad mragis-
tros compatriolas (voir DÉXIFLÉ, Geschichte der Universitaien
tm Mittclaller, 1885). Ce caractère international manque aux
Universités depuis leur étatisation ect le réveil des nationalités.
L'Université souffre tantôt des interventions de la raison d'État,
tantôt de l'influence des intérêts particuliers des comités diri-
geants dans les Universités privées, tantôt de l'étroitesse per-
sonnelle et locale des facultés autonomes. Dans plusieurs sciences,
lorsque ces phénomènes s’accentuent, le progrès de la pensée
s’accomplit en dehors des Universités, Nous croyons que
l'échange des professeurs et des étudiants n’est qu'un faible
commencement d'un mouvement qui donnera, par l'interna-
tionalisation de certaines activités umiversitaires, plus d'indé-
pendance au corps enseignant, qui activera la pénétration de
méthodes nouvelles et qui fortifiera le jugement personnel des
étudiants et facilitera le choix de leur carrière intellectuelle.

 

Un mouvement pareil de reconstruction ne se fait que par
étapes. Nous laissons de côté les questions d'organisation (par
exemple des congrès périodiques, introduits dans certaines Uni-
versités américaines}. Nous nous bornons qu'à certaines remar-
ques qui s'appliquent aux sciences sociales :

a) Institution d’un centre international pour dresser la liste
des personnalités appelées à professer avec indication de leurs
ouvrages, de leurs succès antérieurs, de leurs connaissances des
langues ;

b}] Quant aux étudiants, outre des annonces dans les insti-
tutions d'enseignement supérieur, des bourses de voyage de-
vraient être instituées pour réaliser ce but ; il faudrait également
s'adresser aux organisations ouvrières et coopératives pour Ja
sélection de leurs meilleures intelligences ;

c} On peut combiner une faculté de science sociale avec des
cours supplémentaires d'hygiène, d'histoire, de biologie et de
psychologie ; :

4) It faut combiner selon les circonstances des cours, des con-
férences et tâcher d'en former un ensemble ; |

e) Le point de vue comparatif international doit prédominer
sans exclure des cours sur des sujets exclusivement nationaux ;
et le choix des langues dépend des circonstances locales et du
choix personnel du professeur ;

{} Une organisation internationale tâchera probablement
d'obtenir une organisation fédérative entre les universités
existantes, en offrant à celles qui veulent v appartenir ses ser-
vices de correspondance ct de renseignements. Il n'est pas impos-
sible que plus tard f'une ou l'autre des grandes universités organise
complètement son enseignement, la méthode de la nomination
des professeurs, l'établissement des programmes, en vue des
besoins internationaux, Le courant actuel qui souvent vise à
l'exclusion des étudiants étrangers, ne permet pas d’entrevoir
une réalisation pareille comme prochaine.

E. — Utilisation des Expositions universelles,

1. Notre Association n’a pas encore organisé des expositions.

2. Notre Association a pris part à l'exposition internationale
de l'Union des Associations Internationale. Quelques sections

 
— 16 —
nationales ont exposé à l'Exposition internationale d'hygiène
sociale de Dresde et de Rome, 1912; l'Office international
exposera à l'Exposition nationale suisse à Berne en 1914 ct peut-
être à l'Exposition universelle à San Francisco en 1915.

3. Chaque fois qu'une dernande de participation nous arrive
de la part d’un comité d'une exposition internationale, nous lui
recommandons d'admettre les représentants de nos sections
nationales, surtout les présidents et les secrétaires dans le comité
dirigeant de la section d'économie sociale. Quant aux congrès,
notre Association a organisé à Zurich en 1912, la première semaine
sociale internationale.

4. Nous approuvons la création d’une section internationale
au sein des expositions universelles. 7

VI. — LANGAGE SCIENTIFIQUE ET TECHNIQUE

À. — Terminologie, Nomenclature.

1. Un certain principe de nomenclature a été établi dans le
Bulletin de l'Office international du Travail (par exemple : pro-
tection ouvrière générale; protection ouvrière professionnelle).
Dans notre domaine, le besoin d'une unification de la classifica-
tion professionnelle se fait sentir davantage que celle de la

nomenclature, Au lieu de construire une classification nouvelle,

nous avons adopté Ja classification allemande. Puisque chaque
administration aime à construire une classification différente,
un pareil procédé est plus économique qu'un autre.

B. — Emploi des langues.

1. Notre Association admet l'usage de l'allemand, de l'anglais
ét du français, L

2. a} Ses publications ont paru dans trois éditions dans les
trois langues différentes.

b) Les discussions et congrès sont traduits succinctement par
des interprètes ; en commission on se bome quelquefois par
courtoisie à l'allemand ou au français.

c} La correspondance administrative et scientifique se fait

en allemand, en anglais, en français et, par courtoisie, en italien,
et en espagnol.

3. Nous avons répondu au Secrétariat de l’Entente scienti-
fique internationale pour j'adoption d’une langue auxiliaire par
letire du 25 mars 1913, conçue en ces termes :

« Nous sommes convaincus que l'emploi du français, de l'afle-
mand ct de l'anglais suffit pour le moment aux relations inter-
nationales. Nos expériences prouvent que même des ouvriers
allemands ont appris, dans l'intérêt de la solidarité sociale, les
rudiments du français et que des délégués français à leur tour,
ont appris à comprendre des discours allemands. La plus grande
difficulté linguistique existe encore pour les délégués des pays
anglo-saxons. C'est plutôt des préférences de ces pays que
dépendra le choix d'une langue auxiliaire. Ces préférences ne
sont pas encore perceptibles. Il serait donc, à l'heure qu'il est,

absolument inadmissible de se livrer à des pronostics sur un

accord futur en faveur d'une seule de ces langues. Actuellement,
l'extension de chacune dans le monde se poursuit presque pari
passu.

» Dans les relations entre les bureaux des Associations Inter.
nationales, l'usage actuel comporte l'emploi quasi exclusif du
français. Il est évident, que nous correspondrions en allemand
avec un Officc international ayant-son siège à Berlin et en
anglais avec un Institut international à Londres. La langue
auxiliaire s'impose dans tous ces cas pour des raisons d'économie
de temps, de frais et de courtoisie réciproque.

» Quant aux langues artificielles, il nous parait douteux que
l'effort intéressant des adeptes de chacun des multiples idiomes
rivaux aboutisse à une résultat appréciable dans nos rangs,
d'ici à dix ans. Nous préférons concentrer nos énergies sur l'unité
sociale des peuples, d'élargir les frontières intérieures qui Îles

séparent, de combattre la xénophobie des institutions, plutôt

que d’épuiser nos efforts dans une unification des manifestations
extérieures. En effet, il est préférable de sacrifier aux habitudes
nationales, plutôt que de perdre l’appui de l'opinion publique
dans tous les pays par l'adoption d'une langue exclusive.

s Mieux vaut s'entendre que se comprendre. »

 
