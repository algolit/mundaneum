 

 

CONGRÈS MONDIAL

DES

ASSOCIATIONS INTERNATIONALES N° 23
, Deuxième session : GAND-BAUXELLES, 15-18 juin 1913

Organisé par l’Union des Associations [nternationales

Office central : Bruxelles, Sbis, ruc de la Régence 2e Section

 

 

 

 

Actes du Congrès. — Documents préliminaires.

 

Statut Juridique des Associations
Internationales

Le Statut international des Associations Charitables
à la Conférence diplomatique d'assistance des étrangers
(Paris 1912-1913)

PAR

CYR. VAN OVERBERGH

[347.471.6]

Un fait nouveau d'une portée internationale considé-
rable s'est produit à la Conférence d'assistance aux étrangers,
conférence diplomatique des États, réunie à Paris, en novembre
dernier. Le protocole des débats vient de parvenir aux inté-
ressés : et, suivant une entente formelle, il est permis, à partir
de ce moment, d'en saisir l'opinion publique.

Le fait nouveau qui intéresse toutes les Associations Interna-
tionales, c'est le vote unanime par les représentants des vingt
Puissances, réunies en Conférence diplomatique, d’un vœu en
faveur d’un projet de convention relatif à la création d’un
statut international des associations et établissements constitués
en vue de l'assistance aux étrangers.

Nous en examinerons sommairement les origines, la teneur
et la portée.

 

 

 
— à —

_ On verra que cette manifestation d’internationalisme, dans
une dés matières les plus délicates et les plus difficiles du Droit,
marque un progrès important des idées internationales.

I. — LES ORIGINES

À raison de la facilité des communications et de l’internatio-
nalisation du marché du travail, le nombre des étrangers aug-
mentc d'année en année dans la plupart des États civilisés.
D'où l'augmentation presque proportionnelle des indigents
étrangers et la nécessité de secourir à leur indigence. Cette
obligation de secours incombe-t-elle à l'État de résidence ou à
l'État d'origine? Grosse question sur laquelle les spécialistes
discutèrent longtemps sans s'entendre. Le Cangrès international
de Copenhague de rg10, vota à l'unanimité une résolution d’ac-
cord. Le gouvernement danois accepta de mettre en branle la
diplomatie. Finalement, vingt États se réunirent en novembre
1912 à Paris, et adoptèrent provisoirement la plupart des vœux
du Congrès de Copenhague.

Ün de ces vœux visait la forme juridique à reconnaître aux
soicétés pour l'assistance des nationaux établis sur le territoire
étranger. En voici le texte : « Les sociétés pour l'assistance des
nationaux établies sur territoire étranger doivent être encoura-
gées le plus possible par le Gouvernement du pays où elles sont
établies et par Îc pays d'origine ; elles seront autorisées À pos-
séder, à recevoir des dons et des legs suivant un séafuf interna-
hional adapté à leur but : elles jouiront des mesures de faveur en
matière d'impôts, elles pourront prétendre à la collaboration
des agents diplomatiques et elles recevront les subsides néccs-
saires à laccomplissement de leur mission. »

Comment en arriva-t-on à l’idée de statut international?

À la suite des nombreuses séances préparatoires au Congrès
de Copenhague, tenues par le Comité international pour l'étude
de l'assistance aux étrangers, le rapporteur général avait intro-
duit dans ses conclusions le texte suivant, comme base de dis-
cussion : « Les sociétés pour l'assistance des nationaux établies
sur le territoire étranger, seront encouragécs le plus possible
par Île Gouvernement du pays où elles sont établies et par le

 

— 3 —

pays d’origine ; elles seront autorisées à posséder, à recevoir des
dons et des legs{r).»

Entre la publication de cette conclusion et le Congrès de Co-
penhague se produisit un fait capital : la réunion à Bruxelles,
du premier Congrès Mondial des Associations Internationales,
où fut discuté avec tant d'éclat, le statut international.

Les comités internationaux de bienfaisance, représentés à
ce Congrès, avaient été vivement frappés par les arguments et les
conclusions de ces débats.

La veille du Congrès de Copenhague, la délégation française
déposait l'amendement suivant : « Les Gouvernements prendront
de concert, les mesures nécessaires pour déterminer par un
statut international, les droits particuliers et les obligations spé-
ciales des associations ou établissements de bienfaisance, chargés
d'assurer à l'étranger du secours à leurs nationaux. Ces établis-
sements seront créés par l’État dont les indigents sont en cause
et agréés par l'État où ils doivent fonctionner. Ils pourront, avec
l'autorisation de chacun de ces gouvernements, accepter les dons
et les legs qui leur seront faits. »

Le rapporteur général qui lui-même avait été un des secré-
taires généraux du Congrès de Bruxelles, fit au Congrès de Co-
penhague, et d'accord avec fe Comité international, les déclara-
tions suivantes : ‘

« Maintenant j'en viens à une question très importante : la
question des sociétés pour l'assistance des nationaux établis sur
territoire étranger. M. le délégué de Norvège m'a demandé des
explications spéciales sur le complément que, d'accord avec les
délégués de France et de Suisse, nous proposons d'ajouter à la
cinquième résolution : « Elles (les sociétés), seront autorisées à
» posséder, à recevoir des dons et des legs, suivant un séatné
» international spécial adapté à leur but, etc. » Voici ce que je puis
vous dire à ce sujet :

» Nous avons eu au mois de mai, à Bruxelles, un Congrès
international d'un genre un peu spécial, qui peut-être. a échappé
à l'attention du grand public, mais qui a vivement préoccupé
tous ceux qui s'intéressent au mouvement international des idées,

(1) L'assistance aux étrangers, par CYR. VAX UIVERBERGH. Bruxelles.
Dewitt, 1912, P. 245.

 
— à —

le Congrès Mondial des Associations Internationales. C'est là
un fait nouveau dans lhistoire de l'humanité. Je ne parle ici
que des sociétés constituées en vue de buts non économiques. Il
en existe plus de 200. Savez-vous combien nous avons pu en
réunir à Bruxelles? 132 ! Toutes ont été heureuses de se trouver
à ce rendez-vous pour délibérer sur leurs intérêts communs et
elles ont constitué entr'elles des liens permanents, entre autres
sous la forrne d'un Office Central. Eh bien, voici des institutions
dans lesquelles les différents pays adhérents ont chacun le même
droit. Ce sont des groupements constitués en vue de sauvegarder
des intérêts déterminés très élevés : science, philanthropie, etc.,
et dans lesquels les différentes nations sont absolument sur ie
même pied d'égalité. Comment ces sociétés font-elles pour vivre?
Car elles vivent, ne fût-ce que d’une vie précaire. Si elles sont
en Belgique, elles s'adaptent à nos institutions belges. Si elles ont
leur siège en Suisse, elles se conforment aux lois suisses ; de même
en Allemagne, etc. Mais tous ces droits divers sont faits pour les
sociétés nationales et non pour les sociétés internationales, qui,
en effet, pour vivre sont forcées de prendre la forme de sociétés
nationales. De là des difficultés, des heurts. On s’est donc réuni
à Bruxelles : des hommes éminents sont venus de tous les points
du monde — je ne nommerai que M. Clunet, le savant président
de la Société de Droit international, qui a son siège à Paris, et
M. Picard, le jurisconsulte belge universellement connu, — pour
chercher une forme juridique nouvelle, appropriée aux besoins
nouveaux, et cette forme nouvelle is l’ont trouvée. On a fait,
en somme, ce qu’on appelle en droit, un «statut international »..

» J'ai voulu vous donner ces explications pour vous faire voir
que pour ces sociétés d'assistance établies à l'étranger, il ya aussi
à faire quelque chose de pareil. Et c’est tout simplement ce que
nous avons voulu dire dans l’ajoute à la proposition cinquième.
À nous de chercher et de trouver la forme la meilleure au point
de vue juridique et qui puisse le mieux s'adapter aux besoins
nouveaux. Nous voulons signaler le problème aux diplomates,
aux gouvernements, aux spécialistes, à l'opinion publique {1}. »

Avec ces commentaires, l'idée du statut international fit son
entrée dans le vœu du Congrès de Copenhague, qui le vota à
l'unanimité.

{1} Idem, p. 217-218.

 

— Ss —

Sous cet aspect, le statut international fut introduit par le
Gouvernement danois auprès des Puissances.
À la Conférence diplomatique de Paris, deux projets se trou-

-vèrent en présence. L'un émanait de la délégation française,

l'autre de la délégation belge.

Le premier se contentait de déclarer : « Les sociétés de bienfai-
sance, ayant pour but d'assister les étrangers pourront être cons-
tituées conformément aux lois, soit de l’État où elles sont créées,
soit de l'État dont elles doivent secourir les nationaux: ainsi
légalement constituées, elles fonctionneront et s'organiseront
librement sous réserve de l'application des lois de police et de
sûreté du pays où elles ont leur siège et bénéficieront des avan-
tages ci-après. »

Cette conception qui n'avait avec le statut international
qu'une bien vague parenté, fut combattue et évitée par la
Conférence.

Le projet belge disait nettement dans son article premier :
« Les Puissances contractantes accorderont la reconnaissance
légale, chacune dans les limites de sa juridiction, aux associa-
tions et établissements étrangers d'assistance qui remplissent
les conditions ci-après ».…

Après une brillante discussion, cette conception fut adoptée
à l’unanimité. Voilà le fait saillant qui intéresse tous les interna-
tionalistes.

Sous les auspices des diplomates et des spécialistes, le statut
international est officiellement introduit, par une Conférence
officielle, auprès de chacun des Gouvernements représentés à
Paris, à savoir : l'Allemagne, les États-Unis d'Amérique, la
République argentine, l'Autriche, la Hongrie, la Belgique, le
Danemark, l'Espagne, la France, la Grande-Bretagne,la Grèce,
l'Italie, le Japon, le Luxembourg, la Norvège, les Pays-Bas, Ja
Roumanie, la Russie, la Suède et la Suisse.

Il. — LE STATUT INTERNATIONAL

« La Conférence exprime le vœu de voir étudier par les difté-
rents Gouvernements représentés à la Conférence de Paris,
le projet ci-après de statut international des associations et
établissements constitués en vue de l'assistance aux étrangers.

 
6 —

» Entre les Puissances signataires de la présente Convention

et celles qui, ultérieurement, v adhéreront, il est convenu ce qui
suit :

» ARTICLE PREMIER. — Les Puissances contractantes accorde-
ront la reconnaissance légale aux associations et établissements
constitués en vue de l'assistance aux étrangers qui rempliront
les conditions ci-après. |

n ART. 2. — Les statuts de ces établissements et associations
contiendront les règles essentielles et les organes nécessaires à
leur fonctionnement.

» ART. 3. — S'il y a lieu, la publication, l'enregistrement et
l'approbation des statuts se font conformément aux lois, règle-
ments et usages du pays où l'association ou l'établissement a son
siège. | ._  *

» ART. 4. — Ces associations et établissements jouiront de la
capacité juridique, notamment du droit d’ester en justice, de
recevoir des cotisations et des subventions.

» [ls pourront posséder les immeubles nécessaires à la réalisa-
tion de leur but charitable et de leur administration ; ils pour-
ront recevoir des libéralités, sous réserve de l’autorisation du
Gouvernement du pays où l'association ou l'établissement a
son siège quand cette autorisation est exigée par les lois de ce
pays.

n ART. 5. — Les immunités et diminutions d'impôts accordées
aux associations et établissements qui secourent les nationaux
leur sont applicables.

» ART. 6. — Les associations ne pourront être dissoutes et les
établissements supprimés — en dehors des motifs tirés de l'ordre
public, — que pour violation de la lai ou des statuts.

» Si l'association ou l'établissement n'a pas pris de décision
relativement à l'attribution de son patrimoine on à défaut des
dispositions contenues dans les actes de libéralité, l'actif sera
attribué par l’État d’origine à des associations ou établissements
similaires.»

s

Lorsqu'on consulte les travaux préparatoires et notamment
le projet belge, les discussions de la Conférence et le rapport de
M. Brondi, délégué pour l'Italie et professeur de droit à l'Uni-

 

 

—f —

versité de Turin, on est frappé de la précision rare des textes
officiellement adoptés.

Une analyse sommaire de cette évolution des idées au sein de .
la Conférence sera de la plus haute utilité pour les Associations
Internationales qui auront à mettre sur pied une œuvre sem-
blable ou similaire.

ARTICLE PREMIER. — Entre le texte de la proposition belge
et le texte adopté, il n’y a qu'une seule différence : la Conférence
a biffé les mots : « dans les limites de sa juridiction ».

Pourquoi? Parce qu'ainsi étaient soulevées toutes les ques-
tions se rattachant à la notion de juridiction, peu précise,
comme on sait, en droit international. De plus, quelques déléga-
tions étaient tentées de soulever, si le terme était conservé dans
le texte, le problème si difficile de l'extension de la convention
aux colonies.

La Conférence a été unanime pour écarter ces causes acces-
soires de dissentiments, qui auraient pu limiter dès le début la
sympathie de plusieurs États.

Comme les mêmes termes se retrouvent dans divers projets
de statut international des Associations Internationales, la lecon
a son importance, bien que pour celles-ci, il n’y ait pas identité
de motifs.

ART, 2. — Le grand débat se produisit à propos de l'article
deuxième. Pour le comprendre il importe d'avoir le texte belge
sous les veux. Il était formulé ainsi :

« Les statuts de chacun de ces établissements et associations
régleront :

» 19 La dénomination adoptée:
» 29 Le siège social ;
39 L'objet ;

» 4° Les diverses catégories de membres et les conditions
d'entrée et de sortie ;

» 3° Les droits, les obligations et les responsabilités de mem-
bres; sauf dispositions spéciales des statuts, les membres ne
seront tenus, du chef de leur souscription, qu’au montant de
leur cotisation ;

» 60 L'organisation de la direction de l'association ou de

 
 

BR —

l'établissement et de la gestion des biens, les modes de nomina-
tion et les pouvoirs des personnes chargées de cette direction et
de cette gestion, notamment la désignation du membre aux
poursuites et diligences duquel s'exerce le droit d'ester en justice ;
à défaut de pareille désignation, le trésorier est le membre com-
pétent ;

» 70 Les conditions et les formes de la modification aux sta-
tuts, ainsi que celles de la dissolution et, dans ce cas notamment,
la destination du patrimoine. »

Souvenez-vous du texte adopté par la Conférence : « Les sta-
tuts de ces établissements et associations contiendront les règles
essentielles et les organes nécessaires à leur fonctionnement. »

Entre ces deux textes se place un échange de vues et des dé-
clarations du plus haut intérêt.

La délégation autrichienne soutint que les conditions des
statuts étant plus sévères que celles qui étaient exigées des
associations charitables par les lois autrichiennes, elle ne pouvait
donner son adhésion.

Il lui fut répondu qu'en principe, l’article 2 du projet belge
ne visait dans son essence que les conditions essentielles à la vie
d’une association et qu’en fait, à supposer que l’une ou l'autre
condition ne fut vraiment pas nécéssaire, le délégué pour l'Au-

triche était prié d'introduire un amendement.

L'examen contradictoire de la loi autrichienne montra que
la seule condition qui n'était pas prévue in terminis était la pre-
mière : la dénomination adoptée.

Les défenseurs du projet belge se montrèrent disposés à tran-
siger, soit en donnant une signification encore plus large au
terme proposé, soit en retranchant cette condition purement
et simplement.

Certaines délégations présentèrent d’autres objections : l’une
disait en substance : « Nous voulons conserver la plénitude de
notre pouvoir de police à l'égard des étrangers qui, sous prétexte
de sociétés de bienfaisance pourraient être un danger pour
l'État »; l’autre ajoutait : « Si nous approuvons la convention,
les nations adhérentes devront s'engager à réformer leur législa-
tion intérieure dans ce sens ; or, mon pays, à raison des circons-
tances spéciales, ne peut prendre pareil engagement actuelle-

— 9 —

ment ». Aux défenseurs de la première objection, il fut répondu :
« Votre droit de police restera cntier ; nous demandons seulement
pour les associations qui s'en tiendront à leur but de charité, le
droit de vivre et de se développer chez vous suivant un statut
défini, accepté par les diverses nations.» Aux tenants de la
seconde objection, on fit observer qu'à raison de la diversité des
législations en matière de personnification civile, la plupart des
nations contractantes devraient modifier plus ou moins leur loi
intérieure ; que telle était là conséquence ordinaire des conven-
tions internationales : et qu'adopter le traité mternational était
souvent le moyen le plus aisé pour introduire dans un pays de
régime parlementaire, une réforme utile et progressive : l'inter-
nationalisation d’une mesure fait naître l’opportunité.

Ces raisons ne furent pas suffisantes pour emporter l'adhésion
des délégations objectantes. L’inopportunité d'apporter des
modifications à la législation sur les associations et le défaut
d'instructions nouvelles de leur Gouvernement, les poussèrent
finalement à l’abstention.

Afin de bien souligner le caractère de cetteabstention/Qui est
de pure opportunité, et aussi dans le but de réserver l'avenir,
la délégation italienne, d'accord avec la délégation belge, pro-
posa de réduire l’article 2 à son expression la plus simple : « Les
statuts ne devront contenir que les règles essentielles et les or-
ganes nécessaires à leur fonctionnement.

Ainsi chaque nation resterait libre dans l'appréciation des
conditions essentielles de la vie d’une association. Elle applique-
rait donc le statut suivant sa conception nationale de l'associa-
tion à personnification civile.

C'était du coup mettre à néant toutes les objections résultant
des législations nationales. La pointe de pénétration du statut
international était effilée à souhait. Rien ne l’empêcherait
plus de pénétrer aisément dans l’organisation juridique des
États.

La genèse de cette disposition, on en conviendra, est du plus
haut enseignement pour ceux qui poursuivent l'introduction
du statut international des Associations Internationales, quoique,
encore une fois, il n'y ait pas identité de motifs. /

ART. 3. — Le projet belge s’exprimait ainsi : « La publication
et l'enregistrement — et, s’il y a lieu, l'approbation, — du statut,

 
— 10 —

se font conformément aux lois et usages du pays de résidence. »

La discussion montra la nécessité de ne pas faire de différence
entre les diverses formalités. Toutes trois furent mises sur La
même ligne, en parfaite adaptation à la législation territoriale.

Le rapport de M. Brondi justifie ainsi cette mesure : « Une
partie assez ample est laissée aux divers droits territoriaux, car
selon l'article 3, la publication. l'enregistrement et l'approbation,
s'il y à lieu, des statuts se font conformément aux lois et usages
du pays de résidence. Tout cela paraît parfaitement convenable ;
l'assomption de ces divers points et arguments dans une con-
vention internationale unificatrice se heurterait dans des innom-
brables difficultés touchant à des conditions de fait et de droit,
qu'il faudrait complètement changer. D'ailleurs, cette matière

se rattache aussi, en partie, à des questions de procédure et de
_ forme, qui.peuvent à juste titre revendiquer une nature terri-
toriale. »

ART. 4. — Les débats sur cet article furent brefs.

Aussi bien, il est la reproduction exacte du projet belge, sauf
un point.

À la fin du paragraphe 2, le projet belge disait : «avec, s’il y

a lieu dans les deux cas, l'autorisation du Gouvernement du
pays où l'association ou l'établissement a son siège. »

Le projet français donnait une solution semblable.

Sur l'observation de certaines délégations des pays où sem-
blable autorisation n'est pas requise pour les nationaux, l'ar-
ticle prit sa forme la plus générale : « Sous réserve de l’antorisa-
tion du Gouvernement du pays où l'association ou l'établisse-
ment a son siège quand celle autorisation est exigée par la loi de
ce pays.» Nouvelle manifestation de l'esprit d'adaptation aux
droits territoriaux, qui animait la Conférence.

ART, 5. —— Sur les faveurs fiscales, tous les projets et motions
furent unanimes. .

Le rapport Brondi les défend en ces termes :

« La justification de telle disposition est très claire et très
simple.

» Ce sont d’abord des raisons d'humanité, qui imposent la

renonciation à ce léger profit de la part de l'État, où résident de
telles mstitutions bienfaisantes.

3

 

 

» C'est après, un motif, dirais-je, d’eurythmie législative et
juridique qui le suggère, car ce serait une vraie inelegantia juris,
que des associations ou des établissements, jouant le même rôle
social et visant substantiellement aux mêmes buts, se trouvas-
sent dans une condition différente, eu égard aux contributions
publiques,

» C'est enfin un calcul bien adroit qui le conseille, car aussi
l'État de résidence des indigents a tout intérêt à ôter les
entraves et à créer un milieu favorable au fécond épanouisse-
ment de ces institutions. »

ART. 6. — C'est la reproduction du projet belge sauf une modi-
fication au second paragraphe.

Le projet belge disait : «Si l'association où ‘établissement
n'a pas pris de décision relativement à l'attribution de son patri-
moine, l'actif sera attribué par l’État d'origine à des associations
ou établissements similaires. »

On constate aussitôt que l'amélioration est de pure forme,
Elle résulte d’un échange de vues entre les délégations d'Angle-
terre, de France et de Belgique.

M. Hébrard de Villeneuve, président de la Conférence, fit
observer que, dans l'esprit des auteurs de l’article, l’intervention
de l'État d'origine a été prévue pour éviter que le patrimoine des
œuvres de bienfaisance ne fût attribué, dans certains cas, à l'État
de résidence, Il cita notamment l'exemple de la législation fran-
çaise, qui dans l'article 713 du Code civil décide que les biens
«vacants et sans maître » sont attribués à l'État.

Sur la disposition intercalaire «ou à défaut de dispositions
contenues dans les actes de libéralité », on fut toujours unanime.
Pour les uns, cela allait de soi. Pour les autres, il était préférable
d'insérer une clause semblable afin d'éviter le malentendu.

II - LA PORTÉE DU STATUT INTERNATIONAL
La portée du séatut international, qui vient d'être analysé est

marquée par deux documents de la Conférence de Paris : le rap-
port de M. Brondi et le discours final de l’auteur du projet, en

. Séance plénière.

 
— 12 —

« La disparité du point de vue (à la Commission de la bienfai-
sance privée}, dit M. Brondi, ct les diverses appréciations mani-
festées dans cette discussion ont clairement démontré que la
question n'était pas mûre pour permettre des conclusions résolu-
tives. C’est pour cela que M. van Overbergh a cru convenable
de reprendre le projet dans une autre forme, c'est-à-dire de le
présenter, avec les amendements et les compléments introduits
au cours des diverses séances, comme objet d'étude pour les
différents Gouvernements, au lieu de le présenter comme un
ensemble d'accords à conclure : et dans cet ordre d'idées, la Com-
mission propose à la Conférence qu'elle veuille bien exprimer
le vœu de voir étudier, par les différents Gouvernements repré-
sentés à la Conférence de Paris, le dit projet de convention
relative à la création d’un statut intemational des associations
et établissements constitués en vue de l'assistance aux étrangers.

» Cette étude de la part des divers Gouvernements contribuera
à préciser toujours davantage les termes de la question, à éclair-
cir les divergences et les points de contact et d'entente et à
établir si et dans quelle mesure existe un substrat d'idées com-
munes, sur lesquelles l’on puisse compter pour la réalisation
pratique du projet. »

Et voici le commentaire, non contredit et applaudi de l’auteur
du projet :

» Basé sur les travaux du Congrès de Copenhague, un projet
de statut international des associations de bienfaisance avait
été déposé devant la Commission. Le projet fut discuté de ma-
nière approfondie. Au vote, cinq délégations firent une réserve
générale ; leurs instructions ne leur permettaient pas d'accepter
un projet sur les associations, qui amènerait des modifications
à leurs législations nationales. Dans Flespoir d’un meilleur
résultat à la suite d’un examen ultérieur et de nouvelles instruc-
tions plus favorables de la part de ces cinq États, les délégations
ralliées au projet, désireuses d'aboutir à l'adoption du statut
international par tous les États représentés à la Conférence,
résolurent de surseoir à la proposition deréalisation immédiate :
c'est dans ces conditions et avec cet espoir que l’auteur du pro-
jet le retira, le représentant aussitôt sous la forme d’un vœu qui
consistait à soumettre le projet à l'étude de tous les États repré-
sentés, sans aucune exception. »

 

… Jetant, dit le Protocole, un dernier coup d’œilsur les résultats
des travaux de la Commission, l'orateur, tout en regrettant que
le projet de statut international n'ait pu être adopté dès mainte-
nant, signale les progrès que le simple vœu de statut internatio-
nal peut réaliser. Ce vœu fait sortir l’idée du domaine des hypo-
thèses ; il a reçu ici, il va recevoir définitivement par le vote de
l'assemblée une consécration en quelque sorte officielle, puisqu'il
sera soumis avec faveur à l'examen des divers Gouvernements
et qu'il aura pour défenseurs les représentants les plus autori-
sés de chaque pays. Ce statut est le prermier essai d'umification
des législations dé bienfaisance. Il est en même temps un modèle-
type pour les conventions particulières entre les États, relatives
aux sociétés d'assistance. Il peut enfin être pris en considération
par les divers États au moment oùles modifications et leur légis-
lation interne viendraient à être discutées…

» L'orateur émet le vœu que le double effort des Gouvernements
et de l'initiative privée ait pour effet de développer cette forme
élémentaire du statut international; grâce aux travaux de la
Commission d'assistance privée et à la critique savante qu'a
subi ce projet de statut, celui-ci s'est épuré, quintessencié, sim-
plifié, avec une pointe de pénétration si aiguë qu'il paraît ca-
pable d'entrer sans causer de douleur presque, dans les divers
organismes juridiques nationaux. Ïl convient naturellement
d'agir sur l'opinion publique ; et nous attendons beaucoup à ce
point de vue du Comité international du Congrès d’assistance
publique et privée, qui tiendra sans doute à remettre la ques-
tion à l’ordre du jour du Congrès de Londres (1915). »

Aux veux des amis du statut international des associations,
la Conférence de Paris 1912-1913, aura bien mérité. Grâce à elle,
la question est sortie de la période des congrès et des académies
pour entrer dans le domaine diplomatique et réaliste. Voici
vingt Gouvernements saisis par un vœu solennel d'une Confé-
rence officielle. Et voyez dans quelles conditions favorables :

Les représentants de ces Puissances ont été convaincus de la
possibilité de réaliser l'idée qui, en théorie, emporte les sympa-
thies générales. Voilà autant d'avocats du statut international,
auprès des Gouvernements les plus puissants du monde.

Pour la première fois l’idée a affronté et avec succès, la coali-
tion des intérêts nationaux.

 
Il a été prouvé à l'évidence et admis, que les objections se
réduisent à des arguments d'opportunité politique.

De la discussion courtoise et approfondie, il est apparu que
l'idée pouvait s'adapter aisément à toutes les exigences des
droits territoriaux existants, malgré leurs divergences.

L'opinion générale de la Conférence était que l'idée avait pour
elle La raison, la possibilité de réalisation et l'avenir.

Il sera du plus haut intérêt de poursuivre la marcheen avant
du projet de statut international; le voici soumis aux Puis-
sances par la voie officielle ; quelles seront les objections?

Nous attendons ce referendum avec confiance.

Notez bien que si le statut international cueille la victoire sur

le terrain de la bienfaisance, il devra paraître irrésistible sur les
autres. domaines et notamment sur celui de l’Association Inter-
national à but non lucratif.

Ici, en effet, les difficultés sont bien plus grandes. Rappelez-
vous l’état presqu'anarchique du droit en matière de bicenfai-
sance. Autant de pays, autant de législations différentes. De
même pour le vêtement juridique de l'association de charité :

un véritable habit d'Arlequin. Et, pour comble de complication,

chacune de ces particularités apparaît comme une plante du sol,
consacrée par l'usage et d'autant plus chère, sinon plus véné-
rable. Connaissez-vous un orgueil plus têtu que celui de la cou-
tume ancestrale, notamment en matière de charité? Il semble,
dans certains milieux, que ce soit un sacrilège de toucher même
à l'abus. |

Or, c'est sur ce terrain qu'il s’agit d'opérer pour implanter le

statut international des associations charitables. Mesurez la

difficulté !

Au contraire pour l'Association Internationale à but lucratif.
Ici c'est du terrain neuf, du terrain vierge. Le besoin est
nouveau. L'institution est récente. Le préjugé n'est que juri-
dique, et encore ce droit a vu ses racines théoriques ébranlées
peu à peu par la science contemporaine. De la vient que la cam-
pagne en faveur d’un statut international des Associations Inter-
nationales paraît un jeu d’enfant à côté de celle du statut inter-
national des Associations de charité.

Ajoutez que pour cette dernière, dans certains pays, il y a les

 

 

.— 15 —

complications des questions religieuses et confessionnelles, que
Vous ne trouvez pas pour les premières.

En vérité, si le Statut international de la Conférence de Paris
aboutit avant celui des Associations Internationales, ce n’est
pas à la facilité plus grande de l’œuvre qu’il faudra l'attribuer.

Quelle que soit au surplus la destinée des deux espèces de
statuts internationaux, il restera que les discussions du Congrès
Mondial des Associations Internationales de Bruxelles de 1010,
rendirent les plus grands services à ceux qui édifièrent le Statut
international des Associations charitables. Puissent les travaux
préparatoires du Statut international des Associations chari-
tables à la Conférence diplomatique de Paris rendre à son tour
quelque service à ceux qui édifieront le statut international des
Associations Internationales au Congrès mondial de 1913!

 
