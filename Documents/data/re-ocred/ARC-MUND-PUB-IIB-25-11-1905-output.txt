 
 

   
  

Ù : PATHOLOGIE EXTERNE

 

617 Pathologieexterne.Chirurgie.

 

 

 
 

DIVISIONS GÉNÉRALES DE LA PATHOLOGIE EXTERNE

 

 
 

   
 
 
 
 
 
 

617 (0) ! Généralités et ouvrages généraux.
617.0 Pathologie externe en général,
617,1 Traumatismes en général. î
617.2 Complications des traumatismes, î
617.3 Orthopédie, Difformités,
617.4 Affections chirurgicales et éenane ‘par systèmes.
617.5 Affections chirurgicales et opérations par régions.
617.6 Odontologie,
617.7 Ophtalmologie.
617.8 Otologie, .
16179 Médecine opératoire proprement dite.

 

 

  

617 (0) Généralités. Ouvrages généraux.

La pathologie externe et toutes ses divisions peuvent être combinées avec
les subdivisions de généralités, de forme, de lieu et de temps. Voir les
tables spéciales des subdivisions communes, dont les principales ont été
reproduites sous 616 (0). Ex. :

   
  

617 (05) (44) Revre française de Chirurgie.

 

  

617.0 PATHOLOGIE EXTERNE EN GÉNÉ-
RAL.

 

 

  

 SUBDIVISIONS COMMUN ES

 

 

 

 
 

Le classement par subdivisions communes se réalise de la même manière

? que pour la pathologie interne, à savoir: 1° par. organes ef par Systèmes :

_les chiffres qui correspondent à ces subdivisions sont précédés de deux

Zéros; 2° Par maladies : les nombres propres à ces subdivisions sont

précédés d’un zéro. ?

. Ces subdivisions sont les mêmes pour la pathologie ere que pour la

pathologie interne. Toutefois, vu leur importance en CHR ila fallu
multiplier les subdivisions de la rubrique :

 
  
 
 
 
  
 

:oor ‘ Lésions traumatiques diverses. :

 

Les subdivisions de cette rubrique sont emprüntées à la division de la
pathologie externe enparticulier :

  

617.1 Lésions traumatiques en général,

  

La manière de combiner les subdivisions communes avec les subdivisions
principales a été exposée en détail/sous 616.0; il y aura lieu de s’y
reporter. Ex. :

-617.553.0011 ZrA/ures du DE Het.

Il résulte des observations qui précèdent que, dans nombre de cas, on
pourrait indifléremment classer certaines maladies à la pathologie

 
  
 

l

 

   

 
 

 

   
    
    
   
 
   
 
 
    
  
 
 
 
 
 
     
   
   
   
    

617.1 PATHOLOGIE EXTERNE

externe ou à la pathologie interne. Par exemple, la tuberculose testicu-
laire pourrait se classer à :

616.68.0025 (Pathologie interne).

617.558.81.0025 (Pathologie externe).

Ce sera la nature du traitement habituellement appliqué qui servira de
guide. L'usage décide, en effet, dans telle ou telle maladie, si elle est
plutôt du ressort du médecin que du chirurgien. Toutefois, en cas de
doute, il faut classer de préférence à la rubrique de la pathologie interne,
qui est considérée comme le siège principal.

PATHOLOGIE EXTERNE Een PARTICULIER.

617.1 Lésions traumatiques en général.

Il ne s’agit ici que des lésions traumatiques en général. Dès qu'il s'agit d'une
lésion limitée à un organe déterminé, le classement a lieu sous la rubrique
de cet organe, subdivisée par la subdivision commune correspondante à la
lésion envisagée. Ex. :

617.582.0015  Zracture du fémur.
617.587.0011 Gelure des orteils.

IL  brüiures Gélures

12 Choc électrique. Insolations.

9 Contusions. Commotions. .

.I4 Plaies. Traumatismes.

.I4I Plaies par instruments tranchants.

.142 —  contuses. S

.143 —  déchirées.

-144 — par instruments pointus.

.145 — par armes à feu.

.146- — avec corps étrangers.
147 — avec complète séparation des parties.
Ho — eémpoisonnées.

.149 Autres plaies.

.15  Fractures.

.16 Dislocations. Luxations. :
“17 _ Entorses. |

.18 Asphyxies.

 

181 Asphyxie par air irrespirable. :
ne LOZ —= — les gaz.

.184 — — strangulation.

.185 — — -pendaison.

.186 — — submersion.

.19 Autres traumatismes.

 

 

 
 

PATHOLOGIE EXTERNE 617.39

617.2 Complications des traumatismes.

.21 Commotion. Choc traumatique.
222 .Inflammation des plaies.
223 Suppurations. Abcès.
Lorsqu'un abcès affecte un organe, le classement a lieu sous la rubrique de
cet organe subdivisée par ..0023. «4
.24 Ulcérations.
20 Mortification. Gangrène.

Observation identique à celle qui a été faite pour 617.23, sauf que la subdi-
vision commune correspondante est ..0024.

.27 Tétanos.

617.3 Chirurgie orthopédique. Difformités.

Pour la clarté du classement, toute la matière des diflormités est réunie ici,
bien que leur traitement n’exige pas toujours une intervéntion chirurgicale, On

 

pourra subdiviser chacune des rubriques qui suivent comme 611 Anatomie,

: Ex. : 6rr.:074 Anatomie de la main.
617.3:.074 Développement incomplet de la main.

Voir aussi 573.8 Nains'et géants, 573.9 Monstruositésæt phénomènes humains,
613.07 Tares congénitales.

 

au Has

; ‘ar Développement incomplet des: organes.

| .32  Suture incomplète. Bec de lièvre.

+ à 39 Suture anormale.

; .34 Commissures exagérées. Dédoublement apparent.
39 Suture de fœtus. F rères siamois.
.36 Organes ou parties d’organe supplémentaires.

5 .37 Développement disproportionné.
.38 Transposition ou hétérotopie.

.39 | Malformations congénitales. Pied bot. Pied plat.

SC AL SR PSE

 

 

 

 

 

 

 
 

 

 

 

617.4 PATHOLOGIE EXTERNE

 

617.4 Affections et opérations chirurgicales.
Classement par systèmes. “e

Les subdivisions de cette rubrique concordent avec celles de la pathologie
interne [616]. La plupart des opérations chirurgicales ont un caractère
régional et sont classées pour ce motif sous la rubrique suivante [617.5],
dont les subdivisions sont fort détaillées. Dans le doute, ce sera toujours
sous cette dernière rubrique qu'il faudra classer de préférence.

 

 

 

«4I Système circulatoire.
413 Artères.
414 Veines.
.414:002 Phlébite. 3
.414.0034 Varices. #
.42 Appareil respiratoire.
.43 — digestif.
.44 — lymphatique et glandulaire.
.46 — génito-urinaire.
47. Appareil moteur et tégumentaire.
471 Os et cartilage.
.471.0804. Ligatures. Sutures.
A ATIOBON Ostéotomies.
.471:0896 Amputations.
.471.0807 Résections.
.471.0898 -  Ostéoplasties.
.472 Articulations. :
.472:0032 Hydarthroses.
.472.0891I Injections intra-articulaires.
; .472.0805 Arthrotomies.
.472:08091I Réductions des luxations.
.472.08992 Désarticulations.
.472:5 Synoviales.
472.9 Autres affections articulaires.
.472:92 Raideurs articulaires.
. .472:93 Ankyloses.
.472.94 Lésions trophiques.
474 Muscles.
475 Tendons.
.476 Aponévroses. :
47Ÿ Tissu conjonctif sous-cutané. Muqueuses. Bourses
séreuses des muqueuses. Gaïines tendineuses.
.478 Peau.
.479 Ongles.

 

 

 

 
 

   
     
  
    
 

PATHOLOGIE EXTERNE 617.547

 

617.5 Affections et opérations chirurgicales.

Classement par régions.

Voir pour les affections des dents 617.6 Odontologie, pour celles des yeux
617.7 Ophtalmologie, poux celles des oreilles 617.8 Otologie.

 

 

 

.5I Crâne. Tête. Colonne vertébrale.

“Shox Crâne. |

.512 Régions diverses du crâne. Z

.513 Encéphale. k
PCOIS Cerveau.

91323 Protubérance.

.513.8 Méninges.

.514 Nerfs intracrâniens.

910 .Colonne vertébrale.

.516 Régions diverses de la colonne vertébrale.

OL Moelle épinière.

.518 Nerfs intrarachidiens.

52 Face.

“527 Lèvres.

022 Bouche.

523 Joues.

.524 Langue.

.525 Voûte palatine. Voile du palais. Luette. Amygdales.

.526 Mâchoires.

.526.1 Mâchoire supérieure.

.526:2 Mäàchoire inférieure.

“27 Glandes salivaires.

329 Autres organes de la face.

.529.3 Sinus faciaux.

03 Cou.

JE Pharynx.

092 Œsophage. Se

5359 Larynx.

534 Corps thyroïde.

.535 Trachée. :

.536 Vaisseaux et lymphatiques du cou.

.54 Thorax.

.542 Cœur.

.943. Gros vaisseaux intrathoraciques.
544 \  Poumons.

.545 Plèvre.

.547 Médiastin.

 
 

 

 

 

 

 

617.548 PATHOLOGIE EXTERNE
617.548 Cage thoracique.
.549 Autres organes du thorax.
949.2 Bronches. e
.549.3 ‘Thymus.
09 Abdomen.
.551 Paroi abdominale.
.551.0895 Laparotomies.
.551.93 Hernies abdominales.
.551.031 Hernie ombilicale.
.551.037 —  inguinale.
.551.939 Autres hernies.
.352 Péritoine et cavité abdominale.
.552.002 Péritonites et infections péritonéales.
.552.0025 Péritonite tuberculeuse.
559 Tube digestif sous-diaphragmatique.
“HOJNT Estomac.
.553.11 Pylore.
593:2 Intestin grêle.
.253-27 Duodénum.
.593:20I : Perforation intestinale.
.553.2092 . Obstruction intestinale.
29323 Région iléo-cæcale.
.553.31 Cæcum.
993.33 Appendice iléo-cæcal.
.553.33.002 Appendicite.
553.4: Gros intestin. 2
087 Rectum.
.553.8 Anus.
554 Mésentère. Epiploon. Mésocolon.
299 Foie et voies biliaires.
.DH027 Voies biliaires en général.
00919 Canalicules biliaires et origine du canal hépatique.
.55.4 Canal hépatique.
.555.5 Canal cholédoque.
DZ Canal cystique.
,955.8 Vésicule biliaire.
-.556 Pancréas. se Es
007 Rate.
.558 Voies urinaires et génitales.
Pour les organes urinaires et génitaux de la femme, voir 618.
.558.008 ‘Froubles fonctionnels.
.558.0087 Fièvre urinaire.
.558.0083 Abcès urineux.
.558.0084 Infiltration d'urine.
.558.0085 ‘Anurie.

 

 

try

 

 

 

 
 

 

 

PATHOLOGIE EXTERNE 617.577
; É 617.558.0086 :  Rétention d'urine.
É .558.0087 Incontinence d'urine.
L .558:1 Rein et uretères.
.558.1.0807 Néphrectomie.
.558:11 Uretères.
558012 Bassinets.
59810 Tissus pararénaux.
; .558.2 Vessie.
ee .558.2.002 — Cystite.
LE. .558.2.0037 Câlculs vésicaux.
k . .558:4 - © Urèthre.. ;
AE .558.4.0023 .  Uréthrite blennorhagique.
de. .558.4.00231 . Uréthrite blennorhagique aiguë.
| .558.4.00232 : _ _ chronique.
‘ .b58.4.00341 Fistules. 3
. .558.4.00343 Rétrécissements.
: ; .558.4.0891 Cathétérisme.
* .558.4.0895 Uréthrotomie.
.558.4.0898 Uréthroplastie.
.558.5 Prostate.
.558.7 . Appareil génital externe.
.558.76 Pénis.
.558.8 Appareil génital interne.
.558.87 _  Testicules.
.558.82 Epididyme.
.558.83 Tunique vaginale.
.558.83.0033 Hydrocèle. ;
.558.84 Cordon spermatique..
.558.893 Vésicules séminales
.96 Petit bassin.
107 Membres supérieurs.

 

 

Chacune des subdivisions de 67.57 et de 617.58 peut être subdivisée à son
tour par les divisions suivantes: .r Parties molles; .2 Muscles; .3 Väis-
seaux ; .4 Nerfs; .5 Os; .6 Articulations, Ex. :

617.576 Main,
617.576.2 Muscles de la main.

y Epaule. Clavicule. Omoplate.
72e Bras. Humérus.

079 Coude.

974 Avant-bras. Cubitus. Radius.
579 . Poignet. Métacarpe.

DyOE Main. Carpe.

O7 Doigts. Phalanges.

D A

  

  

 
 

 

 

  
  
  
  
  
   
   
  
  
   
  
   
  
  
   
  
  
  
   

 

 

 

 

617.58 PATHOLOGIE EXTERNE
617.58 Membres inférieurs.
Voir observätion faite sous 617.57
.587 Hanche.
.582 Cuisse. Fémur.
.583 Genou. Rotule.
.584 Jambe. Tibia. Péroné.
.585 Cou de pied. +
.586 Pied. Tarse. Métatarse.
.587 Orteils.

617.6 Odontologie. Art dentaire.

6x Pulpe dentaire.
.62 Dentine. Cément.
.63 Périoste alvéolo-dentaire.
.64 Malformations etinsertions défectueuses des dents.
.65  Odontalgie. Névralgie dentaire.
.66 Avulsions dentaires. £ à
‘672 Plombage des dents.
.68 Transplantation des dents.
.69 Dents artificielles.
: Voir aussi 617.028.6 Prothèse, dentiers.
617.7 Ophtalmologie.
.71 Conjonctive. Cornée. Sclérotique.
er Conjonctive ;
72) Cornée.
715 Sclérotique.
72 Iris. Choroïde. Corps ciliaire.

OT Chambre antérieure.
722 - Tractus uvéal.
729 Iris.

- .723.002 [ritis.
.723.0033 Colobome.
.723.0039 Hernie de l'iris.
.72A Pupille.
72 Corps ciliaire. %
.726 Chambre postérieure. È
5727 Choroïde.
73 Nerf optique. Rétine.
791 Nerf optique.
739 Rétine.

.735.8 Héméralopie.

 

 

 

 

 
nts.

 

PATHOLOGIE EXTERNE

 

617.74
.7AT
74.5
+744
+747
+749
+749.5
79
HOT
72e
5793
70)
790
+757

.76
.761
.766

.78

.8.008
:8.0081I
.8.0082
.8.0084
.8.0086
.8.0087
.8.00871
.8.00872
.8.00875
.8.00876
.8.00877
.8.00878
.8.00879

.81
82

.83

. .84
847

85

 

Eee

 

Cristallin et parties voisines.
Cristallin.
Cataracte.
Capsule cristalline.
Humeur vitrée.
Globe de l'œil.
Glaucome.
Troubles de la vision.
Myopie.
Presbytie.
Amblyopie.
Daltonisme.
Elypermétropie.
Astimagtisme.
Appareil musculaire. Appareil lacrymal.
Strabisme.
Canal lacrymal.
Paupières.

Orbite et parties voisines.

617.8 Otologie.

Troubles fonctionnels.
Troubles nerveux articulaires.
Otalgie.

Troubles de l'équilibre.

Vertige de Ménière.

Troubles sensoriels.
Hallucinations.
Bruits subjectifs.
Surdité.
Surdi-mutité.
Audition colorée.
Dysacousie.
Autres troubles.

Oreille externe.
Pavillon de l'oreille.
Conduit auditif externe.

Oreille moyenne.
Osselets.

Membrane du tympan.

 
 

 

 

 

rl re

 

à
5
#!
S

 

 

   

 

617.86 PATHOLOGIE EXTERNE

 

617.86 Trompe d’Eustache.
.87 Cellules mastoïdiennes.

-.88 Oreille interne.

617.9 Médecine opératoire.

.OI Appareil instrumental.

.OII Instruments d’après leur nature.

.OII.I Lavage, irrigation. Seringues, canules.
.OII.2 Ponction.

.011.3 Exploration, bougies, sondes.

.OII.4 Préhension, compression, écrasement.
.O11.5 Section.

.911.6 Rétraction, suspension. Spéculums.
“OIL. 7 Ablation. Cautères.
.011.8 Suturce,.

.012 Instruments d’après leur usage.

A subdiviser par : suivi du nombre spécial aux, affections à la guérison
desquelles les instruments sont destinés. Ex. :

617.912 : 617.7 Instruments d’ophtalmologie.

617.912 : 619 Instruments d’art vétérinaire.

.92 Appareils orthopédiques et bandages.
Voir aussi 617.3 Chirurgie orthopédique. Er

+921 Extension. Contre-extension. Redressement.
922 Maintien.
922.1 Attelles,
-022.8 Pessaires.
.023 Bandages.
028197 Bandages herniaires.
* 928 = * Prothèse.
__.928.6 — Dentiers.
Voir aussi 617.69 Dents artificielles.
.028.7 Œiül artificiel.
.928.79 Lunettes.
.028.8 Cornets acoustiques,
.03 Matériel de pansement et d'opération.
.938 Fils à ligatures et à sutures.
.94 . Médecine opératoire.
.94 (07) Enseignement.
.94 (074) Musées et collections.
OL Statistiques opératoires.

.95 Chirurgie plastique.

.06 Anesthésie chirurgicale.
Voir aussi 615.781 Anesthésiques

 

 

 

 

 
 

 

PATHOLOGIE EXTERNE 617.99

 

617.07 Asepsie et antisepsie.

 

.07I Préopération.
.072 Opération.
:073 Postopération.

.08 Thérapeutique chirurgicale.
.989 Opérations d’après leur nature.

Ne sont classées ici que les études qui traitent des opérations d’une
manière générale. Dès qu'il s’agit d'une opération faite sur un organe
déterminé, le classement doit avoir lieu sous la rubrique de cet organe
subdivisée par la subdivision commune correspondante à l'opération
considérée, Le développement de la subdivision commune {..089 Traïte-
ment chirurgical] est emprunté à la présente rubrique. Ex.:

617-551 Paroi abdominale.
617.551.0805  Laparotomie.
617.558.1.0807 Néphrectomie.

:989.1I Petites opérations. Petite chirurgie.
.989.2 : Ponctions.
.989.3 . Incisions.
089.4 Sutures. Organoraphies. Ligatures.
.989.5 ‘Organotomies.
.089.6 Organostomies.

- 089.7 Organectomies. Ablations. Résections.
.980.8 Organopexies. ‘
.989.9 Autres opérations.

.09 Chirurgie militaire et navale.

 

 

 
 

 

 
 
 
