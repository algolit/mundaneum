Epreuves FASC, 41

1910 05-7

CONGRÉS MONDIAL

DES

ASSOCIATIONS INTERNATIONALES

BRUXELLES, g-11 MAI 5g1o.

LISTE DES MEMBRES

ASSOCIATION INTERNATIONALE ADHÉRENTE

Association artistique et littéraire internationale.

Association catholique internationale des œuvres pour la protec-
tion de la jeune fille,

Association chirurgicale internationale.

Associalion générale des ingénieurs, architectes et hygiénistes
municipaux des pays de langue française.

Association internationale d’agronomie tropicale.

Association internationale d'auteurs, composileurs et écrivains.
Association internationale de la presse sténographique.
Association internationale des botanistes.

Association internationale permanente des Congrès de navi-
galion.

Associalion internationale pour la protection de la propriété
industrielle.

 
_ 2 —

Association internationale pour la protection légale des travail-
leurs.

Association internationale pour l'étude du cancer.

Association médicale internationale contre la guerre.

Association stomatologique internationale.

Association universelle de médecine cspérantiste.

Bureau du Conseil international pour l'exploration de la mer.

Bureau international de l'Union pour la protection de la propriété
industrielle.

Bureau international de l'Union pour la protection des œuvres
littéraires et arlistiques.

Bureau international de l'Union télégraphique.

Bureau international des fédérations d’instituteurs.

Bureau international des poids et mesures.

Bureau international permanent de la paix.

Bureau permanent international des scerétaires communaux.

Bureau polaire international.

Comité de jonction des Congrès internationaux de sténographie.

Comité international de psychologie pédagogique.

Comité international olympique.

Comité international permanent pour l’exécation photographique
de la curle du ciel.

Comité maritime international.

Comité permanent des Congrès d'esperanto.

Comité permanent des Congrès internationaux d'actuaires.

Comité permanent des Congrès internationaux des chambres de
commerce et des associations commerciales et indus-
trielles.

Comité permanent des Congrès inlernalionaux des habitations à

bon marché.

Comité permanent international des Congrès médicaux des acei-
dents du travail.

Commission internationale d'éducation familiale.

 

 

3 —

Commission internationale de l’enseignement agricole.
Commission internationale de l’enseignement mathématiqu e.

Commission internationale d’unification des méthodes d'anal ys
des denrées alimentaires.

Commission permanente de l’associalion internation
grès des chemins de fer.

Commission polaire internationale.

Conciüliation internationale.

Coneilium hibliographieum.

Conférence internationale pour la lutte contre le chômage.
Congrès international d’agronomie tropicale.

Congrès international de botanique.

Congrès international de la mutualité.

Congrès internalional de la presse périodique.

Congrès international de l'éducation familiale.

Congrès international de l'éducation physique.

Congrès international de lélevage et de l'alimentation.
Congrès international de numismatique et de l’art de la médaille.
Congrès international de radiologie et d'électricité.

Congrès international des associations agricoles et de la démo-
graphie rurale.

Congrès mternational des associations d’inventeurs et d'artistes
industriels.

Congrès international des habitations ouvrières.

Congrès international des sciences administratives.

Congrès international des stations de recherches forestières.
Congrès international d'horticulture.

Congrès international d'hygiène alimentaire.

Congrès juridique internalional des sociétés par actions ei des
sociétés coopéralives.
Congrès panceltic international.

Délégation pour l'adoption d'une langue auxiliaire internationale.

Entente scientifique internationale pour l'adoption d’une langue
auxiliaire.

 
   

sis

   
 

Fédération abolitionniste internationale.

Fédération dentaire internationale.
Fédération internationale de laiterie.
Fédération européenne de gymnastique.

Fédération internationale de l’industrie du bâtiment et des tra-
vaux publics.

Fédération internationale des associations de tilateurs de lin et
étoupe.

Fédération internationale des avocats.
Fédération internationale des comités permanents d'expositions.
Fédération internationale des employés.

Fédération internationale pour l’extension et la culture de la
langue française.

Institut colonial international.

Institut de drait international.

Institut [nternational d'art public.

Institut International de bibliographie.

Institut International de photographie documentaire.

Institut International de sociologie.

Institut [International de statistique.

Institut international pour la diffusion des expériences sociales.

Institut international pour l'étude du problème des classes
moyennes.

Institut Marey.
Internacia Scienca Asocio Esperantista.
International bureau of American Republics.
International electrotechnical Commission.
International moral education Congress.
International Union of Ethical Societies.
International Woman Suffrage Alliance.
Ligue internationale de l'aliment pur.

Ligue sociale d'acheteurs.

Musée international de la guerre.

  

=

Office international de bibliographie.
Ofice international de documentation aéronautique.
Office international de documentation pour la chasse.
Office international de documentation pour la pêche.
Société internationale des électriciens.

Société internationale de musique.

Internacia Societo de Esperantistoj Juristoj.

Société internationale de dialectologie romane.

Société internationale pour le développement de l’enseignement
commercial.

Union cycliste internationale.
Union économique internationale.
Union internationale des patronages.

Union internationale des tramways et des chemins de fer d’inté-
rêt local.

Union internationale pour la protection de l'enfance du premier
âge.

Union interparlementaire.

Uniono di l’amiki di la Linguo internaciona.

Universal Races Congress.

Universala Esperanto Asocio.

 

 
Epreuves

n10-05'7.

 

CONGRÈS MONDIAL

DES

ASSOCIATIONS INTERNATIONALES

BRUXELLES, g-11 MAI 1910

LISTE DES ADHÉRENTS

Académie royale des sciences, lettres et beaux arts de Belyique,
Palais des Académies, Bruxelles.
Ansiaux, professeur à l’Université libre de Bruxelles.
Délégué de la Fédération internationale pour l'extension
et la culture de la langue française [Bruxelles].
Antonelli, Alb., docteur en médecine.
Secrétaire et délégué de l'Association médicale internatio-
nale contre la guerre [Paris].
Arendt, Ernest, conseiller d'Etat, conseiller à la Cour supérieure
de justice.
Délégué du Gouvernement grand-ducal [Luxembourg].
Arnhold, Dresde.
Délégué de la Société internationale des juristes espéran-
tistes [Douai].
Assistencia Nacional aos Tuberculosos.
OEuvre portugaise de la tuberculose [Lisbonne].

Association des journalistes portugais, Lisbonne.

 

 
ne

Barré, J., docteur en médecine, Paris.

Délégué de l'Association médicale internationale contre
la guerre [Paris].

Barret, président et délégué de l'International Bureau of Ameri-
can Republics, Washington.

Bassia, Typaldo, député, ancien vice-président de la Chambre des
députés, professeur agrégé à l'Université d'Athènes, membre
de la Cour internationale d'arbitrage.

Délégué du Gouvernement grec.

Bauer, directeur de l'Association internationale pour la protec-
uon légale des travailleurs, Bâle.
Délégué.
Bauwens, John, membre du Conseil central de l'Office internatio-
nal de documents pour la pêche, Bruxelles.
Délégué.
Beco, gouverneur de la province du Brabant, Bruxelles.

Délégué du Congrès international des sciences adminis-
tratives [Bruxelles].

Beernaert, ministre d'Etat, membre de la Chambre des représen-
tants, Bruxelles.

Délégué de l'Union interparlementaire | Bruxelles].

Belga Ligo Esperantista, 26, rue de l’Aigle, Anvers.

Benoit, René, délégué du Bureau international des poids et
mesures [Paris].

Biddaer, secrétaire communal, Anderlecht.

Délégué du Bureau permanent international des secré-

taires communaux [Bruxelles].

Bôgh, Luf, consul général de Norvège en Belgique, Anvers.

Bolingbroke, Mudie, Londres.
Délégué du British Esperanto Association [Londres].

Prince Roland Bonaparte, membre de l’Académie des sciences,
10, avenue d'Eéna, Paris.

Bottenheim, P., avocat et délégué international, secrétaire de la
Societé de législation comparée, Paris.

5

Boucheret, ingénieur conseil, professeur à l’École de physique et
de chimie industrielle de la ville de Paris.

Délégué de la Société internationale des électriciens
[Paris].

Bourlet, Carlo, professeur à l'École des Beaux-Arts, professeur
de mécanique …an Conservatoire national des Arts et Métiers,
Paris.

Délégué de la Fédération espéraatiste de la région pari-
sienne [Paris].

British Esperanto Association 153 136, High Eobborn, Lon-
don W. C.

Broda, secrétaire et délégué général de l’Institut international
pour la diflusion des expériences sociales, Paris.
Bruggman, secrétaire général et délégué de la Fédération inter-
halionale des employés, Gand.
Bultinek, membre de la Commission administrative de l'Office,
Ostende.
Délégué de l'Office international de documentation pour la
Pêche [Bruxelles].
Bureau bibliographique de Paris, 44, rue de Rennes, Paris.
Callier, président de la Société royale d'agriculture et de bota-
nique, Gand.
Délégué du Congrès international des Associations agri-
coles et de la démographie rurale [Bruxelles].
Canon-Legrand, président du Comité permanent des congrès
internalionaux, des chambres de commerce et des associa-
tions commerciales et industrielles, Mons.
Capitaine, avocat à la Cour d'appel, Liége.
Délégué de l'Association internationale pour la protection
de la propriété industrielle.
Cardinal, lieutenant, professeur à l’École militaire de Belgique,
Bruxelles.
Délégué de la Délégation pour l'adoption d'une langue
auxiliaire internationale [Paris (6°)].
Carnegie, Andrew, 2 East 91, New-York.

 
=

Carton de Wiart (M Henri), Bruxelles.
Déléguée de l'Association catholique internationale des

œuvres pour la protection de la jeune lille [Fribourg].
Cazard, Max, secrétaire et délégué général de la Conférence
internationale pour la lutte contre le chômage, Paris.

Centner, R., Verviers.

Chabon, Octave, sous-directeur au Ministère des Finances,
Bruxelles.

Délégué du « Pioniro » [Bruxelles].

Champendal, docteur en médecine. Genève.

Délégué de l'Union internationale pour la protection de
l'enfance du premier âge [Bruxelles].

Chaumat, avocat à la Cour d'appel de Paris, ancien vice-prési-
dent et délégué de la Société de législation comparée, Paris.

Chevalier, député permanent, Wihéries.

Délégué du Congrès international des Associations agri-
coles et de la démographie rurale [Bruxelles].

Ce Clary, Justinien, vice-président et délégué français de l'Office
international de documentation pour la chasse, Bruxelles.

Claveirole, 4, docteur en droit, 2, place Badouillère, Saint-
Étienne.

Clunet, avocat à la Cour d'appel, Paris, président et délégué de
linstitut de droit international [Gand].

Collard, lils, avocat, Louvain.

Lélégué du Congrès international des sciences adminis-
trauives | Bruxelles].

C* Colleredo Maunsfeld, vice-président autrichien et délégué de
l’Oflice international de documentation pour la chasse,
Bruxelles.

Comité international de la Croix-Rouge, Genève.

[Envoi d'un delégué ad audiendum. Mognier, consul de
Belgique, Genève.]

Comité linguistique espérantiste, Ésperantista Lingua Romitato,
51, rue de Clichy, Paris.

Comité federal de la libre pensée, Lisbonne.

br

Congrès de 1910 pour le perfectionnement du matériel colonial,
Wilryek lez-Anvers.

Conti, sénateur, Milan.
Délégué de l'Union internationale pour la protection de
l'enfance du premier âge [Bruxelles].

Cooreman, président de la Chambre des représentants, Gand.
Délégué du Congrès international des sciences admi-
nistratives [Bruxelles].

Corman, directeur général de l’enseignement primaire, Bruxelles.

Délégué du Congrès international de l'éducation fami-
liale.

Cot, Georg. C., Londres.

Délégué du British Esperanto Association [Londres].

Cupérus, président et délégué de la Fédération européenne de
gymnastique, Anvers.

Daguin, F., avocat à la Cour d'appel de Paris, secrétaire général
et délégué de la Société de législation comparée, Paris.
Daniel, secrétaire général et délégué du Congrès international de

radiologie et d'électricité, Bruxelles.

De Beukelaer, président de l'Union cycliste internationale,
Anvers.

Délégué de l'Union cycliste internationale [Paris].

Prince de Cassano, président du Comité permanent des Congrès
de la Fédération européenne et de l’Instituto Htaliano si
cooperasione Sociale, 440, Corto Umberto, Rome.

Decherf, docteur en médecine, Tourcoing.

l'élégué de l’Union internationale pour la protection de
l'enfance du premier âge.

de Contreras, directeur des Offices internationaux de documen-
tation pour la chasse et la pêche, Bruxelles.

Délégué de l'Office international de documentation pour
la chasse [Bruxelles}, de l'Office international de documen-
tation pour la pêche [Bruxelles].

de Coubertin (baron), Pierre, Paris.

Délégué du Comité international olympique [Paris].

 
sûr

Degon, avoué à la Cour d'appel, Douai.
Délégué de la Société internationale de Juristes espéran-
tistes et de la Société française pour la propagation de
l’espéranto [Douai].

De Heen, professeur à l'Université de Liége, Liége.
Délégué du Congrès international de radiologie et d’élec-
tricité [Bruxelles].

Dejace, professeur à l'Université de Liége, Liége.
Délégué du Congrès international de l'éducation familiale
[Bruxelles].

De Leener, protesseur à l'Université libre de Bruxelles, Bruxelles,
Délégué de l’Institut Solvay.

Delvaux, [l., gouverneur de la province de Liége, Liége.
Délégué de la Commission internationale d'éducation
familiale [Bruxelles], de la Commission internationale de
l’enseignement agricole [Bruxelles], du Congrès interna-
tional de l'éducation familiale [Bruxelles].

C® Jean de Mérode, grand maréchal de la Cour, vice-président et
délégué de l'Office international de documentation pour la
chasse, Bruxelles.

De Moor, professeur à l'Université libre, Bruxelles.
Délégué du Congrès international de l'éducation familiale
[Bruxelles].

Denekamp, docteur en médecine, Rotterdam.
Délégué de l'Union internationale pour la protection de
l'enfance du premier âge [Bruxelles].

Depoin, sténographe des congrès internationaux de la Presse et
du Comité directeur des Unions de Presse, Luxembourg.
Délégué de l'Association internationale de la Presse sténo-
graphique [Paris] et du Comité de fonction des congrès inler-
nationaux de sténographie.
C* de Ribaucourt, ingénieur agricole.
Délégué du Congrès international d’horticulture {Bru-
xelles|].

= De

de Ro, Georges, notaire, Bruxelles.

Délégué de l’Associalion internationale pour la protection
de la propriété industrielle.

Despret, membre du Conseil central de l'Office international de
documentation pour la chasse, Bruxelles.

Délégué de l'Office international de documentation pour la
chasse ! Bruxelles].

d'Estournelles de Constant, Bavon, sénateur de la Sarthe, lauréat
du prix Nobel, Paris.

Délégué de la Conciliation internationale [Paris].

de Torrès Mendiola, docteur en médecine, secrétaire et délégué
de l'Association médicale internationale contre la guerre,
Paris.

Baron de Tuyl de Serooskerwen, vice-président néerlandais et
délégné de l'Office international de documentation pour la
chasse, Bruxelles.

Devaux, docteur en médecine, Bruxelles.

Délégué de l'Union internationale pour la protection de
l'enfance du premier âge [Bruxelles].

de Vilmorin.

Délégué du Congrès international d’agronomie tropicale.

De Vuyst, inpecteur principal de l’agriculture, Bruxelles.

Délégué de la Commission internationale de l'enseigne
ment agricole { Bruxelles}, du Congrès international de l'édu-
cation familiale [Bruxelles), du Congrès international des
sciences administratives {Bruxelles}, du Congrès interna-
tional d’horticulture [Bruxelles], de la Fédération interna-
tionale de laiterie [Bruxelles].

De Wildeman, conservateur au Jardin botanique de l'État, Bru-

xelles.
Délégué de l'Association internationale d'agronomie tro-

picale [Paris].
d'Eylan, Paris.
Délégué de l'Association médicale internationale contre la
guerre [Paris].
S. Dickstein, Varsovie.

 
_8g—

Dictionnaires techniques illustrés, Munich.
Die Redaktion der illustrierten technischen Worterbücher,
Munich.
Dierckx, chef de cabinet de M. le Ministre de l'intérienr, membre
du Conseil central de l'Office de la chasse, Bruxelles.
Délégué de l'Office international de documentalion pour
la chasse [Bruxelles].
Dietrich, docteur en médecine, Berlin.
Délégué de l'Union internationale pour la protection de
l'enfance du premier âge [Bruxelles],
Dor, Henri, doeteur en médecine, Lyon.
Délégué de l'Association universelle des médecins espé-
rantistes [Lyon].
Baron d'Otreppe, Liége. |
Délégué du Congrès international de Associations agri-
coles el de la démographie rurale [Uccle].
Doutrepont, professeur à l'Université de Liége.
Délégué de la Société internationale de dialectologie
romane [Halle a. S.).
Dufort, docteur en médecine, Bruxelles.
Délégué de l'Union internationale pour la protection de
l'enfance du premier âge [Bruxelles],
du Moulin, Alexis, membre du Conseil central et délégué de l'Office
international de documentation pour la pêche |Bruxelles].

Dupont, ministre d’État, sénateur, Liége.
Délégué du Congrès inlernalional de l'éducation familiale
[Bruxelles].

Puriaux.
Délégué de la Société française pour la propagation de

l'Espéranto [Paris].

Epstan, Felix, Hamburg.

Eyschen, Président du Gouvernement, ministre d'État, Luxem-
bourg.

Fédération Espérantisie de la région parisienne, Paris.

Pehr, Secrétaire général de la Commission internalionale de
l'enseignement mathématique, Genève,

y —

Field, Haviland, directeur du Concilium bibliographicum de
Zürich.

Délégué du Conciliom bibliographieum [Zürich].

Frahault, Ch., président et délégué de l’Associalion interna-
tionale des botauistes, Levde.

De Foerster, Wilhelm, lauréat du prix Nobel, Charlottenburg.

Francotte.

Délégué de la Fédération internationale des comités per-
manents d'expositions [Bruxelles}.

Fromageot, H., secrétaire perpétuel de la Commission d'arbitrage
de La Haye, avocat à la Cour d'appel de Paris.

Délégué de la Société de législation comparée [Paris].

Frum, député à Clervaux, Grand duché du Luxembourg.

Délégué de Hi Commission internationale d'éducation
familiale [Bruxelles].

Fürstenhoff, docteur en sciences naturelles, Bruxelles.

Délégué de la Fédération internationale pour l'extension
et la culture de la langue française [Bruxelles].

Gaillard, Arthur, archiviste général du Royaume, Bruxelles.

Gariel, professeur à l'Académie, membre de l'Académie de
médecine, Paris.

Gedoelst, Délégué de la Fédération internationale de laiterie,
Bruxelles.

Geoufire de Lapradelte, professeur à la Faculté de droil de Paris,
membre de l'Institut de droil international, Paris.

Gilmont, docteur en droit, chef de bureau honoraire au Ministère
de l'Intérieur, secrétaire et délégué du Congrès interna-
tional des sciences administratives, Bruxelles.

Gillon, professeur à F'Üniversité de Eouvain, directeur du Musée
royal d'histoire naturelle de Belgique; Louvain.

Délégué du Bureau du Conseil international pour l'explo-
ration de la mer [Copenhagtie].

Giminne, A-J., lieutenant, Inslilnt cartographique militaire,
Bruxelles.

Délégué de l’Uniono di l'Amitti di la Lingno internacional
[£ürich}.

 
= =

Glaesener, Mathias, conseiller d'État, conseiller à la Cour supé-
rieure de justice, Luxembourg.
Délégué du Gouvernement grand-dueal.
Gobat, A.. lauréat du prix Nobel, délégué du Bureau interna;
tional permanent de la paix [Berne].
Baron Goflinet, ministre plénipotentiaire et secrétaire honoraire
des commandements de S. M. le Roi des Belges, Bruxelles.
Délégué de l'Oflice international de documentation pour
la pêche [Bruxelles].
Golder, secrétaire de l'Ofice international de documentation pour
la pêche, Ostende.
Délégué de l'Ollice international de documentation pour
la pêche (Bruxelles).
Goyau-Faure (M), Paris.
Déléguée de la Commission internationale d'éducation
familiale [Bruxelles].
Gravis, professeur à l'Université de Liége.
Délégué du Congrès international de botanique [Bru-
xelles].
Guilbard, avocat, Le Havre.
Délégué de la Ligue sociale d'acheteurs [Fribourg].
Guilleaume, délégué du Bureau international des poids et mesures
[Paris].
Guye, Ph.-A., professeur a l'Université de Genève.

Hahn, C., docteur en médecine, Paris.
Délégué de l'Association médicale internationale contre la
guerre | Paris].

Halot, Alex., consul de Japon, avocat à la Cour d'appel, Bruxelles.
Delégué du Congrès international de l'éducation familiale

{ Bruxelles].

Hamman, membre de la Chambre des Représentants, président
du Comité de mariculture, vice-président de l'Oflice interna-
tional de documentation pour la pêche, Ostende.

Délégué de l'Office international de documentation pour
la pêche [Bruxelles] et du bureau du Conseil mternational
pour l'exploration de la mer [Copenhague].

mi =.

Hennebieq, juge au tribunal de 1° instance, Bruxelles.
Délégué de l'entente scientifique internationale pour
l'adoption d’une langue auxiliaire.
Hepites, Stefan, C., membre de l'Académie roumaine, vice-pré-
sident de l'Association internationale de sismologie, Bucarest.
Hesse, Carlos, A., Iquique.

Hodler, Genève.

Délégué de l’Universala Esperanto asocio [Genève].

Houzeau de Lehaie, sénateur, trésorier et délégué de l'Union
interparlementaire, Mons.

Hubert, directeur de l’Institut agricole, Gembloux.

Délégué de la Commission internationale de l’enseigne-
ment agricole [Bruxelles].

Huet, chirurgien dentiste, Bruxelles.

Délégué de la Fédération dentaire internationale [Bru-
xelles].

Ingelbleck, docteur en sciences politiques et administratives,
secrétaire de S. M. le Roi, Bruxelles.

Délégué du Congrès international des sciences adminis-
tratives [Bruxelles].

Institut de Sociologie, Bruxelles.

Jacobs, Fernand, président de la Société belge d'astronomie,
Bruxelles.

Jamin, Joseph, architecte, Bruxelles.

Délégné de la délégation pour l’adoption d’une langue
auxiliaire internationale [Paris].

Janet, professeur à l’Université de Paris, directeur du Labora-
toire central et de l'Ecole supérieure d'électricité, Paris.

Délégué de la Société internationale des électriciens
[Paris].

Baron Janssen, président de l'Union internationale de tramways
et de chemins de fer d’intérêt local, 15, avenue de la Toison
d'Or, Bruxelles.

Johannessen, docteur en médecine, Christiania.

Délégué de l'Union internationale pour la protection de
l'enfance du premier âge [Bruxelles].

 
_— 132 —

Jottrand, ancien conseiller juridique du roi de Siam, directeur de
l'Institut supérieur de commerce, Mons.

Délégué du Comité permanent des Congrès internationaux
des chambres de commerce et des associations commér-
ciales et industrielles [Bruxelles].

Janod, secrétaire général et délégué de la Société internationale
pour le développement de l'enseignement commercial,
Berne.

Keller, docteur en médecine, Berlin.

Délégué de l'Union internationale pour la protection de
l'enfance du premier âge | Bruxelles].

Klein, directeur de l'agriculture, Luxembourg.

Délégué de la Commission internationale de l'enseigne-
ment agricole | Bruxelles).

Korofl, docteur en médecine, Sotia.

Délégué de l'Union internationale pour la protection de
l'entance du premier âge [Bruxelles].

Kovalewski, membre de la Douma, Saint-Pétersbourg.

Délégué de la Commission internationale d'éducation
familiale [Bruxelles].

Krains, Hubert, secrétaire du Bureau international de l'Union
postale universelle, 7, Zäkringerstrasse, Berne.

Kramers, Martina G.

Délégué de l'international Woman Sufrage Alliance
iRotierdam].

Lambeau, agent de change, Bruxelles.

Délégué du Congrès international des associations agri-
coles et de la démographie rurale [Bruxelles).

Lambrechis.

Délégué de l'Institut international pour l'étude du problème
des classes moyennes [Bruxelles].

Lange, secrétaire général de lÜnion interparlementaire, Bruxelles.
Délégué de l'Union interparlementaire [Bruxelles].
Lavrvgard de Menezes, Rodrigo, Octavio, professeur de droit inter-
national à la Faculté de droit de Rio de Janeiro, délégué
plénipotentiaire à la Conférence de droil international

marilime, #4, Hôtel Balzac, rue Balzac, Paris.

 

Lecomte, Georges, directeur scientifique à FObservatoire royal,
Uccie. .

. Délégué du Bureau polaire international [Bruxelles}, du
Comité international permanent pour l'exécation photo-
graphique de la Carte du ciel {Paris].

Lefèvre, Émile, professeur d'analyse mathématique à l'école mili-
taire et à l'école de guerre, Bruxelles.
Délégué du Pioniro [Bruxelles].
Lemaire, Ch., commandant, Cokailagne.
Délégué de l’Uniono de l'Amiki di la: Linguo internaciona
[Zürich]. conte orne
Le Maistro, secrétaire général de la Commission électrotechnique
internationale, Londres.
Délégué de Fa Commission électrotechnique internationale
[Londres!.
Lemaire, docteur en médecine, Louvain.
Délégué de PUnion internationale pour la protection de
l'enfance du premier âge [Bruxelles],
Lepart, IL, directeur de la documentation politique et sociale,
rue Hautefteuille, 21, Paris.
Leplae, professeur à l’Institut agronomique dé l'Université de
* Louvain.
Délégué de la Commission internationale de l'enseigne-
menti agricole | Bruxelles].
Lesage, docteur en médecine, Paris,
Délégué de l'Union intern. pour la protection de l'enfance
du premier âge [Bruxelles].
Leschevin, avocat, secrétaire général de l'Office international de
documentation pour la chasse, Tournai.
Délégué de l'Office international -de documentation pour
la chasse [Bruxelles].
Levniers de Volder, Bruxelles.
Délégué du Congrès international des Associations agri-
coles et de la démographie rurale [ Bruxelles]
Ligue maritime espérantiste, rue de Clichy, Paris.
Ligue portugaise de la paix, Lisbonne,

 
— d4 —

Losseau, avocat, docteur en sciences politiques et administralives,
rue de Nimy, Mons.

Lotss, J. P. secrétaire général de l'Association internationale des
botanistes, Leyde.

Délégué de l'Association internationale des botanistes
[Leyde].

Lust, deeteur en médecine, secrétaire général de l'Union interna-
tionale pour la protection de l’entance du premier âge,
Bruxelles.

Délégué de l'Union internationale pour la protection de
l'entance du premter âge [Bruxelles].

Maeuhout, membre de la Chambre des Représentants.

Délégué du Congrès international des Associations agri-
coles et de la démographie rurale [Uccle].

Magalhaes, Lima.

Délégué de l’Association des jouraalistes et écrivains por-
tugais, de la ligne portugaise de la paix et du Comité fédéral
de la libre pensée [Lisbonne].

Mahaim, professeur à l'Université de Liége, Liége.

Délégué de l'Association internationale pour la protection
légale iles travailleurs [Bâle].

Maillard, avocat à la Cour de Paris, président de l'Association
artistique et littéraire internationale | Paris].

Délégué de l'Association mternationale pour la protection
de la propriété industrielle [Paris] et de l'Association arlis-
tique et liltéraire internationale Paris].

Marchandise, docteur en médecine, Bruxelles.

Délégué de l'Union internationale pour la protection de
l'enfance du premier âge [Bruxeiles].

Marcq. avocal à la Cour d'appel, Bruxelles.

Délégué du Congrès intérnalional des sciences adminis-
tratives [Bruxelles:.

Mazerr, docteur en médecine, secrétaire général de l'Association
médicale interuationale contre la guerre, Paris.
Délégué de l'Association médicale internationale contre la
guerre [Paris].

Michiels van Verduysen, secrétaire général de la Cour permanente
d'arbitrage, La Haye.

 

— 45 —

Morel-Jamar, Major, Boitsfort.
Délégué du Congrés international des sciences administra-
tives [Bruxelles], de l'Office international de documentation
pour la chasse [Bruxelles].

Musée social, 5, rue Las-Cases, Paris.

Nerinckx, professeur à l'Université de Louvain, Louvain.
Délégué du Congrès international des sciences administra-
lives [Bruxelles].
Novicov, rue Joukoiski, Odessa.

Office central Esperantiste (Esperantista centra oficejo), rue de
Clichy, 4, Paris.
Osterrieth, Berlin.
Délégué de l'Association internationale pour la protection
de la propriété industrielle.

Ostwayd, Wilb., lauréat du Prix Nobel, Gross-Bothen, Kgr-Saxe,
Landhaus Energie.

Paillot.
Délégué de l'Internacia scienca asocio Esperantista {Lille}.

Baron Peers, professeur et délégué de la Fédération interna-
üonale de la laiterie [Bruxellesi.
Pénot, Paris.
Délégué du Congrès international d’agronomie tropicale
[Bruxelles].
Pensa, banquier, chevalier de la Légion d'honneur, Lyon.
Penso, Joseph, consul général et commissaire général de la
République Dominicaine à l'Exposition de Bruxelles, avenue
Louise, 117, Bruxelles.
Peyré, docteur en médecine, Paris.
Délégué de l'Association médicale internationale contre
la guerre |Paris}.
Pien, chef de bureau au Ministère de l'Agriculture [Bruxelles].
Délégué de la Commission internationale d'éducation

familiale [Bruxelles] et du Congrès internationale de l'édu-
cation familiale [Bruxelles].

 
— 46 —

Pieraerts (Mr}, aumonier de la Cour, Bruxelles.
Délégué du Congrès international de l'éducation familiale
{Bruxelles].

Pioniro, 34, rue Van Ostade, Bruxelles.

Plener, Vienne,

Poels, docteur en médecine, Bruxelles.

Délégué du Comité permanent international des Congrès
médicaux des accidents du travail [Bruxelles|.

Polako, président de la Société de la morale de la nature, Ville-
neuve.

Proost, directeur général de l'Office rural, Bruxelles.

Délégué du Congrès international de l'éducation familiale.

Raimondi, docteur en médecine, Paris.

Délégué de l'Union internationale pour la protection de
l'enfance du premier âge [Bruxelles].

Ramsay, président du Congrès de chimie, lauréat du Prix Nobel,
Éondres.

Richardson, abbé, professeur à l'Institut Saint-Louis, Bruxelles.

Délégué de Universala Esperanto Associo [Genève] et de
la Belga Ligo Esperantista [Bruxelles].

Richet, professeur à l'Université, membre de l'Académie de
médecine, Paris.

Délégué de l'Institut Marey [Boulogne-sur-Seime|.

Rivière, Joseph-Alexandre, docteur en médecine, président de
l'Association médicale internationale contre la guerre,
Paris.

Délégué de l'Association médicale internationale contre la
guerre [Paris].

Rolin, Alberich, secrétaire général de l'Institut de droit interna-
tional, Gand. |

Délégué de l'institut de droit international [Gand].

Rollet de l'Isle, ingénieur hydrographe en chef de la marine,
Paris.

Délégué de la Ligue espérantiste [Paris].

Rombaut, Eugène.

 

Rosenthal, chirurgien dentiste, Bruxelles.
Délégué de la Fédération dentaire internationale [Rru-
xelles].

Rousseau, secrétaire et délégué de l'Union cycliste internationale,
Paris.

Saleiller, Raymond, professeur à la Faculté de droit de Paris.

Sand, René, docteur en médecine, agrégé de l’Université libre de
Bruxelles. -

Délégué du Comité permanent international des Congrès
médicaux des accidents du travail [Bruxelles].

Sartiaux, E., ancien président du syndicat professionnel des
industries électriques, Paris.

Délégué de l'Union des syndicats de l'électricité, Paris.

Sauveur, secrélaire général du Ministère de l'Intérieur et de
l'Agriculture, Bruxelles.

Délégué du Congrès international des sciences adminis-
tratives [Bruxelles], de l’Institut international de statistique
[Rome].

Schäde!, privat-docent à l'Université, Halle.

Délégué de la Sociélé internationale de dialectologie
romane [Halle a. S&.].

Schlomann, ingénieur, Munich. |

Délégué des Dictionnaires techniques illustrés, Munich.

Général Sébert, membre de l'Institut de France, Paris.

Délégué de la Fédération espérantiste de la région pari-

sienne, Paris.

Simoens, chef de section au service géologique.

Siltard, député au Reïchstag, Aix-la-Chapelle.
Délégué de la Commission internationale d'éducation
familiale [Bruxelles].

Smeesters, Albert, secrétaire de la Commission administrative de
l'Office international de documentation pour la péche,
Anvers.

Délégué de l'Office international de documentation pour
la pêche [Bruxelles].

 
   

= 4 =

Smeesters, Constant, secrétaire général de l'Oflice international
de documentation pour la pêche, Anvers.
Délégué de l'Office international de documentation pour
la pêche [Bruxelles).

Smith, Miss Constance, 51, Moorestreet, Cadogan Square,
Londres S. W.
Déléguée de l'Association internationale pour la protection
légale des travailleurs.

Société belge de chimie, Bruxelles.

Société de législation comparée, 16, rue du Pré-aux-Cleres,
Paris.

Société des ingénieurs civils de France, Paris.

Société française pour la propagation de l’esperanto, 53, rue
Lacépède, Paris.

Solvay, Armand, membre du Conseil central et délégué de l'Office
international de documentation pour la chasse, Bruxelles.

Spiller, Londres.

Délégué de l’Universal Races Congress [Londres], de l'In-
ternational Union of Ethical societies [Londres|.

Stevens, J., directeur de l’Institut international pour l'étude du
problème des classes moyennes.

Délégué de l'Institut international pour l'étude du pro-
blème des classes moyennes [Bruxelles], de l'Office interna-
tional de documentation pour la pêche [Bruxelles].

Sieger, docteur en médecine, Cologne.

Délégué de l'Union internationale pour la protection de
l'enfance du premier âge [Bruxelles].

Taillefer, avocat à la Cour de Paris, secrétaire général et délégué
de l’Association artistique et littéraire internationale [Paris].

Ten Bosch, A., ingénieur électricien, 68, Anna Paulownastraat,
La Haye.

Terlinden, premier avocat général à la Cour de cassation, prési-
dent de l'Office international de documentation pour la
chasse, Bruxelles.

Délégué de l'Office international de documentation pour
la chasse [Bruxelles].

 

     
  

 

— 49 —

 

Tibbaut, avocat à la Cour d'appel, membre de la Chambre des
Représentants, Bruxelles.
Délégué du longrès international des Associations agri-
coles et de la démographie rurale. [Bruxelles], du Congrès
international des sciences administratives [ Bruxelles].

Paul ’t Serstevens, secrétaire général de l'Union internationale de
tramways et de chemins de fer d'intérêt local, 45, avenue de
la Toison d'Or, Bruxelles.

Typaldo-Bassin, A., ancien président intérimaire du Parlement,
député, avocat à la Cour suprême, professeur agrégé à l’Uni-
versité, membre de la Cour internationale permanente
d'arbitrage, délégué officiel près la Commission internatio-
nale pénitentiaire de Berne et près le catalogue international
scientifique de Londres, président du Touring elub de Grèce,
délégué officiel, 20, rue Homère, Athènes.

Ulveling, Auguste, président de la Chambre des comptes, prési-
dent de la Commission d'administration des établissements
pénitentiaires, Luxembourg.

Délégué du Gouvernement grand-ducal.

Union des syndicats de l'électricité, 27, rue Tronchet, Paris.

Van den Brandeler, Louis, Fredenkspark, 5, Haarlem.

Van den Corput-Du Toict, avocat à la Cour d'appel, Bruxelles.
Délégué du Congrès international de l'éducation familiale
{[Bruxelles;.

Van der Biest Andelhof, président de la Belga Ligo Esperantista
[Anvers].
Délégué de la Belga Ligo Esperantista, Anvers.
Van dervaeren, inspecteur de l’agriculture, Bruxelles.

Délégué de la Commission internationale de l’enseigne-
ment agricole [Bruxelles] et du Congrès international des
Associations agricoles et de la démographie rurale.

Van de Velde, A.-J.-J., professeur à l'Institut supérieur de bras-
serie, Gand, secrétaire général de la Commission interna-

tionale d’unification des méthodes d'analyse des denrées=ali-
mentaires [Gand].
_— 920 —

Van Hoeck, directeur général de l’agriculture de Hollande, La
Haye.
Délégué de la Commission internationale de l'enseigne-
ment agricole [Bruxelles].

Van Laer, protesseur à l'École des mines, Mons.
Délégué de l'entente scientifique internationale pour
l'adoption d’une langue auxiliaire [Bruxelles].

Van Ophem, architecte, Schaerbeek.
Délégué de la Fédération internationale de Findustrie du
bâtiment et des travaux publics [Bruxelles].

Van Schoor, Oscar, pharmacien, trésorier de la Belga Ligo Espe-
rantista, Anvers,
Délégué de la Belga Ligo Esperantista [Anvers].

Varlez, avocat à la Cour d'appel, Gand.
Délégué de fa Conférence internationale pour la lutte
contre le chômage [Paris].

Velghe, directeur général au Ministère de l'Intérieur et de l'Agri-
culture, Bruxelles.
Délégué du Congrès internalional de l’éducation familiale
[Bruxelles].

Vliebergh.
Délégué du Congrès international des Associations agri-
coles et de la démographie rurale [Bruxelles].

Bebr-Pinnow, conseiller du Cabinet (Kabinetsrath) de
S. M. l'Impératrice d'Allemagne. Altonaerstrasse, 56, Ber-
lin, N. W. 93.

Bernuth, vice-président de l'Office international de docu-
mentation pour la pêche.

Délégué de l'Office international de documentation pour
la pêche [Bruxelles].

4

Liebenberg, professeur à l’École supérieure d'agriculture,
Vienne.

Délégué de la Commission internationaie de l'enseigne-
ment agricole [Bruxelles].

 

 

— gd —

Wauwermans, avocat à la Cour d'appel, membre de la Chambre
des Représentants, conseiller communal, Bruxelles, prési-
dent de l'Association artistique et littéraire internationale
[Parisi.

Délégué de l'Association artistique el littéraire interna-
tionale.

Weell, Louis, professeur au lycée Louis-le-Grand, 17, boule-
vard Saint-Michel, Paris,

Wernstedt, docteur en médecine, Stockhoim.

Délégué de l'Union internationale pour la protection de
l'enfance du premier àge | Bruxelles].

Wéry, sous-direcleur de l’Institut agronomique de Paris, Paris.

Délégué de la Commission internationale de l’enseigne-
mer agricole [Bruxelles].

Wilmotte, professeur à l'Université de Liége, membre de l’Aca-
démie royale-de Belgique, Bruxelles.

Délégué de l'Académie royale des sciences, letires.et des
beaux-arts de Belgique. Palais des Académies, Bruxelles.

Wittmaek, professeur à l'École supérieure d'agriculture, Berlin.

Délégué de la Commission internationale de l’enseigne-
ment agricole [ Bruxelles.

D° Zamenhof, L. L., Sir, Dzika, n° 9, Varsovie.

Letter, E., président du Syndicat professionnel des industries
électriques, Paris.

Délégué de l'Union des syndicats de l'électricité, Paris.

 
